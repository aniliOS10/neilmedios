
import UIKit
import MapKit
import IoniconsKit
import GoogleMaps
import GooglePlaces
import SearchTextField
import SwiftyJSON
import DBNumberedSlider


class PlanMyDaySearchOffline: InterfaceExtendedController,CLLocationManagerDelegate {
    
    var searchResultController: SearchResultsController!
    var resultsArray = [String]()
    var gmsFetcher: GMSAutocompleteFetcher!
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.activityType = .automotiveNavigation
        _locationManager.distanceFilter = 10.0  // Movement threshold for new events
          _locationManager.allowsBackgroundLocationUpdates = true
        // allow in background
        
        return _locationManager
    }()
    // storyBorad
    
    @IBOutlet weak var buttons: YNSearchMainView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet fileprivate var Radius : DBNumberedSlider?
    @IBOutlet fileprivate var searchBtn : UIButton?
    @IBOutlet fileprivate var DropDownTextFeild : SearchTextField?
  
    //   var dataPickerView = UIPickerView()
    //   var toolBar = UIToolbar()
    fileprivate var Pickerdatavalue = [JSON]()
    var indexvalu = 0
    var RowValue = 0
    
    var CheckBool : Bool = false
    
    // fixed labels storyboard outlets
    
    @IBOutlet fileprivate var lblSpeciality : UILabel?
    @IBOutlet fileprivate var lblChooseRadius : UILabel?
    @IBOutlet fileprivate var lblCategory : UILabel?
    
    // variables
    fileprivate var categoryNames = [String]()
    fileprivate var specialityNames = [String]()
    
    
    fileprivate var SearchFromKey = ""
    
    fileprivate var categoryChoose = ""
    fileprivate var latValue : Double = 0.0
    fileprivate var longValue : Double = 0.0
    fileprivate var keyWordSpeciality : String = ""
    fileprivate var RadiusRangeValue : Int = 25
    fileprivate var isSaveSearch : Bool = false
    
    fileprivate var isGoogle : Bool = true
    fileprivate var isLocal : Bool = false
    fileprivate var entering : Bool = true
    
    fileprivate var RecentSearch : [JSON] = [JSON]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttons.delegates = self
      
        
        DropDownTextFeild?.maxNumberOfResults = 5
        DropDownTextFeild?.theme.font = UIFont.systemFont(ofSize: 15)
        DropDownTextFeild?.theme.bgColor = .white
        DropDownTextFeild?.theme.borderColor = .gray
        DropDownTextFeild?.theme.borderWidth = 1.0
        DropDownTextFeild?.theme.separatorColor = .lightGray
        DropDownTextFeild?.delegate = self
        DropDownTextFeild?.tag = 999
        

        NotificationCenter.default.addObserver(self, selector: #selector(Update_PlanMyDaySearch), name: NSNotification.Name( "Update_PlanMyDaySearch"), object: nil)
        
        FetchingOfflineData()
        
    }
    
    @objc func Update_PlanMyDaySearch (notfication: NSNotification)  {
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
 
    fileprivate func FetchingOfflineData()
    {
        if !NetworkState.isConnected()
        {
            
            DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: "CategoryList") { (data) in
                AppDelegate.hideWaitView()
                if let object = data
                {
                    var jsonArray = [JSON]()
                    for jsonData in object
                    {
                        if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                            jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                                jsonArray.append(json!)
                            })})
                        }
                    }
                   
                        self.categoryNames = jsonArray[0]["category"].map({ (str,strJson) -> String in
                            
                            return strJson["name"].stringValue
                            
                    })
                    
                    let height = self.buttons.initView(categories: self.categoryNames)
                    self.viewHeight.constant = height + 20
                    
                    self.Pickerdatavalue = [jsonArray[0]["speciality"]]
                    
                    print(self.Pickerdatavalue)
                    
                    self.specialityNames = jsonArray[0]["speciality"].map({ (str,strJson) -> String in
                        
                        return strJson["name"].stringValue
                    })
                    self.DropDownTextFeild?.filterStrings(self.specialityNames)
                   
                    
                    self.view.layoutIfNeeded()
                    
                 
                }
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: PlanNAVBAR(), BtnColor: UIColor.white)
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        searchResultController = SearchResultsController()
        searchResultController.delegate = self
        
        let fetcherFilter = GMSAutocompleteFilter()
        guard let info = SignedUserInfo.sharedInstance else
        {
            return
        }
        fetcherFilter.country = info.country_code ?? "us"
        
        
        gmsFetcher = GMSAutocompleteFetcher(bounds: nil, filter: fetcherFilter)
        gmsFetcher.delegate = self
        
        if entering
        {
            entering = false
        }
        
    }
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Plan My Day Search")
    }
    //MARK: -  Action Handler
    
    @IBAction fileprivate func searchBtnAction(_ sender : UIButton)
    {
        if categoryChoose.isEmpty
        {
            self.alert(self.languageKey(key: "Please select a category to continue"))
            return
        }
        else if CheckBool == true
        {
            
            if self.DropDownTextFeild?.text == ""
            {
                self.alert(self.languageKey(key: "Please select a Speciality to continue"))
                return
            }
            else
            {
                self.keyWordSpeciality = self.DropDownTextFeild?.text ?? ""
              
                SearchFromKey = "local"
                
                
                self.locationFetch(categoryName: self.categoryChoose, keyWord: self.keyWordSpeciality, lattitude: latValue, longtitude: longValue, titleName:"", RadiusRange: self.RadiusRangeValue, saveSearchBool: self.isSaveSearch,searchfrom: SearchFromKey)
                        
            }
       
        }
        else
        {
            self.keyWordSpeciality = self.DropDownTextFeild?.text ?? ""
          
                SearchFromKey = "local"
            
            
            self.locationFetch(categoryName: self.categoryChoose, keyWord: self.keyWordSpeciality, lattitude: latValue, longtitude: longValue, titleName:  "", RadiusRange: self.RadiusRangeValue, saveSearchBool: self.isSaveSearch,searchfrom: SearchFromKey)
            
        }
    }
    
    @IBAction fileprivate func RadiusSlider(_ sender : UISlider)
    {
        self.Radius?.labelFormat = String(Int(sender.value))
        self.RadiusRangeValue = Int(sender.value)
    }
    
    // MARK: - api hit
    
    fileprivate func locationFetch(categoryName : String,keyWord : String ,lattitude: Double,longtitude : Double , titleName : String,RadiusRange : Int ,saveSearchBool : Bool,searchfrom:String)
    {
       
        var param = [String : Any]()
        param["type"] = categoryName
        param["keyword"] = keyWord
        param["location_name"] = titleName
        param["lat"] = lattitude
        param["long"] = longtitude
        let meters = Double(RadiusRange) * 1609.34
        param["radius"] = Int(meters)
        param["savesearch"] = saveSearchBool
        param["searchfrom"] = searchfrom
        
        print(param)
        guard let controller = PlanMyDay_Visit_offline.instance() else
        {
            return
        }
        
        controller.Radius = meters
        controller.category_name = categoryName
        controller.primary_speciality = keyWord
        controller.lattitude = lattitude
        controller.longtitude = longtitude
        
        controller.parama = param
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
}

extension PlanMyDaySearchOffline : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return Pickerdatavalue[0].count
        
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Pickerdatavalue[0][row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        
        RowValue = row
        
    }
}


// MARK: - Category Button Sender Delegates

extension PlanMyDaySearchOffline : CategoryButtonActionProtocol
{
    func CategoryButtonAction(_ sender: UIButton, BtnArray: [UIButton]) {
        let _ = BtnArray.map({$0.isSelected = false
            $0.backgroundColor = .white
            $0.setTitleColor(.lightGray, for: .normal)
            $0.borderColor = .lightGray
        })
        sender.backgroundColor = PlanNAVBAR()
        
        sender.setTitleColor(.white, for: .normal)
        sender.isSelected = true
        sender.borderColor = PlanNAVBAR()
        
        print(sender)
        
        let jsons = self.categoryNames[sender.tag]
        
        print(jsons)
        
        if jsons == "Doctor" || jsons == "Physician"
        {
            
            
            CheckBool = true
            
            //  DropDownTextFeild?.isUserInteractionEnabled = false
            
            
            categoryChoose = sender.titleLabel?.text ?? ""
            
            
            guard let controller = Speciality_List.instance() else
            {
                return
            }
            controller.delegate = self
            controller.Speciality_Value = Pickerdatavalue
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        else
        {
            CheckBool = false
            
          
            DropDownTextFeild?.text = ""
            categoryChoose = sender.titleLabel?.text ?? ""
            
            DropDownTextFeild!.inputView = nil
            DropDownTextFeild!.inputAccessoryView = nil
            self.view .endEditing(true)
        }
        
        
        
        self.view .endEditing(true)
        
    }
    
    
}
// MARK: - location Map with search Bar

extension PlanMyDaySearchOffline :  UISearchBarDelegate , LocateOnTheMap,GMSAutocompleteFetcherDelegate,UITextFieldDelegate
{
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
        print("Error" + error.description)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last
        
        
        self.longValue = userLocation!.coordinate.longitude
        self.latValue = userLocation!.coordinate.latitude
        
        print("Latitude :- \(userLocation!.coordinate.latitude)")
        print("Longitude :- \(userLocation!.coordinate.longitude)")
        
        locationManager.stopUpdatingLocation()
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag != 999
        {
            
            let searchController = UISearchController(searchResultsController: searchResultController)
            
            searchController.searchBar.delegate = self
            searchController.searchBar.sizeToFit()
            
            
            navigationController?.navigationBar.isTranslucent = false
            searchController.hidesNavigationBarDuringPresentation = false
            
            // This makes the view area include the nav bar even though it is opaque.
            // Adjust the view placement down.
            // self.extendedLayoutIncludesOpaqueBars = true
            //self.edgesForExtendedLayout = .top
            self.present(searchController, animated:true, completion: nil)
            // self.navigationController?.navigationBar.isHidden = true
            return false
        }
        else
        {
            if CheckBool == false
            {
                return true
            }
            else
            {
                let controller = Speciality_List.instance()
                controller!.delegate = self
                controller!.Speciality_Value = Pickerdatavalue
                self.navigationController?.pushViewController(controller!, animated: true)
                return false
            }
            
        }
        return true
    }
    
    
    public func didFailAutocompleteWithError(_ error: Error) {
        //        resultText?.text = error.localizedDescription
    }
    
    /**
     * Called when autocomplete predictions are available.
     * @param predictions an array of GMSAutocompletePrediction objects.
     */
    public func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        //self.resultsArray.count + 1
        
        for prediction in predictions {
            
            if let prediction = prediction as GMSAutocompletePrediction?{
                self.resultsArray.append(prediction.attributedFullText.string)
            }
        }
        self.searchResultController.reloadDataWithArray(self.resultsArray)
        //   self.searchResultsTable.reloadDataWithArray(self.resultsArray)
        print(resultsArray)
    }
    
   
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String) {
//        self.longValue = lon
//        self.latValue = lat
//        
//        DispatchQueue.main.async { () -> Void in
//            
        }
        
    }
    
// MARK: - instance

extension PlanMyDaySearchOffline
{
    class func instance()->PlanMyDaySearchOffline?{
        
        let storyboard = UIStoryboard(name: "OfflineStoryBoard", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "PlanMyDaySearchOffline") as? PlanMyDaySearchOffline
        
        
        return controller
    }
}


extension PlanMyDaySearchOffline : SpecialityProtocol
{
    func Speciality_String(List_Iteam: String, ProductArray: [Int]) {
        
    }
    
    func Speciality_String(List_Iteam : String) {
        
        
        DropDownTextFeild?.text = List_Iteam
        
        print("List_Iteam :---> ",List_Iteam)
        
    }
    
    
}


