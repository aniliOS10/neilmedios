//
//  OfflineVisitListVC.swift
//  MedMobilie
//
//  Created by dr.mac on 01/05/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

//
//  OfflineVisitListVC.swift
//  MedMobilie
//
//  Created by dr.mac on 17/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit
import IoniconsKit
import DGElasticPullToRefresh
import SwiftyJSON
import SwipeCellKit

class OfflineVisitListVC: InterfaceExtendedController ,UISearchResultsUpdating{
    
    var Titlename : String = ""
  
    
    
    
    var contactId : Int = 0
   
    var comeFromContact : Bool = false
    @IBOutlet weak var lblPlusSign: UILabel!
    @IBOutlet weak var btnView: UIView!
    
    fileprivate var JsonData : [JSON]?
    fileprivate var mainJsonData : [JSON]?
    
    @IBOutlet weak var tableView: UITableView!
    fileprivate var resultSearchController = UISearchController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if comeFromContact == true
        {
            
            btnView.isHidden = true
        }
        btnView.createCircleForView()
        
        lblPlusSign.font = UIFont.ionicon(of: 30)
        lblPlusSign.text = String.ionicon(with: .plus )
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = false
    }
    
    func SetUpSearchBar()
    {
        self.resultSearchController = ({
            
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.searchBar.placeholder = self.languageKey(key: "Search Visits".languageSet)
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.hidesNavigationBarDuringPresentation = false
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.resultSearchController.dismiss(animated: false, completion: nil)
    }
    

    //MARK: - SearchBar Update
    
    func updateSearchResults(for searchController: UISearchController) {
        // filteredTableData.removeAll(keepingCapacity: false)
        
        if searchController.searchBar.text != ""
        {
            let Filterarray = self.mainJsonData?.filter({ (element) -> Bool in
                if element["title"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "") || element["visit_type"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "")
                {
                    return true
                }
                return false
            })
            
            self.JsonData = Filterarray
            self.tableView.reloadData()
        }
        else
        {
            self.JsonData = mainJsonData
            self.tableView.reloadData()
            
        }
        
    }
   
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.SetUpSearchBar()
        
        if  Titlename == "Visit" {
            
            PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: VisitNAVBAR(), BtnColor: UIColor.white)
        }
       
        
        
       // AppDelegate.showWaitView()
        VisitListDataFetch()
    }
    @objc fileprivate func VisitListDataFetch()
    {
       
        
            if !NetworkState.isConnected() {
                
                DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.VisitList.rawValue) { (data) in
                    AppDelegate.hideWaitView()
                    if let object = data
                    {
                        var jsonArray = [JSON]()
                        for jsonData in object
                        {
                            if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                                jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                                    jsonArray.append(json!)
                                })})
                            }
                        }
                        
                        AppDelegate.hideWaitView()
                        self.mainJsonData = jsonArray
                        self.JsonData = self.mainJsonData
                        self.tableView.dg_stopLoading()
                        self.tableView.reloadData()
                    }
                    else
                    {
                        
                        AppDelegate.hideWaitView()
                        self.JsonData?.removeAll()
                        self.tableView.reloadData()
                        self.tableView.dg_stopLoading()
                    }
                }
            }
        }
        
        
        
        
    
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: Titlename)
    }
   
    
    @IBAction func createNewVisit(_ sender: Any) {
        
        
        self.popupAlertWithSheet(title: Bundle.appName(), message: nil, actionTitles: [ self.languageKey(key: "Create Visit"),self.languageKey(key: "Express Visit")], actions:[{action1 in
            
           
            
            guard let controller = CreateVisit.instance()
                else{return}
            
            self.navigationController?.pushViewController(controller, animated: true)
            
            
            },{action2 in
                
                
                
                guard let controller = ExpressVisit.instance()
                    else{return}
                
                self.navigationController?.pushViewController(controller, animated: true)
                
            }, nil])
        
    }
    deinit {
        tableView.dg_removePullToRefresh()
    }
    
}

extension OfflineVisitListVC : UITableViewDelegate , UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return JsonData?.count ?? 0
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let json = JsonData?[indexPath.row]
            else{return}
        guard let controller = OfflineVisitDetailsVC.instance()
            else{return}
        controller.jsonMain = [json]
       // controller.visitId = json["id"].intValue
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? VisitListingCell
            else{return VisitListingCell()}
        
        guard let json = JsonData?[indexPath.row]
            else{return cell}
        
        
        if json["is_express"].boolValue
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.red)
        }
        else
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.green)
        }
        
       
        cell.lblTask?.text = json["title"].stringValue
        cell.lbldate?.text = json["visit_date"].stringValue
        
        cell.lblVisitType?.text = json["visit_type"].stringValue
        cell.lblVisitedCount?.text = String(json["visited_count"].intValue)
        cell.lblPendingCount?.text = String(json["pending_count"].intValue)
        cell.lblRescheduleCount?.text = String(json["rescheduled_count"].intValue)
        cell.selectionStyle = .none
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if !comeFromContact
        {
            return true
        }
        return false
    }
   
  
    
    
    
}
extension OfflineVisitListVC
{
    class func instance()->OfflineVisitListVC?{
        let storyboard = UIStoryboard(name: "OfflineStoryBoard", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "OfflineVisitListVC") as? OfflineVisitListVC
        //self.definesPresentationContext = true
        
        return controller
    }
}
extension OfflineVisitListVC
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
         
            
            
            self?.VisitListDataFetch()
            }, loadingView: loadingView)
        if  Titlename == "Visit" {
            
            tableView.dg_setPullToRefreshFillColor(VisitNAVBAR())
        }
        else
        {
            tableView.dg_setPullToRefreshFillColor(SettingNAVBAR())
        }
        
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
}



