//
//  OfflineOfflineVisitDetailsVC.swift
//  MedMobilie
//
//  Created by dr.mac on 02/05/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//


import UIKit
import SwiftyJSON
import Letters
import IoniconsKit
import DGElasticPullToRefresh
import SwipeCellKit
import EPSignature
import FontAwesome_swift

class OfflineVisitDetailsVC: InterfaceExtendedController {
    
    // for waiting time
    var codeTextField: UITextField?;
    var numberTextField: UITextField?;
    
    var ClassType: String?;
    
    
    var visitId : Int = 0
    fileprivate var selectedContactId : Int = 0
     var jsonMain : [JSON]?
    @IBOutlet weak var tableView: UITableView!
    fileprivate let sections = ["Description","Meeting With","Attachments"]
    
   
    
    fileprivate var jsonAddress : String = ""
    fileprivate var jsonContact : [JSON]?
    fileprivate var jsonAttachment : [JSON]?
   
   
   
    
    fileprivate var BoolCVheck  : Bool = false
    
    // @IBOutlet fileprivate var NotesView : AddNotesView?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = false
       
        //self.NotesView?.isHidden = true
        //self.NotesView?.mainView?.backgroundColor = VisitNAVBAR()
        
        
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        AppDelegate.showWaitView()
        VisitListDataFetch()

        
        // self.navigationController?.navigationBar.isHidden = false
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: VisitNAVBAR(), BtnColor: UIColor.white)
        
    }
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Visit Details")
    }
    fileprivate func VisitListDataFetch()
    {
        
        AppDelegate.hideWaitView()

        
        if !NetworkState.isConnected() {
           
            if let jsonMain = self.jsonMain
            {
                let tempjson = JSON(parseJSON: jsonMain[0]["visit_contact"].stringValue)
                
                if let contacts = tempjson.array
                {
                
                    self.jsonContact = contacts
                }
                
                let tempjsonAttachment =  jsonMain[0]["visit_attachment"].arrayValue
                self.jsonAttachment = tempjsonAttachment
                
                 let tempjsonAddress = "\(jsonMain[0]["address1"].stringValue) , \(jsonMain[0]["address2"].stringValue) , \(jsonMain[0]["city"].stringValue ) , \(jsonMain[0]["state_name"].stringValue ) , \(jsonMain[0]["country_name"].stringValue )"
                self.jsonAddress = tempjsonAddress
                
                self.tableView.dg_stopLoading()
                self.tableView.reloadData()
            }
            
            
        }
     
        
    }
 
    
    
    
    deinit {
        tableView.dg_removePullToRefresh()
    }
    
}

extension OfflineVisitDetailsVC : AddContactsToCreateOrderProtocol
{
    func contactdetails(name: String, Contact_id: Int) {
        AppDelegate.hideWaitView()
        
        
        self.VisitListDataFetch()
    }
    
    
}
// tableView Setup
extension OfflineVisitDetailsVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.section == 2
        {
            guard let imgString = jsonAttachment?[indexPath.row].stringValue else{return}
//            let filename =
            if let imgData = Data(base64Encoded: imgString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)
            {
                guard let controller = ShowImage.instance() else{return}
             controller.img_Var = UIImage(data: imgData)
                
                self.navigationController?.pushViewController(controller, animated: true)
            }
            
           
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
            
        case 1 :
            return jsonContact?.count ?? 0
            
        case 2:
            return jsonAttachment?.count ?? 0
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0 :
            return 0.0
        case 1 :
            if jsonContact?.count ?? 0 > 0
            {
                return 40.0
            }
            else
            {
                return 0.0
            }
            
        case 2:
            if jsonAttachment?.count ?? 0 > 0
            {
                return 40.0
            }
            else
            {
                return 0.0
            }
            
        default:
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch section {
        case 1 :
            return sections[section]
            
        case 2:
            return sections[section]
        default:
            return ""
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if jsonMain != nil
        {
            if jsonAttachment != nil
            {
                return sections.count
            }
            else
            {
                return sections.count - 1
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "detail") as? VisitDetailsCell
                else
            {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            guard let json = jsonMain?[0] else{return cell}
            cell.VisitName?.text = json["title"].stringValue
            cell.lblDetails?.text = json["description"].stringValue
            cell.lblDetails?.textColor = UIColor.black
            
            cell.lblVisitDate?.text = json["visit_date"].stringValue
            cell.lblVisitType?.text = json["visit_type"].stringValue
            
            // for address
            
            cell.lblAddressVisit?.text = self.jsonAddress// CompleteAddress
            
            
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as? TeamCell
                else
            {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            
            guard let json = jsonContact?[indexPath.row] else{return cell}
            
            
            if json["is_express"].boolValue
            {
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.red)
            }
            else
            {
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
            }
            
            cell.lblUserName.text = "\(json["salutation"].stringValue) \(json["contact_name"].stringValue)"
            cell.lblMessage.text = json["category_name"].stringValue
            cell.imgUser?.setImage(string: json["contact_name"].stringValue, color: nil, circular: true)
            
          
            
            switch json["status"].intValue
            {
            case 1:
                cell.lblStatus?.text = "Pending"
                cell.lblStatus?.textColor = hexStringToUIColor(hex: "#FFA20D")
                
                break
            default:
                cell.lblStatus?.text = "Pending"
                cell.lblStatus?.textColor = hexStringToUIColor(hex: "#FFA20D")
                
            }
            
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AttachmentCell", for: indexPath) as? TeamCell
                else
            {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
             cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
            guard let json = jsonAttachment?[indexPath.row] else{return cell}
            if let filename = jsonMain?[0]["filepath"][indexPath.row].stringValue
            {
                
                cell.lblUserName.text = filename
  
            cell.imgUser.image = UIImage.fontAwesomeIcon(name: .fileImage, style: .regular, textColor: VisitNAVBAR(), size: CGSize(width: 40.0, height: 40.0))
            
            
            }
            
            return cell
            
            
            
            
        default:
            print("error")
        }
        return UITableViewCell()
    }
    
    
    
}


extension OfflineVisitDetailsVC
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.VisitListDataFetch()
            }, loadingView: loadingView)
        tableView.dg_setPullToRefreshFillColor(VisitNAVBAR())
        
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
}


extension OfflineVisitDetailsVC
{
    class func instance()->OfflineVisitDetailsVC?{
        let storyboard = UIStoryboard(name: "OfflineStoryBoard", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "OfflineVisitDetailsVC") as? OfflineVisitDetailsVC
        //self.definesPresentationContext = true
        
        return controller
    }
}
