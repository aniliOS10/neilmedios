//
//  OfflineTaskDetailVC.swift
//  MedMobilie
//
//  Created by dr.mac on 30/04/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//



import UIKit
import SwiftyJSON
import Letters
import IoniconsKit
import DGElasticPullToRefresh
import SwipeCellKit
import EPSignature
import FontAwesome_swift

class OfflineTaskDetailVC: InterfaceExtendedController {
    
    
    
    var TaskId : Int = 0
    fileprivate var selectedContactId : Int = 0
     var jsonMain : [JSON]?
    @IBOutlet weak var tableView: UITableView!
    fileprivate let sections = ["","Notes"]
   
    
    fileprivate var jsonContact : [JSON]?
    fileprivate var jsonAttachment : [JSON]?
    fileprivate var pickerDataHours = [Array(0...24),Array(0...60)]
    fileprivate var waitingHours  : Int = 0
    fileprivate var waitingMinutes  : Int = 0
    
    // @IBOutlet fileprivate var NotesView : AddNotesView?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = false
       
        
        AppDelegate.showWaitView()
        OfflineTaskDetailFetch()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        
        // self.navigationController?.navigationBar.isHidden = false
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: TaskNAVBAR(), BtnColor: UIColor.white)
        
    }
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Task Details")
    }
    
    fileprivate func OfflineTaskDetailFetch()
    {
            AppDelegate.hideWaitView()
                if jsonMain != nil
                {
                        self.tableView.reloadData()
                }
        
    }
    
    
//    fileprivate func VisitListDataFetch()
//    {
//        if !NetworkState.isConnected() {
//            AppDelegate.hideWaitView()
//            AppDelegate.alertViewForInterNet()
//            return
//        }
//        DataFetch_Api().Fetch(ID: TaskId ,urlString: "/taskappdetails") { (status, message,data) in
//
//            AppDelegate.hideWaitView()
//            if !status
//            {
//                self.tableView.dg_stopLoading()
//                self.alert(message)
//                return
//            }
//            if data != nil
//            {
//                self.jsonMain = data
//
//                print(data)
//                guard let tempjsonContact =  self.jsonMain?[0]["task_visits"].arrayValue else{return}
//
//                self.jsonContact = tempjsonContact
//                guard let tempjsonAttachment =  self.jsonMain?[0]["task_attachments"].arrayValue else{return}
//                self.jsonAttachment = tempjsonAttachment
//
//
//                self.tableView.dg_stopLoading()
//                self.tableView.reloadData()
//            }
//        }
//
//
//    }
    
}

// tableView Setup
extension OfflineTaskDetailVC : UITableViewDelegate,UITableViewDataSource , SwipeTableViewCellDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
       return 1
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0 :
            return 0.0
       
        case 1:
            return UITableView.automaticDimension
            
        default:
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch section {
        case 1 :
            return sections[section]
        default:
            return ""
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if jsonMain != nil
        {
            
            
                return sections.count
            
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "detail") as? VisitDetailsCell
                else
            {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            guard let json = jsonMain?[0] else{return cell}
            cell.VisitName?.text = json["name"].stringValue
            cell.lblDetails?.text = json["description"].stringValue
            cell.lblDetails?.textColor = UIColor.black
            let dateStr = json["deadline"].stringValue
            cell.lblVisitDate?.text = dateStr
            cell.lblVisitType?.text =  "\("Assign by: ") Self"
            
            
            cell.lblAddressVisit?.text = ""
            
            return cell
            
        
            
        case 1:
            
            guard let notesCell = tableView.dequeueReusableCell(withIdentifier: "NotesCell") as? VisitDetailsCell
                else{return VisitDetailsCell()}
            guard let json = jsonMain?[0] else{return notesCell}
            notesCell.selectionStyle = .none
            notesCell.lblDetails?.text = json["notes"].stringValue
            
            return notesCell
            
        default:
            print("error")
        }
        return UITableViewCell()
    }
    
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]?
    {
        return[]
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    
    
}


extension OfflineTaskDetailVC :  UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // edit by harsh
        
        return pickerDataHours[component].count
        //return Pickerdatavalue[0].count
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel()
        label.text = String(row)
        label.textAlignment = .center
        return label
    }
    
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(pickerDataHours[component][row])
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        if let label = pickerView.view(forRow: row, forComponent: component) as? UILabel {
            
            if component == 0, row > 1 {
                label.text = String(row) + " hours"
                waitingHours = pickerDataHours[component][row]
                
            }
            else if component == 0 {
                label.text = String(row) + " hour"
                waitingHours = pickerDataHours[component][row]
            }
            else if component == 1 {
                label.text = String(row) + " min"
                waitingMinutes = pickerDataHours[component][row]
            }
            
        }
        /*
         if component == 0
         {
         waitingHours = pickerDataHours[component][row]
         }
         else if component == 1
         {
         waitingMinutes = pickerDataHours[component][row]
         }*/
        // RowValue = row
        
    }
}





extension OfflineTaskDetailVC
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            }, loadingView: loadingView)
        tableView.dg_setPullToRefreshFillColor(TaskNAVBAR())
        
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
}

extension OfflineTaskDetailVC
{
    class func instance()->OfflineTaskDetailVC?{
        let storyboard = UIStoryboard(name: "OfflineStoryBoard", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "OfflineTaskDetailVC") as? OfflineTaskDetailVC
        
        return controller
    }
}
