
import UIKit
import IoniconsKit
import DGElasticPullToRefresh
import SwiftyJSON
import SwipeCellKit
import CoreData

class OfflineTaskListVC: InterfaceExtendedController , UISearchResultsUpdating {
    
    @IBOutlet weak var lblPlusSign: UILabel!
    @IBOutlet weak var btnView: UIView!
    fileprivate var JsonData : [JSON]?
    var contact_ID : Int = 0
    fileprivate var mainJsonData : [JSON]?
    fileprivate var resultSearchController = UISearchController()
    var datefrom : String = ""
    var dateto : String = ""
    var type : String = ""
    var priority : Int = 0
    var reportKey : String = ""
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnView.createCircleForView()
        lblPlusSign.font = UIFont.ionicon(of: 30)
        lblPlusSign.text = String.ionicon(with: .plus )
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = false
        
        
        if contact_ID == 0
        {
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(TaskListDataFetch), name: NSNotification.Name( "UpdateTaskList"), object: nil)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: TaskNAVBAR(), BtnColor: UIColor.white)
        SetUpSearchBar()
        AppDelegate.showWaitView()
        TaskListDataFetch()
    }
   
    
    func SetUpSearchBar()
    {
        self.resultSearchController = ({
            
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.searchBar.placeholder = "Search Task".languageSet
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.hidesNavigationBarDuringPresentation = false
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.resultSearchController.dismiss(animated: false, completion: nil)
    }
    
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Tasks")
    }
    @IBAction func createNewVisit(_ sender: Any) {
        
        guard let controller = OfflineCreateTaskVC.instance()
            else{return}
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    // data fetching
    @objc fileprivate func TaskListDataFetch()
    {
        if !NetworkState.isConnected() {
           
            DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.TaskList.rawValue) { (data) in
              
                if let object = data
                {
                    var jsonArray = [JSON]()
                for jsonData in object
                {
                if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                        jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                        jsonArray.append(json!)
                        })})
                                    }
                }
                
                AppDelegate.hideWaitView()
                
                    self.mainJsonData = jsonArray
                    self.JsonData = self.mainJsonData
                    self.tableView.reloadData()
                    self.tableView.dg_stopLoading()
                }
            }
        }
        
        
        
    }
    fileprivate func TaskDelete_Or_TaskUpdate(Taskid:Int , Task_delete : Bool )
    {
        
        if !NetworkState.isConnected() {
            tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
           // AppDelegate.alertViewForInterNet()
            return
        }
        var urlstr : String = "/taskdelete"
        if !Task_delete
        {
            urlstr = "/taskupdatestatus"
        }
        
        ContactDelete_Api().Request(idvalue: Taskid ,url_string: urlstr ) { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: message, actionTitles:["ok"], actions:[{action1 in
//                    AppDelegate.showWaitView()
//                    self.TaskListDataFetch()
                    }, nil])
            }
            
        }
        
        
    }
    
    //MARK: - SearchBar Update
    
    func updateSearchResults(for searchController: UISearchController) {
        // filteredTableData.removeAll(keepingCapacity: false)
        
        if !searchController.isActive {
            print("Cancelled")
        }
        
        if searchController.searchBar.text != ""
        {
            let Filterarray = self.mainJsonData?.filter({ (element) -> Bool in
                if element["contact_name"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "")
                {
                    return true
                }
                return false
            })
            
            self.JsonData = Filterarray
            self.tableView.reloadData()
        }
        else
        {
            self.JsonData = mainJsonData
            self.tableView.reloadData()
            
        }
        
    }

    deinit {
        tableView.dg_removePullToRefresh()
    }
  
}
extension OfflineTaskListVC : UITableViewDelegate , UITableViewDataSource , SwipeTableViewCellDelegate
{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return JsonData?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        guard let list = self.JsonData?[indexPath.row] else{return }
        
        guard let controller = OfflineTaskDetailVC.instance()
            else{return}
        
        controller.jsonMain = [list]
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? TaskListCell
            else{return TaskListCell()}
        // cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
        guard let json = JsonData?[indexPath.row]
            else{return cell}
        
        
        if json["status"].intValue == 2
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.red)
            cell.delegate = nil
        }
        else
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.green)
            cell.delegate = nil
        }
   
        
        cell.lblassign_by?.text = "Self"
        cell.lblTask_name?.text = json["name"].stringValue
        cell.lblTime?.text = json["deadline"].stringValue
        
 
        cell.lblStatus?.text = json["priority_name"].stringValue
        
        if json["priority"].intValue == 1
        {
            cell.lblStatus?.textColor = UIColor.orange
        }
        else  if json["priority"].intValue == 2
        {
            cell.lblStatus?.textColor = UIColor.green
        }
        else
        {
            cell.lblStatus?.textColor = .red
        }
        
        
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
        
        
    }
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        guard let list = self.JsonData?[indexPath.row] else{return []}
        let deleteAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            
            
            if !list["is_editable"].boolValue
            {
                self.alert(self.languageKey(key: "You are not authorized to Delete this Task"))
            }
            else
            {
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: "Are you sure you want to delete this Task?"), actionTitles: [self.languageKey(key: "NO"),self.languageKey(key: "YES")], actions: [{action1 in
                    return
                    },{action2 in
                        
                        self.JsonData?.remove(at: indexPath.row)
                        self.tableView.reloadData()
                        DispatchQueue.global(qos: .background).async {
                            DispatchQueue.main.async {
                                self.TaskDelete_Or_TaskUpdate(Taskid: list["id"].intValue, Task_delete: true)
                                //  self.visitDelete(Contactid: list["id"].intValue)
                            }
                            
                            
                        }
                        
                    }, nil])
                
                
            }
            
        }
        
        
        
        
        let editAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            
            
            if !list["is_editable"].boolValue
            {
                self.alert(self.languageKey(key: "You are not authorized to Edit this Task"))
            }
            else
            {
                guard let json = self.JsonData?[indexPath.row]
                    else{return}
                guard let controller = EditTask.instance()
                    else{return}
                controller.TaskId = json["id"].intValue
                self.navigationController?.pushViewController(controller, animated: true)
                
            }
            
        }
        
        
        let complete = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            
            self.JsonData?[indexPath.row]["status"] = 3
            self.tableView.reloadData()
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    self.TaskDelete_Or_TaskUpdate(Taskid: list["id"].intValue, Task_delete: false)
                }
                
                
            }
            
        }
        
        
        deleteAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor: .white, size: CGSize(width: 32, height: 32))
        
        complete.backgroundColor = UIColor.lightGray
        complete.image = UIImage.ionicon(with: .thumbsup, textColor: .white, size: CGSize(width: 32, height: 32))
        
        
        editAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        editAction.image = UIImage.ionicon(with: .edit, textColor: .white, size: CGSize(width: 32, height: 32))
        
        
        return [deleteAction,complete,editAction]
        
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    
}
extension OfflineTaskListVC
{
    class func instance()->OfflineTaskListVC?{
        let storyboard = UIStoryboard(name: "OfflineStoryBoard", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "OfflineTaskListVC") as? OfflineTaskListVC
        //self.definesPresentationContext = true
        
        return controller
    }
}
extension OfflineTaskListVC
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            // AppDelegate.showWaitView()
            
            self!.datefrom = ""
            self!.dateto = ""
            
            
            self!.type  = ""
            self!.priority  = 0
            
            
          //  self?.TaskListDataFetch()
            }, loadingView: loadingView)
        
        tableView.dg_setPullToRefreshFillColor(TaskNAVBAR())
        
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
}



extension OfflineTaskListVC : FilterProtocol
{
    func Filterdetails(FromDate : String , ToDate : String)
    {
        
        
        datefrom = FromDate
        dateto = ToDate
        
        print("datefrom :---> ",datefrom)
        print("dateto :---> ",dateto)
        
  
    }
    
    
    
    
}
