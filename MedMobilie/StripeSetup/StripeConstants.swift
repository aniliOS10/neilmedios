
import Foundation

enum StripeConstants {
  static let publishableKey = "pk_test_lv9MDRrUO3AvCH9aQkwWfHS7"
  
  static let defaultCurrency = "usd"
  static let defaultDescription = "Purchase from RWPuppies iOS"
}
