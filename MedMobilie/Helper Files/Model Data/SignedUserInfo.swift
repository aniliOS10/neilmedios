

import UIKit
import SwiftyJSON

@objc class SignedUserInfo: UserInfo , NSCoding{
    
    var accessToken : String?
    var notificationCount : Int = 0
    
    private static var _sharedInstance : SignedUserInfo?
    
    static var sharedInstance : SignedUserInfo?{
        get{
            if(_sharedInstance != nil){
                return _sharedInstance
            }
            
            
            let instance = retreiveInstance()
            _sharedInstance = instance
            
            return instance
        }
    }
    
    static let encodingKey = "userInfo"
    //    static func initSharedInstanceWithRawJSON(rawString : String?)->SignedUserInfo{
    //     //   let json =  rawString?.toJSON()
    //
    //        return initSharedInstance(userInfoJSON: json)
    //    }
    //
    static func initSharedInstance(userInfoJSON : [String : JSON]?)->SignedUserInfo{
        let oldInstance = sharedInstance
        let info = SignedUserInfo(userInfoJSON : userInfoJSON)
        _sharedInstance = info
        info.save()
        info.accessToken = oldInstance?.accessToken ?? ""
        return info
    }
    static func tokenSave(token : String)
    {
        let ud = UserDefaults.standard
        ud.set(token, forKey: "token")
        
    }
    override init(){
        super.init()
    }
    
    override init(userInfoJSON : [String:JSON]?){
        super.init(userInfoJSON: userInfoJSON)
    }
    
    override func fillInfo(info : [String:JSON]?){
        super.fillInfo(info: info)
        save()
    }
    
    func save(){
        
        let ud = UserDefaults.standard
        let instance = self
        let data = NSKeyedArchiver.archivedData(withRootObject: instance)
        ud.set(data, forKey: SignedUserInfo.encodingKey)
    }
    
    private static func retreiveInstance()->SignedUserInfo?{
        
        let ud = UserDefaults.standard
        guard let data = ud.object(forKey: encodingKey) as? Data
        else{
            return nil
        }
        
        let unarc = NSKeyedUnarchiver(forReadingWith: data)
        unarc.setClass(SignedUserInfo.self, forClassName: "SignedUserInfo")
        let signedUserInfo = unarc.decodeObject(forKey: "root") as? SignedUserInfo
        
        return signedUserInfo
    }
    
    
    required convenience init(coder aDecoder: NSCoder) {
        self.init()
        
        
        user_id = aDecoder.decodeObject(forKey: "id") as? String
        username = aDecoder.decodeObject(forKey: "name") as? String
        userImg = aDecoder.decodeObject(forKey: "image") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        currency_symbol = aDecoder.decodeObject(forKey: "currency_symbol") as? String
        language = aDecoder.decodeObject(forKey: "language") as? String
        phone = aDecoder.decodeObject(forKey: "phone") as? String
        gender = aDecoder.decodeObject(forKey: "gender") as? String
        currency_code = aDecoder.decodeObject(forKey: "currency_code") as? String
        country_code = aDecoder.decodeObject(forKey: "country_code") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        
        
        aCoder.encode(user_id, forKey: "id")
        aCoder.encode(username, forKey: "name")
        aCoder.encode(userImg, forKey: "image")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(currency_code, forKey: "currency_code")
        aCoder.encode(currency_symbol, forKey: "currency_symbol")
        aCoder.encode(language, forKey: "language")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(gender, forKey: "gender")
        aCoder.encode(country_code, forKey: "country_code")
        
    }
    
    func clear(){
        
        let ud = UserDefaults.standard
        DataBaseHelper.ShareInstance.DeleteContext(ClassName: OfflineDataClass.TaskList.rawValue)
        DataBaseHelper.ShareInstance.DeleteContext(ClassName: OfflineDataClass.OrderList.rawValue)
        DataBaseHelper.ShareInstance.DeleteContext(ClassName: OfflineDataClass.VisitList.rawValue)
        SignedUserInfo._sharedInstance = nil
        
        ud.set(nil, forKey: "OfflineNewVisitCreate")
        ud.set(nil, forKey: "OfflineNewContactCreate")
        ud.set(nil, forKey: "OfflineNewTaskCreate")
        ud.set(nil, forKey: SignedUserInfo.encodingKey)
        ud.set(nil, forKey: "accessToken")
        ud.synchronize()
        notificationCount = 0
        
    }
}
