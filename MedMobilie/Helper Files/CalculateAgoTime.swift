//
//  CalculateAgoTime.swift
//  MedMobilie
//
//  Created by dr.mac on 09/04/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation

import UIKit

class CalculateAgoTime: NSObject {
    
    func timeAgoSinceDate(date:NSDate, numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
        
        if (components.year! >= 2) {
            return "\(components.year!) " + "years ago".languageSet
        }else if (components.year! >= 1){
            if (numericDates){
                return "1 " + "year ago".languageSet
            } else {
                return "Last year".languageSet
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) " + "months ago".languageSet
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 " + "month ago".languageSet
            } else {
                return "Last month".languageSet
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) " + "weeks ago".languageSet
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 " + "week ago".languageSet
            } else {
                return "Last week".languageSet
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) " + "days ago".languageSet
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 " + "day ago".languageSet
            } else {
                return "Yesterday".languageSet
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) " + "hours ago".languageSet
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 " + "hour ago".languageSet
            } else {
                return "An hour ago".languageSet
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) " + "minutes ago".languageSet
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 " + "minute ago".languageSet
            } else {
                return "A minute ago".languageSet
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) " + "seconds ago".languageSet
        } else {
            return "Just now".languageSet
        }
        
    }
    
    
}
