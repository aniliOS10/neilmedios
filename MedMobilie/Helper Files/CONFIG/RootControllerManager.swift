//
//  RootControllerManager.swift
//  MedMobilie
//
//  Created by dr.mac on 03/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import MMDrawerController
import UIKit
import LocalAuthentication


class RootControllerManager : NSObject{
    
   
    var drawerContainer: MMDrawerController?
    
    func setRoot(){
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let window = appDelegate?.window
         if UserDefaults.standard.value(forKey: "NewUser") == nil
         {
            guard let controller = Local_Language_Setup.instance()
                else{
                    return
            }
            let nav = UINavigationController(rootViewController: controller)
            window?.rootViewController = nav
            return
                

            }
        let userInfo = SignedUserInfo.sharedInstance
        if(userInfo == nil){
            
           
            
                    
                    guard let controller = LoginViewController.instance()
                        else{
                            return
                    }
                    let nav = UINavigationController(rootViewController: controller)
                    window?.rootViewController = nav
                    return
            
          
               
        }
            
        if userInfo != nil {
             RootOfflineDataFetch().FetchingAll()
        }
       
//        guard let initialViewController =  DataSyncVC.instance() else{return}
//                window?.rootViewController = initialViewController
//                window?.makeKeyAndVisible()
            appDelegate?.buildNavigationDrawer()
        return

//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
        
        
    }
    
    func authenticationWithTouchID() {
        let localAuthenticationContext = LAContext()
        
        
        var authError: NSError?
        let reasonString = "To access the secure data"
        
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString) { success, evaluateError in
                
                if success {
                   
                        self.loginWithuserDetail()
                    //TODO: User authenticated successfully, take appropriate action
                    
                } else {
                    //TODO: User did not authenticate successfully, look at error and take appropriate action
                    guard let error = evaluateError else {
                        return
                    }
                        return
                    
                    //TODO: If you have choosen the 'Fallback authentication mechanism selected' (LAError.userFallback). Handle gracefully
                    
                }
            }
        } else {
            
            guard let error = authError else {
                return
            }
            
            //TODO: Show appropriate alert if biometry/TouchID/FaceID is lockout or not enrolled
            print(FaceIDErrorConstant.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
        }
    }
    
    
    
    
    
    
    fileprivate func loginWithuserDetail()
    {
        let ud = UserDefaults.standard
        if let email = ud.value(forKey: "LoginEmailId") as? String , let pass = ud.value(forKey: "LoginPassword") as? String
        {
           
            if !NetworkState.isConnected() {
                AppDelegate.alertViewForInterNet()
                return
            }
            DispatchQueue.global(qos: .background).async {
               
                LoginProcessor().Request(email: email, password: pass) { (status,message, info) in
                   
                    DispatchQueue.main.async {
                          RootControllerManager().setRoot()
                    }
                  
                    
                    //            let objAppDelegate = UIApplication.shared.delegate as? AppDelegate
                    //            objAppDelegate?.buildNavigationDrawer()
                }
            }
            }
            
            
        
    }
    
    
    
    func errorMessage(errorCode:Int) -> String{
        
        var strMessage = ""
        
        switch errorCode {
        case LAError.authenticationFailed.rawValue:
            strMessage = "Authentication Failed"
            
        case LAError.userCancel.rawValue:
            strMessage = "User Cancel"
            
        case LAError.userFallback.rawValue:
            strMessage = "User Fallback"
            
        case LAError.systemCancel.rawValue:
            strMessage = "System Cancel"
            
        case LAError.passcodeNotSet.rawValue:
            strMessage = "Passcode Not Set"
        case LAError.biometryNotAvailable.rawValue:
            strMessage = "TouchI DNot Available"
            
        case LAError.biometryNotEnrolled.rawValue:
            strMessage = "TouchID Not Enrolled"
            
        case LAError.biometryLockout.rawValue:
            strMessage = "TouchID Lockout"
            
        case LAError.appCancel.rawValue:
            strMessage = "App Cancel"
            
        case LAError.invalidContext.rawValue:
            strMessage = "Invalid Context"
            
        default:
            strMessage = "Some error found"
            
        }
        
        return strMessage
        
    }
    
    
    func setFaceIdNotDetect()
    {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let window = appDelegate?.window
        guard let controller = FaceIdVC.instance()
        else
        {
            return
        }
        
        window?.rootViewController = controller
    }
    
}

