//
//  ServerProcessor.swift

//
//  Created by Harsh garg on 21/01/18.
//  Copyright © 2016 harsh garg. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

class ServerProcessor : NSObject{

    enum httpMethod {
        case get
        case post
        case put
        case delete

        public func libHttpMethod()->Alamofire.HTTPMethod{
            switch self {
            case .get:
                return Alamofire.HTTPMethod.get
            case .post:
                return Alamofire.HTTPMethod.post
            case .put:
                return Alamofire.HTTPMethod.put
            case .delete:
                return Alamofire.HTTPMethod.delete
            }
        }
    }

    enum encoding {

        case defaultEncoding
        case httpBody
        case queryString
        case jsonEncoding

        public func libEncoding()->Alamofire.ParameterEncoding{
            switch self {
            case .defaultEncoding:
                return URLEncoding.default
            case .httpBody:
                return URLEncoding.httpBody
            case .queryString:
                return URLEncoding.queryString
            case .jsonEncoding:
                return JSONEncoding.default
            }
        }
    }

    fileprivate var requestRef : Request?
    fileprivate var completion : ((_ success : Bool, _ response : JSON?)->())?

    func cancelRequest(_ ignoreResponse : Bool = true){
        if(ignoreResponse){
            completion = nil
        }
        requestRef?.cancel()
    }

    func upload(image : UIImage?, imageName : String, _ URLString: String,
                parameters: [String: Any]? = nil,headers: [String: String]? = nil, authorize : Bool = false ,completion : @escaping ((_ success : Bool, _ response : JSON?)->())){

        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
        }
        self.completion = completion
        var headers = headers ?? [String : String]()
        
        if(authorize){
            headers["authorization"] = getAuthorizationToken()
        }
      
        Alamofire.upload(multipartFormData: { (multipartFormData) in

            _ = self.fillMultiform(image: image ?? nil, imageName : imageName, parameters: parameters, multipartFormData: multipartFormData)

        }, to: URLString , method: .post, headers: headers) { (result) in
            switch result {
            case .success(let upload, _, _):

                self.requestRef = upload
                upload.responseJSON { response in
                    DispatchQueue.main.async(execute: {

                        self.handleResponse(response)
                        return
                    })
                    return
                }

            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    
    
    
    func uploadmuliple(image : [UIImage], imageName : String, _ URLString: String,
                parameters: [String: Any]? = nil,headers: [String: String]? = nil, authorize : Bool = false ,completion : @escaping ((_ success : Bool, _ response : JSON?)->())){
        
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
        }
        self.completion = completion
        var headers = headers ?? [String : String]()
        
        if(authorize){
            headers["authorization"] = getAuthorizationToken()
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            _ = self.OtherfillMultiform(image: image, imageName: imageName, parameters: parameters, multipartFormData: multipartFormData)
            
        }, to: URLString , method: .post, headers: headers) { (result) in
            switch result {
            case .success(let upload, _, _):
                
                self.requestRef = upload
                upload.responseJSON { response in
                    DispatchQueue.main.async(execute: {
                        
                        self.handleResponse(response)
                        return
                    })
                    return
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }

    
    fileprivate func OtherfillMultiform(image : [UIImage], imageName : String, parameters: [String: Any]? = nil, multipartFormData : Alamofire.MultipartFormData)->Alamofire.MultipartFormData{
        
        
        let count = image.count ?? 0
        let otherarray = [NSData]?.self
        for i in 0..<count
        {
           
            let imageData =  image[i].jpegData(compressionQuality: 0.5)
            
            
            
            multipartFormData.append(imageData!, withName: "attachments"+String(format:"%d",i), fileName: "attachment"+String(format:"%d",i+1)+".jpg", mimeType: "image/jpeg")
            
          
            
            print("success");
            
        }
       
        
        guard let parameters = parameters
            else{
                return multipartFormData
        }
        
        for (key, value) in parameters {
            if let encodedValue = encodedString(value: value){
                multipartFormData.append(encodedValue, withName: key)
                
            }
            
        }
        return multipartFormData
    }
    
    
    

    fileprivate func fillMultiform(image : UIImage?, imageName : String, parameters: [String: Any]? = nil, multipartFormData : Alamofire.MultipartFormData)->Alamofire.MultipartFormData{

        let timeInterval = NSDate().timeIntervalSince1970
        let stampString = String(timeInterval)
        let stampedImageName = stampString + ".jpg"
        if let imageData =  image?.jpegData(compressionQuality: 0.5){multipartFormData.append(imageData, withName: imageName,fileName: stampedImageName, mimeType: "image/jpg")
        }

        guard let parameters = parameters
            else{
                return multipartFormData
        }

        for (key, value) in parameters {
            if let encodedValue = encodedString(value: value){
                multipartFormData.append(encodedValue, withName: key)
                
            }

        }
        return multipartFormData
    }

    fileprivate func encodedString(value : Any)->Data?{
        let stringValue = String(describing: value)
       // Log.echo(key: "", text: "encodedString  ==> \(stringValue)")
        let encodedValue = stringValue.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        return encodedValue
    }

    func request(
        _ method: httpMethod,
        _ URLString: String,
        parameters: [String: Any]? = nil,
        encoding: encoding = .defaultEncoding,
        headers: [String: String]? = nil, authorize : Bool = false,
        completion : @escaping ((_ success : Bool, _ response : JSON?)->())){
//        AppDelegate.InternetCheck()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
        }
        
        self.completion = completion
        var headers = headers ?? [String : String]()

        if(authorize){
            headers["authorization"] = getAuthorizationToken()
        }

       // Log.echo(key: "token", text: "param => " + (URLString))
       // Log.echo(key: "token", text: "param => " + (parameters?.JSONDescription() ?? ""))
        let request = Alamofire.request(URLString, method : method.libHttpMethod(), parameters: parameters, encoding: encoding.libEncoding(), headers: headers)
        self.requestRef = request

        request
        .validate()
        .validate(statusCode: 200 ..< 300)
            .responseJSON { (response) in
                DispatchQueue.main.async(execute: {
                        print(response)
                    self.handleResponse(response)
                    return
                })
                return
        }
    }

    fileprivate func getAuthorizationToken()->String{
        let user = UserDefaults.standard
    
        
               guard let token = user.value(forKey: "accessToken") as? String else
               {
                return ""
                }
                  //  return user.value(forKey: "accessToken") as! String
        return token
        /*guard let info = SignedUserInfo.sharedInstance
            else{
                return ""
        }

        let token = info.accessToken ?? ""

        //Log.echo(key: "token", text: token)
        return token*/
    }



    fileprivate func respond(success : Bool, response : JSON?){

        guard let completion = self.completion
            else{
                return
        }

        completion(success, response)
        self.completion = nil
    }

    fileprivate func handleResponse(_ response : Alamofire.DataResponse<Any>){

        if let data = response.data{
            let responseData = String(data: data , encoding: String.Encoding.utf8)
          //  Log.echo(key: "identifie", text: "Responsdata in server processor param => \(responseData)")
        }

        if(response.result.isSuccess){
            extractToken(httpResponse: response.response)
        }


        guard let data = response.data
            else{
                respond(success: false, response: nil)
                return
        }

        respond(success: response.result.isSuccess, response: JSON(data))
        return
    }


    private func extractToken(httpResponse : HTTPURLResponse?){
        let headerInfo = httpResponse?.allHeaderFields
       // Log.echo(key: "token", text: "headerInfo extracted ==>  \(headerInfo)")
        guard let accessToken = headerInfo?["x-session-token"] as? String
            else{
                //Log.echo(key: "token", text: "accessToken not found ")
                return
        }

        guard let instance = SignedUserInfo.sharedInstance
            else{
         //       Log.echo(key: "token", text: "user instance not found ")
                return
        }

       // Log.echo(key: "token", text: "token extracted ==>  " + accessToken)
        instance.accessToken = accessToken
    }


}
