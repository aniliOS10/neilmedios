//
//  NSObject.swift
//  MedMobilie
//
//  Created by dr.mac on 26/04/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import LocalAuthentication
import SwiftyJSON

extension NSObject {
    static func storeJSON(dataToStore: JSON, completion: (_ data: Data?) -> Void) {
        do {
            let data = try dataToStore.rawData(options: .prettyPrinted)
            completion(data)
        } catch let error as NSError {
            print("NSJSONSerialization Error: \(error)")
            completion(nil)
        }
    }
    
   
}

extension Data
{
    
    func retrieveJSON(completion: (_ json: JSON?) -> Void) {
        
            do {
                let nsJSON = try JSONSerialization.jsonObject(with: self, options: .mutableContainers)
                completion(JSON(nsJSON))
            } catch let error as NSError {
                print("NSJSONSerialization Error: \(error)")
                completion(nil)
            }
        
    }
    
}



extension LAContext {
    enum BiometricType: String {
        case none
        case touchID
        case faceID
    }
    
    var biometricType: BiometricType {
        var error: NSError?
        
        guard self.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            // Capture these recoverable error thru Crashlytics
            return .none
        }
        
        if #available(iOS 11.0, *) {
            switch self.biometryType {
            case .none:
                return .none
            case .touchID:
                return .touchID
            case .faceID:
                return .faceID
            }
        } else {
            return  self.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touchID : .none
        }
    }
}
