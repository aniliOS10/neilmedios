//
//  NSLayoutConstraint.swift
//  M1 Pay
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
extension NSLayoutConstraint
{
    @IBInspectable var iPhone5:CGFloat{
        get {
            return 0
        }
        set {
            
            if UIScreen.main.isPhone5
            {
                self.constant = newValue
            }
            else if UIScreen.main.isPhone6
            {
                self.constant = newValue + 10
            }
            else if UIScreen.main.isPhone6Plus
            {
                self.constant = newValue + 20
            }
            else if UIScreen.main.isPhoneX || UIScreen.main.isPhoneXR || UIScreen.main.isPhoneXsMaX
            {
                self.constant = newValue + 50
            }
        }
    }
}
