//
//  UIImage.swift
//  MedMobilie
//
//  Created by dr.mac on 18/02/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import UIKit
extension UIImage
{
    func textToImage(drawText: String, inImage: UIImage, atPoint: CGPoint) -> UIImage {
        let textColor = UIColor.white
        let textFont = UIFont(name: "Helvetica Bold", size: 16)!
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(inImage.size, false, scale)
        
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
            ] as [NSAttributedString.Key : Any]
        inImage.draw(in: CGRect(origin: CGPoint.zero, size: inImage.size))
        
        let rect = CGRect(origin: atPoint, size: inImage.size)
        drawText.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
