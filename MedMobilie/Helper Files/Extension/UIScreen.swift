//
//  UIScreen.swift
//  M1 Pay
//
//  Created by Apple on 25/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//
import Foundation
import UIKit

extension UIScreen {
    
    var isPhone4: Bool {
        return self.nativeBounds.size.height == 960;
    }
    var isPhone5: Bool {
        return self.nativeBounds.size.height == 1136;
    }
    var isPhone6: Bool {
        return self.nativeBounds.size.height == 1334;
    }
    var isPhone6Plus: Bool {
        return self.nativeBounds.size.height == 2208;
    }
    var isPhoneXR: Bool {
        return self.nativeBounds.size.height == 1792;
    }
    var isPhoneX: Bool {
        return self.nativeBounds.size.height == 2436;
    }
    var isPhoneXsMaX: Bool {
        return self.nativeBounds.size.height == 2688;
    }
    
}
extension Bundle {
    static func appName() -> String {
        guard let dictionary = Bundle.main.infoDictionary else {
            return ""
        }
        if let version : String = dictionary["CFBundleName"] as? String {
            return LocalizationSystem.SharedInstance.localizedStingforKey(key: version, comment: version)
        } else {
            return ""
        }
    }
}
