//
//  ExtendedViewController.swift
//  Dating
//
//  Created by harsh garg on 05.09.18.
//  Copyright (c) 2018 SwipeCubes. All rights reserved.
//


import UIKit
import IoniconsKit
import CoreData
import SwiftyJSON

class ExtendedController: UIViewController {

    private var isLoaded = false;
        var NotificationBtn = SSBadgeButton()
        var CartBtn = SSBadgeButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.LanguageSet()
        // self.NavBarIcon()
        
         NotificationCenter.default.addObserver(self, selector: #selector(LanguageSet), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        if NetworkState.isConnected()
        {
            if SignedUserInfo.sharedInstance != nil
            {
                    OfflineDataPush().PushData()
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        DispatchQueue.main.async {
            
            
            self.TaskPending_fetch()
            
        }

        
    }
    
    func NavBarIcon()
    {
       
        NotificationBtn.frame = CGRect(x: 0, y: 0, width: 18, height: 16)
        NotificationBtn.setImage(UIImage.ionicon(with: .androidNotificationsNone, textColor: .black, size: CGSize(width: 30, height: 30)), for: .normal)
        NotificationBtn.badgeEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 5)

        NotificationBtn.badgeLabel.isHidden = true
        NotificationBtn.addTarget(self, action: #selector(CartBtn_Action), for: .touchUpInside)
        let Button1 = UIBarButtonItem(customView: NotificationBtn)
        
        
        
        CartBtn.frame = CGRect(x: 0, y: 0, width: 18, height: 16)
        CartBtn.setImage(UIImage.ionicon(with: .androidCart, textColor: .black, size: CGSize(width: 30, height: 30)), for: .normal)
        CartBtn.badgeEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 5)

        CartBtn.badgeLabel.isHidden = true
        CartBtn.addTarget(self, action: #selector(Notification_Action), for: .touchUpInside)
        let Button2 = UIBarButtonItem(customView: CartBtn)
        
        
       self.navigationItem.rightBarButtonItems = [Button2,Button1]
        
        
        
//        let Notification = UIBarButtonItem(image: UIImage.ionicon(with: .androidCart, textColor: UIColor.black, size: CGSize(width: 40, height: 30)), style: .plain, target: self, action: #selector(Notification_Action))
//        Notification.tintColor = UIColor.black
//        navigationItem.rightBarButtonItems = [Notification]
//
        
        
    }
    
    func TaskPending_fetch()
    {
        if !NetworkState.isConnected() {
          //  AppDelegate.hideWaitView()
           // AppDelegate.alertViewForInterNet()
            return
        }
        NotificationFetch_Api().FetchDashBoardData { (status, message, data) in
         //   AppDelegate.hideWaitView()
            if !status
            {
                self.NotificationBtn.badgeLabel.isHidden = true
                self.CartBtn.badgeLabel.isHidden = true
                return
            }
            
            guard let infos = data?[0]  else{return}
            if infos["cart_count"].intValue > 0
            {
                 self.CartBtn.badgeLabel.isHidden = false
                self.CartBtn.addBadgeToButon(badge: String(infos["cart_count"].intValue))
                
            }
            else
            {
                self.CartBtn.badgeLabel.isHidden = true
                
            }
            if infos["notification_count"].intValue != 0
            {
                self.NotificationBtn.badgeLabel.isHidden = false
                self.NotificationBtn.addBadgeToButon(badge: String(infos["notification_count"].intValue))
            }
            else
            {
                self.NotificationBtn.badgeLabel.isHidden = true
            }
            
            //self.CartBtn.badgeLabel.isHidden = false
            
            
            
            
             //infos["welcometext"].stringValue
            
        }
    }
    
    
    
   
    @objc func Notification_Action(){
        
       
        
        guard let controller = ShowCartVC.instance()
            else{
                return
        }
        controller.ComingFromcartBtn = true
        
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    @objc func CartBtn_Action()
    {
        guard let controller = NotificationVC.instance()
            else{
                return
        }
        controller.class_type = true
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @objc func LanguageSet(){
        
        
        
    }
    func languageKey(key: String)->String
    {
       return LocalizationSystem.SharedInstance.localizedStingforKey(key: key, comment: key)
    }
   
    func viewDidLayout(){
    }
}
