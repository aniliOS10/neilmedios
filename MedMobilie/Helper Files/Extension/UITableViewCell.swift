//
//  UITableViewCell.swift
//  MedMobilie
//
//  Created by dr.mac on 06/03/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import UIKit
extension UITableViewCell
{
    func CellLanguageSet(key : String) -> String
    {
        return LocalizationSystem.SharedInstance.localizedStingforKey(key: key, comment: "")
    }

}
