//
//  InterfaceExtendedController.swift
//  Dating
//
//  Created by harsh garg on 05.09.18.
//  Copyright (c) 2018 SwipeCubes. All rights reserved.
//

import UIKit
import IoniconsKit
import MMDrawerController
import Toast_Swift

class InterfaceExtendedController : ExtendedController {
    //    var geometricLoader = GeometricLoader()
    var navBackgroundColor : UIColor?
    
    func NavigationBarTitleName(Title : String)
    {
        self.title = LocalizationSystem.SharedInstance.localizedStingforKey(key: Title, comment: "")
    }
    
    func PaintNavigationBar(TitleColor:UIColor,BackgroundColor:UIColor,BtnColor:UIColor)
    {
     

        navigationController?.view.backgroundColor = BackgroundColor

        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: TitleColor]
        
        // Navigation bar color
        self.navigationController?.navigationBar.barTintColor = BackgroundColor
        
        self.navBackgroundColor = BackgroundColor
        // Back button Txet Color
        self.navigationController!.navigationBar.tintColor = BtnColor
        
    }
    
    
    
    func paintMenu(){
        
        /*  let button = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
         let image = UIImage(named: "menu")
         button.setBackgroundImage(image, for: .normal)
         button.addTarget(self, action: #selector(openMenuButtonAction), for: .touchUpInside)
         let barButton = UIBarButtonItem(customView: button)
         self.navigationItem.leftBarButtonItem = barButton*/
    }
    func alert(_ message: String)
    {
        var mess = ""
        if message != ""
        {
            mess = message
        }
        else
        {
            mess = "something went wrong".languageSet
        }
        
        self.view.makeToast(mess.languageSet, duration: 3.0, position: .top)
        
        //
        //
        //        let titleName = Bundle.appName()
        //        self.popupAlert(title: titleName, message: self.languageKey(key: mess), actionTitles:[self.languageKey(key: "ok")], actions:[{action1 in
        //            print("ok")
        //            }, nil])
        
    }
    ///// bhushan Code
    
    func RightActionButton(Title:String){
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 70, height: 20))
        button.contentVerticalAlignment = .bottom
        button .setTitle(Title, for: .normal)
        button.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        button.sizeToFit()
        let barItem = UIBarButtonItem(customView: button)
        var items = self.navigationItem.rightBarButtonItems ?? [UIBarButtonItem]();
        items.append(barItem)
        self.navigationItem.rightBarButtonItems = items
    }
    
    func RightActionButtons()
    {
        let BTNSave = UIButton(frame: CGRect(x: 0, y: 0, width: 15, height: 20))
        BTNSave .setTitle("Save", for: .normal)
        BTNSave.addTarget(self, action: #selector(rightButtonSaveAction), for: .touchUpInside)
        
        let item1 = UIBarButtonItem()
        item1.customView = BTNSave
        
        //        let BTNSelectAll = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        //        BTNSelectAll .setTitle("Select All", for: .normal)
        //        BTNSelectAll.addTarget(self, action: #selector(rightButtonSelectAllAction), for: .touchUpInside)
        //
        //        let item2 = UIBarButtonItem()
        //        item2.customView = BTNSelectAll
        //
        self.navigationItem.rightBarButtonItems = [item1]
    }
    
    @objc func rightButtonSaveAction()
    {
    }
    
    @objc func rightButtonSelectAllAction()
    {
    }
    
    @objc func rightButtonAction()
    {
    }
    
    func PaintleftSideMenu()
    {
        let btSlider = UIBarButtonItem(image: UIImage.ionicon(with: .androidMenu, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), style: .plain, target: self, action: #selector(clickedOpen))
        btSlider.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = btSlider
    }
    
    @IBAction func clickedOpen(_ sender: Any) {
        // Access an instance of AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
        //elDrawer?.setDrawerState(.opened, animated: true)
        
    }
    
    
    func rightButtonWithIonicon(name: Ionicons){
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 70, height: 20))
        button.contentVerticalAlignment = .bottom
        button.setImage(UIImage.ionicon(with: name, textColor: UIColor.white, size: CGSize(width: 40, height: 30)), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        button.sizeToFit()
        let barItem = UIBarButtonItem(customView: button)
        var items = self.navigationItem.rightBarButtonItems ?? [UIBarButtonItem]();
        items.append(barItem)
        self.navigationItem.rightBarButtonItems = items
    }
    
    func paintCartBtn(){
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 70, height: 20))
        button.contentVerticalAlignment = .bottom
        button.setImage(UIImage.ionicon(with: .androidCart, textColor: UIColor.white, size: CGSize(width: 40, height: 30)), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(cartAction), for: .touchUpInside)
        button.sizeToFit()
        let barItem = UIBarButtonItem(customView: button)
        var items = self.navigationItem.rightBarButtonItems ?? [UIBarButtonItem]();
        items.append(barItem)
        self.navigationItem.rightBarButtonItems = items
    }
    func paintNotificationBellBtn(){
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 70, height: 20))
        button.contentVerticalAlignment = .bottom
        button.setImage(UIImage.ionicon(with: .androidNotificationsNone, textColor: UIColor.white, size: CGSize(width: 40, height: 30)), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(NotificationBell), for: .touchUpInside)
        button.sizeToFit()
        let barItem = UIBarButtonItem(customView: button)
        var items = self.navigationItem.rightBarButtonItems ?? [UIBarButtonItem]();
        items.append(barItem)
        self.navigationItem.rightBarButtonItems = items
    }
    
    @objc func cartAction(){
        guard let controller = ShowCartVC.instance()
        else{
            return
        }
        controller.ComingFromcartBtn = true
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    @objc func NotificationBell(){
        
        guard let controller = NotificationVC.instance()
        else{
            return
        }
        controller.class_type = true
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func paintNavigationLogo(_ imageName :String ){
        
        let image = UIImage(named: imageName)
        let frame = CGRect(x: 0 , y: 0, width: 194, height: 41)
        let imageView = UIImageView(frame: frame)
        //
        imageView.contentMode = . scaleAspectFit
        imageView.image = image
        
        //imageView.addHeightConstraint(height: 41)
        imageView.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 900), for: .horizontal)
        imageView.setContentHuggingPriority(UILayoutPriority(rawValue: 900), for: .horizontal)
        self.navigationItem.titleView = imageView
    }
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func sessionExpired()
    {
        guard let info = SignedUserInfo.sharedInstance else{return}
        info.clear()
        self.popupAlert(title: Bundle.appName(), message: "Session Expired", actionTitles: ["OK"], actions: [{action1 in
            
            RootControllerManager().setRoot()
        }, nil])
    }
    
    
    func paintNavigationTitle(title : String,Color : UIColor? = .white){
        
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.font = UIFont (name: "HelveticaNeue", size: 20)
        label.textAlignment = .center
        label.textColor = Color
        label.text = title.languageSet
        label.sizeToFit()
        self.navigationItem.titleView = label
        
    }
    
    func paintBackButton(_ ImageName : String){
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        let image = UIImage(named: ImageName)
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: UIControl.State.normal)
        button.imageEdgeInsets =  UIEdgeInsets(top: 0.0,left: -30.0, bottom: 0.0,right: 0.0)
        button.addTarget(self, action: #selector(backTapped), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    
    
    
    func removeRightActionButton(){
        self.navigationItem.rightBarButtonItems = nil
    }
    
    
    
    
    @objc func backTapped(){
        let _ = self.navigationController?.popViewController(animated: true)
    }
}

extension InterfaceExtendedController
{
    override func loadView() {
        super.loadView()
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = self.navBackgroundColor ?? BillNAVBAR()
    }
}
