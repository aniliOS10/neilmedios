//
//  Speciality_List.swift
//  MedMobilie
//
//  Created by MAC on 06/04/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import SwiftyJSON

class Speciality_List: InterfaceExtendedController {

    @IBOutlet weak var Mtableview: UITableView!
    
    var Speciality_Value = [JSON]()
    var checked = [String]()
    var selectedJson : [JSON]? = [JSON]()
    var Speciality_STR  = ""
    var delegate : SpecialityProtocol?

    // MARK: - Class life cycle

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.Mtableview.delegate = self
        self.Mtableview.dataSource = self
        self.Mtableview.tableFooterView = UIView()
                
        self.checked = Array(repeating: "", count:
             Speciality_Value[0].count)
        
      //  RightActionButton(Title:languageKey(key:"Save"))
        RightActionButtons()

        self.LanguageSet()
    }
    
    // MARK: - Navigationbar Title Language Setting
    
    @objc override func LanguageSet()
    {
        NavigationBarTitleName(Title: "Speciality")
    }
    
    override func rightButtonSaveAction()
    {
        let filteredItems = checked.filter { $0 != ""}
        if  filteredItems.count != 0
        {
            
            for index in 0..<(filteredItems.count) {
                Speciality_STR += "\(filteredItems[index] as String)\(",")"
                print(Speciality_STR)
            }
            self.navigationController?.popViewController(animated: true)
    
            self.delegate?.Speciality_String(List_Iteam: Speciality_STR)
            print(Speciality_STR)
        }
        else
        {
            self.popupAlert(title: Bundle.appName(), message: self.languageKey(key: "you must select atleast one Speciality"), actionTitles: [self.languageKey(key: "OK")], actions:[{action1 in
                print("ok")
                return
                }, nil])
        }
    }
    
//override func rightButtonSelectAllAction() {
//
//
//    for i in 0..<Speciality_Value[0].count
//    {
//
//         checked[i] = Speciality_Value[0][i]["name"].stringValue
//
//    }
//
//
//    Mtableview.reloadData()
//
//
//    }
    
    // MARK: - Save Speciality List
    
//    override func rightButtonAction(){
//
//        let filteredItems = checked.filter { $0 != ""}
//
//
//        if  filteredItems.count != 0
//        {
//
//            for index in 0..<(filteredItems.count) {
//                Speciality_STR += "\(filteredItems[index] as String)\(",")"
//                print(Speciality_STR)
//            }
//
//            self.navigationController?.popViewController(animated: true)
//
//            self.delegate?.Speciality_String(List_Iteam: Speciality_STR)
//            print(Speciality_STR)
//
//        }
//        else
//        {
//            self.popupAlert(title: Bundle.appName(), message: self.languageKey(key: "you must select atleast one Speciality"), actionTitles: [self.languageKey(key: "OK")], actions:[{action1 in
//                print("ok")
//                return
//                }, nil])
//        }
//
    @IBAction func btnSelectAllList(_ sender: Any)
    {
        for i in 0..<Speciality_Value[0].count
        {
            checked[i] = Speciality_Value[0][i]["name"].stringValue
        }
        Mtableview.reloadData()
    }

}

// MARK: - Table View Delegate/DataSource Methods

extension Speciality_List: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !Speciality_Value.isEmpty
        {
            return Speciality_Value[0].count
        } else {
            return 0
        }
    }

    private func tableView (tableView:UITableView , heightForHeaderInSection section:Int)->Float
    {
        return 20.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
        cell.Title_LBL.font = SystemFont(FontSize: 18)
        cell.Title_LBL.textColor = UIColor.black
        cell.Title_LBL.text = Speciality_Value[0][indexPath.row]["name"].stringValue
        cell.selectionStyle = .none
        //checked.removingDuplicates()
        if checked[indexPath.row] == "" {
            cell.accessoryType = .none
        } else {
            cell.accessoryType = .checkmark
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath)
        {
            if cell.accessoryType == .checkmark
            {
                cell.accessoryType = .none
                checked[indexPath.row] = ""
            } else {
                cell.accessoryType = .checkmark
                
                checked[indexPath.row] = Speciality_Value[0][indexPath.row]["name"].stringValue
            }
        }
    }
}

// MARK: - Class Instance

extension Speciality_List
{
    class func instance()->Speciality_List?{
        let storyboard = UIStoryboard(name: "PlanMyDay", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Speciality_List") as? Speciality_List
        
        return controller
    }
}
