
import UIKit
import MapKit
import IoniconsKit
import GoogleMaps
import GooglePlaces
import SearchTextField
import SwiftyJSON
import DBNumberedSlider


class PlanMyDaySearch: InterfaceExtendedController,CLLocationManagerDelegate {
    
    // variables
    fileprivate var Pickerdatavalue = [JSON]()
    fileprivate var categoryNames = [String]()
    fileprivate var categoryCodes = [String]()
    fileprivate var specialityNames = [String]()
    fileprivate var SearchFromKey = ""
    fileprivate var categoryChoose = ""
    fileprivate var latValue : Double = 0.0
    fileprivate var longValue : Double = 0.0
    fileprivate var keyWordSpeciality : String = ""
    fileprivate var RadiusRangeValue : Int = 25
    fileprivate var isSaveSearch : Bool = false
    fileprivate var isGoogle : Bool = true
    fileprivate var isLocal : Bool = false
    fileprivate var entering : Bool = true
    fileprivate var RecentSearch : [JSON] = [JSON]()
    
    var indexvalu = 0
    var RowValue = 0
    var CheckBool : Bool = false
    
    // fixed labels storyboard outlets
    
    @IBOutlet fileprivate var lblSpeciality : UILabel?
    @IBOutlet fileprivate var lblSaveSearch : UILabel?
    @IBOutlet fileprivate var lblChooseRadius : UILabel?
    @IBOutlet fileprivate var lblCategory : UILabel?
    @IBOutlet weak var buttons: YNSearchMainView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet fileprivate var LocationBtn : UIButton?
    @IBOutlet fileprivate var locationTextField : UITextField!
    @IBOutlet fileprivate var Radius : DBNumberedSlider?
    @IBOutlet fileprivate var searchBtn : UIButton?
    @IBOutlet fileprivate var saveSearchBtn : UIButton?
    @IBOutlet fileprivate var DropDownTextFeild : SearchTextField?
    @IBOutlet fileprivate var tableView : UITableView?
    @IBOutlet  var GoogleSearchBtn : UIButton?
    @IBOutlet  var LocalSearchBtn : UIButton?
    @IBOutlet  var GoogleSearchLBL : UILabel?
    @IBOutlet  var LocalSearchLBL : UILabel?
    
    // google auto complete
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.activityType = .automotiveNavigation
        _locationManager.distanceFilter = 10.0  // Movement threshold for new events
        //  _locationManager.allowsBackgroundLocationUpdates = true // allow in background
        
        return _locationManager
    }()
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // buttons.backgroundColor = .red
        locationTextField.delegate = self
        buttons.delegates = self
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.estimatedRowHeight = 100
        tableView?.separatorStyle = .none
        tableView?.allowsMultipleSelection = false
        
        locationManager.startUpdatingLocation()
        
        LocationBtn?.setImage(UIImage.ionicon(with: .iosLocation, textColor: UIColor.white, size: CGSize(width: 20, height: 20)), for: UIControl.State.normal)
        
        DropDownTextFeild?.maxNumberOfResults = 5
        DropDownTextFeild?.theme.font = UIFont.systemFont(ofSize: 15)
        DropDownTextFeild?.theme.bgColor = .white
        DropDownTextFeild?.theme.borderColor = .gray
        DropDownTextFeild?.theme.borderWidth = 1.0
        DropDownTextFeild?.theme.separatorColor = .lightGray
        DropDownTextFeild?.delegate = self
        DropDownTextFeild?.tag = 999
        //  DropDownTextFeild?.isUserInteractionEnabled = false
        // self.navigationController?.navigationBar.isHidden = true
        
        saveSearchBtn?.setBackgroundImage(UIImage.ionicon(with: .androidCheckboxOutlineBlank, textColor: .gray, size: CGSize(width: 30, height: 30)), for: .normal)
        saveSearchBtn?.backgroundColor = .clear
        
        GoogleSearchBtn?.setBackgroundImage(UIImage.ionicon(with: .androidCheckboxOutline , textColor: .gray, size: CGSize(width: 30, height: 30)), for: .normal)
        GoogleSearchBtn?.backgroundColor = .clear
        
        LocalSearchBtn?.setBackgroundImage(UIImage.ionicon(with: .androidCheckboxOutlineBlank , textColor: .gray, size: CGSize(width: 30, height: 30)), for: .normal)
        LocalSearchBtn?.backgroundColor = .clear
        
        //   createPickerView()
        AppDelegate.showWaitView()
        dataFetch()
        
        NotificationCenter.default.addObserver(self, selector: #selector(Update_PlanMyDaySearch), name: NSNotification.Name( "Update_PlanMyDaySearch"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: PlanNAVBAR(), BtnColor: UIColor.white)
        locationManager.requestAlwaysAuthorization()
        FixedLabelSetup()
        
    }
    // MARK: - Navigationbar Title Language Setting
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Plan My Day Search")
    }
    
    // MARK: - NSNotification For Fetch Planmyday data
    
    @objc func Update_PlanMyDaySearch (notfication: NSNotification)  {
        
        AppDelegate.showWaitView()
        dataFetch()
    }
    // MARK: - Fixed Label Setup
    
    
    fileprivate func FixedLabelSetup()
    {
        lblSpeciality?.text = self.languageKey(key: "Speciality")
        lblSaveSearch?.text = self.languageKey(key: "Save Search")
        searchBtn?.setTitle(self.languageKey(key: "Search"), for: .normal)
        lblChooseRadius?.text = self.languageKey(key: "Choose Radius(in Miles)")
        lblCategory?.text = self.languageKey(key: "Category")
        locationTextField.placeholder = self.languageKey(key: "Enter Palace")
        DropDownTextFeild?.placeholder = self.languageKey(key: "Enter , If Other")
        GoogleSearchLBL?.text = self.languageKey(key: "Google")
        LocalSearchLBL?.text = self.languageKey(key: "Local")
    }
    
    //MARK: -  Action Handler
    
    @IBAction fileprivate func searchBtnAction(_ sender : UIButton)
    {
        if categoryChoose.isEmpty
        {
            self.alert(self.languageKey(key: "Please select a category to continue"))
            return
        }
        
        else if CheckBool == true
        {
            
            if self.DropDownTextFeild?.text == ""
            {
                self.alert(self.languageKey(key: "Please select a Speciality to continue"))
                return
            }
            else
            {
                self.keyWordSpeciality = self.DropDownTextFeild?.text ?? ""
                
                if isGoogle == true && isLocal == true
                {
                    SearchFromKey = "both"
                }
                else if isGoogle == true && isLocal == false
                {
                    SearchFromKey = "google"
                }
                else if isGoogle == false && isLocal == true
                {
                    SearchFromKey = "local"
                }
                
                self.locationFetch(categoryName: self.categoryChoose, keyWord: self.keyWordSpeciality, lattitude: latValue, longtitude: longValue, titleName: locationTextField.text ?? "", RadiusRange: self.RadiusRangeValue, saveSearchBool: self.isSaveSearch,searchfrom: SearchFromKey)
                
            }
        }
        else if locationTextField.text?.isEmpty ?? true
        {
            self.alert(self.languageKey(key: "Please select a location"))
            return
        }
        else
        {
            self.keyWordSpeciality = self.DropDownTextFeild?.text ?? ""
            
            if isGoogle == true && isLocal == true
            {
                SearchFromKey = "both"
            }
            else if isGoogle == true && isLocal == false
            {
                SearchFromKey = "google"
            }
            else if isGoogle == false && isLocal == true
            {
                SearchFromKey = "local"
            }
            
            self.locationFetch(categoryName: self.categoryChoose, keyWord: self.keyWordSpeciality, lattitude: latValue, longtitude: longValue, titleName: locationTextField.text ?? "", RadiusRange: self.RadiusRangeValue, saveSearchBool: self.isSaveSearch,searchfrom: SearchFromKey)
            
        }
    }
    // MARK: - Save or notSave Search Action
    
    @IBAction fileprivate func saveSearch(_ sender : UIButton)
    {
        if sender.isSelected {
            // set deselected
            saveSearchBtn?.setBackgroundImage(UIImage.ionicon(with: .androidCheckboxOutlineBlank , textColor: .gray, size: CGSize(width: 30, height: 30)), for: .normal)
            saveSearchBtn?.backgroundColor = .clear
            sender.isSelected = false
            self.isSaveSearch = false
        } else {
            // set selected
            saveSearchBtn?.setBackgroundImage(UIImage.ionicon(with: .androidCheckboxOutline , textColor: .gray, size: CGSize(width: 30, height: 30)), for: .normal)
            saveSearchBtn?.backgroundColor = .clear
            sender.isSelected = true
            self.isSaveSearch = true
        }
    }
    
    // MARK: - GoogleSearch or Without Google Search
    
    
    @IBAction fileprivate func GoogleSearchAction(_ sender : UIButton)
    {
        if isGoogle == true {
            // set deselected
            
            if isLocal == false
            {
                LocalSearchBtn?.setBackgroundImage(UIImage.ionicon(with: .androidCheckboxOutline , textColor: .gray, size: CGSize(width: 30, height: 30)), for: .normal)
                self.isLocal = true
            }
            
            
            GoogleSearchBtn?.setBackgroundImage(UIImage.ionicon(with: .androidCheckboxOutlineBlank , textColor: .gray, size: CGSize(width: 30, height: 30)), for: .normal)
            
            self.isGoogle = false
        } else {
            // set selected
            // button.backgroundColor = UIColor.red
            GoogleSearchBtn?.setBackgroundImage(UIImage.ionicon(with: .androidCheckboxOutline , textColor: .gray, size: CGSize(width: 30, height: 30)), for: .normal)
            self.isGoogle = true
            //checked[sender.tag] = true
        }
        
        
    }
    
    // MARK: - Local data or Without Local data Search Action
    
    @IBAction fileprivate func LocalSearchAction(_ sender : UIButton)
    {
        if isLocal == true  {
            
            if isGoogle == false
            {
                GoogleSearchBtn?.setBackgroundImage(UIImage.ionicon(with: .androidCheckboxOutline , textColor: .gray, size: CGSize(width: 30, height: 30)), for: .normal)
                self.isGoogle = true
            }
            
            LocalSearchBtn?.setBackgroundImage(UIImage.ionicon(with: .androidCheckboxOutlineBlank , textColor: .gray, size: CGSize(width: 30, height: 30)), for: .normal)
            self.isLocal = false
            
            
        } else {
            // set selected
            // button.backgroundColor = UIColor.red
            LocalSearchBtn?.setBackgroundImage(UIImage.ionicon(with: .androidCheckboxOutline , textColor: .gray, size: CGSize(width: 30, height: 30)), for: .normal)
            self.isLocal = true
        }
    }
    
    // MARK: - Get current Location
    
    @IBAction fileprivate func currentLocationBtn(_ sender : UIButton)
    {
        locationManager.startUpdatingLocation()
        self.locationTextField.text = ""
    }
    
    // MARK: - Radius Slider
    
    @IBAction fileprivate func RadiusSlider(_ sender : UISlider)
    {
        self.Radius?.labelFormat = String(Int(sender.value))
        self.RadiusRangeValue = Int(sender.value)
    }
    // MARK: - Fetch PlanmyDay Lists
    
    fileprivate func dataFetch()
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        DataFetch_Api().Fetch(ID: 0 ,urlString: "/planmydaylists") { (status, message,data) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                // self.mTableview.dg_stopLoading()
                self.alert(message)
                return
            }
            if let tempdata = data
            {
                self.categoryNames = tempdata[0]["category"].map({ (str,strJson) -> String in
                    
                    return strJson["name"].stringValue
                })
                self.categoryCodes = tempdata[0]["category"].map({ (str,strJson) -> String in
                    
                    return strJson["code"].stringValue
                })
                
                let height = self.buttons.initView(categories: self.categoryNames)
                self.viewHeight.constant = height + 20
                
                self.Pickerdatavalue = [tempdata[0]["speciality"]]
                
                print(self.Pickerdatavalue)
                
                self.specialityNames = tempdata[0]["speciality"].map({ (str,strJson) -> String in
                    
                    return strJson["name"].stringValue
                })
                self.DropDownTextFeild?.filterStrings(self.specialityNames)
                self.RecentSearch = tempdata[0]["savedsearches"].arrayValue
                self.tableView?.reloadData()
                self.view.layoutIfNeeded()
                //Radius?.updateLabel()
                // self.Radius?.layoutSubviews()
            }
        }
    }
    
    // MARK: - Fetch PlanmyDay Lists
    
    fileprivate func locationFetch(categoryName : String,keyWord : String ,lattitude: Double,longtitude : Double , titleName : String,RadiusRange : Int ,saveSearchBool : Bool,searchfrom:String)
    {
        
        var param = [String : Any]()
        param["type"] = categoryName.lowercased()
        param["keyword"] = keyWord
        param["location_name"] = titleName
        param["lat"] = lattitude
        param["long"] = longtitude
        let meters = Double(RadiusRange) * 1609.34
        param["radius"] = Int(meters)
        param["savesearch"] = saveSearchBool
        param["searchfrom"] = searchfrom
        
        print(param)
        guard let controller = PlanMyDay_Visit.instance() else
        {
            return
        }
        controller.parama = param
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
}

// MARK: - PickerView Delegate/DataSource Methods

extension PlanMyDaySearch : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return Pickerdatavalue[0].count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Pickerdatavalue[0][row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        RowValue = row
    }
}
// MARK: - TableView For Recent Search

extension PlanMyDaySearch : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let json = RecentSearch[indexPath.row]
        let miles = json["radius"].doubleValue
        
        self.locationFetch(categoryName: json["type"].stringValue, keyWord: json["keyword"].stringValue, lattitude: json["latitude"].doubleValue, longtitude: json["longitude"].doubleValue, titleName: json["location_name"].stringValue, RadiusRange: Int(miles/1609.34), saveSearchBool: false,searchfrom: SearchFromKey)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RecentSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "recentSearch", for: indexPath) as? PlaceListCell else{return PlaceListCell()}
        let json = RecentSearch[indexPath.row]
        cell.selectionStyle = .none
        cell.viewShadow?.bottomViewShadow(ColorName: .gray)
        cell.lblTitleName?.text = json["location_name"].stringValue
        cell.lblQueryName?.text = ""
        
        if json["keyword"].stringValue != ""
        {
            cell.lblQueryName?.text = "Query".languageSet + " : \(json["keyword"].stringValue)"
        }
        
        let miles = json["radius"].doubleValue
        cell.lblRadiusRange?.text = "\(String(Int(miles / 1609.34))) \(self.languageKey(key: "Miles"))"
        cell.lblCategoryName?.text = "Category".languageSet + " : \(json["type"].stringValue)"
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

// MARK: - Category Button Sender Delegates

extension PlanMyDaySearch : CategoryButtonActionProtocol
{
    func CategoryButtonAction(_ sender: UIButton, BtnArray: [UIButton]) {
        let _ = BtnArray.map({$0.isSelected = false
            $0.backgroundColor = .white
            $0.setTitleColor(.lightGray, for: .normal)
            $0.borderColor = .lightGray
        })
        sender.backgroundColor = PlanNAVBAR()
        sender.setTitleColor(.white, for: .normal)
        sender.isSelected = true
        sender.borderColor = PlanNAVBAR()
        
        print(sender)
        let jsons = self.categoryCodes[sender.tag]
        
        print(jsons)
        
        if jsons == "Doctor" || jsons == "Physician"
        {
            CheckBool = true
            
            //  DropDownTextFeild?.isUserInteractionEnabled = false
            
            categoryChoose = jsons
            
            guard let controller = Speciality_List.instance() else
            {
                return
            }
            controller.delegate = self
            controller.Speciality_Value = Pickerdatavalue
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        else
        {
            CheckBool = false
            //   DropDownTextFeild?.isUserInteractionEnabled = true
            //  DropDownTextFeild?.isUserInteractionEnabled = true
            DropDownTextFeild?.text = ""
            categoryChoose = jsons
            
            DropDownTextFeild!.inputView = nil
            DropDownTextFeild!.inputAccessoryView = nil
            self.view .endEditing(true)
        }
        self.view .endEditing(true)
        
    }
}
// MARK: - location Map with search Bar

extension PlanMyDaySearch : UITextFieldDelegate
{
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
        print("Error" + error.description)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last
        
        self.geoAddress(lat: Double(userLocation?.coordinate.latitude ?? 0.0), long:  Double(userLocation?.coordinate.longitude ?? 0.0))
        print("Latitude :- \(userLocation!.coordinate.latitude)")
        print("Longitude :- \(userLocation!.coordinate.longitude)")
        
        locationManager.stopUpdatingLocation()
    }
    
    func PresentGMSVC()
    {
        let fetcherFilter = GMSAutocompleteFilter()
        guard let info = SignedUserInfo.sharedInstance else
        {
            return
        }
        fetcherFilter.country = info.country_code ?? "US"
        //fetcherFilter.country = "AUS"
        
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        acController.autocompleteFilter = fetcherFilter
        
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.isTranslucent = false
        UINavigationBar.appearance().barTintColor = UIColor(red: 44.0/255, green: 44.0/255, blue: 49.0/255, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        
        locationManager.stopUpdatingLocation()
        // This makes the view area include the nav bar even though it is opaque.
        // Adjust the view placement down.
        self.extendedLayoutIncludesOpaqueBars = true
        self.edgesForExtendedLayout = .top
        self.navigationController?.pushViewController(acController, animated: true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag != 999
        {
            self.PresentGMSVC()
            
            //            self.present(acController, animated: true, completion: nil)
            
            // self.navigationController?.navigationBar.isHidden = true
            return false
        }
        else
        {
            if CheckBool == false
            {
                return true
            }
            else
            {
                
                let controller = Speciality_List.instance()
                controller!.delegate = self
                controller!.Speciality_Value = Pickerdatavalue
                self.navigationController?.pushViewController(controller!, animated: true)
                return false
            }
            
        }
        return true
    }
    
    fileprivate func AddressWithLongitude(_ lon:Double, andLatitude lat:Double, andTitle title: String)
    {
        self.longValue = lon
        self.latValue = lat
        
        DispatchQueue.main.async { () -> Void in
            self.locationTextField.text = title
            
        }
    }
    
    func geoAddress(lat : Double , long : Double)
    {
        let geocoder = GMSGeocoder()
        
        let coordinate = CLLocationCoordinate2DMake(lat, long)
        
        print(coordinate)
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                guard let lines = address.lines as? [String] else{return}
                //     print(lines)
                currentAddress = lines.joined(separator: ",")
                print(currentAddress)
                self.AddressWithLongitude(long, andLatitude: lat, andTitle: currentAddress)
                //currentAddress = lines.joinWithSeparator("\n")
                
                //currentAdd(returnAddress: currentAddress)
            }
        }
    }
}

// MARK: - Autocomplete Methods

extension PlanMyDaySearch: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        self.longValue = place.coordinate.longitude
        self.latValue = place.coordinate.latitude
        DispatchQueue.main.async {
            self.locationTextField.text = place.name
        }
        dismissAutoComplete(picker: viewController)
        // Dismiss the GMSAutocompleteViewController when something is selected
        
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismissAutoComplete(picker: viewController)
    }
    
    private func dismissAutoComplete(picker : GMSAutocompleteViewController){
        picker.navigationController?.popViewController(animated: true)
        
        self.tabBarController?.tabBar.isHidden = false
        UIApplication.shared.isStatusBarHidden = false
    }
    
}
// MARK: - Class Instance

extension PlanMyDaySearch
{
    class func instance()->PlanMyDaySearch?{
        
        let storyboard = UIStoryboard(name: "PlanMyDay", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "PlanMyDaySearch") as? PlanMyDaySearch
        
        return controller
    }
}

extension PlanMyDaySearch : SpecialityProtocol
{
    func Speciality_String(List_Iteam: String, ProductArray: [Int]) {
        
    }
    
    func Speciality_String(List_Iteam : String) {
        
        DropDownTextFeild?.text = List_Iteam
        
        print("List_Iteam :---> ",List_Iteam)
    }
    
}
