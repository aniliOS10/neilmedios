//
//  YNSearchMainView.swift
//  YNSearch
//
//  Created by YiSeungyoun on 2017. 4. 16..
//  Copyright © 2017년 SeungyounYi. All rights reserved.
//

import UIKit

open class YNSearchMainView: UIView {
//    let width = UIScreen.main.bounds.width
//    let height = UIScreen.main.bounds.height

    open var categoryLabel: UILabel!
    open var ynCategoryButtons = [YNCategoryButton]()
   fileprivate var checked = [Bool]()
    var delegates : CategoryButtonActionProtocol?
    open var searchHistoryLabel: UILabel!
   // open var ynSearchHistoryViews = [YNSearchHistoryView]()
   // open var ynSearchHistoryButtons = [YNSearchHistoryButton]()
    open var clearHistoryButton: UIButton!

    
    var margin: CGFloat = 10
   // open var delegate: YNSearchMainViewDelegate?
    
   // open var ynSearch = YNSearch()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        
       // guard let categories = YNSearch.shared.getCategories() else { return }
       // self.initView(categories: ["menu","fgdfbb","fgbgb"])
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
//    open func setYNCategoryButtonType(type: YNCategoryButtonType) {
//        for ynCategoryButton in self.ynCategoryButtons {
//            ynCategoryButton.type = type
//        }
//    }
    
    @objc open func ynCategoryButtonClicked(_ sender: UIButton) {
      self.delegates?.CategoryButtonAction(sender, BtnArray: ynCategoryButtons)
       /* if let button = sender as? UIButton {
            if button.isSelected {
                // set deselected
                button.backgroundColor = UIColor.white
                button.isSelected = false
                checked[sender.tag] = false
            } else {
                // set selected
                button.backgroundColor = UIColor.red
                button.isSelected = true
                checked[sender.tag] = true
            }
            print(checked)
            
        }*/
        
    }
    
    @objc open func ynSearchHistoryButtonClicked(_ sender: UIButton) {
       
    }
    
    @objc open func clearHistoryButtonClicked() {
      
        
    }
    
    @objc open func closeButtonClicked(_ sender: UIButton) {
        
            }
    
    open func initView(categories: [String]) -> CGFloat {
        checked = Array(repeating: false, count: categories.count)
//        self.categoryLabel = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 40, height: 50))
//        self.categoryLabel.text = "Categories"
//        self.categoryLabel.font = UIFont.systemFont(ofSize: 13)
//        self.categoryLabel.textColor = .lightGray
//        self.addSubview(self.categoryLabel)
        
        let font = UIFont.systemFont(ofSize: 15)
        let userAttributes =  [NSAttributedString.Key.font : font, NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        
        var formerWidth: CGFloat = margin
        var formerHeight: CGFloat = 0
        
        for i in 0..<categories.count {
            let size = categories[i].size(withAttributes: userAttributes)
            if i > 0 {
                formerWidth = ynCategoryButtons[i-1].frame.size.width + ynCategoryButtons[i-1].frame.origin.x + 10
                if formerWidth + size.width + margin + 20 > UIScreen.main.bounds.width  {
                    formerHeight += ynCategoryButtons[i-1].frame.size.height + 10
                    formerWidth = margin
                }
            }
            let button = YNCategoryButton(frame: CGRect(x: formerWidth, y: formerHeight, width: size.width + 10, height: size.height + 10))
            button.addTarget(self, action: #selector(ynCategoryButtonClicked(_:)), for: .touchUpInside)
            button.setTitle(categories[i], for: .normal)
            
            button.tag = i
            button.cornerRadiusForView(Value : 12)
            button.borderColor = .lightGray
            button.setTitleColor(.lightGray, for: .normal)
            ynCategoryButtons.append(button)
            self.addSubview(button)
            
        }
        
       return formerHeight + 10
    }

    
}
