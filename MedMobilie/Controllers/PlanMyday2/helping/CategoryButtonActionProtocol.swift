//
//  CategoryButtonActionProtocol.swift
//  DemoSwiftySignature
//
//  Created by dr.mac on 11/02/19.
//  Copyright © 2019 Walzy. All rights reserved.
//

import Foundation
import UIKit

protocol CategoryButtonActionProtocol {
    func CategoryButtonAction(_ sender : UIButton ,BtnArray : [UIButton])
}
