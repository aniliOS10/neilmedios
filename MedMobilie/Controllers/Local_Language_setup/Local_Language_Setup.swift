
import UIKit
import MMDrawerController
import CoreLocation
import DGElasticPullToRefresh
import SwiftyJSON
class Local_Language_Setup: InterfaceExtendedController {
    fileprivate var currencyName : String?
    fileprivate var dataList = [JSON]()
    fileprivate var locationBool = false
    
    let   locationManager:CLLocationManager = CLLocationManager()
    
    var Language_String : String?
    var arrSettingMenuList = NSArray()
    var arrLanguageList = NSArray()
    var arrSctionTitle = NSArray()
    var arrNotificationTitle = NSArray()
    var  cell = UITableViewCell()
    var btSlider = UIBarButtonItem()
   
    @IBOutlet weak var Mtableview: UITableView!

    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        LocalizationSystem.SharedInstance.SetLanguage(languageCode: "en")
        
        self.Mtableview.delegate = self
        self.Mtableview.dataSource = self
        self.Mtableview.tableFooterView = UIView()
        CurrencyListData()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        btSlider = UIBarButtonItem(image: UIImage.ionicon(with: .androidArrowForward, textColor: UIColor.red, size: CGSize(width: 60, height: 30)), style: .plain, target: self, action: #selector(OpenSlider))
        btSlider.tintColor = UIColor.white
        btSlider.isEnabled = false
        
        
        navigationItem.rightBarButtonItem = btSlider
        
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        
        
    }
    
    
    // MARK: - Language Setting
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Get Started")
        
        
        arrSctionTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Location Settings", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Language", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Currency Settings", comment: "")]
        
        arrSettingMenuList = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Change password", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Edit Profile", comment: "")]
        
        
        arrNotificationTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Show notification", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Sound", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Show preview", comment: "")]
        
        
        self.ShowLanguage()
    }
    
    // MARK: - OpenSlider
    
    @objc func OpenSlider(){
        let ud = UserDefaults.standard
        if ud.value(forKey: "NewUser") == nil
        {
            ud.set(true, forKey: "NewUser")
            if ud.value(forKey: "currency_code") == nil
            {
                ud.set(1, forKey: "currency_code")
            }
            if ud.value(forKey: "language") == nil
            {
                ud.set("en", forKey: "language")
            }
            ud.synchronize()
        }
        
        RootControllerManager().setRoot()
        
    }
    
    // MARK: - Currency ListData

    
    fileprivate func CurrencyListData()
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            self.Mtableview.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        CurrencyList_Api().Request { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.Mtableview.dg_stopLoading()
                self.alert(message)
                return
            }
            guard let tempdata = data else
            {
                return
            }
            self.dataList = tempdata
            self.currencyName = "\(self.dataList[0]["symbol"].stringValue) \(self.dataList[0]["code"].stringValue)"
            self.Mtableview.reloadData()
            self.Mtableview.dg_stopLoading()
            
        }
    }
    
    
    
    // MARK: - ShowLanguage

    func ShowLanguage()
    {
        
        
        if LocalizationSystem.SharedInstance.getLanguage() == "en"
        {
            Language_String = LocalizationSystem.SharedInstance.localizedStingforKey(key: "English", comment: "")
        }
        else if LocalizationSystem.SharedInstance.getLanguage() == "ja"
        {
            Language_String = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Japanese", comment: "")
        }
        else if LocalizationSystem.SharedInstance.getLanguage() == "es"
        {
            Language_String = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Spanish", comment: "")
        }
        
        Mtableview.reloadData()
        
    }
    
    
    // MARK: - ActionSheet

  fileprivate  func ActionSheet(tag:Int)
    { let ud = UserDefaults.standard
        if tag == 1
        {
           
            self.popupAlertWithSheet(title: Bundle.appName(), message: languageKey(key: "Select Language"), actionTitles: [languageKey(key: "English"),languageKey(key: "Japanese"),languageKey(key: "Spanish")], actions:[{action1 in
                ud.set("en", forKey: "language")
                LocalizationSystem.SharedInstance.SetLanguage(languageCode: "en")
                },{action2 in
                     ud.set("ja", forKey: "language")
                    LocalizationSystem.SharedInstance.SetLanguage(languageCode: "ja")
                },{action3 in
                     ud.set("es", forKey: "language")
                    LocalizationSystem.SharedInstance.SetLanguage(languageCode: "es")
                }, nil])
           
        }
        else if tag == 2
        {
            if self.dataList.count > 0
            {
            let alertController = UIAlertController(title: Bundle.appName(), message: self.languageKey(key: "Select Currency"), preferredStyle: .actionSheet)
        
            for data in 0..<self.dataList.count
            {
                let currency_name = self.dataList[data]["name"].stringValue
                let action = UIAlertAction(title: currency_name, style: .default, handler: { action in
                    let index = IndexPath(row: 0, section: data)
                    self.currencyName = "\(self.dataList[index.section]["symbol"].stringValue) \(self.dataList[index.section]["code"].stringValue)"
                    
                    ud.set(self.dataList[index.section]["id"].intValue, forKey: "currency_code")
                    
                    self.Mtableview.reloadData()
                    
                    
                })
                alertController.addAction(action)
            }
            let cancelAction = UIAlertAction(title: self.languageKey(key: "Dismiss"), style: .cancel, handler: { action in
            })
            alertController.addAction(cancelAction)
                
                
                alertController.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
                
                self.present(alertController, animated: true) {
                    print("option menu presented")
                }
                
                
                
          ///  present(alertController, animated: true)
            

            }
             ud.synchronize()
        }
        
       
        }
    
    
    // MARK: - Switch button Status

    
    @objc func SwitchStatus (notfication: NSNotification)  {
        
        print(notfication.object!)
        
    }
    
    
    // MARK: - switch Value DidChange

    @objc func switchValueDidChange(_ sender: UISwitch){
        
        NotificationCenter.default.addObserver(self, selector: #selector(SwitchStatus), name: NSNotification.Name( "SwitchStatus"), object: nil)
        
        if sender.isOn {
            
            if LocationOn() == true
            {
                
                sender.isOn = true
                self.locationBool = true
                
            }
            else
            {
                sender.isOn = false
                self.locationBool = false
            }
            
        } else {
            self.locationBool = false
            btSlider.isEnabled = false
            
            
        }
        
    }
    
    
    
    // MARK: -  LocationOn button

    func LocationOn()-> Bool  {
        
        
        locationManager.delegate = self;
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        return LocationStatus(Status: false)
        
    }
    
    
    
    // MARK: -  Location Status Delegate

    func LocationStatus(Status:Bool) -> Bool {
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .denied:
                
                
                
                
                
                let alertController = UIAlertController(title: languageKey(key: "Location Services Disabled"), message: languageKey(key: "To re-enable, please go to Settings and turn on Location Service for this app"), preferredStyle: .alert)
                let OKAction = UIAlertAction(title: languageKey(key: "OK"), style: .default,
                                             handler: nil)
                alertController.addAction(OKAction)
                OperationQueue.main.addOperation {
                    self.present(alertController, animated: true,
                                 completion:nil)
                    
                    
                }
                
                btSlider.isEnabled = false
                
            case .notDetermined:
                
                btSlider.isEnabled = true
                
                
                
            case .restricted:
                btSlider.isEnabled = true
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                btSlider.isEnabled = true
            }
        } else {
            btSlider.isEnabled = false
            print("Location services are not enabled")
        }
        
        return btSlider.isEnabled
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print(status)
        
        switch status {
        case .notDetermined:
            
            manager.startUpdatingLocation()
            break
            
        case .denied:
            
            let alertController = UIAlertController(title: "Location Services Disabled", message: "To re-enable, please go to Settings and turn on Location Service for this app.", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default,
                                         handler: nil)
            alertController.addAction(OKAction)
            OperationQueue.main.addOperation {
                self.present(alertController, animated: true,
                             completion:nil)
            }
            self.Mtableview.reloadData()
            btSlider.isEnabled = false
            
            
            manager.stopUpdatingLocation()
            
            break
            
        case .authorizedWhenInUse:
            // clear text
            
            
            btSlider.isEnabled = true
            manager.startUpdatingLocation() //Will update location immediately
            break
            
        case .authorizedAlways:
            // clear text
            
            btSlider.isEnabled = true
            manager.startUpdatingLocation() //Will update location immediately
            break
        default:
            break
        }
        
        
        
        
    }
    
    
    
    
    
}


// MARK: -  Location Status Delegate


extension Local_Language_Setup :CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
}


// MARK: - Table View Delegate/DataSource Methods

extension Local_Language_Setup: UITableViewDelegate,UITableViewDataSource
{
    
    // MARK: - Table view data source
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrSctionTitle.count
    }
    
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
        
        
        return arrSctionTitle[section] as? String
    }
    
    private func tableView (tableView:UITableView , heightForHeaderInSection section:Int)->Float
    {
        return 20.0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0 {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
            
            cell.lblTitle.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Location Enable", comment: "")
            cell.lblTitle.font = SystemFont(FontSize: 18)
            cell.lblTitle.textColor = ColorValue(Rvalue:61, Gvalue: 140, Bvalue: 250, alphavalue: 0.9)
            
            cell.swSwitch.isOn = self.locationBool
            cell.swSwitch.onTintColor = SettingNAVBAR()
            
            
            cell.swSwitch.addTarget(self, action: #selector(switchValueDidChange), for: .valueChanged)
            
            
            
            cell.selectionStyle = .none
            return cell
            
            
            
        }
            
            
        else  if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
            
            cell.Title_LBL.font = SystemFont(FontSize: 18)
            cell.Title_LBL.textColor = ColorValue(Rvalue:61, Gvalue: 140, Bvalue: 250, alphavalue: 0.9)
            
            cell.SubTitle_LBL.text = Language_String
            cell.SubTitle_LBL.textColor = UIColor.black
            cell.Title_LBL.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Language", comment: "")
            cell.selectionStyle = .none
            cell.accessoryType = .disclosureIndicator
            return cell
            
        }
            
        else  if indexPath.section == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
            
            cell.Title_LBL.font = SystemFont(FontSize: 18)
            cell.Title_LBL.textColor = ColorValue(Rvalue:61, Gvalue: 140, Bvalue: 250, alphavalue: 0.9)
            if dataList.count > 0
            {
           cell.SubTitle_LBL.text = self.currencyName
            } else
            {
                cell.SubTitle_LBL.text = ""
            }
            cell.SubTitle_LBL.textColor = UIColor.black
            cell.Title_LBL.text = languageKey(key: "Currency")
            cell.selectionStyle = .none
            cell.accessoryType = .disclosureIndicator
            return cell
            
        }
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if indexPath.section == 0 {
            
            
            
        }
        else if indexPath.section == 1
        {
            
            self.ActionSheet(tag: indexPath.section)
            
        }else if indexPath.section == 2
        {
            
            self.ActionSheet(tag: indexPath.section)
            
        }
        
        
        
        
    }
}




// MARK: - Local_Language_Setup

extension Local_Language_Setup
{
    override func loadView() {
        super.loadView()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = self.navBackgroundColor ?? BillNAVBAR()
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = SettingNAVBAR()
        Mtableview.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.CurrencyListData()
            
            }, loadingView: loadingView)
        Mtableview.dg_setPullToRefreshFillColor(navBackgroundColor ?? WhiteColor())
        Mtableview.dg_setPullToRefreshBackgroundColor(Mtableview.backgroundColor!)
    }
}


// MARK: - Class instance

extension Local_Language_Setup
{
    class func instance()->Local_Language_Setup?{
        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Local_Language_Setup") as? Local_Language_Setup
        
        
        return controller
    }
}
