//
//  LoginViewController.swift
//  MedMobilie
//
//  Created by dr.mac on 22/11/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit
import IoniconsKit
import LocalAuthentication


class LoginViewController: InterfaceExtendedController {
    
    @IBOutlet weak var viewShadow: UIView!
    
    @IBOutlet weak var User_Name_LBL: UILabel!
    @IBOutlet weak var Password_LBL: UILabel!
    @IBOutlet weak var ImgEmailValidationCheck : UIImageView!
    
    //@IBOutlet weak var Remember_me_BTN: UIButton!
    @IBOutlet fileprivate var txtFieldEmail : UITextField!
    @IBOutlet fileprivate var txtFieldPassword : UITextField!
    @IBOutlet weak var Forgot_Password_BTN: UIButton!
    @IBOutlet weak var Signin_BTN: UIButton!
    
    var eyeClick = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewShadow.dropShadow(color: .gray, cornerRadius: 10, opacity: 0.7, offSet: CGSize.init(width: 0.0, height: 0.0), radius: 5)
        self.txtFieldEmail.placeholder = "abc@company.com".languageSet
        //  viewShadow.bottomViewShadow()
        checkPrevious()
        addEye()
        self.txtFieldEmail.text = "madhwa.s@neilmed.com"
        self.txtFieldPassword.text = "Neilmed1"
    }
    
    func addEye()
    {
        let button1 = UIButton(type: .custom)
        button1.setImage(UIImage(named: "EyeHide"), for: .normal)
        button1.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        button1.frame = CGRect(x: CGFloat(txtFieldPassword.frame.size.width - 35), y: CGFloat(5), width: CGFloat(35), height: CGFloat(35))
        button1.addTarget(self, action: #selector(self.reveal1), for: .touchUpInside)
        txtFieldPassword.rightView = button1
        txtFieldPassword.rightViewMode = .always
    }
    
    @IBAction func reveal1(_ sender: UIButton) {
        if(eyeClick == true) {
            sender.setImage(UIImage(named: "Eye"), for: .normal)
            txtFieldPassword.isSecureTextEntry = false
        } else {
            sender.setImage(UIImage(named: "EyeHide"), for: .normal)
            txtFieldPassword.isSecureTextEntry = true
        }
        eyeClick = !eyeClick
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // self.txtFieldEmail.placeholder = "abc@company.com".languageSet
    }
    
    fileprivate func checkPrevious()
    {
        if LAContext().biometricType.rawValue == "faceID"
        {
            
            let userDefault = UserDefaults.standard
            if userDefault.value(forKey:"FaceIdEnable") as? Bool == true
            {
                if userDefault.value(forKey: "LoginEmailId") != nil && userDefault.value(forKey: "LoginPassword") != nil
                {
                    RootControllerManager().authenticationWithTouchID()
                }
                else
                {
                    return
                }
            }
        }
    }
    
    @objc override func LanguageSet(){
        
        //        let range1 = ("Email ID" as NSString).range(of: "*")
        //        let attributedUser_Name = NSMutableAttributedString(string:"Email ID")
        //        attributedUser_Name.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range1)
        
        
        let range = (LocalizationSystem.SharedInstance.localizedStingforKey(key: "Email ID *", comment: "") as NSString).range(of: "*")
        let attributeduserName = NSMutableAttributedString(string:LocalizationSystem.SharedInstance.localizedStingforKey(key: "Email ID *", comment: ""))
        attributeduserName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
        
        User_Name_LBL.attributedText = attributeduserName
        
    
        let range2 = (LocalizationSystem.SharedInstance.localizedStingforKey(key: "Password *", comment: "") as NSString).range(of: "*")
        let attributedPassword = NSMutableAttributedString(string:LocalizationSystem.SharedInstance.localizedStingforKey(key: "Password *", comment: ""))
        attributedPassword.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range2)
        
        Password_LBL.attributedText = attributedPassword
        
        // Password_LBL.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Password*", comment: "")
        
        //Remember_me_BTN.setTitle(NSLocalizedString(LocalizationSystem.SharedInstance.localizedStingforKey(key: "Remember me", comment: ""), comment: "comment"), for: .normal)
        
        Forgot_Password_BTN.setTitle(LocalizationSystem.SharedInstance.localizedStingforKey(key: "Forgot Password?", comment: ""), for: .normal)
        Signin_BTN.setTitle(LocalizationSystem.SharedInstance.localizedStingforKey(key: "Sign In", comment: ""), for: .normal)
        
    }
    
    // MARK: - Button Action
    
    @IBAction func btnActForLogin(_ sender: UIButton) {
        
        guard let psWrd = txtFieldPassword.text,let email = txtFieldEmail.text else{return}
        if !txtFieldEmail.EmailValidation()
        {
            self.alert("Please enter your correct Email ID")
            return
        }
        else if psWrd.containsWhitespace
        {
            txtFieldPassword.text = ""
            self.alert("Your Password can't contains Blank Space!")
            return
        }
        else if psWrd.count < 5
        {
            txtFieldPassword.text = ""
            self.alert("The Password must be at least 5 Characters long.")
            return
        }
        
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        LoginProcessor().Request(email: email, password: psWrd) { (status,message, info) in
            AppDelegate.hideWaitView()
            if !status{
                self.alert(message)
                return
            }
            let userDefault = UserDefaults.standard
            
            if userDefault.value(forKey: "LoginEmailId") as? String != email
            {
                //Anil
                //  RootOfflineDataFetch().contactFetch()
                //  AppDelegate.hideWaitView() 
                
            }
            
            DataBaseHelper.ShareInstance.DeleteContext(ClassName: OfflineDataClass.Contact.rawValue)
            // Anil
            UserDefaults.standard.set(true, forKey: "contactData_Fetch")
            UserDefaults.standard.set(1, forKey: "totalContact_OffSet")
            UserDefaults.standard.set(0, forKey: "contact_OffSet")

            userDefault.set(email, forKey: "LoginEmailId")
            userDefault.set(psWrd, forKey: "LoginPassword")
            userDefault.synchronize()
            RootControllerManager().setRoot()
                        
            //            let objAppDelegate = UIApplication.shared.delegate as? AppDelegate
            //            objAppDelegate?.buildNavigationDrawer()
        }
        
        // self.performSegue(withIdentifier: segueID.kyDrawerIdentifierSegue, sender: nil)
    }
    
    @IBAction func btnActForForgetPasswordVC(_ sender: UIButton) {
        showInputDialog(title: Bundle.appName(),
                        subtitle: "Forgot Password".languageSet,
                        actionTitle: "Submit".languageSet,
                        cancelTitle: "Cancel".languageSet,
                        inputPlaceholder: "Enter Your email ID".languageSet,
                        inputKeyboardType: .default)
        { (input:String?) in
            guard let emailId = input else{return}
            if !emailId.EmailValidation()
            {
                self.alert("enter a valid Email Id")
                return
            }
            AppDelegate.showWaitView()
            if !NetworkState.isConnected() {
                AppDelegate.hideWaitView()
                AppDelegate.alertViewForInterNet()
                return
            }
            ForgotPasswordProcess().Request(email: emailId , completion: { (status, message, otp) in
                AppDelegate.hideWaitView()
                if !status
                {
                    self.alert(message ?? "something Wrong")
                    return
                }
                
                let ResetViewController = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                ResetViewController.emailID = emailId
                
                self.navigationController?.pushViewController(ResetViewController, animated: true)
                
            })
        }
        //self.performSegue(withIdentifier: segueID.forgotPasswordIdentifier, sender: nil)
    }
    
    @IBAction func btnActForRemeberme(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
        
    }
    
}
/*extension LoginViewController : UITextFieldDelegate
 {
 func textFieldShouldReturn(_ textField: UITextField) -> Bool {
 
 if textField == txtFieldEmail
 {
 if !txtFieldEmail.EmailValidation()
 {
 ImgEmailValidationCheck.image = UIImage.ionicon(with: .iosCloseOutline, textColor: .red, size: CGSize(width: 20.0, height: 20.0))
 ImgEmailValidationCheck.isHidden = false
 return false
 }
 ImgEmailValidationCheck.image = UIImage.ionicon(with:.iosCheckmarkOutline, textColor: .green, size: CGSize(width: 20.0, height: 20.0))
 ImgEmailValidationCheck.isHidden = false
 
 textField.resignFirstResponder()
 return true
 }
 return true
 }
 
 }*/
extension LoginViewController
{
    class func instance()->LoginViewController?{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
        
        
        return controller
    }
}

