

import UIKit
import IoniconsKit
import DGElasticPullToRefresh
import SwiftyJSON
import SwipeCellKit

class SelectedVisit: InterfaceExtendedController {
    fileprivate var JsonData : [JSON]?

    var Titlename : String = ""
    var contactId : Int = 0
    var comeFromContact : Bool = false
    var checked_values = [Int]()
    var CheckArray = NSMutableArray ()
    var checked = [Int]()
    var ContactArray = [String : Any]()
    var delegate : SelectedContactProtocol?
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
      
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = false
        AppDelegate.showWaitView()
        TaskListDataFetch()
        
        NotificationCenter.default.addObserver(self, selector: #selector(TaskListDataFetch), name: NSNotification.Name( "UpdateVisitList"), object: nil)
        
        
        checked_values = CheckArray.value(forKey: "id") as! [Int]
        
        print(checked_values)
        
        RightActionButton(Title:languageKey(key:"Add"))
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       
            PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: TaskNAVBAR(), BtnColor: UIColor.white)
        
    }
    
    // MARK: - Language Setting
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: Titlename)
    }
    
    // MARK: - Save Button Action

    override func rightButtonAction(){
        
        print(checked)
        
        
        // checked.
        for index in 0..<(checked.count) {
            
            print(checked[index])
            print(checked_values)
            
            
            let filteredItems = self.checked_values.filter { $0 == checked[index] }

            
            print(filteredItems.count)
            
            if filteredItems.count == 0 && checked[index] != 90000
            {
                let infos = JsonData?[index]
                
                        
                ContactArray = ["id":infos!["id"].intValue,"taskvisit_id":0,"is_express":infos!["is_express"].stringValue,"visit_date":infos!["visit_date"].stringValue,"visit_type":infos!["visit_type"].stringValue,"visited_count":infos!["visited_count"].stringValue,"pending_count":infos!["pending_count"].stringValue,"rescheduled_count":infos!["rescheduled_count"].stringValue,"title":infos!["title"].stringValue]
                CheckArray.add(ContactArray)
                
                print(CheckArray)
                print(CheckArray.count)
            }
        }
        
        print(CheckArray)
        print(CheckArray.count)
        self.navigationController?.popViewController(animated: true)
        
        
       self.delegate?.selected(selected_array: CheckArray)
        
        
    }
    
    // MARK: - Task Data Fetch

    @objc fileprivate func TaskListDataFetch()
    {
        if !NetworkState.isConnected() {
            self.tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        let DataValue = [String : Any]()
       
        VisitList_Api().fetch(param: DataValue) { (status, message, data) in
            
            
            AppDelegate.hideWaitView()
            
            if !status
            {
                self.tableView.dg_stopLoading()
                self.alert(message )
                return
            }
            if data != nil
            {
                
                

                
                self.JsonData = data ?? nil
                
                self.checked = Array(repeating: 90000, count: self.JsonData?.count ?? 0)
                
                for index in 0..<(self.JsonData?.count)! {
                    
                    let json = self.JsonData?[index]
                    
                    let idvalue = json!["id"]
                    
                    let filteredItems = self.checked_values.filter { $0 == idvalue.int }
                    
                    
                    
                    
                    
                    if filteredItems.count != 0
                    {
                        self.checked[index] = idvalue.int!
                    }
                    
                    
                }

                
                self.tableView.dg_stopLoading()
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Visit Delete API

    fileprivate func visitDelete(Contactid:Int)
    {
        
        if !NetworkState.isConnected() {
            tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        ContactDelete_Api().Request(idvalue: Contactid ,url_string: "/visitdelete" ) { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
            }
        }
        
    }
    
    // MARK: - Create New Visit API

    @IBAction func createNewVisit(_ sender: Any) {
        
        
        self.popupAlertWithSheet(title: Bundle.appName(), message: nil, actionTitles: [ self.languageKey(key: "Create Visit"),self.languageKey(key: "Express Visit")], actions:[{action1 in
            
         //   NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 0)
            
            guard let controller = CreateVisit.instance()
                else{return}
            
            
            self.navigationController?.pushViewController(controller, animated: true)
            
            
            },{action2 in
                
       //         NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 0)
                
                guard let controller = ExpressVisit.instance()
                    else{return}
                
                self.navigationController?.pushViewController(controller, animated: true)
                
            }, nil])
        
    }
    deinit {
        tableView.dg_removePullToRefresh()
    }
    
}


// MARK: - TableView Delegate/DataSource Methods

extension SelectedVisit : UITableViewDelegate , UITableViewDataSource , SwipeTableViewCellDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return JsonData?.count ?? 0
        
    }
   
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let custrom = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        custrom.backgroundColor = .white
        let lbl = UILabel()
        // lbl.center = custrom.center
        lbl.text = "Past History"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.lineBreakMode = .byWordWrapping
        lbl.textAlignment = .center
        lbl.textColor = SettingNAVBAR()
        
        custrom.addSubview(lbl)
        lbl.widthAnchor.constraint(equalTo: custrom.widthAnchor).isActive = true
        lbl.centerXAnchor.constraint(equalTo: custrom.centerXAnchor).isActive = true
        lbl.centerYAnchor.constraint(equalTo: custrom.centerYAnchor).isActive = true
        return custrom
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if !comeFromContact
        {
            return 0.0
        }
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? VisitListingCell
            else{return VisitListingCell()}
        
        guard let json = JsonData?[indexPath.row]
            else{return cell}
        
        
        if json["is_express"].boolValue
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.red)
        }
        else
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.green)
        }
        
        
       
        cell.lblTask?.text = json["title"].stringValue
        cell.lbldate?.text = json["visit_date"].stringValue
        //cell.lblToTime?.text = json["visit_totime"].stringValue
        //cell.lblFromTime?.text = json["visit_fromtime"].stringValue
        cell.lblVisitType?.text = json["visit_type"].stringValue
        cell.lblVisitedCount?.text = String(json["visited_count"].intValue)
        cell.lblPendingCount?.text = String(json["pending_count"].intValue)
        cell.lblRescheduleCount?.text = String(json["rescheduled_count"].intValue)
        cell.selectionStyle = .none
        if !comeFromContact
        {
            cell.delegate = self
        }
        
        //cell.textLabel?.text = numbers[indexPath.row]
        
        
        
        if checked[indexPath.row] == 90000 {
            
            cell.accessoryType = .none
            
        } else {
            
            cell.accessoryType = .checkmark
            
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if !comeFromContact
        {
            return true
        }
        return false
    }
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        
        let deleteAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            guard let list = self.JsonData?[indexPath.row] else{return }
            
            if !list["is_editable"].boolValue
            {
                self.alert(self.languageKey(key: "You are not authorized to Delete this contact"))
                // self.DeleteContacts(Contactid: list["id"].intValue)
            }
            else
            {
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: "Are you sure you want to delete this Visit?"), actionTitles: [self.languageKey(key: "NO"),self.languageKey(key: "YES")], actions: [{action1 in
                    return
                    },{action2 in
                        
                        self.JsonData?.remove(at: indexPath.row)
                        self.tableView.reloadData()
                        DispatchQueue.global(qos: .background).async {
                            DispatchQueue.main.async {
                                self.visitDelete(Contactid: list["id"].intValue)
                            }
                            
                            
                        }
                        
                    }, nil])
                
                
            }
            
        }
        
        
        // handle action by updating model with deletion
        
        // customize the action appearance
        
        let editAction = SwipeAction(style:.destructive, title:nil) { action, indexPath in
            
            
            guard let json = self.JsonData?[indexPath.row]
                else{return}
            guard let controller = EditVisitVC.instance()
                else{return}
            controller.visitId = json["id"].intValue
            
            controller.BackDataBlock = {infos in
                print(infos)
                self.addANDEditVisit(link: infos,newFlag: false)
            }
            
            self.navigationController?.pushViewController(controller, animated: true)
            
            
            
        }
        
        // customize the action appearance
        
        deleteAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor: .white, size: CGSize(width: 32, height: 32))
        editAction.backgroundColor = UIColor.lightGray
        editAction.image = UIImage.ionicon(with: .edit, textColor: .white, size: CGSize(width: 32, height: 32))
        
        
        return [deleteAction,editAction]
        
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        guard let infos = JsonData?[indexPath.row]  else{return }
        
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark {
                
                let json = self.JsonData?[indexPath.row]
                
                let idvalue = json!["id"]
                
                print(idvalue)
                
                
                print(checked_values)
     
                
                if let index = checked_values.index(where: {$0 == idvalue.intValue}) {
                    checked_values.remove(at: index)
                    
                    CheckArray.removeObject(at: index)
                    
                }
                print(checked_values)
                
                cell.accessoryType = .none
                checked[indexPath.row] = 90000
            } else {
                cell.accessoryType = .checkmark
                checked[indexPath.row] = infos["id"].intValue
            }
        }
        
        print(checked)
        
    }
    
    func addANDEditVisit(link : [JSON],newFlag : Bool = true)
    {
        if !newFlag
        {
            if let infos = self.JsonData
            {
                infos.enumerated().forEach({
                    if $0.element["id"].intValue == link[0]["id"].intValue
                    {
                        self.JsonData?[$0.offset] = link[0]
                        
                    }
                })
                
            }
        }
        else
        {
            self.JsonData?.insert(link[0], at: 0)
        }
        
        self.tableView.reloadData()
    }

    
}

// MARK: - PullToRefresh

extension SelectedVisit
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.TaskListDataFetch()
            }, loadingView: loadingView)
        
            tableView.dg_setPullToRefreshFillColor(TaskNAVBAR())
        
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
}
// MARK: - Class Instance

extension SelectedVisit
{
    class func instance()->SelectedVisit?{
        let storyboard = UIStoryboard(name: "Task", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SelectedVisit") as? SelectedVisit
        return controller
    }
}
