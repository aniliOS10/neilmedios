import UIKit
import IoniconsKit
import MMDrawerController
import SwiftyJSON
import Photos
import SwipeCellKit


class CreateTask: InterfaceExtendedController {
    
    fileprivate var selectedContact_array : [String]?
    fileprivate var temp_textField = UITextField()
    fileprivate var Pickerdatavalue = [JSON]()
    fileprivate var ContactName  = [String]()
    fileprivate var contactId = [Int]()
    
    let imagePicker = UIImagePickerController()
    
    var indexvalu = 0
    var RowValue = 0
    var numberOfCell = 2
    var ContactString = String()
    var Datevalue = String()
    var Timevalue = String()
    var ImgArray = [UIImage]()
    var ImgSaveArray = [String]()
    var selectedCountryId : Int = 0
    var dataPickerView = UIPickerView()
    var Time_Picker = UIDatePicker()
    var Date_Picker = UIDatePicker()
    var visit_id  = ""
    var PriorityList: [String] = ["Normal","Low","Urgent"]
    var arrSctionTitle = NSArray()
    var arrTitle = NSArray()
    var  cell = UITableViewCell()
    var toolBar = UIToolbar()
    var delegate  : AddContactsToCreateOrderProtocol?
    var DummyArray = NSMutableArray ()
    var ContactArray = [String : Any]()
    var dicSet = NSMutableDictionary()
    var skippedArray : NSMutableArray =         ["","","","","",""]
    var SaveArray : NSMutableArray =         ["","","","","",""]
    
     var BackDataBlock: ((_ json : [JSON]) -> ())?
    
    @IBOutlet weak var mTableview: UITableView!
   
    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        createPickerView()
        imagePicker.delegate = self
        self.mTableview.delegate = self
        self.mTableview.dataSource = self
        self.mTableview.tableFooterView = UIView()
      
        RightActionButton(Title:languageKey(key:"Save"))
       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: TaskNAVBAR(), BtnColor: UIColor.white)
        
    }
    
    // MARK: - Language Setting
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Create Task")
        
        arrSctionTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Task", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Attached Files", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Attached Visit", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Description", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Notes", comment: "")]
        
        
        arrTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Title", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Deadline", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Priority", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Description", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Notes", comment: "")]
        
    }
    
    // MARK: - Create PickerView

    func createPickerView(){
        dataPickerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216)
        dataPickerView.delegate = self
        dataPickerView.dataSource = self
        dataPickerView.backgroundColor = UIColor.white
        dataPickerView.showsSelectionIndicator = true
        
        
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        if #available(iOS 13.4, *) {
            if #available(iOS 14.0, *) {
                Date_Picker.preferredDatePickerStyle = .wheels
            } else {
                Date_Picker.preferredDatePickerStyle = .wheels
            }
        } else {
            // Fallback on earlier versions
        }
        
        Date_Picker.datePickerMode = UIDatePicker.Mode.date
        Date_Picker.addTarget(self, action: #selector(datePickerChanged), for: UIControl.Event.valueChanged)
        
        
        
        Time_Picker.datePickerMode = UIDatePicker.Mode.time
        Time_Picker.addTarget(self, action: #selector(TimePickerChanged), for: UIControl.Event.valueChanged)
        
    }
    
    
    // MARK: - Lable Border Line

    func DashedLineBorder (title: String)-> UILabel {
        
        let ContainerView = UILabel(frame: CGRect(x: 10, y: 10, width: mTableview.frame.size.width-20, height: 55))
        ContainerView.layer.addSublayer(Border(YourLable: ContainerView))
        ContainerView.textAlignment = .center
        ContainerView.font = UIFont.boldSystemFont(ofSize: 16)
        ContainerView.textColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        ContainerView.text = title
        return ContainerView
        
    }
   
    // MARK: -Create TextView

    func textView (Title: String,tagValue:Int)-> UITextView
    {
        let view = UITextView(frame: CGRect(x: 10, y: 10, width: mTableview.frame.size.width-20, height: 80))
        view.delegate = self
        view.backgroundColor = OffWhiteColor()
        view.text = Title
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.gray.cgColor
        view.font = UIFont.systemFont(ofSize: 15)
        view.tag = tagValue
        return view
    }
    
    // MARK: - Save Button Action

    override func rightButtonAction(){
        
 

        let array1 = DummyArray.value(forKey: "id")
        
        
         print(array1)
        
        
        
        
        
        
        for index in 0..<(DummyArray.count) {

             visit_id += "\(((DummyArray.object(at: index) as! NSDictionary).object(forKey: "id") as! Int) ) \(",")"
            
            
            print(visit_id)
            
           
            
        }
        
       

            
            var datavalues = [String : Any]()
            datavalues["name"] = SaveArray[0]
            datavalues["deadline"] = SaveArray[1]
            datavalues["priority"] = SaveArray[2]
            datavalues["description"] = SaveArray[3]
            datavalues["notes"] = SaveArray[4]
            datavalues["visit_id"] = visit_id
            datavalues["AttachmentFiles"] = ImgArray
            
            print(datavalues)
            
            AddTaskRequest(datavalues, Method: "/taskapp_create")
            
   
        
    }
    
    // MARK: - Create Task Request API

    fileprivate func AddTaskRequest(_ param : [String : Any],Method :String)
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        AddContactApi().Fetch(param: param,Url: Method) { (status, message, info) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                self.alert(message)
                return
            }
            else
            {
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: message), actionTitles: [self.languageKey(key: "Ok")], actions: [{action1 in
                    
                    if let createdTask = info
                    {
                        self.BackDataBlock?(createdTask)
                    }
                    self.navigationController?.popViewController(animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateTaskList"), object: nil)
                    
                    
                 
                    
                    
                    
                    }, nil])
            }
            
            
            
        }
        
        
        
        
        
    }
    
   
    
}

// MARK: - Table View Delegate/DataSource Methods

extension CreateTask: UITableViewDelegate,UITableViewDataSource,SwipeTableViewCellDelegate
{
 
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            self.mTableview.separatorColor = UIColor.black
            return 3
  
     
       
        case 1:
            
            self.mTableview.separatorColor = UIColor.black
            return (self.ImgArray.count) + 1
            
            
        case 2:
            
            self.mTableview.separatorColor = UIColor.black
            return (self.DummyArray.count) + 1
            
        case 3:
            
            self.mTableview.separatorColor = UIColor.black
            return 1
            
        case 4:
            
            self.mTableview.separatorColor = UIColor.black
            return 1
        default:
            return 0
        }
        
        //return arrTitle.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrSctionTitle.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = OffWhiteColor()
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: tableView.frame.width, height: 30)
        label.text = arrSctionTitle[section] as? String
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = UIColor.black
        //  label.backgroundColor = UIColor.yellow
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        
        print("Section Index",indexPath.section)
        
        if indexPath.section == 0 {
            return  75
        }
       else if indexPath.section == 1 {
            
            return  75
        }
            else if indexPath.section == 2
        {
            
            if self.DummyArray.count-1 < indexPath.row
            {
                return 75
            }
            else
            {
                return 150
            }
            
        }
        else
        {
            
            return 100
        }
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
            let str2 = arrTitle[indexPath.row] as? String
            cell.lbtitleAC.text = arrTitle[indexPath.row] as? String
            //str2!.replacingOccurrences(of: "*", with: "", options:
            //    NSString.CompareOptions.literal, range: nil)
            cell.txtTitleAC.text = skippedArray[indexPath.row] as? String
            cell.txtTitleAC.tag = indexPath.row
            cell.txtTitleAC.delegate = self
            cell.selectionStyle = .none
            
            
            if  indexPath.row == 1
                
            {
                
                cell.txtTitleAC.rightViewMode = .always
                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosCalendarOutline)
                
                
                self.pickUpDate(cell.txtTitleAC)
            }
                
            else   if indexPath.row == 2
            {
                cell.txtTitleAC.rightViewMode = .always
                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosArrowDown)
                
                
                self.pickUpDate(cell.txtTitleAC)
            }
            else
            {
                cell.txtTitleAC.rightViewMode = .never
                cell.txtTitleAC.rightView = nil
                //cell.accessoryView = HideDropDownImage()
            }
            
            
            
            return cell
            
        }
            

        
            
        else  if indexPath.section == 1
        {
            let myCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
            myCell.backgroundColor = UIColor.white
            
            
            let totalRows = tableView.numberOfRows(inSection: indexPath.section)
            
            if indexPath.row == totalRows - 1 {
                
                myCell.contentView .addSubview(DashedLineBorder(title: "Attached file here".languageSet))
            }
                
            else
            {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "AttachmentCell", for: indexPath) as? TeamCell
                    else
                {
                    return UITableViewCell()
                }
                cell.selectionStyle = .none
                
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
                cell.lblUserName.text = ImgSaveArray[indexPath.row]
                cell.imgUser.image = UIImage.fontAwesomeIcon(name: .fileImage, style: .regular, textColor: VisitNAVBAR(), size: CGSize(width: 40.0, height: 40.0))
                cell.delegate = self
                
                
                return cell
                
                
            }
            
            
            
            myCell.selectionStyle = .none
            return myCell
        }
        else  if indexPath.section == 2
        {
            let myCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
            myCell.backgroundColor = UIColor.white
            
            
            let totalRows = tableView.numberOfRows(inSection: indexPath.section)
            
            if indexPath.row == totalRows - 1 {
                
                myCell.contentView .addSubview(DashedLineBorder(title: "Attached Visit here".languageSet))
            }
                
            else
            {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "VisitListingCell", for: indexPath) as? VisitListingCell
                    else{return VisitListingCell()}
               
               
                   cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)

               
                
               
                cell.lblTask?.text = ((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "title") as! String)
                cell.lbldate?.text = ((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "visit_date") as! String)
                cell.lblVisitType?.text = ((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "visit_type") as! String)
                cell.lblVisitedCount?.text = ((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "visited_count") as! String)
                cell.lblPendingCount?.text = ((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "pending_count") as! String)
                cell.lblRescheduleCount?.text = ((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "rescheduled_count") as! String)
                cell.selectionStyle = .none

                cell.delegate = self

                return cell
                
                
            }
            
            
            
            myCell.selectionStyle = .none
            return myCell
        }
            
        else  if indexPath.section == 3
        {
            
            
            let myCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
            myCell.backgroundColor = UIColor.white
            myCell.selectionStyle = .none
             myCell.tag = 100
            myCell.contentView.addSubview(textView(Title: SaveArray[3] as! String, tagValue: 100))
            return myCell
            
        }
        
        else  if indexPath.section == 4
        {
            
            
            let myCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
            myCell.backgroundColor = UIColor.white
            myCell.selectionStyle = .none
            myCell.tag = 101
            myCell.contentView.addSubview(textView(Title: SaveArray[4] as! String, tagValue: 101))
            return myCell
            
        }
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        
        let deleteAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            
            if indexPath.section == 2
            {
                if self.DummyArray.count > 0
                {
                    self.DummyArray.removeObject(at: indexPath.row)
                }
            }
            else if indexPath.section == 1            {
                
                self.ImgArray.remove(at: indexPath.row)
                
            }
            self.mTableview.reloadData()
        }
        
        deleteAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor: .white, size: CGSize(width: 32, height: 32))
        
        return [deleteAction]
        
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 1
        {
            
            
            let totalRows = tableView.numberOfRows(inSection: indexPath.section)
            
            if indexPath.row == totalRows - 1 {
                
                
                self.ActionSheet()
                
            }
            else
            {
                
                
                
                guard let controller = ShowImage.instance() else{return}
                controller.img_Var = ImgArray[indexPath.row]
                self.navigationController?.pushViewController(controller, animated: true)
                
                
            }
            
            
            
            
            
        }
        else if indexPath.section == 2
        {
            
            let totalRows = tableView.numberOfRows(inSection: indexPath.section)
            
            if indexPath.row == totalRows - 1 {
                
                
                guard let controller = SelectedVisit.instance()
                    else{return}
                controller.delegate = self
                controller.CheckArray = DummyArray
                self.navigationController?.pushViewController(controller, animated: true)
                
            }
            else
            {
                
              
                
                guard let controller = VisitDetailsVC.instance()
                    else{return}
                controller.visitId = ((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "id") as! Int)
                self.navigationController?.pushViewController(controller, animated: true)
                
            }
            }
        
    }
    
    //- Show DropDown Image
    func ShowDropDownImage(Image: Ionicons) -> UIImageView {
        
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 40, width: 30, height: 30)
        imageView.image = UIImage.ionicon(with: Image, textColor: UIColor.black, size: CGSize(width: 50, height: 50))
        
        return imageView
        
    }
    
    //- Action Sheet
    func ActionSheet()
    {
        
        let alert = UIAlertController(title: nil, message: languageKey(key: "Choose option"), preferredStyle: .actionSheet)
        
        
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Take Photo"), style: .default , handler:{ (UIAlertAction)in
            
            self.openCamera()
            
        }))
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Choose Photo"), style: .default , handler:{ (UIAlertAction)in
            
            self.photoLibrary()
            
        }))
        
        
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Cancel") , style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        alert.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        
        self.present(alert, animated: true) {
            print("option menu presented")
        }
        
    }
    
    //- Open Camera
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            navigationController!.setNavigationBarHidden(true, animated: false)
            // Add it as a subview
            
            addChild(imagePicker)
            view.addSubview(imagePicker.view)
            self.tabBarController?.tabBar.isHidden = true
        }
    }
    
    //- Photo Library
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            // imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            navigationController!.setNavigationBarHidden(true, animated: false)
            // Add it as a subview
            
            addChild(imagePicker)
            view.addSubview(imagePicker.view)
            self.tabBarController?.tabBar.isHidden = true
        }
    }
    
}

// MARK: - TextField Delegate Methods
extension CreateTask : UITextFieldDelegate
{
    // datepickerView
    
    func pickUpDate(_ textField : UITextField){
        
        print("TagValue:----->>>",textField.tag)
        
       
        
    }
    
    
    func CurrentDate()->String{
        let currentDateTime = Date()
        
        // initialize the date formatter and set the style
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: currentDateTime)
        
    }
    
    func CurrentTime()->String{
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter.string(from: currentDateTime)
        
    }
    
    
    @objc func datePickerChanged(datePicker:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        
        Datevalue = dateFormatter.string(from: datePicker.date)
        
        print(Datevalue)
        
    }
    
    
    @objc func TimePickerChanged(datePicker:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeStyle = .short
        
        
        Timevalue = dateFormatter.string(from: datePicker.date)
        
        print(Timevalue)
        
    }
    
    @objc func doneClick() {
        
        
        if indexvalu == 2 {
            
            skippedArray[2] = PriorityList[RowValue]
            
            SaveArray[2] = RowValue + 1
            
            
            print(Pickerdatavalue)
            
        }
            
       
        else if indexvalu == 1
        {
            
            
            if Datevalue == ""
            {
                skippedArray[1] = CurrentDate()
                SaveArray[1] = CurrentDate()
                
                
                
            }
            else
            {
                skippedArray[1] = Datevalue
                SaveArray[1] = Datevalue
            }
       
            
        }
    
        
        if indexvalu == 2
        {
            self.dataPickerView.reloadAllComponents()
            
            self.dataPickerView.selectRow(0, inComponent: 0, animated: false)
            
            
        }
        
        Timevalue = ""
        
        RowValue = 0
        
        mTableview.reloadData()
        
        
        self.view .endEditing(true)
        
    }
    @objc func cancelClick() {
        
        Timevalue = ""
        
        if indexvalu == 2
        {
            
            self.dataPickerView.reloadAllComponents()
            
            self.dataPickerView.selectRow(0, inComponent: 0, animated: false)
            
        }
        RowValue = 0
        self.view .endEditing(true)
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        
        print("textField-----> ",textField.tag)
        
        indexvalu = textField.tag
        
        if textField.tag == 2 {
            
            textField.inputView = self.dataPickerView
            textField.inputAccessoryView = self.toolBar
        
            self.dataPickerView.reloadAllComponents()
            
        }
        
        
        else if textField.tag == 1
        {
            textField.inputView = self.Date_Picker
            textField.inputAccessoryView = self.toolBar
            Date_Picker.reloadInputViews()
            
            
        }
       
        else
        {
            textField.inputView = nil
            textField.inputAccessoryView = nil
            self.Date_Picker.removeFromSuperview()
            self.toolBar.removeFromSuperview()
            
        }
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        print("shouldChangeCharactersIn :---",textField.tag)
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        
        if textField.tag < 100
            
        {
            skippedArray[textField.tag] = newString
            
            SaveArray[textField.tag] = newString
        }
        else
        {
            
            ContactString = newString as String
            
        }
        
        
        
        
        
        print(skippedArray)
        
        
        return true
    }
    
}

// MARK: - TextView Delegate Methods

extension CreateTask : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
        print("Text:------>  ",text)
        
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        if textView.tag == 100
        {
        skippedArray[3] = changedText
        SaveArray[3] = changedText
        }
        else if textView.tag == 101
        {
            skippedArray[4] = changedText
            SaveArray[4] = changedText
            
        }
        
        
        print("TextView:------>  ",changedText)
        
        
        return true
    }
    
}
// MARK: - PickerView Delegate Methods
extension CreateTask : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       
            return PriorityList.count
     
       
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return PriorityList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        
        RowValue = row
        
    }
}

// MARK: - Protocol Methods
extension CreateTask : SelectedContactProtocol
{
    func selected(selected_array: NSMutableArray) {
        
        
        print(selected_array)
        print(selected_array.count)
        
        
        DummyArray = selected_array
        print(DummyArray)
        mTableview.reloadData()
        
    }
}

// MARK: - ShapeLayer  Methods
extension CreateTask
{
    func Border(YourLable: UILabel) -> CAShapeLayer{
        
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.black.cgColor
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = YourLable.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: YourLable.bounds).cgPath
        
        
        return yourViewBorder
        
    }
    
}

// MARK: - ImagePickerControllerDelegate Methods
extension CreateTask: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
      
        
        var selectedImage: UIImage?
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
            
            
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
            
        }
        
        
        let fileUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL
        
        ImgArray.append(selectedImage!)
        
        if fileUrl == nil
        {
            ImgSaveArray.append("attachment"+String(format:"%d",ImgArray.count)+String(format:".%@","png"))
        }
        else
        {
            ImgSaveArray.append("attachment"+String(format:"%d",ImgArray.count)+String(format:".%@",fileUrl!.pathExtension))
        }
        
        
        
        
        
        
        mTableview.reloadData()
        dismissPicker(picker: picker)
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
       dismissPicker(picker: picker)
    }
    
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?) {
        
    }
    private func dismissPicker(picker : UIImagePickerController){
        picker.view!.removeFromSuperview()
        picker.removeFromParent()
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        UIApplication.shared.isStatusBarHidden = false
    }
}


// MARK: - Class  Instance
extension CreateTask
{
    class func instance()->CreateTask?{
        
        let storyboard = UIStoryboard(name: "Task", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CreateTask") as? CreateTask
        
        
        return controller
    }
}



