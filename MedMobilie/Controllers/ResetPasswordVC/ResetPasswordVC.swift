//
//  ResetPasswordVC.swift
//  MedMobilie
//
//  Created by dr.mac on 27/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//
import UIKit

class ResetPasswordVC: InterfaceExtendedController,UITextFieldDelegate {
    
    var emailID : String?

    @IBOutlet weak var lblOldPassword: UILabel!
    @IBOutlet weak var lblNewPassword: UILabel!
    @IBOutlet weak var lblConfirmPassword: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    
    // MARK: - Done Action

    @IBAction func DoneAction(_ sender: Any) {
    
        guard let OTPText = txtOTP.text ,let NewPswrd = txtNewPassword.text , let CnfrmPswrd = txtConfirmPassword.text else
                {
                    return
                }
        if  NewPswrd.containsWhitespace  {
            self.alert("Your New Password Field can't contains Blank Space!")
            txtNewPassword.text = ""
            txtConfirmPassword.text = ""
        }
        else if NewPswrd.count < 5
        {
            txtNewPassword.text = ""
            txtConfirmPassword.text = ""
            self.alert("The Password must be at least 5 Characters long.")
            return
        }
        if NewPswrd != CnfrmPswrd
        {
            txtNewPassword.text = ""
            txtConfirmPassword.text = ""
            self.alert("Confirm password does not match")
            return
            
        }
        ResetPassWRD_api(paswrd: NewPswrd,OTP: Int(OTPText) ?? 0000)
        
    }
    
    // MARK: - Reset PassworD_api

    
    func ResetPassWRD_api(paswrd : String,OTP : Int)
    {
        
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        ResetPasswordVerification().verify(email: emailID ?? "", OTP, paswrd) { (status, Message) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(Message)
                return
            }
            let titleName = Bundle.appName()
            self.popupAlert(title: titleName, message: "Your Newpassword set succesfully".languageSet, actionTitles:[self.languageKey(key: "ok")], actions:[{action1 in
                print("ok")
                 self.navigationController?.popViewController(animated: true)
                }, nil])
           
           
        }
        
    }
    
    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        txtOTP.Padding()
        txtOTP.textFieldBottomBorder()
        txtOTP.backgroundColor = UIColor.clear
        txtOTP.delegate = self
        
        txtNewPassword.Padding()
       txtNewPassword.textFieldBottomBorder()
        txtNewPassword.backgroundColor = UIColor.clear
        txtNewPassword.delegate = self
        
        txtConfirmPassword.Padding()
        txtConfirmPassword.textFieldBottomBorder()
        txtConfirmPassword.backgroundColor = UIColor.clear
        txtConfirmPassword.delegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        
        
    }
    
    // MARK: - Language Setting

    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "OTP Verify")
        
        
        lblOldPassword.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Enter Your OTP", comment: "")
        
        lblNewPassword.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Enter New Password", comment: "")
        
        lblConfirmPassword.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Confirm New Password", comment: "")
        
        btnDone.setTitle(LocalizationSystem.SharedInstance.localizedStingforKey(key: "Submit", comment: ""), for: .normal)
        
        
    }
    
    
    
    // MARK: - TextField Delegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
}


// MARK: - Class instance

extension ResetPasswordVC
{
    class func instance()->ResetPasswordVC?{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as? ResetPasswordVC
        
        
        return controller
    }
}
