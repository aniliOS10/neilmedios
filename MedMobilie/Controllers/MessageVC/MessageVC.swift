//
//  MessageVC.swift
//  MedMobilie
//
//  Created by dr.mac on 20/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit
import DGElasticPullToRefresh
import IoniconsKit
import SwiftyJSON

class MessageVC: InterfaceExtendedController ,UISearchResultsUpdating{
    
   
    fileprivate var MessageData : [JSON]?
    fileprivate var mainMessageData : [JSON]?
    fileprivate var resultSearchController = UISearchController()
    
    @IBOutlet weak var lblPlusSign: UILabel!
    @IBOutlet weak var mtableview: UITableView!
    @IBOutlet weak var btnView: UIView!
    
    // MARK: - Class life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var parama = [String:Any]()
        parama["user_id"] = 0
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Check_Chat_ID"), object: parama)
        self.tabBarController?.navigationController?.navigationBar.isHidden = true
        paintNavigationTitle(title : "Message".languageSet,Color : .black)
        self.NavBarIcon()
        PaintleftSideMenu()
        
        btnView.createCircleForView()
        // btnView.isHidden = true
        lblPlusSign.font = UIFont.ionicon(of: 30)
        lblPlusSign.text = String.ionicon(with: .plus )
        self.mtableview.tableFooterView = UIView()
        self.mtableview.separatorStyle = .none
        mtableview.rowHeight = UITableView.automaticDimension
        mtableview.estimatedRowHeight = 90
     
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SetUpSearchBar()
        AppDelegate.showWaitView()
        MessageList_fetch()
        
        self.navigationController?.isNavigationBarHidden = false
        PaintNavigationBar(TitleColor: UIColor.black, BackgroundColor: WhiteColor(), BtnColor: UIColor.black)
        
        self.tabBarController?.tabBar.isHidden = false
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.resultSearchController.dismiss(animated: false, completion: nil)
    }
    
    // MARK: - Language Setting
    @objc override func LanguageSet(){
        
        
    }
    
    // MARK: - Message List fetch
    func MessageList_fetch() //_ Team_id: String
    {
        if !NetworkState.isConnected() {
             self.mtableview.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        MessageList_Api().Fetch { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.mtableview.dg_stopLoading()
                self.alert(message ?? "Something Went Wrong")
                return
            }
            self.mainMessageData = data
            self.MessageData = self.mainMessageData
            self.mtableview.reloadData()
            self.mtableview.dg_stopLoading()
        }
    }
    
    // MARK: - SetUp SearchBar
    func SetUpSearchBar()
    {
        self.resultSearchController = ({
            
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.searchBar.placeholder = self.languageKey(key: "Search Messages".languageSet)
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.hidesNavigationBarDuringPresentation = false
            self.mtableview.tableHeaderView = controller.searchBar
            
            return controller
        })()
    }
    
    // MARK: - update Search Results
    func updateSearchResults(for searchController: UISearchController) {
        // filteredTableData.removeAll(keepingCapacity: false)
        
        if searchController.searchBar.text != ""
        {
            let Filterarray = self.mainMessageData?.filter({ (element) -> Bool in
                if element["name"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "") || element["message"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "")
                {
                    return true
                }
                return false
            })
            
            self.MessageData = Filterarray
            self.mtableview.reloadData()
        }
        else
        {
            self.MessageData = mainMessageData
            self.mtableview.reloadData()
            
        }
        
    }
   
    // MARK: - create New Contact Action
    @IBAction func createNewContact(_ sender: Any) {
        
        guard let obj_TeamVC = TeamVC.instance()
            else{return}
        obj_TeamVC.ClassName = "Message"
        self.navigationController?.pushViewController(obj_TeamVC, animated: true)
        
    }
    
   
}

// MARK: - Table View Delegate/DataSource Methods
extension MessageVC : UITableViewDelegate,UITableViewDataSource
{
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TeamCell
        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
        guard let infos = MessageData?[indexPath.row]  else{return cell}
        // cell.imgUser.image =
        cell.lblUserName.text = infos["name"].stringValue
        cell.lblMessage.text = infos["message"].stringValue
    
        var dateString = infos["created_date"].stringValue
        
        
        if infos["gender"].stringValue == ""
        {
            cell.gender = "Group_PH"
        }
        else if infos["gender"].stringValue != "male"
            {
                cell.gender = "Female_PH"
            }
        
        cell.DateTime?.text = CalculateAgoTime().timeAgoSinceDate(date: dateString.stringToDate(), numericDates: true)
        cell.imgUser.sd_setImage(with: infos["image"].url, placeholderImage: UIImage(imageLiteralResourceName: cell.gender ), options:.cacheMemoryOnly, completed: nil)
        cell.selectionStyle = .none
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
      
        
       let infos = MessageData?[indexPath.row]
        
      //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 1)
        guard let controller = ChatController.instance()
            else{return}
       controller.userid = infos!["id"].intValue
       controller.UserName = infos!["name"].stringValue
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let completeAction = UITableViewRowAction(style: .destructive, title: "Clean") { (action, indexpath) in
            print("Delete Action Tapped")
        }
        let PendingAction = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexpath) in
            print("Delete Action Tapped")
        }
        
        completeAction.backgroundColor = UIColor.lightGray
        PendingAction.backgroundColor = UIColor.red
        
        
        
        return [PendingAction,completeAction]
    }
}

// MARK: - PullToRefresh
extension MessageVC
{

override func loadView() {
    super.loadView()
    self.navigationController?.navigationBar.isTranslucent = false
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    self.navigationController?.navigationBar.shadowImage = UIImage()
    self.navigationController?.navigationBar.barTintColor = self.navBackgroundColor ?? BillNAVBAR()
    let loadingView = DGElasticPullToRefreshLoadingViewCircle()
    loadingView.tintColor = SettingNAVBAR()
    mtableview.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
      
        self?.MessageList_fetch()
        
        }, loadingView: loadingView)
    mtableview.dg_setPullToRefreshFillColor(navBackgroundColor ?? WhiteColor())
    mtableview.dg_setPullToRefreshBackgroundColor(mtableview.backgroundColor!)
}
}

// MARK: - Class Instance
extension MessageVC
{
    class func instance()->MessageVC?{
        let storyboard = UIStoryboard(name: "Team", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MessageVC") as? MessageVC
        
        
        return controller
    }
}
