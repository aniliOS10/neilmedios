
import UIKit
import IoniconsKit
import DGElasticPullToRefresh
import SwiftyJSON
import SwipeCellKit
import MMDrawerController


class LeaveListVC: InterfaceExtendedController , UISearchResultsUpdating {
    
    fileprivate var JsonData : [JSON]?
    fileprivate var mainJsonData : [JSON]?
    fileprivate var resultSearchController = UISearchController()
    
    var datefrom : String = ""
    var dateto : String = ""
    var type : String = ""
    var priority : Int = 0
    var reportKey : String = ""
    var contact_ID : Int = 0
    
    @IBOutlet weak var lblPlusSign: UILabel!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Class life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnView.createCircleForView()
        lblPlusSign.font = UIFont.ionicon(of: 30)
        lblPlusSign.text = String.ionicon(with: .plus )
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = false
        
        if contact_ID == 0
        {
            self.NavBarFilter()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        AppDelegate.showWaitView()
        self.LeaveListDataFetch()
        
      //  SetUpSearchBar()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.resultSearchController.dismiss(animated: false, completion: nil)
    }
    // MARK: - Language Setting
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Leaves")
    }
    
    // MARK: - Set navigationbar Icons
    func NavBarFilter()
    {
        
        let Filter = UIBarButtonItem(image: UIImage.ionicon(with: .androidOptions, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), style: .plain, target: self, action: #selector(Filter_Action))
        Filter.tintColor = UIColor.white
        
        navigationItem.rightBarButtonItem = Filter
        
        
        let btSlider = UIBarButtonItem(image: UIImage.ionicon(with: .androidMenu, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), style: .plain, target: self, action: #selector(OpenSlider))
        btSlider.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = btSlider
        
    }
    
    // MARK: - Filter Action
    @objc func Filter_Action(){
        
        self.popupAlertWithSheet(title: nil, message: nil, actionTitles: [ self.languageKey(key: "Pending"),self.languageKey(key: "Approved"),self.languageKey(key: "Rejected")], actions:[
            {action1 in
                
                self.type = "1"
                self.datefrom = ""
                self.dateto = ""
                AppDelegate.showWaitView()
                self.LeaveListDataFetch()
                
            },{action2 in
                
                self.type = "2"
                self.datefrom = ""
                self.dateto = ""
                AppDelegate.showWaitView()
                self.LeaveListDataFetch()
                
                
            },{action3 in
                
                self.type = "3"
                self.datefrom = ""
                self.dateto = ""
                AppDelegate.showWaitView()
                self.LeaveListDataFetch()
                            
                
            },{action4 in
                
     
            }
            , nil])
    }
    
    // MARK: - Open Slider top left
    
    @objc func OpenSlider(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    
    
    // MARK: - SetUp SearchBar
    func SetUpSearchBar()
    {
        self.resultSearchController = ({
            
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.searchBar.placeholder = "Search Task".languageSet
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.hidesNavigationBarDuringPresentation = false
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
    }
    
   
    // MARK: - create New Task Action
    @IBAction func createNewVisit(_ sender: Any) {
        
        guard let controller = SubmitLeaveVC.instance()
            else{return}
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    func addANDEditTask(link : [JSON],newFlag : Bool = true)
    {
        if !newFlag
        {
            if let infos = self.JsonData
            {
                infos.enumerated().forEach({
                    if $0.element["id"].intValue == link[0]["id"].intValue
                    {
                        self.JsonData?[$0.offset] = link[0]
                        
                    }
                })
                
            }
        }
        else
        {
            self.JsonData?.insert(link[0], at: 0)
        }
        
        self.tableView.reloadData()
    }
    
    // MARK: - Leave ListData fetching
    @objc fileprivate func  LeaveListDataFetch()
    {
        
        if !NetworkState.isConnected() {
            self.tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        
        
        var DataValue = [String : Any]()
        DataValue["status"] = type
        DataFetch_Api().fetch( param: DataValue , urlString: "/leavelist", completion:  { (status, message, data) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                AppDelegate.hideWaitView()
                self.JsonData?.removeAll()
                self.alert(message )
                self.tableView.reloadData()
                
                return
            }
            self.mainJsonData = data
            self.JsonData = self.mainJsonData
            self.tableView.reloadData()
            self.tableView.dg_stopLoading()
            
        })
        
    }
    
    // MARK: - TaskDelete_Or_TaskUpdate API
    
    fileprivate func TaskDelete_Or_TaskUpdate(Taskid:Int , Task_delete : Bool )
    {
        
        if !NetworkState.isConnected() {
            tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        let urlstr : String = "/deleteleave"
       
        
        ContactDelete_Api().Request(idvalue: Taskid ,url_string: urlstr ) { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: message, actionTitles:[self.languageKey(key: "OK")], actions:[{action1 in
                    AppDelegate.showWaitView()
                    self.LeaveListDataFetch()
()
                    }, nil])
            }
            
        }
        
        
    }
    
    //MARK: - SearchBar Update
    
    func updateSearchResults(for searchController: UISearchController) {
        // filteredTableData.removeAll(keepingCapacity: false)
        
        if !searchController.isActive {
            print("Cancelled")
        }
        
        if searchController.searchBar.text != ""
        {
            let Filterarray = self.mainJsonData?.filter({ (element) -> Bool in
                if element["name"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "")
                {
                    return true
                }
                return false
            })
            
            self.JsonData = Filterarray
            self.tableView.reloadData()
        }
        else
        {
            self.JsonData = mainJsonData
            self.tableView.reloadData()
            
        }
        
    }
    
    
    deinit {
        tableView.dg_removePullToRefresh()
    }
    
}

// MARK: - Table View Delegate/DataSource Methods
extension LeaveListVC : UITableViewDelegate , UITableViewDataSource , SwipeTableViewCellDelegate
{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return JsonData?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let json = self.JsonData?[indexPath.row] else{return }
        
        if json["status"].stringValue == "1"
        {
            guard let controller = SubmitLeaveVC.instance()
                else{return}
            
            controller.Leave_id = json["id"].intValue
            controller.SaveArray[0] = json["type_id"].stringValue
            controller.skippedArray[0] = json["type_name"].stringValue
            controller.SaveArray[1] = json["startdate"].stringValue
              controller.skippedArray[1] = json["startdate"].stringValue
            controller.SaveArray[2] = json["enddate"].stringValue
            controller.skippedArray[2] = json["enddate"].stringValue

            controller.SaveArray[3] = json["description"].stringValue
            controller.skippedArray[3] = json["description"].stringValue

            controller.updatecheck = "1"
            self.navigationController?.pushViewController(controller, animated: true)
        }
       else if json["status"].stringValue == "2"
        {
            guard let controller = SubmitLeaveVC.instance()
                else{return}
            
            controller.Leave_id = json["id"].intValue
            controller.SaveArray[0] = json["type_id"].stringValue
            controller.skippedArray[0] = json["type_name"].stringValue
            controller.SaveArray[1] = json["startdate"].stringValue
            controller.skippedArray[1] = json["startdate"].stringValue
            controller.SaveArray[2] = json["enddate"].stringValue
            controller.skippedArray[2] = json["enddate"].stringValue
            
            controller.SaveArray[3] = json["description"].stringValue
            controller.skippedArray[3] = json["description"].stringValue
            
            controller.updatecheck = "2"
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else
        {
            if json["reason"].stringValue != ""
            {
                
                
            self.popupAlert(title:self.languageKey(key: "Reason to reject"), message: json["reason"].stringValue, actionTitles:[self.languageKey(key: "OK")], actions:[{action1 in
                self.navigationController?.popViewController(animated: true)
                }, nil])
                
            }
        }
        
       
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? TaskListCell
            else{return TaskListCell()}
        guard let json = JsonData?[indexPath.row]
            else{return cell}
        
        
        
        if json["status"].stringValue == "3"
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.red)
            cell.delegate = nil
            cell.lblStatus?.textColor = .red
            
            cell.lblid?.text = self.languageKey(key: "Rejected by:")

        }
        else if json["status"].stringValue == "2"
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.green)
            cell.delegate = nil
            cell.lblStatus?.textColor = UIColor.green
            
            cell.lblid?.text = self.languageKey(key: "Approved by:")


        }
        else
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
            cell.lblStatus?.textColor = UIColor.orange
           
            cell.lblid?.text =  self.languageKey(key: "Approval by:")
            cell.delegate = self


        }
      
        cell.lblassign_by?.text = json["approvalby"].stringValue
        cell.lblTask_name?.text = json["type_name"].stringValue
        cell.lblTime?.text = json["created_date"].stringValue
        
    
        cell.lblStatus?.text = json["status_text"].stringValue
       
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        guard let list = self.JsonData?[indexPath.row] else{return []}
        let deleteAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            
             self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: "Are you sure you want to delete this Leave?"), actionTitles: [self.languageKey(key: "NO"),self.languageKey(key: "YES")], actions: [{action1 in
                    return
                    },{action2 in
                        
                        self.JsonData?.remove(at: indexPath.row)
                        self.tableView.reloadData()
                        DispatchQueue.global(qos: .background).async {
                            DispatchQueue.main.async {
                                self.TaskDelete_Or_TaskUpdate(Taskid: list["id"].intValue, Task_delete: true)
                                //  self.visitDelete(Contactid: list["id"].intValue)
                            }
                            
                            
                        }
                        
                    }, nil])
                
                
            
        }
        
    
        
        let complete = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            
            self.JsonData?[indexPath.row]["status"] = 3
            self.tableView.reloadData()
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    self.TaskDelete_Or_TaskUpdate(Taskid: list["id"].intValue, Task_delete: false)
                }
                
                
            }
            
        }
        
        
        deleteAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor: .white, size: CGSize(width: 32, height: 32))
        
        complete.backgroundColor = UIColor.lightGray
        complete.image = UIImage.ionicon(with: .thumbsup, textColor: .white, size: CGSize(width: 32, height: 32))
        
        
     
        
        return [deleteAction]
        
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    
}

// MARK: - PullToRefresh
extension LeaveListVC
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            
            self!.type  = ""
            
            
            self!.LeaveListDataFetch()
            }, loadingView: loadingView)
        
        tableView.dg_setPullToRefreshFillColor(SettingNAVBAR())
        
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
}

// MARK: - FilterProtocol
extension LeaveListVC : FilterProtocol
{
    func Filterdetails(FromDate : String , ToDate : String)
    {
        
        
        datefrom = FromDate
        dateto = ToDate
        
        print("datefrom :---> ",datefrom)
        print("dateto :---> ",dateto)
        
        
        
        AppDelegate.showWaitView()
       self.LeaveListDataFetch()
    }
    
    
    
    
}

// MARK: - Class Instance
extension LeaveListVC
{
    class func instance()->LeaveListVC?{
        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LeaveListVC") as? LeaveListVC
        //self.definesPresentationContext = true
        
        return controller
    }
}

