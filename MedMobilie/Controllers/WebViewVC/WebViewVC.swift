//
//  WebViewVC.swift
//  MedMobilie
//
//  Created by dr.mac on 31/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//


import UIKit
import WebKit
import MMDrawerController

class WebViewVC: InterfaceExtendedController, WKUIDelegate {
    
    @IBOutlet fileprivate  var webView : WKWebView!
    var linkUrl : URL?
    var MenuButton : Bool = false
    var titleName : String = "Help Center"
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if MenuButton == true
        {
            let btSlider = UIBarButtonItem(image: UIImage.ionicon(with: .androidMenu, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), style: .plain, target: self, action: #selector(OpenSlider))
            btSlider.tintColor = UIColor.white
            navigationItem.leftBarButtonItem = btSlider
            
            PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
            
            self.title = titleName.languageSet
            
        }
        else
        {
            PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: VisitNAVBAR(), BtnColor: UIColor.white)
        }
        
        AppDelegate.showWaitView()
        
        let request = URLRequest(url: linkUrl!)
        webView.load(request)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        //webView.scalesPageToFit = true
        
    }
    
    
    // MARK: - Open Slider
    
    @objc func OpenSlider(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    
}

// MARK: - WKNavigationDelegate


extension WebViewVC : WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        AppDelegate.hideWaitView()
        
    }
}



extension WKWebView {
    
    func loadUrl(string: String) {
        if let url = URL(string: string) {
            if self.url?.host == url.host {
                self.reload()
            } else {
                load(URLRequest(url: url))
            }
        }
    }
}


// MARK: - Class instance

extension WebViewVC
{
    class func instance()->WebViewVC?{
        let storyboard = UIStoryboard(name: "Visit", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "WebViewVC") as? WebViewVC
        
        
        return controller
    }
    
}

