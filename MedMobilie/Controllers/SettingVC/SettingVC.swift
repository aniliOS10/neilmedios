//
//  SettingVC.swift
//  MedMobilie
//
//  Created by MAC on 11/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit
import MMDrawerController
import SwiftyJSON
import LocalAuthentication


class SettingVC: InterfaceExtendedController {
    
    fileprivate var temp_textField = UITextField()
    fileprivate var dataList = [JSON]()
    fileprivate var currency_Name = ""

    var Language_String : String?
    
    var currentType : Bool = false
    
    var arrSettingMenuList = NSArray()
    var arrLanguageList = NSArray()
    var arrSctionTitle = NSArray()
    var arrNotificationTitle = NSArray()
    let viewDemo = UIView()
    var  cell = UITableViewCell()
    var gradePicker: UIPickerView!
    let gradePickerValues = ["usd", "euro", "rupee","usd", "euro", "rupee","usd", "euro", "rupee"]
    
    @IBOutlet weak var mTableView: UITableView!

    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mTableView.delegate = self
        self.mTableView.dataSource = self
        self.mTableView.tableFooterView = UIView()
        guard let userInfo = SignedUserInfo.sharedInstance else {return}
        currency_Name = "\(userInfo.currency_symbol ?? "") \(userInfo.currency_code ?? "")"
        self.CurrencyListData()
        
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let btSlider = UIBarButtonItem(image: UIImage.ionicon(with: .androidMenu, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), style: .plain, target: self, action: #selector(OpenSlider))
        btSlider.tintColor = UIColor.white
        
        
        
        navigationItem.leftBarButtonItem = btSlider
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        
        
        
        print("Check Status :----->", LAContext().biometricType.rawValue)
        
        
        if LAContext().biometricType.rawValue == "faceID"
        {
            currentType = true
        }
        
        
        mTableView.reloadData()
        
        
    }
    
    
    // MARK: - CurrencyListData
    
    fileprivate func CurrencyListData()
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        CurrencyList_Api().Request { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                return
            }
            guard let tempdata = data else
            {
                return
            }
            self.dataList = tempdata
        }
    }
    
    // MARK: - OpenSlider

    @objc func OpenSlider(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    // MARK: - Language Setting
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Setting")
        
        
        arrSctionTitle = [languageKey(key: "Language"),languageKey(key: "Currency Settings"),languageKey(key: "Account Settings"),languageKey(key: "Screen Lock"),languageKey(key: "Reset Settings")]
        
        arrSettingMenuList = [languageKey(key: "Change password"),languageKey(key: "Edit Profile")]
        
        
        arrNotificationTitle = [languageKey(key: "Require Face ID"),languageKey(key: "Sound"),languageKey(key: "Show preview")]
        
        
        self.ShowLanguage()
    }
    
    // MARK: - Show Language

    func ShowLanguage()
    {
        
        
        if LocalizationSystem.SharedInstance.getLanguage() == "en"
        {
            Language_String = languageKey(key: "English")
        }
        else if LocalizationSystem.SharedInstance.getLanguage() == "ja"
        {
            Language_String = languageKey(key: "Japanese")
        }
        else if LocalizationSystem.SharedInstance.getLanguage() == "es"
        {
            Language_String = languageKey(key: "Spanish")
        }
        /*  else if LocalizationSystem.SharedInstance.getLanguage() == "fr"
         {
         Language_String = languageKey(key: "French")
         }*/
        
        
        
        mTableView.reloadData()
        
    }
    
    // MARK: - Open Action Sheet

    
    func ActionSheet()
    {
        
        let alert = UIAlertController(title: nil, message: languageKey(key: "NeilMed Language"), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: languageKey(key: "English"), style: .default , handler:{ (UIAlertAction)in
            
            self.chngeLang("en")
            //LocalizationSystem.SharedInstance.SetLanguage(languageCode: "en")
            
        }))
        
        alert.addAction(UIAlertAction(title: languageKey(key:"Japanese"), style: .default , handler:{ (UIAlertAction)in
            self.chngeLang("ja") //LocalizationSystem.SharedInstance.SetLanguage(languageCode: "ja")
            
        }))
        
        alert.addAction(UIAlertAction(title: languageKey(key:"Spanish"), style: .default , handler:{ (UIAlertAction)in
            self.chngeLang("es") //LocalizationSystem.SharedInstance.SetLanguage(languageCode: "ja")
            
        }))
        
        /* alert.addAction(UIAlertAction(title: languageKey(key:"French"), style: .default , handler:{ (UIAlertAction)in
         self.chngeLang("ja") //LocalizationSystem.SharedInstance.SetLanguage(languageCode: "ja")
         
         }))*/
        
        
        
        alert.addAction(UIAlertAction(title: languageKey(key:"Dismiss"), style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        alert.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        
        self.present(alert, animated: true) {
            print("option menu presented")
        }
        
    }
    
    // MARK: - Change_Currency action

    
    fileprivate func chngeCurrancy()
    {
        let alertController = UIAlertController(title: Bundle.appName(), message: self.languageKey(key: "Select Currency"), preferredStyle: .alert)
        if self.dataList.count > 0
        {
            for data in 0...self.dataList.count - 1
            {
                let currency_name = self.dataList[data]["name"].stringValue
                let action = UIAlertAction(title: currency_name, style: .default, handler: { action in
                    let index = IndexPath(row: 0, section: data)
                    let id = self.dataList[index.section]["id"].intValue
                    self.Change_Currency_Api(id:id,index : index.section )
                    
                    let ud = UserDefaults.standard
                    ud.set(self.dataList[index.section]["id"].intValue, forKey: "currency_code")
                    
                    
                })
                alertController.addAction(action)
            }
        }
        let cancelAction = UIAlertAction(title: self.languageKey(key: "Dismiss"), style: .cancel, handler: { action in
        })
        alertController.addAction(cancelAction)

        
        alertController.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        
        self.present(alertController, animated: true) {
            print("option menu presented")
        }
        
    }
    
    // MARK: - Change_Currency_Api

    fileprivate func Change_Currency_Api(id : Int,index: Int)
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        Change_Currency_ApiRequest().Request(id){ (status, message) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message )
                return
            }
            self.currency_Name = "\(self.dataList[index]["symbol"].stringValue) \(self.dataList[index]["code"].stringValue)"
            self.mTableView.reloadData()
        }
    }
    
    // MARK: - Language Change

    fileprivate func chngeLang(_ language : String)
    {
        
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        LanguageChange_Api().Request(language) { (status,message , lang) in
            AppDelegate.hideWaitView()
            if !status{
                self.alert(message ?? "something went Wrong")
                return
            }
            //self.alert(message ?? "something went Wrong")
//            RootOfflineDataFetch().FetchingWhenLangChange()
            LocalizationSystem.SharedInstance.SetLanguage(languageCode: lang ?? language)
            
            RootControllerManager().setRoot()
            return
            //     return lang
            
        }
        
        
    }
    
    
   
    
}




// MARK: - Table View Delegate/DataSource Methods

extension SettingVC: UITableViewDelegate,UITableViewDataSource
{
    
    // MARK: - Table view data source
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch(section) {
            
        case 0:return 1
            
        case 1:return 1
            
        case 2:return 2
            
            
        case 3:return 1
            
        case 4:return 1
            
        default :return 1
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 4
        
        
        
    }
    
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
        
        
        return arrSctionTitle[section] as? String
    }
    
    private func tableView (tableView:UITableView , heightForHeaderInSection section:Int)->Float
    {
        
        return 20.0
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 3
        {
            if currentType == true
            {
                return 50
            }
            else
                
            {
                return 104
            }
        }
        else
        {
            return 50
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
            
            cell.Title_LBL.font = SystemFont(FontSize: 18)
            cell.Title_LBL.textColor = ColorValue(Rvalue:61, Gvalue: 140, Bvalue: 250, alphavalue: 0.9)
            
            cell.SubTitle_LBL.text = Language_String
            cell.SubTitle_LBL.textColor = UIColor.black
            cell.Title_LBL.text = languageKey(key: "Language")
            cell.selectionStyle = .none
            cell.accessoryType = .disclosureIndicator
            return cell
            
        }
            
            
        else  if indexPath.section == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
            
            cell.Title_LBL.font = SystemFont(FontSize: 18)
            cell.Title_LBL.textColor = ColorValue(Rvalue:61, Gvalue: 140, Bvalue: 250, alphavalue: 0.9)
            cell.SubTitle_LBL.textColor = UIColor.black
            cell.Title_LBL.text = languageKey(key: "Currency")
            cell.SubTitle_LBL.text = currency_Name
            cell.selectionStyle = .none
            cell.accessoryType = .disclosureIndicator
            return cell
            
        }
            
        else  if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
            
            cell.Title_LBL.font = SystemFont(FontSize: 18)
            cell.Title_LBL.textColor = ColorValue(Rvalue:61, Gvalue: 140, Bvalue: 250, alphavalue: 0.9)
            
            cell.Title_LBL.text = arrSettingMenuList[indexPath.row] as? String
            cell.SubTitle_LBL.textColor = UIColor.clear
            cell.accessoryType = .disclosureIndicator
            cell.selectionStyle = .none
            return cell
            
        }
            
        else  if indexPath.section == 3 {
            
            
            
            if currentType == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
                
                cell.lblTitle.text = arrNotificationTitle[indexPath.row] as? String
                cell.lblTitle.font = SystemFont(FontSize: 18)
                cell.lblTitle.textColor = ColorValue(Rvalue:61, Gvalue: 140, Bvalue: 250, alphavalue: 0.9)
                let userdefault = UserDefaults.standard
                if userdefault.value(forKey:"FaceIdEnable") as? Bool == true
                {
                    cell.swSwitch.isOn = true
                }
                else
                {
                    cell.swSwitch.isOn = false
                }
                
                
                cell.swSwitch.addTarget(self, action: #selector(switchChanged(_:)), for: UIControl.Event.valueChanged)
                
                
                cell.swSwitch.onTintColor = SettingNAVBAR()
                cell.selectionStyle = .none
                return cell
                
            }
                
            else
            {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "TouchIDCell") as! TouchIDCell
                
                
                cell.LblTouchID?.text = languageKey(key: "TouchId")
                
                return cell
                
            }
            
            
        }
            
        else  if indexPath.section == 4 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
            
            cell.Title_LBL.font = SystemFont(FontSize: 18)
            cell.Title_LBL.textColor = ColorValue(Rvalue:61, Gvalue: 140, Bvalue: 250, alphavalue: 0.9)
            cell.SubTitle_LBL.textColor = UIColor.clear
            cell.Title_LBL.text = languageKey(key: "Reset")
            cell.selectionStyle = .none
            return cell
            
        }
        
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if indexPath.section == 0 {
            
            self.ActionSheet()
            
        }
        else if indexPath.section == 1
        {
            self.chngeCurrancy()
            //UIView.animate(withDuration: 0.3, animations: {
            
            //                self.viewDemo.frame = CGRect(x: 0, y: self.view.frame.size.height-250, width: self.view.frame.size.width, height: 250)
            //            })
        }
            
            
        else if indexPath.section == 2
        {
            
            
            if indexPath.row == 0 {
                
                let ChangePasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePassword") as! ChangePassword
                self.navigationController?.pushViewController(ChangePasswordViewController, animated: true)
                
            }
            else{
                
                let storyboard = UIStoryboard(name: "Settings", bundle: nil)
                let EditProfileViewController =  storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
                self.navigationController?.pushViewController(EditProfileViewController, animated: true)
            }
            
        }else if indexPath.section == 3
        {
            
        }
        
        
        
        
    }
    
    // MARK: - switch Changed

    @objc func switchChanged(_ sender: UISwitch) {
        
        let userdefault = UserDefaults.standard
        if sender.isOn
        {
                userdefault.set(true, forKey:"FaceIdEnable")
        }
        else
        {
            userdefault.set(false, forKey:"FaceIdEnable")
        }
        
        userdefault.synchronize()
        
    }
    
}
