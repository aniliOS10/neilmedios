//
//  RouteVC.swift
//  MedMobilie
//
//  Created by dr.mac on 19/02/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//


import UIKit
import MapKit
import SwiftyJSON
import GoogleMaps
import IoniconsKit

class RouteVC: InterfaceExtendedController,CLLocationManagerDelegate{
                     
    fileprivate var marker = [CustomMarker]()
    fileprivate var placeData : [JSON]?
    fileprivate var tableData = [JSON]()
    fileprivate var isFlipped: Bool = false
    fileprivate var firstPlace : Bool = false
    fileprivate var currentCordinate : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
    
    // variables
    
    var parama = [String:Any]()

    // storyBoard Outlets
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet fileprivate var tableView : UITableView?
    @IBOutlet fileprivate var TableViewOnFlip : UIView?
    
    // MARK: - Location Manager
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.activityType = .automotiveNavigation
        _locationManager.distanceFilter = 10.0  // Movement threshold for new events
        //  _locationManager.allowsBackgroundLocationUpdates = true // allow in background
        _locationManager.requestWhenInUseAuthorization()
        _locationManager.startUpdatingLocation()
        return _locationManager
    }()
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.startUpdatingLocation()

        mapView.delegate = self
        tableView?.delegate = self
        tableView?.dataSource  = self
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.estimatedRowHeight = 150
        tableView?.separatorStyle = .none
        tableView?.allowsMultipleSelection = false
        
        rightButtonWithIonicon(name: .map)
        mapView.settings.zoomGestures = true
       
        //HB CHANGES SHOW USER LOCATION
        mapView.isMyLocationEnabled = true //hb
        
        AppDelegate.showWaitView()
        self.dataFetch()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: PlanNAVBAR(), BtnColor: UIColor.white)
      //  self.NavBarIcon()
        
    }
    
    // MARK: - Navigationbar Title Language Setting
    
    
    @objc override func LanguageSet(){
        NavigationBarTitleName(Title: "Route Plan")
    }
    
    override func rightButtonAction() {
        print("hii")
        isFlipped = !isFlipped
        
        let cardToFlip = isFlipped ? mapView : TableViewOnFlip
        let bottomCard = isFlipped ? TableViewOnFlip : mapView
        
        UIView.transition(from: cardToFlip!,
                          to: bottomCard!,
                          duration: 0.5,
                          options: [.transitionFlipFromRight, .showHideTransitionViews]) { (status) in
                            if self.isFlipped
                            {
                                self.navigationItem.rightBarButtonItem = nil
                                self.rightButtonWithIonicon(name: .iosListOutline)
                                self.tableView?.reloadData()
                            }
                            else
                            {
                                self.navigationItem.rightBarButtonItem = nil
                                self.rightButtonWithIonicon(name: .map)
                            }
        }
    }
    // MARK: - Visit Route List Methods

    
    fileprivate func dataFetch()
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        DataFetch_Api().Fetch(ID: 0, urlString: "/visitroutelist"){ (status, message, data) in
            
            AppDelegate.hideWaitView()
            if !status
            {
                self.popupAlert(title: Bundle.appName(), message: message, actionTitles:[self.languageKey(key: "ok")], actions:[{action1 in
                    self.navigationController?.popViewController(animated: true)
                    }, nil])
                return
            }
            if data != nil
            {
                
                self.placeData = data ?? nil
                
                
//                let markers1 = GMSMarker()
//
//                if (self.mapView.myLocation)?.coordinate != nil {
//                    markers1.position = (self.mapView.myLocation)!.coordinate
//
//                }
//                markers1.map = self.mapView
//
//                var currentBounds = GMSCoordinateBounds()
//
//                currentBounds = currentBounds.includingCoordinate(markers1.position)
                
              //  let update1 = GMSCameraUpdate.fit(currentBounds, withPadding: 100)
             //   self.mapView.animate(with: update1)

                
                
                let positondata = self.placeData?.enumerated().filter({ (index , position) -> Bool in
                    
                self.mapView.camera = GMSCameraPosition.camera(withLatitude:                   self.placeData?[0]["lattitude"].doubleValue ?? 0.0  , longitude:  self.placeData?[0]["longitude"].doubleValue ?? 0.0 , zoom: 5.0)
                    
                print("index is \(index)")
                print("position is \(position)")

                self.MarkerOnLocation(jsonPosition: position , index: index)
                
                    return true
                })

                
                var bounds = GMSCoordinateBounds()

                for index in 0..<(data!.count) {

                    let latitude = data![index]["lattitude"].doubleValue
                    let longitude = data![index]["longitude"].doubleValue

                    let markers = GMSMarker()
                    markers.position = CLLocationCoordinate2D(latitude:latitude, longitude:longitude)
                    markers.map = self.mapView
                    bounds = bounds.includingCoordinate(markers.position)
                }
//                let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
//                self.mapView.animate(with: update)
                                                
                var markerSort = [CustomMarker]()
                
                markerSort = self.marker
                self.marker.removeAll()
                self.marker = markerSort.sorted(by: { $0.distance < $1.distance })

                print("positondata is \(self.marker)")

                if self.marker.count > 0
                {
                    print("came in ")
                    
                    if (self.mapView.myLocation)?.coordinate != nil {
                        self.drawToLocation(originPosition: (self.mapView.myLocation)!.coordinate, destinationPosition: self.marker[0].position)

                    }
                }
                
                print("current mylocation is \((self.mapView.myLocation)!.coordinate)")
                
                print("marker position is \(self.marker[0].position)")
                
                if self.marker.count >= 2
                {
                    for mark in 0...self.marker.count - 1
                    {
                       // self.marker[0].
                        if self.marker[mark] != self.marker.last
                        {
                            self.drawToLocation(originPosition: self.marker[mark].position, destinationPosition: self.marker[mark + 1].position)
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Marker On Location Methods
    
    fileprivate func MarkerOnLocation(jsonPosition : JSON , index : Int)
    {
        let tempMarker = CustomMarker()
        tempMarker.position = CLLocationCoordinate2D(latitude: jsonPosition["lattitude"].doubleValue, longitude: jsonPosition["longitude"].doubleValue )
        tempMarker.title = jsonPosition["visit_name"].stringValue
        tempMarker.snippet = "Visit \(index + 1)"
        
        let myLocation = CLLocation(latitude: currentCordinate.latitude, longitude: currentCordinate.longitude)

        let myBuddysLocation = CLLocation(latitude: jsonPosition["lattitude"].doubleValue, longitude: jsonPosition["longitude"].doubleValue)

        let distance = myLocation.distance(from: myBuddysLocation) / 1000
        print(String(format: "The distance to my is %.01fkm", distance))
        print(String(format: "Name %@", jsonPosition["visit_name"].stringValue))

        tempMarker.distance = Float(distance)
        
        self.tableData.append(jsonPosition)
        marker.append(tempMarker)
        tempMarker.map = mapView
    }
    
    // MARK: - Direction Path Delegate Methods
    func drawToLocation(originPosition : CLLocationCoordinate2D , destinationPosition : CLLocationCoordinate2D)
    {
        let origin = "\(originPosition.latitude),\(originPosition.longitude)"
        let Destination = "\(destinationPosition.latitude),\(destinationPosition.longitude)"
        
        let mapUrl = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(Destination)&mode=driving&key=\(AppConnectionConfig.googleMapKeyplace)"
        
        ServerProcessor().request(.get, mapUrl, parameters: nil, encoding: .httpBody, headers: nil, authorize: false) { (success, response) in
            
            guard let rawInfo = response
                else{
                    return
            }
            
            guard let routes = rawInfo["routes"].array else{return}
            for route in routes
            {
                let routeOverviewPolyline = route["overview_polyline"].dictionary
                let points = routeOverviewPolyline?["points"]?.stringValue
                let path = GMSPath.init(fromEncodedPath: points!)
                
                let polyline = GMSPolyline(path: path)
                polyline.strokeColor = PlanNAVBAR()
                polyline.strokeWidth = 2.0
                polyline.map = self.mapView
            }
        }
    }
}

// MARK: - Location Manager Delegate Methods

extension RouteVC
{
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
        print("Error" + error.description)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last
        
        currentCordinate = userLocation?.coordinate ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
        print("Latitude :- \(userLocation!.coordinate.latitude)")
        print("Longitude :- \(userLocation!.coordinate.longitude)")
        
       // locationManager.stopUpdatingLocation()
    }
}

// MARK: - Table View Delegate/DataSource Methods

extension RouteVC : UITableViewDelegate , UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let json = tableData[indexPath.row]
      
        let lat = json["lattitude"].doubleValue
        let long = json["longitude"].doubleValue
        let name = json["visit_name"].stringValue
        self.directionCall(destinationLat: lat, destinationLong: long, titleName: name)
        return
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? PlaceListCell else{return PlaceListCell()}
        cell.selectionStyle = .none
            let json = self.tableData[indexPath.row]
        cell.lblTitleName?.text = json["visit_name"].stringValue
        cell.lblCategoryName?.text = "Visit \(indexPath.row + 1)"
        cell.viewShadow?.bottomViewShadow(ColorName: .gray)

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

// MARK: - GMSMapView Delegate Method

extension RouteVC : GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        self.locationManager.startUpdatingLocation()
        if let markingPoint = marker as? CustomMarker
        {
            let lat = markingPoint.position.latitude
            let long = markingPoint.position.longitude
            
            directionCall(destinationLat: lat, destinationLong: long, titleName: markingPoint.title ?? "")
           return
        }
    }
    // MARK: - Direction Path Delegate Methods
    
  fileprivate  func directionCall(destinationLat : Double, destinationLong : Double , titleName : String)
    {
        self.popupAlertWithSheet(title: Bundle.appName(), message: titleName, actionTitles: [self.languageKey(key: "Maps"),self.languageKey(key: "Google Maps")] ,actions: [{ actions1 in
            
            let source = MKMapItem(placemark: MKPlacemark(coordinate: self.currentCordinate))
            source.name = "Current location".languageSet
            
            let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLong)))
            
            
            destination.name = titleName
            
         //   MKMapItem.openMaps(with: [source, destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
            
                    
                    let coordinate = CLLocationCoordinate2DMake(destinationLat,destinationLong)
                                let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary: nil))
                                mapItem.name = titleName
                                mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
            
            
            
            },                                                                                                                                                                                {action2 in
                     if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {                                                                                                                 let urlbase = URL(string: "comgooglemaps://?saddr=&daddr=\(destinationLat),\(destinationLong)")
                        
                        UIApplication.shared.open(urlbase!, options: [:], completionHandler: nil)
                        
                     } else {
                        let webUrl = URL(string: "https://www.google.co.in/maps/dir/?saddr=\(self.currentCordinate.latitude),\(self.currentCordinate.longitude)&daddr=\(destinationLat),\(destinationLong)")
                       
                        UIApplication.shared.open(webUrl!, options: [:], completionHandler: nil)
                        
                }
            },nil])
    }
    
}

// MARK: - Class Instance


extension RouteVC
{
    class func instance()->RouteVC?{
        let storyboard = UIStoryboard(name: "PlanMyDay", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "RouteVC") as? RouteVC
        //self.definesPresentationContext = true
        
        return controller
    }
}
