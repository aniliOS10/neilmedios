

import UIKit

 func SystemFont(FontSize: CGFloat) -> UIFont {
    return UIFont.systemFont(ofSize: FontSize)
}

func BoldSystemFont(FontSize: CGFloat) -> UIFont {
    return UIFont.boldSystemFont(ofSize: FontSize)
}

func ItalicSystemFont(FontSize: CGFloat) -> UIFont {
    return UIFont.italicSystemFont(ofSize: FontSize)
}



