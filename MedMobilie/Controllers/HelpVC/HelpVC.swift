//
//  HelpVC.swift
//  MedMobilie
//
//  Created by MAC on 12/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit
import WebKit
import MMDrawerController

class HelpVC: InterfaceExtendedController,WKUIDelegate {

    var webView: WKWebView!
    
    // MARK: - Class life cycle

    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        self.view = webView
    }
    
    let lbl_view : UILabel = {
        
        let  destxtview = UILabel()
        destxtview.numberOfLines = 40
        destxtview.font = UIFont.boldSystemFont(ofSize: 15)
        destxtview.textAlignment = .center
        destxtview.translatesAutoresizingMaskIntoConstraints = false
        return destxtview
    }()
    
    
    func decorateText(sub:String, des:String)->NSAttributedString{
        
        let textAttributesOne = [NSAttributedString.Key.foregroundColor: UIColor.darkText, NSAttributedString.Key.font: BoldSystemFont(FontSize: 18)]
        
        let textAttributesTwo = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: SystemFont(FontSize: 16)]
        
        let textPartOne = NSMutableAttributedString(string: sub, attributes: textAttributesOne)
        let textPartTwo = NSMutableAttributedString(string: des, attributes: textAttributesTwo)
        
        let textCombination = NSMutableAttributedString()
        textCombination.append(textPartOne)
        textCombination.append(textPartTwo)
        return textCombination
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        let btSlider = UIBarButtonItem(image: UIImage.ionicon(with: .androidMenu, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), style: .plain, target: self, action: #selector(OpenSlider))
                btSlider.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = btSlider
                navigationController?.navigationBar.barTintColor = SettingNAVBAR()
        
        self.view .addSubview(lbl_view)
        
        
        
        lbl_view.attributedText = decorateText(sub: "", des: "\n\n\n Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")
        
        
         SetUpLayout()

        
    }
    
        private func SetUpLayout()
    {
        
        lbl_view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        lbl_view.leadingAnchor.constraint(equalTo:view.leadingAnchor, constant: 10).isActive = true
        lbl_view.trailingAnchor.constraint(equalTo:view.trailingAnchor, constant: -10).isActive = true
        lbl_view.heightAnchor.constraint(equalToConstant: 300).isActive = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        
        
    }
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Help Center")
    }
    
    @objc func OpenSlider(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    
    
}
extension HelpVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
       AppDelegate.hideWaitView()
        
}
}
