
import UIKit
import IoniconsKit
import MMDrawerController
import SwiftyJSON
import Photos
import SwipeCellKit


class SubmitLeaveVC: InterfaceExtendedController {
    
    fileprivate var selectedContact_array : [String]?
    fileprivate var temp_textField = UITextField()
    fileprivate var Pickerdatavalue = [JSON]()
    fileprivate var ContactName  = [String]()
    fileprivate var contactId = [Int]()
    fileprivate var dataList = [JSON]()
    
    let imagePicker = UIImagePickerController()
    var Leave_id = 0
    var updatecheck = ""
  
    var indexvalu = 0
    var RowValue = 0
    var ContactString = String()
    var Datevalue = String()
    var Timevalue = String()
    var ImgArray = [UIImage]()
    var ImgSaveArray = [String]()
    var selectedCountryId : Int = 0
    var dataPickerView = UIPickerView()

    var Date_Picker = UIDatePicker()
    var arrSctionTitle = NSArray()
    var arrTitle = NSArray()
    var  cell = UITableViewCell()
    var toolBar = UIToolbar()
    var delegate  : AddContactsToCreateOrderProtocol?
    var DummyArray = NSMutableArray ()
    var ContactArray = [String : Any]()
    var dicSet = NSMutableDictionary()
    var skippedArray : NSMutableArray =         ["","","","","","",""]
    var SaveArray : NSMutableArray =         ["","","","","","",""]
    
    var BackDataBlock: ((_ json : [JSON]) -> ())?
    
    @IBOutlet weak var mTableview: UITableView!
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createPickerView()
        self.mTableview.delegate = self
        self.mTableview.dataSource = self
        self.mTableview.tableFooterView = UIView()
        self.mTableview.separatorColor = .clear
        
        if updatecheck == ""
        {
        RightActionButton(Title:languageKey(key:"Submit"))
        }
        else if updatecheck == "1"
        {
            RightActionButton(Title:languageKey(key:"UPDATE"))
        }
        else
        {
            self.mTableview.isUserInteractionEnabled = false
        }
        
        print("Save : ---->",SaveArray)
        print("Skip : ---->",skippedArray)
        
        Listsapidata()
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        
    }
    
    // MARK: - Language Setting Function
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Leave Application")
        
        arrSctionTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Description", comment: "") + "*"]
        
        arrTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Type of leave", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Start Date", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "End Date", comment: "") + "*"]
        
    }
    // MARK: - Fetching Leave Type List
    
    fileprivate func Listsapidata()
    {
        if !NetworkState.isConnected() {
            
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        DataFetch_Api().Fetch( ID: 0 , urlString: "/leave_typelist", completion:  { (status, message, data) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                return
            }
            self.dataList = data!
            
        })
    }
    // MARK: - Create PickerView
    
    func createPickerView(){
        
        dataPickerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216)
        dataPickerView.delegate = self
        dataPickerView.dataSource = self
        dataPickerView.backgroundColor = UIColor.white
        dataPickerView.showsSelectionIndicator = true
                
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: self.languageKey(key: "Done"), style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: self.languageKey(key: "Cancel"), style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
                
        if #available(iOS 13.4, *) {
            if #available(iOS 14.0, *) {
                Date_Picker.preferredDatePickerStyle = .wheels
            } else {
                Date_Picker.preferredDatePickerStyle = .wheels
            }
        } else {
            // Fallback on earlier versions
        }
        
        Date_Picker.datePickerMode = UIDatePicker.Mode.date
        Date_Picker.addTarget(self, action: #selector(datePickerChanged), for: UIControl.Event.valueChanged)
        
    }
    
    // MARK: - Label Border Line Method
    
    func DashedLineBorder (title: String)-> UILabel {
        
        let ContainerView = UILabel(frame: CGRect(x: 10, y: 10, width: mTableview.frame.size.width-20, height: 55))
        ContainerView.layer.addSublayer(Border(YourLable: ContainerView))
        ContainerView.textAlignment = .center
        ContainerView.font = UIFont.boldSystemFont(ofSize: 16)
        ContainerView.textColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        ContainerView.text = title
        return ContainerView
        
    }
    
    // MARK: - Create TextView Method
    
    func textView (Title: String)-> UITextView
    {
        let view = UITextView(frame: CGRect(x: 10, y: 10, width: mTableview.frame.size.width-20, height: 80))
        view.delegate = self
        view.backgroundColor = OffWhiteColor()
        view.text = Title
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.gray.cgColor
        view.font = UIFont.systemFont(ofSize: 15)
        return view
    }
    
    // MARK: - Create Billing Action (Save Button)
    
    override func rightButtonAction(){
        
        if skippedArray[0] as? String != "" && skippedArray[1] as? String != "" && skippedArray[2] as? String != "" && skippedArray[3] as? String != ""
        {
            AppDelegate.showWaitView()
            CreateLeaveRequest()
        }
        else
        {
            self.alert("All fields are required".languageSet)
        }
    }
    
    fileprivate func CreateLeaveRequest()
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        var parama = [String : Any]()
       
         parama["type_id"] = SaveArray[0]
        parama["startdate"] = SaveArray[1]
        parama["enddate"] = SaveArray[2]
        parama["description"] = SaveArray[3]
        parama["id"] = Leave_id

        if updatecheck == ""
        {
             parama["url"] = "/submitleave"
        }
        else{
             parama["url"] = "/editleave"
        }
       
        print("data :------>",parama)
        LeaveAddApi().Request(params: parama) { (status, message) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: message, actionTitles:[self.languageKey(key: "OK")], actions:[{action1 in
                    
                    }, nil])
                return
            }
            
            self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: message, actionTitles:[self.languageKey(key: "OK")], actions:[{action1 in
                self.navigationController?.popViewController(animated: true)
                }, nil])
           
    }
}

}

// MARK: - Create Lable Border Method

extension SubmitLeaveVC
{
    func Border(YourLable: UILabel) -> CAShapeLayer{
        
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.black.cgColor
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = YourLable.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: YourLable.bounds).cgPath
        
        return yourViewBorder
        
    }
    
}
// MARK: - Table View Delegate/DataSource Methods

extension SubmitLeaveVC: UITableViewDelegate,UITableViewDataSource
{
    // MARK: - Table view data source
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 3
       
        case 1:
            return 1
            
        default:
            return 0
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrSctionTitle.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = OffWhiteColor()
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: tableView.frame.width, height: 30)
        label.text = arrSctionTitle[section] as? String
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = UIColor.black
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return  5
        }
        else
        {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if indexPath.section == 2 {
            return  100
        }
            
        else
        {
            return 75
        }
    }
    
    func ShowDropDownImage(Image: Ionicons) -> UIImageView {
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 40, width: 30, height: 30)
        imageView.image = UIImage.ionicon(with: Image, textColor: UIColor.black, size: CGSize(width: 50, height: 50))
        
        return imageView
        
    }
    
    func HideDropDownImage() -> UIImageView {
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 20, width: 1, height: 1)
        imageView.image = UIImage.ionicon(with: .iosArrowDown, textColor: .clear, size: CGSize(width: 1, height: 1))
        
        return imageView
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
            
            let str2 = arrTitle[indexPath.row] as? String
            
            cell.lbtitleAC.text = arrTitle[indexPath.row] as? String
           
            cell.txtTitleAC.text = skippedArray[indexPath.row] as? String
            cell.txtTitleAC.tag = indexPath.row
            cell.txtTitleAC.delegate = self
            cell.selectionStyle = .none
            cell.txtTitleAC.keyboardType = .default
            
            if indexPath.row == 0
            {
                
                cell.txtTitleAC.rightViewMode = .always
                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosArrowDown)
                self.pickUpDate(cell.txtTitleAC)
                
            }
            else if indexPath.row == 1
            {
                
                cell.txtTitleAC.rightViewMode = .always
                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosCalendarOutline)
                self.pickUpDate(cell.txtTitleAC)
                        
            }
            else if indexPath.row == 2
            {
                cell.txtTitleAC.rightViewMode = .always
                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosCalendarOutline)
                self.pickUpDate(cell.txtTitleAC)
                
            }
            return cell
        }
                
        else  if indexPath.section == 1
        {
            
            let myCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
            myCell.backgroundColor = UIColor.white
            myCell.selectionStyle = .none
            
            myCell.contentView .addSubview(textView(Title: skippedArray[3] as! String))
            return myCell
            
        }
        return cell
    }
}

// MARK: - TextField Delegate Methods

extension SubmitLeaveVC : UITextFieldDelegate
{
    // datepickerView
    
    func pickUpDate(_ textField : UITextField){
        
        print("TagValue:----->>>",textField.tag)
    }
    
    func CurrentDate()->String{
        let currentDateTime = Date()
        
        // initialize the date formatter and set the style
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: currentDateTime)
        
    }
    
    @objc func datePickerChanged(datePicker:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        Datevalue = dateFormatter.string(from: datePicker.date)
        
        print(Datevalue)
        
    }
   
    @objc func doneClick() {
        
        if indexvalu == 0 {
            
            skippedArray[indexvalu] = Pickerdatavalue[RowValue]["type_name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[RowValue]["type_id"].stringValue
            
            print(Pickerdatavalue)
        }
        else if indexvalu == 1
        {
            
            if Datevalue == ""
            {
                skippedArray[1] = CurrentDate()
                SaveArray[1] = CurrentDate()
            }
            else
            {
                skippedArray[1] = Datevalue
                SaveArray[1] = Datevalue
            }
        }
        else if indexvalu == 2
        {
            if Datevalue == ""
            {
                skippedArray[2] = CurrentDate()
                SaveArray[2] = CurrentDate()
            }
            else
            {
                skippedArray[2] = Datevalue
                SaveArray[2] = Datevalue
            }
            
        }
        if indexvalu == 0
        {
            self.dataPickerView.reloadAllComponents()
            
            self.dataPickerView.selectRow(0, inComponent: 0, animated: false)
        }
        
        Timevalue = ""
        RowValue = 0
        mTableview.reloadData()
        
        self.view .endEditing(true)
        
    }
    @objc func cancelClick() {
        
        Timevalue = ""
        
        if indexvalu == 0
        {
            
            self.dataPickerView.reloadAllComponents()
            
            self.dataPickerView.selectRow(0, inComponent: 0, animated: false)
            
        }
        RowValue = 0
        self.view .endEditing(true)
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print("textField-----> ",textField.tag)
        
        indexvalu = textField.tag
        
        if textField.tag == 0 {
            
            textField.inputView = self.dataPickerView
            textField.inputAccessoryView = self.toolBar
            Pickerdatavalue = self.dataList
            
            print(Pickerdatavalue)
            print(Pickerdatavalue.count)
            
            self.dataPickerView.reloadAllComponents()
        }
            
        else if textField.tag == 1 || textField.tag == 2
        {
            textField.inputView = self.Date_Picker
            textField.inputAccessoryView = self.toolBar
            Date_Picker.reloadInputViews()
            
        }
            
        else
        {
            textField.inputView = nil
            textField.inputAccessoryView = nil
            self.Date_Picker.removeFromSuperview()
            self.dataPickerView.removeFromSuperview()
            self.toolBar.removeFromSuperview()
            
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        print("shouldChangeCharactersIn :---",textField.tag)
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if textField.tag < 100
            
        {
            skippedArray[textField.tag] = newString
            SaveArray[textField.tag] = newString
        }
        else
        {
            ContactString = newString as String
        }
        
        print(skippedArray)
        
        return true
    }
    
}
// MARK: - PickerView Delegate/DataSource Methods


extension SubmitLeaveVC : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if !Pickerdatavalue.isEmpty
        {
            return Pickerdatavalue.count
            
        }
        return 0
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Pickerdatavalue[row]["type_name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        RowValue = row
        
    }
}
// MARK: - TextView Delegate Methods


extension SubmitLeaveVC : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
        print("Text:------>  ",text)
        
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        skippedArray[3] = changedText
        SaveArray[3] = changedText
        
        print("TextView:------>  ",changedText)
        
        return true
    }
    
}
// MARK: - Class Instance


extension SubmitLeaveVC
{
    class func instance()->SubmitLeaveVC?{
        
        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SubmitLeaveVC") as? SubmitLeaveVC
        return controller
    }
}

