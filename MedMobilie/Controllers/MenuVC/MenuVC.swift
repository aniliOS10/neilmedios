
import UIKit
import IoniconsKit
import MMDrawerController
import SDWebImage

class MenuVC: InterfaceExtendedController {
    
  
    var gender : String? = "Male_PH"
    var arrMenuList = NSArray()
    var arrMenuList_IMG : [Ionicons] = [.iosHomeOutline,.iosBoxOutline,.androidNotificationsNone,.paperAirplane,.iosCogOutline,.iosHelpOutline,.iosHelpOutline]
    
    
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var backHolderImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserAddress: UILabel!
    
    
    // MARK: - Class life cycle

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(setDetails), name: NSNotification.Name( "Updateprofile"), object: nil)
        
       self.view.backgroundColor = OffWhiteColor()
        self.mTableView.backgroundColor = OffWhiteColor()
        lblUserName.font = BoldSystemFont(FontSize: 30)
        
        self.mTableView.separatorStyle = .none
        setDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       self.userImage.createCircleForView()
        
    }
    
    
    // MARK: - Language Setup Method

    
    @objc override func LanguageSet(){
        
        arrMenuList = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Dashboard", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Product", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Notifications", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Apply Leave", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Settings", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Help", comment: ""),"Privacy Policy".languageSet]
        
        self.mTableView.reloadData()
        
    
    }
    
    // MARK: - Set User Deatils 

    
    @objc func setDetails()
    {
        guard let info = SignedUserInfo.sharedInstance else{return}
        lblUserName.text = info.username
        
        
        print(info.gender as Any)
        
        if info.gender != "male"
        {
            gender = "Female_PH"
        }
        //
        
        let imageUrl = URL(string: info.userImg ?? "")
        userImage.sd_setImage(with: imageUrl, placeholderImage:UIImage(imageLiteralResourceName: gender ?? "Male_PH" ), options: .cacheMemoryOnly, completed: nil)
        // self.userImage.createCircleForView()
    }
    
    
    // MARK: - elDrawer Methods
    
    func navToTabBar (vc:UIViewController) {
        //   selectedIndexTab = 0
        elDrawer?.mainViewController = UINavigationController(rootViewController: vc)
        elDrawer?.setDrawerState(.closed, animated: true)
    }

    
}

// MARK: - Table View Delegate/DataSource Methods


extension MenuVC: UITableViewDelegate,UITableViewDataSource
{

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuList_IMG.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        
        
        cell.Title_LBL.font = SystemFont(FontSize: 20)
        cell.Title_LBL.textColor = ColorValue(Rvalue:61, Gvalue: 140, Bvalue: 250, alphavalue: 0.9)
        cell.Title_LBL.text = arrMenuList[indexPath.row] as? String
        
        cell.Title_IMG.image =   UIImage.ionicon(with: arrMenuList_IMG[indexPath.row], textColor: UIColor.black, size: CGSize(width: 50, height: 50))
            
            
            
          //  UIImage(named: "\(arrMenuList_IMG[indexPath.row])")
        
        cell.backgroundColor = OffWhiteColor()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 60))
        footerView.backgroundColor = ColorValue(Rvalue: 236, Gvalue: 236, Bvalue: 236, alphavalue: 1.0)
        
        let Fake_btn:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 60))
        Fake_btn.backgroundColor = .clear
        Fake_btn.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
        footerView.addSubview(Fake_btn)
        
    
        var imageView : UIImageView
        imageView  = UIImageView(frame: CGRect(x: (footerView.frame.size.width/2)-70, y: 13, width: 34, height: 34))
        
        imageView.image = UIImage.ionicon(with: .androidExit, textColor: ColorValue(Rvalue:0, Gvalue: 0, Bvalue: 0, alphavalue: 0.6), size: CGSize(width: 35, height: 35))
        footerView.addSubview(imageView)

        
        
        let lblLogout = UILabel(frame: CGRect(x: imageView.frame.origin.x+50, y: 10, width: 150, height: 40))
        lblLogout.textAlignment = .left
        lblLogout.font = BoldSystemFont(FontSize: 20)
       
        lblLogout.textColor = ColorValue(Rvalue:61, Gvalue: 140, Bvalue: 250, alphavalue: 1.0)
        lblLogout.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Logout", comment: "")
        
        
        footerView.addSubview(lblLogout)
        
    return footerView
        
    }
    
    
    
    @objc func buttonClicked(sender: UIButton!) {
        
        self.logOut()
        
      
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 60
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
       
        //            let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //            appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
        
//        indexvalue = indexPath.row
//        
//        
//        print(indexvalue)
       
        if indexPath.row  == 0 {
            
            let objAppDelegate = UIApplication.shared.delegate as? AppDelegate
            objAppDelegate?.buildNavigationDrawer()
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: indexPath.row)
            }
        }
       
        if indexPath.row  == 1 {
            
            let objAppDelegate = UIApplication.shared.delegate as? AppDelegate
            objAppDelegate?.buildNavigationDrawer()
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 3)
            }
            
        }
        
        
        
//        if indexPath.row  < 2 {
//
//            let objAppDelegate = UIApplication.shared.delegate as? AppDelegate
//            objAppDelegate?.buildNavigationDrawer()
//            DispatchQueue.main.async {
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: indexPath.row)
//
//            }
//        }
       
        if indexPath.row == 2 {
            let storyboard = UIStoryboard(name: "Settings", bundle: nil)
            let ProductDetailsVC = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as? NotificationVC
            
            
            
            let aboutNavController = UINavigationController(rootViewController: ProductDetailsVC!)
            
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.drawerContainer!.centerViewController = aboutNavController
            appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
            
           
            
            
        }
         else if indexPath.row == 3 {
            
             let storyboard = UIStoryboard(name: "Settings", bundle: nil)
            let LeaveListVC = storyboard.instantiateViewController(withIdentifier: "LeaveListVC") as? LeaveListVC
            
            let aboutNavController = UINavigationController(rootViewController: LeaveListVC!)
            
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.drawerContainer!.centerViewController = aboutNavController
            appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
            
            
        }
        
        else if indexPath.row == 4 {
            
            let SettingViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            
            let aboutNavController = UINavigationController(rootViewController: SettingViewController)
            
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.drawerContainer!.centerViewController = aboutNavController
            
            appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
            
        }
        else if indexPath.row == 5 {
           
            let storyboard = UIStoryboard(name: "Visit", bundle: nil)
            let ProductDetailsVC = storyboard.instantiateViewController(withIdentifier: "WebViewVC") as? WebViewVC
            let urlstring = "https://salesrep.NeilMed.com/board"
            let url = NSURL(string: urlstring)
            ProductDetailsVC!.linkUrl = url as URL?
            ProductDetailsVC!.MenuButton = true
            let aboutNavController = UINavigationController(rootViewController: ProductDetailsVC!)
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.drawerContainer!.centerViewController = aboutNavController
            appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else if indexPath.row == 6 {
            
            
            
            let storyboard = UIStoryboard(name: "Visit", bundle: nil)
            let ProductDetailsVC = storyboard.instantiateViewController(withIdentifier: "WebViewVC") as? WebViewVC
            
            
            let urlstring = "http://www.NeilMed.com/usa/policy-privacy.php"
            let url = NSURL(string: urlstring)
            
            ProductDetailsVC!.linkUrl = url as URL?
            ProductDetailsVC!.MenuButton = true
            ProductDetailsVC!.titleName = "Privacy Policy"
            let aboutNavController = UINavigationController(rootViewController: ProductDetailsVC!)
            
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.drawerContainer!.centerViewController = aboutNavController
            
            appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
            
        }
        
    }
    
    func logOut () {
        
        self.popupAlert(title: languageKey(key: Bundle.appName()), message: languageKey(key: "Are you sure you want to log out?"), actionTitles: [languageKey(key: "NO"),languageKey(key: "YES")], actions: [{action1 in
            return
            },{action2 in
                if !NetworkState.isConnected() {
                    AppDelegate.hideWaitView()
                    AppDelegate.alertViewForInterNet()
                    return
                }
                
                Logout_api().Request(completion: { (status, message) in
                    if !status
                    {
                        self.alert(message)
                        return
                    }
                    DispatchQueue.global(qos: .background).async {
                        DispatchQueue.main.async {
                    UserDefaults.standard.set("no", forKey: "Check_in")
                    UserDefaults.standard.synchronize()
                    
                    guard let info = SignedUserInfo.sharedInstance else{return}
                    info.clear()
                    RootControllerManager().setRoot()

                    return
                }
                    }})
            }, nil])
        
        
    }
    
    
    
   
}
