//
//  OrderDetailsVC.swift
//  MedMobilie
//
//  Created by dr.mac on 10/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//
//
//  OrderDetailsVCViewController.swift
//  MedMobilie
//
//  Created by dr.mac on 08/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import SwiftyJSON
import IoniconsKit
import SDWebImage
import DGElasticPullToRefresh
import iOSDropDown
import Stripe
class OrderDetailsVC:InterfaceExtendedController {
    
    fileprivate let sectionCount = 2
    fileprivate var order_id : Int? = 0


    var ProductCartData : [JSON]?
    var contact_name : String?
    var ID  = 0
    var TotalPrice : String?
     var customer_id : String?
     var product_price : String?
     var no_of_cases : String?
     var product_name : String?
    
   
    @IBOutlet fileprivate var lblContact : UILabel?
    @IBOutlet fileprivate var lblOrder_id : UILabel?
    @IBOutlet fileprivate var lblTotal : UILabel?
    @IBOutlet fileprivate var lblDiscount : UILabel?
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    
            AppDelegate.showWaitView()
        self.ProductCartDataFetch(idForDetails: ID )
        
       
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        
        
        
        
        self.lblContact?.text = contact_name
        self.lblOrder_id?.text =  customer_id
         if !NetworkState.isConnected() {
            
            guard let symbol = SignedUserInfo.sharedInstance?.currency_symbol  else{return}
            self.lblTotal?.text = "\(symbol) \(TotalPrice!)"
         
        }
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: OrderNAVBAR(), BtnColor: UIColor.white)
        self.LanguageSet()
        
    }
    
    // MARK: - Language Setting
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Order Details")
        
    }
    
    // MARK: - Product Cart Data Fetch

    fileprivate func ProductCartDataFetch(idForDetails : Int)
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            return
        }
        OrderDetailsFetch_Api().Fetch(orderId: idForDetails, completion: { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                
                return
            }
            
            self.ProductCartData = data
            
            let datavalues = self.ProductCartData![0]["detail"][0]
            
            
           guard let symbol = SignedUserInfo.sharedInstance?.currency_symbol  else{return}
            self.lblTotal?.text = "\(symbol) \(datavalues["order_total"].doubleValue.round(to: 2))"
            
            
            let  order_total_original = "\(symbol) \(datavalues["order_total_original"].doubleValue.round(to: 2))"
            
            if (datavalues["order_total"].doubleValue != datavalues["order_total_original"].doubleValue) && (datavalues["order_total_original"].intValue != 0 || datavalues["order_total_original"].doubleValue != 0.0)
            {
                 self.lblDiscount!.attributedText = order_total_original.strikeThrough()
            }
            else
            {
                self.lblDiscount!.text = ""
            }
            
            self.lblDiscount!.attributedText = order_total_original.strikeThrough()
            
            self.tableView.reloadData()
            
        })
        
    }

}

// MARK: - Table View Delegate/DataSource Methods

extension OrderDetailsVC : UITableViewDelegate , UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !NetworkState.isConnected() {
            
            return 1
            
        }
        else
        {
        
        if (self.ProductCartData != nil)
        {
             return self.ProductCartData![0]["data"].count
        }
        
    return 0
        }
       
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if !NetworkState.isConnected() {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductList", for: indexPath) as? ShowCartCell
                else{return ShowCartCell()}
            
              guard let symbol = SignedUserInfo.sharedInstance?.currency_symbol  else{return cell}
          
            
            cell.lblProductName?.text = product_name
            cell.Units?.text = "\(languageKey(key: "No. Of Cases:")) \(no_of_cases!)"
            cell.Price?.text = "\(symbol)\(product_price!)"
            
            cell.selectionStyle = .none
            
            return cell
            
        }
        else
        {
        
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductList", for: indexPath) as? ShowCartCell
                else{return ShowCartCell()}
        
        
        let  attachmentDATA = self.ProductCartData![0]["data"][indexPath.row]
        
        print(attachmentDATA)
            
          //  guard let infos = attachmentDATA[indexPath.row] else{return cell}
            //        cell.viewShadow?.bottomViewShadow()
            guard let symbol = SignedUserInfo.sharedInstance?.currency_symbol  else{return cell}
        
           let discountPrice = (attachmentDATA["pricepercase"].doubleValue.round(to: 2) -  attachmentDATA["discountpercase"].doubleValue.round(to: 2)) * attachmentDATA["no_of_cases"].doubleValue.round(to: 2)
                
         let totalPrice = "\(symbol)\((attachmentDATA["pricepercase"].doubleValue.round(to: 2) * attachmentDATA["no_of_cases"].doubleValue.round(to: 2)).round(to: 2))"
                
        
        if attachmentDATA["discountpercase"].doubleValue != 0.0 || attachmentDATA["discountpercase"].intValue != 0
        {
            cell.Discount?.attributedText = "\(totalPrice)".strikeThrough()
        }
        else
        {
            cell.Discount?.text = ""
        }
        
        
        cell.Price?.text = "\(symbol)\(discountPrice.round(to: 2))"
        
            cell.Units?.text = "\(languageKey(key: "No. Of Cases:")) \(attachmentDATA["no_of_cases"].stringValue)"
            cell.lblProductName?.text = attachmentDATA["product_name"].stringValue
            
            
            cell.imgProductimg?.sd_setImage(with: attachmentDATA["product_image"].url, placeholderImage: UIImage(imageLiteralResourceName: "Product_PH"), options:.cacheMemoryOnly, completed: nil)
            //cell.imgProductimg?
            
            // cell.viewShadow?.backgroundColor = navBackgroundColor
            
            // cell.lblTask?.text = numbers[indexPath.row]
            
            cell.selectionStyle = .none
            
            return cell
        }
        
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0
        {
            return UITableView.automaticDimension
        }
        else if indexPath.section == 1
        {
            return UITableView.automaticDimension
        }
        return 0
    }
   
}

// MARK: - Class instance
extension OrderDetailsVC
{
    class func instance()->OrderDetailsVC?{
        let storyboard = UIStoryboard(name: "Products", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "OrderDetailsVC") as? OrderDetailsVC
        
        
        return controller
    }
}

extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}
