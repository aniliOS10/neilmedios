//
//  EditProfileVC.swift
//  MedMobilie
//
//  Created by dr.mac on 28/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.

// bhushan Code

import UIKit
import SDWebImage
import SwiftyJSON
import IoniconsKit
class EditProfileVC: InterfaceExtendedController {
    
    fileprivate var dataList = [JSON]()
    fileprivate var Pickerdatavalue = [JSON]()
    fileprivate var currentVC: UIViewController?
    fileprivate var param = [String : Any]()
    fileprivate var keys = ["email","name","phone","address1","address2","city","zip_code","state_id","country_id","fax"]
   
    let imagePicker = UIImagePickerController()
    
    var ProfileData : [JSON]?
    var indexvalu = 0
    var RowValue = 0
    var selectedCountryId : Int = 0
    var arrSctionTitle = NSArray()
    var gender : String? = "Male_PH"
    var dataPickerView = UIPickerView()
    var toolBar = UIToolbar()
    
    var skippedArray : NSMutableArray =         ["","","","","","","","","",""]
    var SaveArray : NSMutableArray =             ["","","","","","","","","",""]
    
   
  
    @IBOutlet weak var mTableview: UITableView!
    @IBOutlet weak var imgUserPic: UIImageView!
    
    
    
   // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        imagePicker.delegate = self
        NavigationBarTitleName(Title: "Edit Profile")
      
        
        imgUserPic.createCircleForView()
        imgUserPic.isUserInteractionEnabled = true
        arrSctionTitle = [languageKey(key:"Email id"),languageKey(key:"Name"),languageKey(key:"Phone Number"),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Address 1", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Address 2", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "City", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Zip/Postal Code", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Country", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "State", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Fax", comment: "")]
        
        let UserPicGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UserPicAction))
        UserPicGestureRecognizer.numberOfTapsRequired = 1
        imgUserPic.addGestureRecognizer(UserPicGestureRecognizer)
        
        
        mTableview.delegate = self
        mTableview.dataSource = self
        mTableview.tableFooterView = UIView()
        mTableview.separatorStyle = .none
        
        
        AppDelegate.showWaitView()

        self.Listsapidata()

        ProfileFetch()
        
        RightActionButton(Title:languageKey(key:"Save"))
        
         createPickerView()
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: navBackgroundColor ?? SettingNAVBAR(), BtnColor: UIColor.white)
        
        
    }
    
    
    
    // MARK: - Fetch  Data From Local Databas
    
    fileprivate func Listsapidata()
    {
        
       
        DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.ListingData.rawValue) { (data) in
            
            if let object = data
            {
                var jsonArray = [JSON]()
                for jsonData in object
                {
                    if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                        jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                            jsonArray.append(json!)
                        })})
                    }
                }
                
                self.dataList = jsonArray
            }
        }
    }
    
    
    // MARK: - Create Picker View

    
    func createPickerView(){
        dataPickerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216)
        dataPickerView.delegate = self
        dataPickerView.dataSource = self
        dataPickerView.backgroundColor = UIColor.white
        dataPickerView.showsSelectionIndicator = true
        
        
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        
    
        
    }
    
    
    // MARK: - Done button Click on PickerView

    
    @objc func doneClick() {
        
        
       
            
         if indexvalu == 7
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            skippedArray[indexvalu + 1] = ""
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            SaveArray[indexvalu + 1] = ""
           selectedCountryId = Pickerdatavalue[0][RowValue]["id"].intValue
            
            
            print(Pickerdatavalue)
            
        }
            
        else if indexvalu == 8
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
             print(Pickerdatavalue)
            print(Pickerdatavalue)
            
        }
      
        
            self.dataPickerView.reloadAllComponents()
            
            self.dataPickerView.selectRow(0, inComponent: 0, animated: false)
        
        RowValue = 0
        
        mTableview.reloadData()
        
        
        self.view .endEditing(true)
        
    }
    
    // MARK: - Cancel button Click on PickerView

    
    @objc func cancelClick() {
        
     self.dataPickerView.reloadAllComponents()
    self.dataPickerView.selectRow(0, inComponent: 0, animated: false)
        
        RowValue = 0
        self.view .endEditing(true)
        
    }
    
    
    // MARK: - Save/Update button

    
    override func rightButtonAction(){
     
        
        param = ["name":self.SaveArray[1],"phone":self.SaveArray[2],"address1":self.SaveArray[3],"address2":self.SaveArray[4],"city":self.SaveArray[5],"zip_code":self.SaveArray[6],"country_id":self.SaveArray[7],"state_id":self.SaveArray[8],"fax":self.SaveArray[9]]
        
       print(param)
        
         let img = imgUserPic.image ?? nil
        editProfileRequest(param , image: img)

        
        
        
    }
    
    // MARK: - Update Profile Request

    
    fileprivate func editProfileRequest(_ param : [String : Any], image : UIImage?)
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        ProfileUpdate_Api().request(param: param, image: image ?? nil) { (status, message, info) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                self.alert(message)
                return
            }
             self.navigationController?.popViewController(animated: true)
            
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Updateprofile"), object: nil)
        }
        
       
        
        
    }
    
    // MARK: - Get User Image Action

    
    @objc func UserPicAction(recognizer: UITapGestureRecognizer) {
        
        ActionSheet()
        
    }
    
    // MARK: - Open Action Sheet

    
    func ActionSheet()
    {
        
        let alert = UIAlertController(title: nil, message: languageKey(key: "Choose option"), preferredStyle: .actionSheet)
        
        
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Take Photo"), style: .default , handler:{ (UIAlertAction)in
            
            self.openCamera()
            
        }))
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Choose Photo"), style: .default , handler:{ (UIAlertAction)in
            
            self.photoLibrary()
            
        }))
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Delete Photo"), style: .destructive , handler:{ (UIAlertAction)in
            
            self.deletePhoto()
            
        }))
        
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Cancel") , style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        
        alert.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        
        self.present(alert, animated: true) {
            print("option menu presented")
        }
        
        
      //  self.present(alert, animated: true, completion: {
            
       // })
    }
    
  
    
    // MARK: - Fetch User Profile

    
   fileprivate func ProfileFetch()
    {
        //AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        ProfileDetailsFetch().fetch { (status, message, Data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message ?? "Someting went Wrong")
                return
            }
             self.ProfileData = Data
            guard let infos = self.ProfileData?[0]  else{return}
            
            self.skippedArray[0] = infos["email"].stringValue
             self.skippedArray[1] = infos["name"].stringValue
             self.skippedArray[2] = infos["phone"].stringValue
             self.skippedArray[3] = infos["address1"].stringValue
             self.skippedArray[4] = infos["address2"].stringValue
            self.skippedArray[5] = infos["city"].stringValue
            self.skippedArray[6] = infos["zip_code"].stringValue
            self.skippedArray[7] = infos["country_name"].stringValue
            self.skippedArray[8] = infos["state_name"].stringValue
            self.skippedArray[9] = infos["fax"].stringValue
            
            
            
            self.SaveArray[0] = infos["email"].stringValue
            self.SaveArray[1] = infos["name"].stringValue
            self.SaveArray[2] = infos["phone"].stringValue
            self.SaveArray[3] = infos["address1"].stringValue
            self.SaveArray[4] = infos["address2"].stringValue
            self.SaveArray[5] = infos["city"].stringValue
            self.SaveArray[6] = infos["zip_code"].stringValue
           self.SaveArray[7] = infos["country_id"].intValue
            self.selectedCountryId = infos["country_id"].intValue
            self.SaveArray[8] = infos["state_id"].intValue
            self.SaveArray[9] = infos["fax"].stringValue
            
            
            if infos["gender"].string! != "male"
            {
                self.gender = "Female_PH"
            }
            
            let img = URL(string: infos["image"].stringValue)
            
            self.imgUserPic?.sd_setImage(with: img , placeholderImage: UIImage(imageLiteralResourceName: self.gender ?? "Male_PH" ), options: .cacheMemoryOnly, completed: nil)

            
            
            
            self.mTableview.reloadData()
          //  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Updateprofile"), object: nil)
            
        }
    }
    
    
    // MARK: - Open Camera

    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            navigationController!.setNavigationBarHidden(true, animated: false)
            // Add it as a subview
            
            addChild(imagePicker)
            view.addSubview(imagePicker.view)
            self.tabBarController?.tabBar.isHidden = true
        }
    }
    
    // MARK: - Open Photo Library

    
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            navigationController!.setNavigationBarHidden(true, animated: false)
            // Add it as a subview
            
            addChild(imagePicker)
            view.addSubview(imagePicker.view)
            self.tabBarController?.tabBar.isHidden = true
        }
    }
    
    
    
    
    // MARK: - Delete User Image

   
    fileprivate func deletePhoto()
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        Deleteprofileimage_Api().Request { (status, message) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                return
            }
            
            self.navigationController?.popViewController(animated: true)
            
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Updateprofile"), object: nil)
            guard let info = SignedUserInfo.sharedInstance else{return}
             if info.gender != "male"
             {
            self.imgUserPic.image = UIImage(named: "Female_PH")
                return
                }
            self.imgUserPic.image = UIImage(named: "Male_PH")
            
            return
        }
    }
    
    
  
    
    
}

// MARK: - Table View Delegate/DataSource Methods

extension EditProfileVC: UITableViewDelegate,UITableViewDataSource
{
    
    // MARK: - Table view data source
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrSctionTitle.count
    }
    
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
        
        
        return arrSctionTitle[section] as? String
    }
    
    private func tableView (tableView:UITableView , heightForHeaderInSection section:Int)->Float
    {
        return 20.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileCell") as! EditProfileCell
            cell.UserInfo.placeholder = ""
            cell.UserInfo.text = skippedArray[indexPath.section] as? String
            cell.UserInfo.tag = indexPath.section
            cell.UserInfo.textColor = UIColor.black
            cell.UserInfo.delegate = self
        print(indexPath.section)
        
           if indexPath.section == 0
         {
            cell.UserInfo.rightViewMode = .never
            cell.UserInfo.rightView = nil
            cell.UserInfo.isUserInteractionEnabled = false
            cell.UserInfo.textColor = UIColor.lightGray
            cell.UserInfo.keyboardType = .default
            cell.UserInfo.rightViewMode = .never
            cell.UserInfo.rightView = nil

         }
            
             else if indexPath.section == 1
            
           {
            cell.UserInfo.rightViewMode = .never
            cell.UserInfo.rightView = nil
           }
            
           else if indexPath.section == 2 || indexPath.section == 9
           {
            cell.UserInfo.keyboardType = .numberPad
            cell.UserInfo.rightViewMode = .never
            cell.UserInfo.rightView = nil

           }
        
      else  if indexPath.section == 7
        {
            cell.UserInfo.rightViewMode = .never
            cell.UserInfo.rightView = nil
            cell.UserInfo.isUserInteractionEnabled = false
            cell.UserInfo.textColor = UIColor.lightGray
            cell.UserInfo.keyboardType = .default
            cell.UserInfo.rightViewMode = .never
            cell.UserInfo.rightView = nil
           

        }
        else if indexPath.section == 8
        {
            cell.UserInfo.rightViewMode = .always
            cell.UserInfo.rightView = ShowDropDownImage(Image: .iosArrowDown)
            self.pickUpDate(cell.UserInfo)
           cell.UserInfo.isUserInteractionEnabled = true
            cell.UserInfo.keyboardType = .default

        }
        else
           {
            
             cell.accessoryView = HideDropDownImage()
            cell.UserInfo.rightViewMode = .never
            cell.UserInfo.rightView = nil
            cell.UserInfo.isUserInteractionEnabled = true
            cell.UserInfo.keyboardType = .default

        }
        

        cell.selectionStyle = .none
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        
        
    }
    
    
    func ShowDropDownImage(Image: Ionicons) -> UIImageView {
        
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 40, width: 30, height: 30)
        imageView.image = UIImage.ionicon(with: Image, textColor: UIColor.black, size: CGSize(width: 50, height: 50))
        
        return imageView
        
    }
    
    func HideDropDownImage() -> UIImageView {
        
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 20, width: 1, height: 1)
        imageView.image = UIImage.ionicon(with: .iosArrowDown, textColor: .clear, size: CGSize(width: 1, height: 1))
        
        return imageView
        
    }
    
    
}


// MARK: - Image Picker Controller Delegate Methods


extension EditProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        // imgUserPic.contentMode = .scaleAspectFit
        imgUserPic.image = chosenImage
       dismissPicker(picker: picker)
        
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismissPicker(picker: picker)
    }
    
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?) {
        
    }
    private func dismissPicker(picker : UIImagePickerController){
        picker.view!.removeFromSuperview()
        picker.removeFromParent()
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        UIApplication.shared.isStatusBarHidden = false
    }
}


// MARK: - PickerView Delegate Methods


extension EditProfileVC : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if !Pickerdatavalue.isEmpty
        {
            return Pickerdatavalue[0].count
            
        }
        return 0
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Pickerdatavalue[0][row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        
        RowValue = row
        
    }
}


// MARK: - TextField Delegate Methods


extension EditProfileVC : UITextFieldDelegate
{
    // datepickerView
    
    func pickUpDate(_ textField : UITextField){
        
        print("TagValue:----->>>",textField.tag)
        
      
        
    }
 
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        
        print("textField-----> ",textField.tag)
        
        indexvalu = textField.tag
        
        if textField.tag == 7
        {
            textField.inputView = self.dataPickerView
            textField.inputAccessoryView = self.toolBar
            
            Pickerdatavalue = [self.dataList[0]["country"]]
            
            print(Pickerdatavalue)
            
            
            self.dataPickerView.reloadAllComponents()
        }
        else if textField.tag == 8
        {
            textField.inputView = self.dataPickerView
            textField.inputAccessoryView = self.toolBar
            // edit by harsh
            let stateDict = self.dataList[0]["state"]
            let filteredArrayBool = stateDict.filter{$0.1["country_id"].stringValue == "\(selectedCountryId)"}
            let temp = filteredArrayBool.map{$0.1}
            print(temp)
            let filteredarray  = JSON(temp)
            //filteredArray
            if temp.count > 0{
                
                Pickerdatavalue = [filteredarray]
                
                let resultsArray =  Pickerdatavalue[0].arrayValue
                
                let  b  =  resultsArray.uniq(by: { $0["name"].stringValue})

                
                let categoryData = b.sorted { $0["name"].stringValue.compare($1["name"].stringValue) == .orderedAscending }
                
                let filteredarray  = JSON(categoryData)
                
                if filteredarray.count > 0{
                    Pickerdatavalue = [filteredarray]
                }
                                
                self.dataPickerView.reloadAllComponents()
            }
            else{
                
                if selectedCountryId == 0
                {
                    self.alert("Please select Country First")
                }
                else
                {
                    self.alert("There is no state available in this country")
                }
                
                
            }
        }
        
        else
        {
            textField.inputView = nil
            textField.inputAccessoryView = nil
            self.dataPickerView.removeFromSuperview()
            self.toolBar.removeFromSuperview()
            
        }
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        print("shouldChangeCharactersIn :---",textField.tag)
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        
       
            skippedArray[textField.tag] = newString
            
            SaveArray[textField.tag] = newString
       
        
        
        
        
        print(skippedArray)
        
        
        return true
    }
    
}
