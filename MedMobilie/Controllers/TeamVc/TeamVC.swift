//
//  TeamVC.swift
//  MedMobilie
//
//  Created by MAC on 14/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import IoniconsKit
import DGElasticPullToRefresh

class TeamVC: InterfaceExtendedController ,UISearchResultsUpdating{
    
    fileprivate var mainJsonData : [JSON]?
    fileprivate var resultSearchController = UISearchController()
    
    var TeamData : [JSON]?
    var ClassName : String = ""


    @IBOutlet weak var mtableview: UITableView!

    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // self.LanguageSet()
        self.mtableview.delegate = self
        self.mtableview.dataSource = self
        self.mtableview.tableFooterView = UIView()
        self.mtableview.separatorStyle = .none
       // AppDelegate.showWaitView()
        //TeamMemberList_fetch(idValue: "")
        self.NavBarIcon()
        
         NotificationCenter.default.addObserver(self, selector: #selector(Filter), name: NSNotification.Name( TeamFilters), object: nil)
        
        
        AppDelegate.showWaitView()
        TeamMemberList_fetch(idValue: "")

        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.SetUpSearchBar()
        AppDelegate.showWaitView()
        TeamMemberList_fetch(idValue: "")
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: TeamNAVBAR(), BtnColor: UIColor.white)
          self.navigationController?.isNavigationBarHidden = false
        
        
         self.tabBarController?.tabBar.isHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.resultSearchController.dismiss(animated: false, completion: nil)
    }
    
    //MARK: - Language Setting
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Team")
        
    }
    
    // MARK: - Set navigationbar Icons

    override func NavBarIcon()
    {
        
        let Filter = UIBarButtonItem(image: UIImage.ionicon(with: .androidOptions, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), style: .plain, target: self, action: #selector(Filter_Action))
        Filter.tintColor = UIColor.white
        
        
        navigationItem.rightBarButtonItem = Filter
        
        
    }
    
    // MARK: - Filter Action

    @objc  func Filter_Action(){
        
        guard let obj_TeamVC = TeamFilter.instance()
            else{return}
        
        self.navigationController?.pushViewController(obj_TeamVC, animated: true)
        
    }
    
    
    
    // MARK: - Filter Notification

    @objc func Filter (notfication: NSNotification)  {
        
        
        
        print(notfication.object as! Int)
        
        let idvalue = notfication.object as! Int
        
        
         print(idvalue)
        
        
        TeamMemberList_fetch(idValue: String(idvalue))
        
        
    }
    
    // MARK: - SetUp SearchBar

    func SetUpSearchBar()
    {
        self.resultSearchController = ({
            
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.searchBar.placeholder = "Search Team".languageSet
            controller.searchBar.tintColor = TeamNAVBAR()
            controller.searchBar.barTintColor = .white
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.hidesNavigationBarDuringPresentation = false
            self.mtableview.tableHeaderView = controller.searchBar
            
            return controller
        })()
    }
    
    
    //MARK: - SearchBar Update
    
    func updateSearchResults(for searchController: UISearchController) {
        // filteredTableData.removeAll(keepingCapacity: false)
        
        if searchController.searchBar.text != ""
        {
            let Filterarray = self.mainJsonData?.filter({ (element) -> Bool in
                if element["name"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "")
                {
                    return true
                }
                return false
            })
            
            self.TeamData = Filterarray
            self.mtableview.reloadData()
        }
        else
        {
            self.TeamData = mainJsonData
            self.mtableview.reloadData()
            
        }
        
    }
    
    //MARK: - Team Member List Data Fetch

    fileprivate func TeamMemberList_fetch(idValue : String)
    {
    
    	if !NetworkState.isConnected() {
            self.mtableview.dg_stopLoading()

            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        TeamMemberList_Api().Fetch(team_id: idValue, completion: { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                
                self.mtableview.dg_stopLoading()

                self.mainJsonData = data
                self.TeamData = self.mainJsonData
                self.mtableview.reloadData()
                self.alert(message)
                return
            }
            
            self.mtableview.dg_stopLoading()
            self.mainJsonData = data
            self.TeamData = self.mainJsonData
            self.mtableview.reloadData()
            
        })
        
        
        
        
        
    }
    
    
    
}


// MARK: - Table View Delegate/DataSource Methods


extension TeamVC : UITableViewDelegate,UITableViewDataSource
{
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TeamData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TeamCell
        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
        cell.viewShadow?.backgroundColor = navBackgroundColor
        guard let infos = TeamData?[indexPath.row]  else{return cell}
        
        if infos["gender"].stringValue != "Male"
        {
            cell.gender = "Female_PH"
        }
        
        cell.lblUserName.text = infos["name"].stringValue
        cell.lblMessage.text = infos["type"].stringValue
        cell.imgUser?.sd_setImage(with:infos["image"].url, placeholderImage: UIImage(imageLiteralResourceName: cell.gender ), options: .cacheMemoryOnly, completed: nil)
        cell.lblChatLogo.text = String.ionicon(with: .chatbubbleWorking)
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let infos = TeamData?[indexPath.row]
        
      //  NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 0)
        guard let controller = ChatController.instance()
            else{return}
        controller.userid = infos!["user_id"].intValue
        controller.UserName = infos!["name"].stringValue
        controller.ClassName = ClassName
        self.navigationController?.pushViewController(controller, animated: true)
        
        
        
        
       
        
        
    }
}

// MARK: - PullToRefresh

extension TeamVC
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
         self.mtableview.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            
            
            self!.TeamMemberList_fetch(idValue: "")

            
            }, loadingView: loadingView)
         self.mtableview.dg_setPullToRefreshFillColor(TeamNAVBAR())
        
         self.mtableview.dg_setPullToRefreshBackgroundColor(self.mtableview.backgroundColor!)
    }
    
}

extension TeamVC
{
    class func instance()->TeamVC?{
        let storyboard = UIStoryboard(name: "Team", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "TeamVC") as? TeamVC
        
        
        return controller
    }
}

