//
//  TeamFilter.swift
//  MedMobilie
//
//  Created by MAC on 10/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import IoniconsKit
import MMDrawerController
import SwiftyJSON
class TeamFilter: InterfaceExtendedController,UITextFieldDelegate {

    
    fileprivate var dataList = [JSON]()
    fileprivate var teamArray = [JSON]()
    fileprivate var Pickerdatavalue = [JSON]()
    
    var dataPicker = UIPickerView()
    var indexvalu = 0
    var RowValue = 0
    var tagvalue = 0
    var country_id = 0
    var team_id = 0
    
    @IBOutlet weak var lblRegion: UILabel!
    @IBOutlet weak var txtRegion: UITextField!
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var txtTeam: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        txtRegion.Padding()
        txtRegion.textFieldBottomBorder()
        txtRegion.backgroundColor = UIColor.clear
        txtRegion.delegate = self
        txtRegion.tag = 0
        
        txtTeam.Padding()
        txtTeam.textFieldBottomBorder()
        txtTeam.backgroundColor = UIColor.clear
        txtTeam.delegate = self
        txtTeam.tag = 1
        
       self.PickerView()
        
        TeamFilter()
        self.LanguageSet()
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: TeamNAVBAR(), BtnColor: UIColor.white)
    }
    
    
    // MARK: - Language Setting

    @objc override func LanguageSet(){
         NavigationBarTitleName(Title: "Filter")
        lblRegion.text = languageKey(key: "Region")
        lblTeam.text = languageKey(key: "Team")
        btnSubmit .setTitle(languageKey(key: "Done"), for: .normal)
        btnSubmit.backgroundColor = TeamNAVBAR()
    }
    
    
    // MARK: - PickerView

    func PickerView() {
        
        self.dataPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216)
        
        self.dataPicker.delegate = self
        self.dataPicker.dataSource = self
        self.dataPicker.backgroundColor = UIColor.white
        
        
        txtRegion.inputView = self.dataPicker
        txtTeam.inputView = self.dataPicker
     
        
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtRegion.inputAccessoryView = toolBar
        txtTeam.inputAccessoryView = toolBar
        
        
        
    }
    
    // MARK: - PickerView Done Button Click

    @objc func doneClick() {
        
        
        
         print(self.dataList[0]["team"][0]["id"].intValue)
        
         txtTeam.text = ""
        if tagvalue == 0
        {
            txtRegion.text = Pickerdatavalue[0][RowValue]["name"].stringValue
             country_id = Pickerdatavalue[0][RowValue]["id"].intValue
            
            let teamData = self.dataList[0]["team"].filter { (str , element) -> Bool in
                
                if element["country_id"].intValue == country_id
                {
                    return true
                }
                else
                {
                    return false
                }
            }
          //      let teams = teamData[0].1
           teamArray = [JSON]()
            for element in teamData
            {
                teamArray.append(element.1)
                
            }
            
           
            if !teamData.isEmpty
            {
             txtTeam.text = teamArray[0]["name"].stringValue
                team_id = teamArray[0]["id"].intValue
            }
            else
            {
                self.alert("there is not available any team")
                return
            }
            
            
            
           
            
            
            
        }
        else
        {
            team_id = Pickerdatavalue[0][RowValue]["id"].intValue
            country_id = Pickerdatavalue[0][RowValue]["id"].intValue
            txtTeam.text = Pickerdatavalue[0][RowValue]["name"].string
        }
        
        print(Pickerdatavalue[0][RowValue]["id"].stringValue)
        
        self.dataPicker.reloadAllComponents()
        
        self.dataPicker.selectRow(0, inComponent: 0, animated: false)
        
        RowValue = 0
        
     print(team_id)
        print(country_id)
        
        self.view .endEditing(true)
        
    }
    
    
    // MARK: - PickerView Cancel Button Click

    @objc func cancelClick() {
        
        self.view .endEditing(true)
       
    }
    
    // MARK: - Show DropDown Image

    
    func ShowDropDownImage() -> UIImageView {
        
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 40, width: 30, height: 30)
        imageView.image = UIImage.ionicon(with: .iosArrowDown, textColor: UIColor.black, size: CGSize(width: 50, height: 50))
        
        return imageView
        
    }
    
    
    // MARK: - Filter Button Action

    @IBAction func Filter(_ sender: Any) {
        
        
        
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: TeamFilters), object: team_id)
        
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    // MARK: - Team Filter API Call

    fileprivate func TeamFilter()
    {
    
        AppDelegate.showWaitView()
    	if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        TeamFilter_Api().Request { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                return
            }
            guard let tempdata = data else
            {
                return
            }


            self.dataList = tempdata


            print(self.dataList )




        }
    }
    
    // MARK: - TextField Delegate

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 0 {
            
            
            tagvalue = textField.tag
            
            Pickerdatavalue = [self.dataList[0]["country"]]
            
            
            print(Pickerdatavalue)
            
        }
        else
        {
            tagvalue = textField.tag
         //       self.dataList[0][""]
            
            Pickerdatavalue = [JSON(self.teamArray)]
            
             print(Pickerdatavalue)
        }
        
         self.dataPicker.reloadAllComponents()
       
        
    }
    

}

// MARK: - PickerView Delegate

extension TeamFilter : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Pickerdatavalue[0].count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Pickerdatavalue[0][row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        
        RowValue = row
        
    }
}

// MARK: - Class Instance
extension TeamFilter
{
    class func instance()->TeamFilter?{
        let storyboard = UIStoryboard(name: "Team", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "TeamFilter") as? TeamFilter
        
        
        return controller
    }
}
