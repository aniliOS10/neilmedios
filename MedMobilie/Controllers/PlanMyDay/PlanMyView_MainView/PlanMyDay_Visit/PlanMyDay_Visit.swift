//
//  PlanMyDay_Visit.swift
//  MedMobilie
//
//  Created by MAC on 17/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit
import MapKit
import SwiftyJSON
import GoogleMaps
import IoniconsKit
import JJFloatingActionButton

class PlanMyDay_Visit: InterfaceExtendedController {
    
    
    fileprivate var marker = [CustomMarker]()
    fileprivate var placeData : [JSON]?
    fileprivate var tableData = [JSON]()
    fileprivate var isFlipped: Bool = false
    
    var parama = [String:Any]()
    
    @IBOutlet weak var BTNHome: UIButton!
    @IBOutlet weak var BTNPlanMyDay: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var SortButton: JJFloatingActionButton!
    @IBOutlet fileprivate var tableView : UITableView?
    @IBOutlet fileprivate var TableViewOnFlip : UIView?
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        tableView?.delegate = self
        tableView?.dataSource  = self
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.estimatedRowHeight = 150
        tableView?.separatorStyle = .none
        tableView?.allowsMultipleSelection = false
        
        rightButtonWithIonicon(name: .map)
        if !parama.isEmpty
        {
            let data = JSON(parama)
            print(data)
            mapView.camera = GMSCameraPosition.camera(withLatitude: data["lat"].doubleValue, longitude: data["long"].doubleValue, zoom: 13.0)
            
            AppDelegate.showWaitView()
            
            self.dataFetch(parama)
        }
        SortButtonAction()
        mapView.settings.zoomGestures = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(Update_PlanMyDay), name: NSNotification.Name( "Update_PlanMyDay"), object: nil)
        
        BTNHome.layer.cornerRadius = 30
        BTNHome.tintColor = UIColor.white
        BTNPlanMyDay.layer.cornerRadius = 30
        BTNPlanMyDay.tintColor = UIColor.white
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // mapView.clear()
        //mapView.stopRendering()
        // mapView.removeFromSuperview()
        //mapView = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: PlanNAVBAR(), BtnColor: UIColor.white)
        // self.NavBarIcon()
        
    }
    // MARK: - Language Setting
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Create Territory")
    }
    // MARK: - Update PlanMyDay
    
    @objc func Update_PlanMyDay (notfication: NSNotification)
    {
        AppDelegate.showWaitView()
        self.dataFetch(parama)        
    }
    
    // MARK: - Save Button Action
    
    override func rightButtonAction() {
        print("hii")
        isFlipped = !isFlipped
        
        let cardToFlip = isFlipped ? mapView : TableViewOnFlip
        let bottomCard = isFlipped ? TableViewOnFlip : mapView
        
        UIView.transition(from: cardToFlip!,
                          to: bottomCard!,
                          duration: 0.5,
                          options: [.transitionFlipFromRight, .showHideTransitionViews]) { (status) in
            if self.isFlipped
            {
                self.navigationItem.rightBarButtonItem = nil
                self.rightButtonWithIonicon(name: .iosListOutline)
                self.tableView?.reloadData()
            }
            else
            {
                self.navigationItem.rightBarButtonItem = nil
                self.rightButtonWithIonicon(name: .map)
            }
        }
    }
    
    // MARK: - Home Button Action
    
    @IBAction fileprivate func Home_Action(_ sender : UIButton)
    {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: - PlanMyDay Button Action for PopUp backScreen
    
    @IBAction fileprivate func PlanMyDay_Action(_ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- ButtonActions
    
    fileprivate func SortButtonAction()
    {
        SortButton.buttonColor = PlanNAVBAR()
        SortButton.buttonImage? = UIImage(imageLiteralResourceName: "sort")
        //SortButton.buttonImage = UIImage(imageLiteralResourceName: "Group_PH")
        SortButton.buttonAnimationConfiguration = .transition(toImage: UIImage(imageLiteralResourceName: "sort"))
        SortButton.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 14)
        
        SortButton.addItem(title: "", image: UIImage(imageLiteralResourceName: "new")) { (item) in
            self.tableData = [JSON]()
            self.mapView.clear()
            
            let new =  self.placeData?.filter({ (jsonData) -> Bool in
                if jsonData["filterby"].intValue == 0
                {
                    self.MarkerOnLocation(jsonData: jsonData)
                }
                return false
            })
            print(new)
            if self.isFlipped
            {
                self.tableView?.reloadData()
            }
        }
        
        SortButton.addItem(title: "", image: UIImage(imageLiteralResourceName: "15days")) { (item) in
            self.tableData = [JSON]()
            self.mapView.clear()
            
            let days15data =  self.placeData?.filter({ (jsonData) -> Bool in
                if jsonData["filterby"].intValue <= 1 && jsonData["filterby"].intValue != 0
                {
                    self.MarkerOnLocation(jsonData: jsonData)
                }
                return false
            })
            print(days15data)
            if self.isFlipped
            {
                self.tableView?.reloadData()
            }
        }
        SortButton.addItem(title: "", image:  UIImage(imageLiteralResourceName: "30days")) { (item) in
            self.tableData = [JSON]()
            self.mapView.clear()
            
            print("temp2")
            
            let days30data =  self.placeData?.filter({ (jsonData) -> Bool in
                if jsonData["filterby"].intValue <= 2 && jsonData["filterby"].intValue != 0
                {
                    self.MarkerOnLocation(jsonData: jsonData)
                }
                return false
            })
            print(days30data)
            if self.isFlipped
            {
                self.tableView?.reloadData()
            }
            
        }
        SortButton.addItem(title: "", image: UIImage(imageLiteralResourceName: "60days")) { (item) in
            self.tableData = [JSON]()
            self.mapView.clear()
            
            print("temp3")
            let days60data =  self.placeData?.filter({ (jsonData) -> Bool in
                if jsonData["filterby"].intValue <= 3 && jsonData["filterby"].intValue != 0
                {
                    self.MarkerOnLocation(jsonData: jsonData)
                }
                return false
            })
            print(days60data)
            if self.isFlipped
            {
                self.tableView?.reloadData()
            }
            
        }
        SortButton.addItem(title: "", image: UIImage(imageLiteralResourceName: "90") ) { (item) in
            self.tableData = [JSON]()
            self.mapView.clear()
            
            let days90data =  self.placeData?.filter({ (jsonData) -> Bool in
                if jsonData["filterby"].intValue <= 4 && jsonData["filterby"].intValue != 0
                {
                    self.MarkerOnLocation(jsonData: jsonData)
                }
                return false
            })
            print(days90data)
            if self.isFlipped
            {
                self.tableView?.reloadData()
            }
            
        }
        SortButton.addItem(title: "", image: UIImage(imageLiteralResourceName: "all") ) { (item) in
            self.tableData = [JSON]()
            self.mapView.clear()
            let AllData =  self.placeData?.filter({ position -> Bool in
                self.MarkerOnLocation(jsonData: position)
                return true
            })
            if self.isFlipped
            {
                self.tableView?.reloadData()
            }
        }
        
    }
    
    //MARK: -Fetch Places list Api
    
    fileprivate func dataFetch(_ param : [String:Any])
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        DataFetchWithParam_Api().Request(parameter: param, link: "/placeslist") { (status, message, data) in
            
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                return
            }
            print(data)
            if data != nil
            {
                
                var bounds = GMSCoordinateBounds()
                
                for index in 0..<(data!.count) {
                    
                    let latitude = data![index]["latitude"].doubleValue
                    let longitude = data![index]["longitude"].doubleValue
                    
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2D(latitude:latitude, longitude:longitude)
                    marker.map = self.mapView
                    bounds = bounds.includingCoordinate(marker.position)
                }
                let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
                self.mapView.animate(with: update)
                
                if param["savesearch"] as! Bool == true
                {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_PlanMyDaySearch"), object: nil)
                    
                }
                
                self.placeData = data ?? nil
                
                DispatchQueue.main.async {
                    
                    
                    self.placeData?.filter({ (element) -> Bool in
                        self.MarkerOnLocation(jsonData: element)
                        return true
                    })
                    
                }
                
                self.tableData.removeAll()
                
                
                //                self.placeData?.enumerated().filter({ (index,markerJson) -> Bool in
                //                    if markerJson["place_id"].stringValue == data![index]["place_id"].stringValue
                //                    {
                //                        self.placeData?[index]["is_existing"] = 1
                //                    }
                //                    return true
                //                })
                
                
                
                //                data?.filter({ position -> Bool in
                //
                //
                //                    self.MarkerOnLocation(jsonData: position)
                //
                //                    return true
                //                })
                //
                
            }
        }
    }
    
    
    //MARK: -Fetch create Visit From Place API
    
    fileprivate func createVisitFromPlace(placeId : String , lat : Double,long : Double , titleName : String,address :String, types:String)
    {
        var param = [String:Any]()
        param["place_id"] = placeId
        param["lat"] = lat
        param["long"] = long
        param["title"] = titleName
        param["address"] = address
        
        param["type"] = parama["type"]
        param["types"] = types
        
        print("Post_Data",param)
        
        
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        DataFetchWithParam_Api().Request(parameter: param, link: "/createvisitfromplan") { (status, message, data) in
            
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                return
            }
            self.popupAlert(title: Bundle.appName(), message: message, actionTitles: [self.languageKey(key: "Ok")], actions: [{ action1 in
                self.mapView.clear()
                
                self.tableData = [JSON]()
                self.placeData?.enumerated().filter({ (index,markerJson) -> Bool in
                    if markerJson["place_id"].stringValue == placeId
                    {
                        self.placeData?[index]["is_existing"] = 1
                    }
                    return true
                })
                self.placeData?.filter({ (element) -> Bool in
                    self.MarkerOnLocation(jsonData: element)
                    return true
                })
                if self.isFlipped
                {
                    self.tableView?.reloadData()
                }
            },nil])
            
            /*self.alert("Visit Created Succesfully")
             return*/
            
        }
        
    }
        
    //MARK: - Marker On Location
    func MarkerOnLocation(jsonData : JSON) {
        
        print("Data Value  :-------->",jsonData)
        
        let tempMarker = CustomMarker()
        tempMarker.position = CLLocationCoordinate2D(latitude: jsonData["latitude"].doubleValue, longitude: jsonData["longitude"].doubleValue )
        tempMarker.title = jsonData["name"].stringValue
        tempMarker.snippet = jsonData["address"].stringValue
        //  tempMarker.icon = GMSMarker.markerImage(with: UIColor.green)
        tempMarker.placeID = jsonData["place_id"].stringValue
        tempMarker.ContactCount = jsonData["contacts"].count
        tempMarker.DoctorArrays = jsonData["contacts"]
        tempMarker.types = jsonData["types"].stringValue
        
        if jsonData["is_existing"].intValue == 0
        {
            tempMarker.icon =  GMSMarker.markerImage(with: UIColor.red)
        }
        else 
        {
            tempMarker.icon =  GMSMarker.markerImage(with: UIColor.green)
        }
        
        self.tableData.append(jsonData)
        marker.append(tempMarker)
        
        tempMarker.map = mapView
        tempMarker.map?.selectedMarker
        
        // mapView.selectedMarker = tempMarker.map?.selectedMarker
        // mapView.selectedMarker = tempMarker
    }
}

// MARK: - Table View Delegate/DataSource Methods

extension PlanMyDay_Visit : UITableViewDelegate , UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let json = tableData[indexPath.row]
        
        if json["contacts"].count == 0
        {
            
            self.popupAlertWithSheet(title: json["name"].stringValue, message: json["address"].stringValue, actionTitles: [ self.languageKey(key: "Create Visit")], actions:[{action1 in
                AppDelegate.showWaitView()
                self.createVisitFromPlace(placeId: json["place_id"].stringValue, lat: json["latitude"].doubleValue, long: json["longitude"].doubleValue, titleName: json["name"].stringValue, address: json["address"].stringValue, types: json["types"].stringValue)
                
            },nil])
        }
        else
        {
            
            guard let obj_PlanMyDay = MeetingWith.instance()
            else{return}
            obj_PlanMyDay.placeId = json["place_id"].stringValue
            obj_PlanMyDay.lat = json["latitude"].doubleValue
            obj_PlanMyDay.long = json["longitude"].doubleValue
            obj_PlanMyDay.titleName = json["name"].stringValue
            obj_PlanMyDay.address = json["address"].stringValue
            obj_PlanMyDay.DoctorData = json["contacts"]
            
            self.navigationController?.pushViewController(obj_PlanMyDay, animated: true)
            
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? PlaceListCell else{return PlaceListCell()}
        cell.selectionStyle = .none
        let json = self.tableData[indexPath.row]
        cell.lblTitleName?.text = json["name"].stringValue
        cell.lblCategoryName?.text = json["address"].stringValue
        cell.viewShadow?.bottomViewShadow(ColorName: .green)
        if json["is_existing"].intValue == 0
        {
            cell.viewShadow?.bottomViewShadow(ColorName: .red)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
// MARK: - GMSMapView Delegate/DataSource Methods

extension PlanMyDay_Visit: GMSMapViewDelegate {
    // 1
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        // print(marker.title)
        if let markerPoint = marker as? CustomMarker
        {
            
            if markerPoint.ContactCount == 0
            {
                self.popupAlertWithSheet(title: markerPoint.title ?? "", message: markerPoint.snippet ?? "", actionTitles: [ self.languageKey(key: "Create Visit")], actions:[{action1 in
                    AppDelegate.showWaitView()
                    self.createVisitFromPlace(placeId: markerPoint.placeID, lat: markerPoint.position.latitude, long: markerPoint.position.longitude, titleName: markerPoint.title ?? "",address: markerPoint.snippet ?? "" ,types: markerPoint.types)
                },nil])
            }
            else
            {
                //   print(tableData)
                //  print(tableData[0])
                
                // let jsonData = tableData[0]
                
                guard let obj_PlanMyDay = MeetingWith.instance()
                else{return}
                obj_PlanMyDay.placeId = markerPoint.placeID
                obj_PlanMyDay.lat = markerPoint.position.latitude
                obj_PlanMyDay.long = markerPoint.position.longitude
                obj_PlanMyDay.titleName = markerPoint.title!
                obj_PlanMyDay.address = markerPoint.snippet!
                obj_PlanMyDay.DoctorData = markerPoint.DoctorArrays
                self.navigationController?.pushViewController(obj_PlanMyDay, animated: true)
                
            }
        }
    }
    
    /* func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
     let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
     view.backgroundColor = .green
     return view
     }
     */
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
    }
}

//Mark: - Class Instance

extension PlanMyDay_Visit
{
    class func instance()->PlanMyDay_Visit?{
        let storyboard = UIStoryboard(name: "PlanMyDay", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "PlanMyDay_Visit") as? PlanMyDay_Visit
        //self.definesPresentationContext = true
        
        return controller
    }
}
