//
//  SampleCartVC.swift
//  MedMobilie
//
//  Created by dr.mac on 25/02/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import SwiftyJSON
import IoniconsKit
import SDWebImage
import DGElasticPullToRefresh
import SwipeCellKit
class SampleCartVC:InterfaceExtendedController {
    
    fileprivate var productIdArr : [Int] = []
    fileprivate var productQty : [Int] = []
    
    var contactId : Int = 0
    var visitId : Int = 0
    var ProductCartData : [JSON]?
    
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnOrderNow: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 160
        tableView.separatorStyle = .none
        if ProductCartData != nil
        {
            self.tableView.reloadData()
        }
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: VisitNAVBAR(), BtnColor: UIColor.white)
        
        self.LanguageSet()
    }
    
    // MARK: - Language Setting
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Sample Cart")
        
        
    }
    
    // MARK: - Order Now Button
    @IBAction func OrderNow(_ sender: Any) {
        
        print(ProductCartData)
        
        ProductCartData?.filter({ (dataJson) -> Bool in
            var value = 1
            if  dataJson["no_of_cases"].intValue != 0
            {
                value = dataJson["no_of_cases"].intValue
            }
            
            
            self.productIdArr.append(dataJson["id"].intValue)
            self.productQty.append(value)
            return true
        })
        AppDelegate.showWaitView()
        orderNOWbtn()
        
        
    }
    
    
    // MARK: - Order Now Api Action
    
    fileprivate func orderNOWbtn()
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
        }
        
        IssueSample_Api().Order(contact_id: contactId, Visit_id: visitId, Product_id: productIdArr,Qty: productQty) { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status{
                self.alert(message)
                return
            }
            self.popupAlert(title: Bundle.appName(), message: message, actionTitles:[self.languageKey(key: "ok")], actions:[{action1 in
                
                self.navigationController?.popToRootViewController(animated: true)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwtichToOrderList"), object: nil)
                }, nil])
            
        }
        
    }
    
    
    // MARK: - Total Price calculate
    func calculatePrice(){
        var totalPrice : Double = 0.0
        for x in 0..<(ProductCartData?.count)! {
            
            let json = ProductCartData?[x]
            
            
            print(json!)
            
            let price : Double = json?["pricepercase"].doubleValue.round(to: 2) ?? 0.0
            
            
            
            let discount : Double = json?["discountpercase"].doubleValue.round(to: 2) ?? 0.0
            let caseCount : Int = json?["no_of_cases"].intValue ?? 0
            
            
            print(price)
            print(discount)
            print(caseCount)
            
            let newPrice =  ((price - discount) * Double(caseCount))
            
            print(newPrice)
            
            totalPrice = totalPrice + newPrice
        }
        
        
        
        
        
    }
    
}

// MARK: - Table View Delegate/DataSource Methods

extension SampleCartVC : UITableViewDelegate , UITableViewDataSource,CartFieldDelegate,SwipeTableViewCellDelegate
{
    
    
    func updatedCartContent(discount: String, cases: String, index: Int, pricePerCase: Double) {
        
        
        
        print("Dicount = \(discount)")
        print("Cases = \(cases)")
        print("Index = \(index)")
        
        
        
        
        let dict = ProductCartData?[index]
        print(dict?.dictionary)
        
        let mutDict : NSMutableDictionary = NSMutableDictionary(dictionary: (dict?.dictionary)!)
        mutDict["no_of_cases"] = cases
        mutDict["discountpercase"] = Double(discount)?.round(to: 2)
        
        
        print(mutDict)
        
        ProductCartData?[index] = JSON.init(mutDict)
        
        calculatePrice()
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ProductCartData?.count ?? 0 > 0
        {
            return ProductCartData?.count ?? 0
        }
        else
        {
            
            self.popupAlert(title: Bundle.appName(), message: self.languageKey(key: "not product found"), actionTitles: [self.languageKey(key: "OK")], actions:[{action1 in
                print("ok")
                self.navigationController?.popViewController(animated: true)
                return
                }, nil])
            
        }
        return 0 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShowCartCell", for: indexPath) as? ShowCartCell
            else{return ShowCartCell()}
        
        guard let infos = ProductCartData?[indexPath.row] else{return cell}
        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
        let str = "\(SignedUserInfo.sharedInstance?.currency_symbol ?? "") \(String(infos["pricepercase"].doubleValue.round(to: 2)))"
        
        //cell.lblQtyText?.text = "No of Units".languageSet
        cell.txtDiscountPerCase?.isUserInteractionEnabled = false
        cell.Price?.text = str
        cell.Units?.text = infos["unitspercase"].stringValue
        cell.lblProductName?.text = infos["name"].stringValue
        cell.lblCategoryName?.text = infos["category_name"].stringValue
        
        cell.txtNoOfCase?.text = infos["no_of_cases"].stringValue
        cell.txtNoOfCase?.placeholder = "1"
        cell.txtNoOfCase?.rowIndex = 1
        cell.txtDiscountPerCase?.rowIndex = 2
        cell.txtNoOfCase?.itemIndex = indexPath.row
        cell.txtDiscountPerCase?.itemIndex = indexPath.row
        cell.txtDiscountPerCase?.pricevalue = infos["pricepercase"].doubleValue.round(to: 2)
        
        cell.txtDiscountPerCase?.text = infos["discountpercase"].stringValue
        cell.delegate = self
        cell.dele = self
        
        
        
        //        self.lblTotalAmount.text = OTHERAmount.description
        //  lblTotalAmount.text = String(TotalAmount)
        
        cell.imgProductimg?.sd_setImage(with: infos["image"].url, placeholderImage: UIImage(imageLiteralResourceName: "Product_PH"), options:.cacheMemoryOnly, completed: nil)
        //cell.imgProductimg?
        
        // cell.viewShadow?.backgroundColor = navBackgroundColor
        
        // cell.lblTask?.text = numbers[indexPath.row]
        
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            guard let list = self.ProductCartData?[indexPath.row] else{return }
            self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: "Are you sure you want to delete this Item"), actionTitles: [self.languageKey(key: "NO"),self.languageKey(key: "YES")], actions:[{action1 in
                return
                },{action2 in
                    
                    self.ProductCartData?.remove(at: indexPath.row)
                    self.tableView.reloadData()
                }, nil])
            // handle action by updating model with deletion
        }
        
        // customize the action appearance
        deleteAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor: .white, size: CGSize(width: 40, height: 40))
        
        return [deleteAction]
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
}

// MARK: - Class Instance

extension SampleCartVC
{
    class func instance()->SampleCartVC?{
        let storyboard = UIStoryboard(name: "Visit", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SampleCartVC") as? SampleCartVC
        
        
        return controller
    }
    
}
