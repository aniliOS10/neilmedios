//
//  VisitDetailsVC.swift
//  MedMobilie
//
//  Created by dr.mac on 23/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import SwiftyJSON
import Letters
import IoniconsKit
import DGElasticPullToRefresh
import SwipeCellKit
import EPSignature
import FontAwesome_swift

class VisitDetailsVC: InterfaceExtendedController {
    
    
    fileprivate var jsonAddress : [String : JSON]?
    fileprivate var jsonContact : [JSON]?
    fileprivate var jsonAttachment : [JSON]?
    fileprivate var pickerDataHours = [Array(0...24),Array(0...60)]
    fileprivate var waitingHours  : Int = 0
    fileprivate var waitingMinutes  : Int = 0
    fileprivate var navi : UINavigationController!
    fileprivate var BoolCVheck  : Bool = false
    fileprivate var selectedContactId : Int = 0
    fileprivate var jsonMain : [JSON]?
    fileprivate let sections = ["Description","Meeting With","Attachments"]
    
    // for waiting time
    var codeTextField: UITextField?;
    var numberTextField: UITextField?;
    var ClassType: String?;
    var dateString = ""
    var visitId : Int = 0
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet fileprivate var btnShowTask : UIButton?
    @IBOutlet fileprivate var DummyView : UIView?
    
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = false
        btnShowTask?.backgroundColor = VisitNAVBAR()
        //self.NotesView?.isHidden = true
        //self.NotesView?.mainView?.backgroundColor = VisitNAVBAR()
        
        AppDelegate.showWaitView()
        VisitListDataFetch()
        
        if ClassType == "TaskView"
        {
            DummyView?.isHidden = true
            
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if BoolCVheck == true
        {
            AppDelegate.showWaitView()
            VisitListDataFetch()
            BoolCVheck = false
            
        }
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: VisitNAVBAR(), BtnColor: UIColor.white)
        
    }
    
    
    // MARK: - Language Setting
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Visit Details")
    }
    
    
    // MARK: - Fetch Visit Details data
    fileprivate func VisitListDataFetch()
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        DataFetch_Api().Fetch(ID: visitId ,urlString: "/visitdetail") { (status, message,data) in
            
            AppDelegate.hideWaitView()
            if !status
            {
                self.tableView.dg_stopLoading()
                self.alert(message)
                return
            }
            if data != nil
            {
                self.jsonMain = data
                
                print(data)
                guard let tempjsonContact =  self.jsonMain?[0]["visit_contact"].arrayValue else{return}
                
                
                print("tempjsonContact:----->",tempjsonContact)
                
                
                self.jsonContact = tempjsonContact
                guard let tempjsonAttachment =  self.jsonMain?[0]["visit_attachment"].arrayValue else{return}
                self.jsonAttachment = tempjsonAttachment
                
                guard let tempjsonAddress =  self.jsonMain?[0]["visit_address"].dictionaryValue else{return}
                
                print("visit_address:----->",tempjsonAddress)
                
                self.jsonAddress = tempjsonAddress

                self.tableView.dg_stopLoading()
                self.tableView.reloadData()
            }
        }
    }
    
    
    // MARK: - Waiting time
    fileprivate func Waittime_api(id : Int , waiting : String,notes : String, contactName : String)
    {
        WaitingTimeFor_Api().request(contact_id: id, waitingTime: waiting) { (status, message, data) in
            if !status{
                self.alert(message)
                return
            }
            guard let controller = AddnotesVC.instance() else{return}
            controller.contact_id = id
            controller.contact_name = contactName
            controller.notes = notes
            controller.delegates = self
            controller.apiLink = "/visitcontactnotesupdate"
            self.navigationController?.pushViewController(controller, animated: true)
            
            self.BoolCVheck = true
            
        }
    }
    
    // when express contact true then jump to edit contact page
    fileprivate func expressContactToeditContact(infos : JSON,Message:String)
    {
        self.popupAlert(title: Bundle.appName(), message: self.languageKey(key: Message), actionTitles:[self.languageKey(key: "Cancel"),self.languageKey(key: "Ok")], actions:[
            {action1 in
                return
            },{action2 in
                if !infos["is_editable"].boolValue
                {
                    self.alert(self.languageKey(key: "You are not authorized to edit this contact"))
                    return
                    
                }
                guard let controller = AddContactVC.instance() else{return}
                
                
                
                var productStr = ""
                var ProductToMarket_Array  = [Int]()
                
                
                if infos["product_ids"].exists()
                {
                    
                    for index in 0..<((infos["product_ids"].arrayValue).count) {
                        
                        
                        let product = (infos["product_ids"][index])
                        
                        print(product)
                        
                        print(product["product_name"])
                        
                        productStr += "\(product["product_name"])\(",")"
                        let idvalue = product["product_id"].intValue
                        
                        ProductToMarket_Array.append(idvalue)
                  
                        print(productStr)
                    }
                    
                    if productStr.count != 0
                    {
                        productStr.removeLast()
                    }
                }
                
                controller.ProductArray = ProductToMarket_Array
                
                controller.boolvalue = true
               
                controller.skippedArray = [infos["salutation"].stringValue,infos["contact_name"].stringValue,infos["attn_name"].stringValue,infos["category_name"].stringValue,infos["contact_title_name"].stringValue,productStr, infos["primary_speciality_name"].stringValue, infos["secondary_specialities_name"].stringValue,infos["company_name"].stringValue,infos["phone"].stringValue,infos["email"].stringValue,infos["website"].stringValue,infos["lead_status_name"].stringValue,infos["lead_source"].stringValue,infos["willingtostock"].stringValue,infos["address1"].stringValue,infos["address2"].stringValue,infos["country_name"].stringValue,infos["state_name"].stringValue,infos["city"].stringValue,infos["zipcode"].stringValue,infos["fax"].stringValue,infos["type"].stringValue,infos["default_billing_address"].stringValue,infos["default_shipping_address"].stringValue,infos["contact_id"].intValue,infos["billing_address1"].stringValue,infos["billing_address2"].stringValue,infos["billing_city"].stringValue,infos["billing_zipcode"].stringValue,infos["billing_country_name"].stringValue,infos["billing_state_name"].stringValue,infos["billing_fax"].stringValue,infos["shipping_address1"].stringValue,infos["shipping_address2"].stringValue,infos["shipping_city"].stringValue,infos["shipping_zip_code"].stringValue,infos["shipping_country_name"].stringValue,infos["shipping_state_name"].stringValue,infos["shipping_fax"].stringValue]
                
                controller.SaveArray = [infos["salutation_id"].stringValue,infos["contact_name"].stringValue,infos["attn_name"].stringValue,infos["category_id"].intValue,infos["contact_title_id"].intValue,productStr,infos["primary_speciality_id"].stringValue, infos["secondary_speciality_id"].stringValue,infos["company_name"].stringValue,infos["phone"].stringValue,infos["email"].stringValue,infos["website"].stringValue,infos["lead_status_id"].intValue,infos["lead_source"].stringValue,infos["willingtostock"].stringValue,infos["address1"].stringValue,infos["address2"].stringValue,infos["country_id"].stringValue,infos["state_id"].stringValue,infos["city"].intValue,infos["zipcode"].intValue,infos["fax"].stringValue,infos["type"].stringValue,infos["default_billing_address"].stringValue,infos["default_shipping_address"].stringValue,infos["contact_id"].intValue,infos["billing_address1"].stringValue,infos["billing_address2"].stringValue,infos["billing_city"].stringValue,infos["billing_zipcode"].stringValue,infos["billing_country_id"].stringValue,infos["billing_state_id"].stringValue,infos["billing_fax"].stringValue,infos["shipping_address1"].stringValue,infos["shipping_address2"].stringValue,infos["shipping_city"].stringValue,infos["shipping_zip_code"].stringValue,infos["shipping_country_id"].stringValue,infos["shipping_state_id"].stringValue,infos["shipping_fax"].stringValue]
                
                
                
                controller.delegate = self
                self.navigationController?.pushViewController(controller, animated: true)
            }, nil])
        
    }
    
    // show task button
    @IBAction fileprivate func btnShowTaskAction(sender : UIButton)
    {
        guard let controller  = TaskListVC.instance() else
        {
            return
        }
        controller.contact_ID = self.visitId
        self.navigationController?.pushViewController(controller, animated: true)
    }
    fileprivate func visited_StatusUpdateForContact(id : Int,indexpathvalue:Int)
    {
        statusUpdate_Api(id: id, status_id: 2, rescheduleDate: "",indexvalue:indexpathvalue)
        
    }
    
    // MARK: - Update Visit List Data
    
    fileprivate func statusUpdate_Api(id: Int,status_id : Int ,rescheduleDate : String,indexvalue:Int ){
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        AppDelegate.showWaitView()
        Visit_Contact_Status_Update().Request(id: id, status: status_id, rescheduledDate: rescheduleDate) { (status, message) in
            
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateVisitList"), object: nil)
            
            if !status{
                self.alert(message)
                return
            }
            
            self.jsonContact?[indexvalue]["status"] = 2
            
            self.VisitListDataFetch()
            
        }
        
    }
    
    
    deinit {
        tableView.dg_removePullToRefresh()
    }
    
}


// MARK: - Create Order Protocol
extension VisitDetailsVC : AddContactsToCreateOrderProtocol
{
    func contactdetails(name: String, Contact_id: Int) {
        AppDelegate.hideWaitView()
        
        
        self.VisitListDataFetch()
    }
    
    
}
// MARK: - Table View Delegate/DataSource Methods
extension VisitDetailsVC : UITableViewDelegate,UITableViewDataSource , SwipeTableViewCellDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1{
            guard let json = jsonContact?[indexPath.row] else{return }
            self.selectedContactId = json["id"].intValue
            let Contactname = "\(json["salutation"].stringValue) \(json["contact_name"].stringValue)"
            
            
            let alert = UIAlertController(title: Bundle.appName(), message: self.languageKey(key: "Choose Option"), preferredStyle: .actionSheet)
            if json["selfie"].stringValue == ""
            {
                let takephoto = UIAlertAction(title: self.languageKey(key: "Take Selfie"), style: .default) { (action) in
                    CameraHandler.shared.directOpenCamera(vc : self)
                    CameraHandler.shared.imagePickedBlock = { (image) in
                        
                        print(image)
                        self.image_Upload_Api(sign: image, Contact_id: self.selectedContactId,urlString:"/visitcontactaddselfie" ,imageName: "selfie")
                    }
                    
                }
                alert.addAction(takephoto)
            }
            else{
                let takeSign = UIAlertAction(title: self.languageKey(key: "View Selfie"), style: .default) { (action) in
                    
                    guard let controller = WebViewVC.instance() else{return}
                    controller.linkUrl = json["selfie"].url
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                alert.addAction(takeSign)
                
            }
            if json["signature"].stringValue == ""
            {
                let takeSign = UIAlertAction(title: self.languageKey(key: "Take Signature"), style: .default) { (action) in
                    let signatureVC = EPSignatureViewController(signatureDelegate: self, showsDate: false, showsSaveSignatureOption: false)
                    signatureVC.subtitleText = ""
                    signatureVC.title = Contactname
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(signatureVC, animated: true)
                    
                }
                alert.addAction(takeSign)
            }
            else {
                let takeSign = UIAlertAction(title: self.languageKey(key: "View Signature"), style: .default) { (action) in
                    
                    guard let controller = WebViewVC.instance() else{return}
                    controller.linkUrl = json["signature"].url
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                alert.addAction(takeSign)
            }
            let addNotes = UIAlertAction(title: self.languageKey(key: "Notes/Feedback"), style: .default) { (action) in
                guard let controller = AddnotesVC.instance()else{return}
                controller.contact_name = Contactname
                controller.contact_id = self.selectedContactId
                controller.notes = json["notes"].stringValue
                controller.delegates = self
                controller.apiLink = "/visitcontactnotesupdate"
                
                self.navigationController?.pushViewController(controller, animated: false)
                
            }
            
            let issueSample = UIAlertAction(title: self.languageKey(key: "Issue Sample"), style: .default) { (action) in
                
                if json["is_express"].boolValue
                {
                    self.expressContactToeditContact(infos: json,Message:"All fields are required of this contact before placing order")
                    return
                }
                guard let controller = SelectSampleVC.instance() else{return}
                controller.contactId = json["contact_id"].intValue
                controller.visitId = self.visitId
                self.navigationController?.pushViewController(controller, animated: true)
                
            }
            
            let waitTime = UIAlertAction(title: self.languageKey(key: "Waiting Time"), style: .default) { (action) in
                let name = Contactname
                let id = self.selectedContactId
                let note = json["notes"].stringValue
                self.askForNumber(Con_Name: name, notes: note, idNo: id )
            }
            let dissmis = UIAlertAction(title: self.languageKey(key: "Dismiss"), style: .cancel) { (action) in
                return
            }
            alert.addAction(addNotes)
            alert.addAction(issueSample)
            alert.addAction(waitTime)
            alert.addAction(dissmis)
            
            alert.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
            
            self.present(alert, animated: true) {
                print("option menu presented")
            }
            
        }
        else if indexPath.section == 2
        {
            guard let json = jsonAttachment?[indexPath.row] else{return}
            let filename = json["filepath"].url
            
            guard let controller = WebViewVC.instance() else{return}
            controller.linkUrl = filename
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
            
        case 1 :
            return jsonContact?.count ?? 0
            
        case 2:
            return jsonAttachment?.count ?? 0
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0 :
            return 0.0
        case 1 :
            if jsonContact?.count ?? 0 > 0
            {
                return 40.0
            }
            else
            {
                return 0.0
            }
            
        case 2:
            if jsonAttachment?.count ?? 0 > 0
            {
                return 40.0
            }
            else
            {
                return 0.0
            }
            
        default:
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch section {
        case 1 :
            return sections[section]
            
        case 2:
            return sections[section]
        default:
            return ""
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if jsonMain != nil
        {
            if jsonAttachment != nil
            {
                return sections.count
            }
            else
            {
                return sections.count - 1
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "detail") as? VisitDetailsCell
                else
            {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            guard let json = jsonMain?[0] else{return cell}
            cell.VisitName?.text = json["title"].stringValue
            cell.lblDetails?.text = json["description"].stringValue
            cell.lblDetails?.textColor = UIColor.black
            let dateStr = "\(json["visit_date"].stringValue)"
            cell.lblVisitDate?.text = dateStr
            cell.lblVisitType?.text = json["visit_type"].stringValue
            
            // for address
            guard let addressarry = jsonAddress else{
                cell.lblAddressVisit?.text = ""
                return cell}
            
            
            let CompleteAddress = "\(addressarry["address1"]?.stringValue ?? "") , \(addressarry["address2"]?.stringValue ?? "") , \(addressarry["city"]?.stringValue ?? "") , \(addressarry["state_name"]?.stringValue ?? "") , \(addressarry["country_name"]?.stringValue ?? "")"
            
            cell.lblAddressVisit?.text = CompleteAddress// CompleteAddress
            
            
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as? TeamCell
                else
            {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            
            guard let json = jsonContact?[indexPath.row] else{return cell}
            
            
            if json["is_express"].boolValue
            {
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.red)
            }
            else
            {
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
            }
            
            cell.lblUserName.text = "\(json["salutation"].stringValue) \(json["contact_name"].stringValue)"
            cell.lblMessage.text = json["category_name"].stringValue
            cell.imgUser?.setImage(string: json["contact_name"].stringValue, color: nil, circular: true)
            
            if json["waiting_time"].stringValue != ""
            {
                cell.waitingTimeView?.isHidden = false
                cell.lblWaitingTime?.text = json["waiting_time"].stringValue
            }
            
            switch json["status"].intValue
            {
            case 1:
                cell.lblStatus?.text = "Pending".languageSet
                cell.lblStatus?.textColor = hexStringToUIColor(hex: "#FFA20D")
                cell.delegate = self
                break
            case 2:
                cell.delegate = nil
                cell.lblStatus?.text = "Visited".languageSet
                cell.lblStatus?.textColor = hexStringToUIColor(hex: "#1F713C")
                break
            case 3:
                cell.lblStatus?.text = "Re-scheduled".languageSet
                cell.delegate = self
                cell.lblStatus?.textColor = .red
                break
            default:
                cell.lblStatus?.text = ""
                
            }
            
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AttachmentCell", for: indexPath) as? TeamCell
                else
            {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            guard let json = jsonAttachment?[indexPath.row] else{return cell}
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
            cell.lblUserName.text = json["filename"].stringValue
            let fileEx = json["filepath"].url?.pathExtension
            var fileImage : FontAwesome = .file
            
            switch fileEx
            {
            case "pdf" :
                fileImage = .filePdf
                
                break
            case "jpg" :
                fileImage = .fileImage
                break
            case "jpeg" :
                fileImage = .fileImage
                break
            case "doc" :
                fileImage = .fileWord
                break
            default :
                break
                
            }
            cell.imgUser.image = UIImage.fontAwesomeIcon(name: fileImage, style: .regular, textColor: VisitNAVBAR(), size: CGSize(width: 40.0, height: 40.0))
            
            return cell
        default:
            print("error")
        }
        return UITableViewCell()
    }
    
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        guard let list = self.jsonContact?[indexPath.row] else{return [] }
        
        
        print(list)
        
        let complete = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.visited_StatusUpdateForContact(id: list["id"].intValue,indexpathvalue: indexPath.row)
                }
                
            }
            
        }
        
        let editAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            self.Datepicker(Contact_id:list["id"].intValue,indexpathvalue: indexPath.row)
            
        }
        // customize the action appearance
        complete.backgroundColor = hexStringToUIColor(hex: "#1F713C")
        
        complete.image = UIImage.ionicon(with: .thumbsup, textColor: .white, size: CGSize(width: 40, height: 40))
        editAction.backgroundColor = .red
        editAction.image = UIImage.ionicon(with: .iosTimeOutline, textColor: .white, size: CGSize(width: 40, height: 40))
        
        
        return [complete,editAction]
        
    }
    
    func CurrentDate()->String{
        let currentDateTime = Date()
        
        // initialize the date formatter and set the style
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd,yyyy"
        
        return formatter.string(from: currentDateTime)
    }
    
    func Datepicker(Contact_id:Int,indexpathvalue: Int) {
        
        let alert = UIAlertController(style: .alert, title: "Reschedule Metting")
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: nil, maximumDate: nil) { date in
            // action with selected date
            print(date)
            //  self.From_date = date
            
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd,yyyy"
            
            self.dateString = formatter.string(from: date)
            
            print("Date :---->",self.dateString)
            
        }
        alert.addAction( title: "OK", style: .default, isEnabled: true) { (action) in
            
            if self.dateString == ""
            {
                
                self.dateString = self.CurrentDate()
            }
            
            self.statusUpdate_Api(id: Contact_id, status_id: 3, rescheduleDate: self.dateString, indexvalue: indexpathvalue)
            
            
        }
        alert.addAction(title: "Cancel", style: .cancel)
        
        alert.show()
    }
    
    
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
}

// MARK: - TextField Delegate Methods

extension VisitDetailsVC : UITextFieldDelegate
{
    // for  waiting time
    func handleOk(alertView: UIAlertAction!){
        
        var code: String = self.codeTextField?.text ?? ""
        var number: String = self.numberTextField?.text ?? ""
        
    }
    fileprivate func askForNumber(Con_Name : String , notes:String,idNo : Int){
        
        let alertController = UIAlertController(title: "Select Waiting Time".languageSet + "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: .alert)
        let pickerView = UIPickerView(frame:
            CGRect(x: 0, y: 50, width: 260, height: 162))
        pickerView.dataSource = self
        pickerView.delegate = self
        
        alertController.view.addSubview(pickerView)
        let somethingAction = UIAlertAction(title: "Ok".languageSet, style: UIAlertAction.Style.default){
            action1 in
            
            print(self.waitingHours ,":", self.waitingMinutes)
            let time = "\(self.waitingHours):\(self.waitingMinutes)"
            self.Waittime_api(id: idNo, waiting: time, notes: notes, contactName: Con_Name)
            self.waitingHours = 0
            self.waitingMinutes = 0
        }
        let cancelAction = UIAlertAction(title: "Cancel".languageSet, style: .cancel){
            action1 in
            self.waitingMinutes = 0
            self.waitingHours = 0
        }
        alertController.addAction(somethingAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:{})
    }
    
}

// MARK: - PickerView Delegate Methods

extension VisitDetailsVC :  UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // edit by harsh
        
        return pickerDataHours[component].count
        //return Pickerdatavalue[0].count
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel()
        label.text = String(row)
        label.textAlignment = .center
        return label
    }
    
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(pickerDataHours[component][row])
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        if let label = pickerView.view(forRow: row, forComponent: component) as? UILabel {
            
            if component == 0, row > 1 {
                label.text = String(row) + " hours"
                waitingHours = pickerDataHours[component][row]
                
            }
            else if component == 0 {
                label.text = String(row) + " hour"
                waitingHours = pickerDataHours[component][row]
            }
            else if component == 1 {
                label.text = String(row) + " min"
                waitingMinutes = pickerDataHours[component][row]
            }
            
        }
        /*
         if component == 0
         {
         waitingHours = pickerDataHours[component][row]
         }
         else if component == 1
         {
         waitingMinutes = pickerDataHours[component][row]
         }*/
        // RowValue = row
        
    }
}


// MARK: - Rescheduled DateTime Methods

extension VisitDetailsVC : RescheduledDateTime
{
    func rescheduledTimePass(id: Int, RescheduleDate: String, FromTime: String, ToTime: String) {
        
        print("RescheduleDate :---> ",RescheduleDate)
        print("FromTime :---> ",FromTime)
        print("ToTime :---> ",ToTime)
        
        // self.rescheduled_StatusUpdateForContact(Contact_id: id, dateStr: RescheduleDate, StrFromTime:  FromTime, StrToTime: ToTime)
    }
}
// MARK: - Signature Delegate

extension VisitDetailsVC : EPSignatureDelegate
{
    func epSignature(_ signature: EPSignatureViewController, didCancel error : NSError) {
        print("User canceled")
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func epSignature(_: EPSignatureViewController, didSign signatureImage : UIImage, boundingRect: CGRect) {
        print(signatureImage)
        self.tabBarController?.tabBar.isHidden = false
        
        image_Upload_Api(sign: signatureImage, Contact_id: selectedContactId,urlString:"/visitcontactaddsignature" ,imageName: "signature")
    }
    fileprivate func image_Upload_Api(sign : UIImage,Contact_id : Int,urlString : String,imageName : String)
    {
        AppDelegate.showWaitView()
        TakeSignUpload_Api().Request(urlString: urlString, id: Contact_id, signImage: sign, imageNameString: imageName) { (status, message) in
            
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                return
            }
            self.VisitListDataFetch()
        }
    }
}

// MARK: - Pull To Refresh

extension VisitDetailsVC
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.VisitListDataFetch()
            }, loadingView: loadingView)
        tableView.dg_setPullToRefreshFillColor(VisitNAVBAR())
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
}

// MARK: - Class Instance

extension VisitDetailsVC
{
    class func instance()->VisitDetailsVC?{
        let storyboard = UIStoryboard(name: "Visit", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "VisitDetailsVC") as? VisitDetailsVC
        //self.definesPresentationContext = true
        
        return controller
    }
}
