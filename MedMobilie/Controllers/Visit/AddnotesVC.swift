//
//  AddnotesVC.swift
//  MedMobilie
//
//  Created by dr.mac on 25/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddnotesVC: InterfaceExtendedController {
    
    var contact_id : Int = 0
    var notes : String = ""
    var contact_name : String = ""
    var delegates : AddContactsToCreateOrderProtocol?
    var apiLink : String = ""
    var TitleString : String = ""
    
    @IBOutlet fileprivate var Notestext : UITextView?
    @IBOutlet fileprivate var ContactName : UILabel?
    @IBOutlet fileprivate var SubmitBtn : UIButton?
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.navigationController?.navigationBar.isHidden = true
        SubmitBtn?.backgroundColor = VisitNAVBAR()
        ContactName?.text = contact_name
        Notestext?.text = notes
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: VisitNAVBAR(), BtnColor: UIColor.white)
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(addTapped))
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Language Setting
    
    @objc override func LanguageSet(){
        
        if TitleString == "Report"
        {
            NavigationBarTitleName(Title: "Reports")
            
        }
        else
        {
            NavigationBarTitleName(Title: "Notes/Feedback")
            
        }
        
    }
    // MARK: - Back Button
    
    @objc fileprivate func addTapped()
    {        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    // MARK: - Submit Button
    
    @IBAction func Submit(_ sender : UIButton)
    {
        if Notestext?.text != ""
        {
            AppDelegate.showWaitView()
            notesSubmit_Api(input: Notestext?.text ?? "")
        }
        else
        {
            self.alert(self.languageKey(key: "text must be words"))
            
        }
        
    }
    
    // MARK: - Submit Notes Api
    fileprivate func notesSubmit_Api(input : String)
    {
        VisitContactNotesUpdate().Request(idvalue: contact_id, notes: input,apiLink : apiLink) { (status, message, data) in
            if !NetworkState.isConnected() {
                AppDelegate.hideWaitView()
                AppDelegate.alertViewForInterNet()
            }
            AppDelegate.hideWaitView()
            if !status{
                self.alert(message)
                return
            }
            self.popupAlert(title: Bundle.appName(), message: message, actionTitles:[self.languageKey(key: "ok")], actions:[{action1 in
                
                AppDelegate.hideWaitView()
                
                self.navigationController?.popViewController(animated: true)
                self.delegates?.contactdetails(name: "", Contact_id: 0)
                }, nil])
            
        }
    }
    
    
}

// MARK: - Class Instance

extension AddnotesVC
{
    class func instance()->AddnotesVC?{
        let storyboard = UIStoryboard(name: "Visit", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AddnotesVC") as? AddnotesVC
        //self.definesPresentationContext = true
        
        return controller
    }
    
}
