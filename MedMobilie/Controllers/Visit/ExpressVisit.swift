//
//  ExpressVisit.swift
//  MedMobilie
//
//  Created by MAC on 30/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import IoniconsKit
import MMDrawerController
import SwiftyJSON
import Photos
import SwipeCellKit
import CoreLocation


class ExpressVisit: InterfaceExtendedController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,CLLocationManagerDelegate {
    
    fileprivate var dataList = [JSON]()
    fileprivate var Pickerdatavalue = [JSON]()
    fileprivate var selectedContactParam = [String:Any]()
    
    let OtherArray : NSMutableArray =         ["1","",""]

    
    var dataPicker = UIPickerView()
    var Date_Time_Picker = UIDatePicker()
    var toolBar = UIToolbar()
    var ContactString = String()
    var Contactid = 0
    var ContactTempId = 0
    var VisitTitleString = String()
    var arrSctionTitle = NSArray()
    var arrTitle = NSArray()
    var indexvalu = 0
    var latvalue = 0.0
    var longvalue = 0.0
    var RowValue = 0
    var Datevalue = String()
    var Timevalue = String()
    var ContactArray = [String : Any]()
    var dicSet = NSMutableDictionary()
    var locationManager: CLLocationManager!
    var skippedArray : NSMutableArray =         ["","",""]
    var SaveArray : NSMutableArray =         ["","",""]
    
    var BackDataBlock: ((_ json : [JSON]) -> ())?

    @IBOutlet weak var mTableView: UITableView!
    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mTableView.delegate = self
        self.mTableView.dataSource = self
        self.mTableView.tableFooterView = UIView()
        RightActionButton(Title:languageKey(key:"Save"))
         Listsapidata()
        LocationLatLong()
       
    }
    
    // MARK: -  Language Setting
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Express Visit")
        
        arrSctionTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Visit", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Meeting With", comment: "") + "*"]
        
        
        arrTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Type", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Title", comment: "")]
        
    }
    
    // MARK: -  Get UserLocation

    func LocationLatLong() {
        
        
        locationManager = CLLocationManager()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    
    // MARK: -  location Manager Method

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
         latvalue = locValue.latitude
         longvalue = locValue.longitude
        
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    
    // MARK: -  Filter Data
    fileprivate func gettingNameFromDataList(keyName : String , index : Int) -> String
    {
        var valueName = ""
        self.dataList[0][keyName].forEach({
            if $0.1["id"].stringValue == SaveArray[index] as? String
            {
                valueName = $0.1["name"].stringValue
                return
            }
        })
        return valueName
    }
    
    
    // MARK: -  Create Expess(Save) Visit Button

    override func rightButtonAction(){
        
 
        
        if ContactString == ""
        {
            self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: "Please select a contact to continue"), actionTitles: [self.languageKey(key: "OK")], actions:[{action1 in
                
                return
                }, nil])
            
            return
        }
        else
        {
            var jsonString = ""
            
            if Contactid != 0
            {
                let jsonData = try? JSONSerialization.data(withJSONObject: [self.selectedContactParam], options: [])
                jsonString = String(data: jsonData!, encoding: .utf8) ?? ""
            
            }
            else
            {
                self.selectedContactParam = ["id":0,"contact_name":ContactString,"salutation":"","category_name":"","temp_id":self.ContactTempId]
                let jsonData = try? JSONSerialization.data(withJSONObject: [self.selectedContactParam], options: [])
                jsonString = String(data: jsonData!, encoding: .utf8) ?? ""
            }
            
            var datavalues = [String : Any]()
            if SaveArray[0] as? String != ""
            {
                datavalues["visit_type_id"] = SaveArray[0]
                datavalues["visit_type"] = gettingNameFromDataList(keyName: "visit_types", index: 0)
            }
            else
            {
                self.alert("please select a type of Visit".languageSet)
                return
            }
//            if SaveArray[1] as? String != ""
//            {
                datavalues["title"] = SaveArray[1]
                
//            }
//            else
//            {
//                self.alert("please fill a visit title name".languageSet)
//                return
//
//            }
                datavalues["contact_name"] = ContactString
                datavalues["contact_id"] = Contactid
                datavalues["visit_contact"] = jsonString
                let date = Date()
                let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
                datavalues["visit_date"] = formatter.string(from: date)
              
            datavalues["AttachmentFiles"] = []
            datavalues["lat"] = latvalue
            datavalues["long"] = longvalue
            datavalues["is_express"] = true
            datavalues["pending_count"] = 1
            datavalues["status"] = 1
            
            datavalues["created_at"] = Int(Date().timeIntervalSince1970)
            print(datavalues)
            

            
            
            
       
           
            
            print(datavalues)
            
            
            AddExpressVisitRequest(datavalues, Method: "/visitcreateexpress")
            
        }
        
    }
    
    
    // MARK: -  offline Express Visit Create
    fileprivate func OfflineCreate(param : [String : Any])
    {
        let userdefault = UserDefaults.standard
        var id : Int = 50000
        if userdefault.value(forKey: "OfflineNewVisitCreate") as? Int != nil
        {
            let tempid = userdefault.value(forKey: "OfflineNewVisitCreate") as! Int
            
            id = tempid + 1
            
        }
        
        var parama  = [String:Any]()
        parama = param
        parama["id"] = id
        let dict = JSON(parama)
        
        DataBaseHelper.ShareInstance.AddVisit(VisitData:dict){status in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert("something went wrong".languageSet)
                return
            }
            userdefault.set(id, forKey: "OfflineNewVisitCreate")
            userdefault.synchronize()
            self.navigationController?.popViewController(animated: true)
            
            
            
            
        }
        print(param)
    }
    
    
    
    
    // MARK: -  Create Express Visit Request

    fileprivate func AddExpressVisitRequest(_ param : [String : Any],Method :String)
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            OfflineCreate(param : param)
            return
        }
        
        AddContactApi().Fetch(param: param,Url: Method) { (status, message, info) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                self.alert(message)
                return
            }
            else
            {
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: message), actionTitles: [self.languageKey(key: "Ok")], actions: [{action1 in
                    
                    if let CreatedVisit = info
                    {
                        self.BackDataBlock?(CreatedVisit )
                    }
                self.navigationController?.popViewController(animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateVisitList"), object: nil)
                    
                    
                    }, nil])
            }
           
        }
        
    }
    
    // MARK: -  Get Listing Data
    fileprivate func Listsapidata()
    {
        AppDelegate.showWaitView()
        DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: "ListingData") { (data) in
            
            if let object = data
            {
                var jsonArray = [JSON]()
                for jsonData in object
                {
                    if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                        jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                            jsonArray.append(json!)
                        })})
                    }
                }
                
                AppDelegate.hideWaitView()
                
                self.dataList = jsonArray
            }
        }
    }
    
    // MARK: - PickerView Delegate Methods

     func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        
        func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return Pickerdatavalue[0].count
        }
        
        func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return Pickerdatavalue[0][row]["name"].stringValue
        }
        
        func pickerView(_ pickerView: UIPickerView,
                        didSelectRow row: Int,
                        inComponent component: Int)
        {
            
            RowValue = row
            
        }
    
    func pickUpDate(_ textField : UITextField){
    
        
        self.dataPicker.removeFromSuperview()
        self.toolBar.removeFromSuperview()
        
        // DatePicker
        //  temp_textField = textField
        
        self.dataPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216)
        self.dataPicker.delegate = self
        self.dataPicker.dataSource = self
        self.dataPicker.backgroundColor = UIColor.white
        
        
        // ToolBar
        
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        if textField.tag == 0 {
            
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
        }
        else if textField.tag == 6 || textField.tag == 7
        {
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
        }
        else if textField.tag == 8
        {
            textField.inputView = self.Date_Time_Picker
            textField.inputAccessoryView = self.toolBar
        }
        else if textField.tag == 9 || textField.tag == 10
        {
            textField.inputView = self.Date_Time_Picker
            textField.inputAccessoryView = self.toolBar
        }
            
        else
        {
            textField.inputView = nil
            textField.inputAccessoryView = nil
            
        }
        
    }
    
    // MARK: - Get Current Date
func CurrentDate()->String{
        let currentDateTime = Date()
        
        // initialize the date formatter and set the style
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: currentDateTime)
        
    }
    
    // MARK: - Get Current Time
    func CurrentTime()->String{
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter.string(from: currentDateTime)
        
    }
    
    @objc func datePickerChanged(datePicker:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        Datevalue = dateFormatter.string(from: datePicker.date)
        
        print(Datevalue)
        
    }
    // MARK: - TimePicker Change Method

    func TimePickerChanged(datePicker:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeStyle = .short
        
        Timevalue = dateFormatter.string(from: datePicker.date)
        
        print(Timevalue)
        
    }
    // MARK: - PickerView Done Button Action
    @objc func doneClick() {
        
        if indexvalu == 0 {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            print(Pickerdatavalue)
        }
        Timevalue = ""
        RowValue = 0
        mTableView.reloadData()
        self.view .endEditing(true)
    }
    
    // MARK: - PickerView Cancel Button Action
  @objc func cancelClick() {
        
        Timevalue = ""
        
        if indexvalu == 0 || indexvalu == 6 || indexvalu == 7
        {
            self.dataPicker.reloadAllComponents()
            
            self.dataPicker.selectRow(0, inComponent: 0, animated: false)
            
        }
        RowValue = 0
        self.view .endEditing(true)
        
    }
    
    // MARK: - TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textField-----> ",textField.tag)
        
        indexvalu = textField.tag
        
        if textField.tag == 0 {
            
            Pickerdatavalue = [self.dataList[0]["visit_types"]]
            
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        print("shouldChangeCharactersIn :---",textField.tag)
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if textField.tag == 1
        {
            skippedArray[textField.tag] = newString
            
            SaveArray[textField.tag] = newString
        }
        else
        {
            Contactid = 0
            ContactString = newString as String
        }
        
        return true
    }
}

// MARK: - Table View Delegate/DataSource Methods
extension ExpressVisit: UITableViewDelegate,UITableViewDataSource
{
  
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            
            return 2
            
        case 1:
            return 2
        
        default:
            return 0
        }
        
        //return arrTitle.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrSctionTitle.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = OffWhiteColor()
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: tableView.frame.width, height: 30)
        label.text = arrSctionTitle[section] as? String
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = UIColor.black
        //  label.backgroundColor = UIColor.yellow
        
        headerView.addSubview(label)
        
        return headerView
    }
        
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if indexPath.section == 0 {
            return  60
        }
            
      else  if indexPath.section == 1 {
            
            if indexPath.row == 1
            {
                return 75
            }
            else
            {
                return 75
            }
        }
        else
        {
            return 75
        }
    }
    
    func ShowDropDownImage(Image: Ionicons) -> UIImageView {
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 40, width: 30, height: 30)
        imageView.image = UIImage.ionicon(with: Image, textColor: UIColor.black, size: CGSize(width: 50, height: 50))
        
        return imageView
        
    }
    
    func HideDropDownImage() -> UIImageView {
        
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 20, width: 1, height: 1)
        imageView.image = UIImage.ionicon(with: .iosArrowDown, textColor: .clear, size: CGSize(width: 1, height: 1))
        
        return imageView
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
            
            let str2 = arrTitle[indexPath.row] as? String
            
            cell.lbtitleAC.text = str2!.replacingOccurrences(of: "*", with: "", options:
                NSString.CompareOptions.literal, range: nil)
            cell.txtTitleAC.text = skippedArray[indexPath.row] as? String
            cell.txtTitleAC.tag = indexPath.row
            cell.txtTitleAC.delegate = self
            cell.selectionStyle = .none
        
            
            if OtherArray[indexPath.row] as! String == "1"
                
            {
                cell.txtTitleAC.rightViewMode = .always
                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosArrowDown)
                self.pickUpDate(cell.txtTitleAC)
            }
            else
            {
                cell.txtTitleAC.rightViewMode = .never
                cell.txtTitleAC.rightView = nil
            }
            
            return cell
            
        }
            
        else  if indexPath.section == 1
        {
            if indexPath.row == 0
            {
           
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddContact_Visite_Cell") as! AddContact_Visite_Cell
                cell.ContactTF.backgroundColor = UIColor.clear
                cell.ContactTF.delegate = self
                cell.ContactTF.placeholder = "New Contact name".languageSet
                cell.ContactTF.tag = 100 + indexPath.row
                cell.ContactTF.setBottomBorder(color: "#3EFE46")
                cell.ContactTF.rightViewMode = .always
                cell.ContactTF.rightView = ShowDropDownImage(Image: .iosContact)
                cell.selectionStyle = .none
                
            cell.ContactTF.rightViewMode = .never
            cell.ContactTF.rightView = nil
            
                    cell.ContactTF.text = ContactString
                    cell.ContactTF.isUserInteractionEnabled = true
               
                
                return cell
            }
            else
            {
                let myCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                myCell.backgroundColor = UIColor.white
                myCell.contentView .addSubview(DashedLineBorder(title: "Choose Existing Contact".languageSet))
                
                myCell.selectionStyle = .none
                return myCell
            }
                
            }
            
        return UITableViewCell()
        
    }
    
    func DashedLineBorder (title: String)-> UILabel {
        
        let ContainerView = UILabel(frame: CGRect(x: 10, y: 10, width: mTableView.frame.size.width-20, height: 55))
        ContainerView.layer.addSublayer(Border(YourLable: ContainerView))
        ContainerView.textAlignment = .center
        ContainerView.font = UIFont.boldSystemFont(ofSize: 16)
        ContainerView.textColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        ContainerView.text = title
        return ContainerView
        
    }
    
    @IBAction func ChooseAction() {
            
        }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
   
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 1
        {
            if indexPath.row == 1
            {
                
                guard let objContactsVC = SelectContactVC.instance() else{return}
                objContactsVC.ClassType = "Express"
                
                objContactsVC.SelectedContactDetail = { param in
                    let json = JSON(param)
                    
                            self.ContactString = "\(json["salutation"].stringValue) " + json["contact_name"].stringValue
                    
                            self.Contactid = json["id"].intValue
                            self.ContactTempId = json["temp_id"].intValue
                            self.selectedContactParam = param
                            self.mTableView.reloadData()
                    
                }
                self.navigationController?.pushViewController(objContactsVC, animated: true)
            }
        }
    }
}



// MARK: - Class Instance

extension ExpressVisit
{
    class func instance()->ExpressVisit?{
        
        let storyboard = UIStoryboard(name: "Visit", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ExpressVisit") as? ExpressVisit
        
        return controller
    }
}

//extension ExpressVisit : SelectSingleContactProtocol
//{
//    func Expresscontactdetails(name: String, Contact_id: Int) {
//        
//        ContactString = name
//        Contactid = Contact_id
//        mTableView.reloadData()
//        
//    }
//    
//    
//}
extension ExpressVisit
{
    func Border(YourLable: UILabel) -> CAShapeLayer{
        
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.black.cgColor
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = YourLable.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: YourLable.bounds).cgPath
        
        
        return yourViewBorder
        
    }
    
}
