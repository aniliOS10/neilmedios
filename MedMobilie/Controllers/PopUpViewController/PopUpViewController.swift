//
//  PopViewController.swift
//  MedMobilie
//
//  Created by dr.mac on 22/11/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit

class PopUpViewController: UIViewController {
    
     @IBOutlet weak var Title_LBL: UILabel!
    @IBOutlet weak var Ok_BTN: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Button Action

    @IBAction func btnActForDismiss(_ sender: UIButton) {
        
        let objAppDelegate = UIApplication.shared.delegate as? AppDelegate
        objAppDelegate?.makingRoot("initial")
        
     //   dismissPopIdentifierSegue
    }
    
   

}
