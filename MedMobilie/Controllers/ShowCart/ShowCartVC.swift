//
//  ShowCartVC.swift
//  MedMobilie
//
//  Created by MAC on 07/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import SwiftyJSON
import IoniconsKit
import SDWebImage
import DGElasticPullToRefresh
import SwipeCellKit
class ShowCartVC:InterfaceExtendedController {
    
    var TotalAmount : Double = 0.0
    var OTHERAmount : Int = 0
    var ComingFromcartBtn : Bool = false
    var ProductCartData : [JSON]?
    var arraydatavalue: [String: JSON]?
    
    
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnOrderNow: UIButton!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblTotalLable: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
   
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 160
        lblTotalLable.text = "Total Pay".languageSet
        AppDelegate.showWaitView()
        self.ProductCartDataFetch()
        tableView.separatorStyle = .none
        }
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        
        self.LanguageSet()
        
        self.NavBarIcons()
        
    }
    
    // MARK: - Language Setting
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Cart")
        
        
    }
    
    // MARK: - Set navigationbar Icons

    func NavBarIcons()
    {
        
        let Filter = UIBarButtonItem(image: UIImage.ionicon(with: .androidDelete, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), style: .plain, target: self, action: #selector(Filter_Action))
        //Filter.tintColor = UIColor.white
        
        
        navigationItem.rightBarButtonItems = [Filter]
        
        
    }
    
    // MARK: - Filter Action

    @objc func Filter_Action()
    {
        
        self.popupAlert(title: languageKey(key: Bundle.appName()), message: languageKey(key: "Are you sure you want to clear cart?"), actionTitles:[languageKey(key: "NO"),languageKey(key: "YES")], actions:[{action1 in
            
            
            
            }, {action2 in
                
                self.DeleteCartapi(Product_id: "", ItemDelete: false )
                
            },nil])
        
    }
    
    // MARK: - Order Now
    @IBAction func OrderNow(_ sender: Any) {
   UpdateCartapi(cartproducts:ProductCartData,tagvalue: "Payment")
    }
    
    // MARK: - update Cart
    @IBAction func updateCart(_ sender: Any) {
    UpdateCartapi(cartproducts:ProductCartData,tagvalue: "Update")
       }
  
   
    // MARK: - Update Cart Api

    fileprivate func UpdateCartapi(cartproducts : [JSON]?,tagvalue: String)
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        UpdateCart().Fetch(cartproducts: cartproducts ) { (status, message,data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
	    return
		}
            
                
                if tagvalue == "Payment"
                {
                    guard let controller = CreateOrderVC.instance() else{return}
                    controller.ProductCartData = self.ProductCartData
                    controller.totalValue = self.TotalAmount
                    controller.comingFromCartBtn = self.ComingFromcartBtn
                    // controller.flagsCheck = f
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                else
                {
                    self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: message, actionTitles:[self.languageKey(key: "Ok")], actions:[{action1 in
                        
                        },nil])
                }
                
                
            
            
	    self.tableView.reloadData()
           
        }
        
    }
    
    
    // MARK: - Delete Cart Api

    fileprivate func DeleteCartapi(Product_id : String,ItemDelete : Bool)
    {
        AppDelegate.showWaitView()
        DeleteCart().Fetch(ProductID: Product_id ) { (status, message,data) in
            
            if !status
            {
                AppDelegate.hideWaitView()
               self.alert(message)
                return
            }
            if !ItemDelete
            {
                AppDelegate.hideWaitView()
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: message, actionTitles:[self.languageKey(key: "Ok")], actions:[{action1 in
                    
                    self.navigationController?.popViewController(animated: true)
                    
                    },nil])
               
            }
           self.ProductCartDataFetch()
           
            
        
            return
            
        }
        
    }
    
    
    // MARK: - Product Cart Data Fetch

    fileprivate func ProductCartDataFetch()
    {
        
        
        
        
        ProductCartListingFetch().Fetch(completion: { (status, message,data ) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: message, actionTitles:[self.languageKey(key: "Ok")], actions:[{action1 in
                    
                    self.navigationController?.popViewController(animated: true)
                    
                    },nil])
                return
            }
            self.ProductCartData = data
            
            
            
            self.calculatePrice()
            
            self.tableView.reloadData()
            self.tableView.dg_stopLoading()
            
           // self.Totalvalueamount()
        })
        
    }
    
    
   
    // MARK: - calculate Price

    func calculatePrice(){
        var totalPrice : Double = 0.0
        for x in 0..<(ProductCartData?.count)! {
            
            let json = ProductCartData?[x]
            
            
            print(json!)
            
            let price : Double = json?["pricepercase"].doubleValue.round(to: 2) ?? 0.0
            
            
            
            let discount : Double = json?["discountpercase"].doubleValue.round(to: 2) ?? 0.0
            let caseCount : Int = json?["no_of_cases"].intValue ?? 0
            
            
            print(price)
              print(discount)
              print(caseCount)
            
            let newPrice =  ((price - discount) * Double(caseCount))
            
            print(newPrice)
            
            totalPrice = totalPrice + newPrice
        }
        
         print(totalPrice)
        
      //  totalPrice = round(totalPrice)
        
        
        let Total = "\(SignedUserInfo.sharedInstance?.currency_symbol ?? "") \(String(totalPrice.round(to: 2)))"
      //  let tempStr = String(format: "%.2f", totalPrice)
        self.TotalAmount = totalPrice.round(to: 2)
        
        self.lblTotalAmount.text = Total
    }
    
    
    // MARK: - jsonString With JSONObject
    class func jsonStringWithJSONObject(jsonObject: AnyObject) throws -> String? {
        let data: NSData? = try? JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
        
        var jsonStr: String?
        if data != nil {
            jsonStr = String(data: data! as Data, encoding: String.Encoding.utf8)
        }
        
        return jsonStr
    }
}

// MARK: - Table View Delegate/DataSource Methods

extension ShowCartVC : UITableViewDelegate , UITableViewDataSource,CartFieldDelegate,SwipeTableViewCellDelegate
{
    
    
    func updatedCartContent(discount: String, cases: String, index: Int, pricePerCase: Double) {
        
        
        print("Dicount = \(discount)")
        print("Cases = \(cases)")
        print("Index = \(index)")
        
        
        let dict = ProductCartData?[index]
        print(dict?.dictionary)
        
        let mutDict : NSMutableDictionary = NSMutableDictionary(dictionary: (dict?.dictionary)!)
        mutDict["no_of_cases"] = cases
        mutDict["discountpercase"] = Double(discount)?.round(to: 2)
        
        
        print(mutDict)
        
        ProductCartData?[index] = JSON.init(mutDict)
        
        calculatePrice()
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProductCartData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShowCartCell", for: indexPath) as? ShowCartCell
            else{return ShowCartCell()}
        
        guard let infos = ProductCartData?[indexPath.row] else{return cell}
        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
        let str = "\(SignedUserInfo.sharedInstance?.currency_symbol ?? "") \(String(infos["pricepercase"].doubleValue.round(to: 2)))"
        if infos["is_sample"].boolValue
        {
            //cell.lblNoOfCase?.text = "No of Units".languageSet
            cell.txtDiscountPerCase?.isUserInteractionEnabled  = false
        }
        else
        {
            //cell.lblNoOfCase.text = "No of cases".languageSet
             cell.txtDiscountPerCase?.isUserInteractionEnabled  = true
        }
        
        cell.Price?.text = str
        cell.Units?.text = infos["unitspercase"].stringValue
        cell.lblProductName?.text = infos["product_name"].stringValue
        cell.lblCategoryName?.text = infos["category_name"].stringValue
        cell.txtNoOfCase?.text = infos["no_of_cases"].stringValue
        cell.txtNoOfCase?.rowIndex = 1
        cell.txtDiscountPerCase?.rowIndex = 2
        cell.txtNoOfCase?.itemIndex = indexPath.row
       cell.txtDiscountPerCase?.itemIndex = indexPath.row
        cell.txtDiscountPerCase?.pricevalue = infos["pricepercase"].doubleValue.round(to: 2)
        
        cell.txtDiscountPerCase?.text = infos["discountpercase"].stringValue
        cell.delegate = self
        cell.dele = self
     
        
        cell.imgProductimg?.sd_setImage(with: infos["product_image"].url, placeholderImage: UIImage(imageLiteralResourceName: "Product_PH"), options:.cacheMemoryOnly, completed: nil)
       
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            guard let list = self.ProductCartData?[indexPath.row] else{return }
            self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: "Are you sure you want to delete this Item"), actionTitles: [self.languageKey(key: "NO"),self.languageKey(key: "YES")], actions:[{action1 in
                return
                },{action2 in
                    
                    self.DeleteCartapi(Product_id: String(list["cartproduct_id"].intValue), ItemDelete: true)
                }, nil])
            // handle action by updating model with deletion
        }
        
        // customize the action appearance
        deleteAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor: .white, size: CGSize(width: 40, height: 40))
        
        return [deleteAction]
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
   
    fileprivate func ItemDeleteRequest(order: Int)
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            self.tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        OrderDelete_Api().Request(orderID: order) { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                return
            }
            AppDelegate.showWaitView()
            self.ProductCartDataFetch()
            return
            
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
}


// MARK: - PullToRefresh

extension ShowCartVC
{
    override func loadView() {
        super.loadView()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = self.navBackgroundColor ?? BillNAVBAR()
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = SettingNAVBAR()
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.ProductCartDataFetch()
            
            }, loadingView: loadingView)
        tableView.dg_setPullToRefreshFillColor(navBackgroundColor ?? WhiteColor())
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
}

// MARK: - Class Instance

extension ShowCartVC
{
    class func instance()->ShowCartVC?{
        let storyboard = UIStoryboard(name: "Products", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ShowCartVC") as? ShowCartVC
        
        
        return controller
    }
    
    
    
    
}



