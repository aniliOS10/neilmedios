//
//  ContactsVC.swift
//  MedMobilie
//
//  Created by dr.mac on 20/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//
import UIKit
import DGElasticPullToRefresh
import SwiftyJSON
import SDWebImage
import SwipeCellKit
import Letters
import CoreData


class ContactsVC: InterfaceExtendedController  ,UISearchBarDelegate {
    
    fileprivate var contactsData = [JSON]()
    fileprivate var mainJsonData = [JSON]()
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var ComeFromReports : Bool = false
    var datefrom : String = ""
    var dateto : String = ""
    var reportKey : String = ""
    var resultSearchController = UISearchController()
    
    
    @IBOutlet weak var lblPlusSign: UILabel!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var mtableview: UITableView!
    
    fileprivate var offset : Int = 0
    fileprivate var totalContacts : Int = 0
    fileprivate var FilterString : String = ""
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // self.LanguageSet()
        btnView.createCircleForView()
        
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
        
        
        self.tabBarController?.navigationController?.navigationBar.isHidden = true
        paintNavigationTitle(title : "Contacts".languageSet,Color : .black)
        if !self.ComeFromReports
        {
            PaintleftSideMenu()
            self.NavBarIcon()
        }
        lblPlusSign.font = UIFont.ionicon(of: 30)
        lblPlusSign.text = String.ionicon(with: .plus )
        self.mtableview.delegate = self
        self.mtableview.dataSource = self
        self.mtableview.tableFooterView = UIView()
        self.mtableview.separatorStyle = .none
        mtableview.rowHeight = UITableView.automaticDimension
        mtableview.estimatedRowHeight = 100
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            if self.reportKey == ""
            {
//                AppDelegate.showWaitView()
                self.FetchTotalContactsOfContacts()
                self.FetchingOfflineData(skip: 0)
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if reportKey == ""
        {
            SetUpSearchBar()
            
        }
        PaintNavigationBar(TitleColor: UIColor.black, BackgroundColor: WhiteColor(), BtnColor: UIColor.black)
        if reportKey != ""
        {
            mainJsonData = [JSON]()
            AppDelegate.showWaitView()            
            DataFetch()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.resultSearchController.dismiss(animated: false, completion: nil)
    }
    
    // MARK: - Language Setting
    
    @objc override func LanguageSet(){
        
    }

    fileprivate func editContactData(_ infos :JSON)
    {
        guard let AddContactViewController =  AddContactVC.instance() else{return}
        AddContactViewController.boolvalue = true
        //   AddContactViewController.isAuthrizedContact = !infos["is_editable"].boolValue
        AddContactViewController.isAuthrizedContact = false
        var productStr = ""
        var ProductToMarket_Array  = [Int]()
                        
        if infos["product_ids"].exists()
        {
            
            for index in 0..<((infos["product_ids"].arrayValue).count)
            {
                let product = (infos["product_ids"][index])
                print(product)
                print(product["product_name"])
                productStr += "\(product["product_name"])\(",")"
                let idvalue = product["product_id"].intValue
                ProductToMarket_Array.append(idvalue)
                print(productStr)
            }
            
            if productStr.count != 0
            {
                productStr.removeLast()
            }
            
        }
        
        AddContactViewController.ProductArray = ProductToMarket_Array
       
        AddContactViewController.skippedArray = [infos["salutation"].stringValue,infos["contact_name"].stringValue,infos["attn_name"].stringValue,infos["category_name"].stringValue,infos["contact_title_name"].stringValue,productStr, infos["primary_speciality_name"].stringValue, infos["secondary_specialities_name"].stringValue,infos["company_name"].stringValue,infos["phone"].stringValue,infos["email"].stringValue,infos["website"].stringValue,infos["lead_status_name"].stringValue,infos["lead_source"].stringValue,infos["willingtostock"].stringValue,infos["address1"].stringValue,infos["address2"].stringValue,infos["country_name"].stringValue,infos["state_name"].stringValue,infos["city"].stringValue,infos["zipcode"].stringValue,infos["fax"].stringValue,infos["type"].stringValue,infos["default_billing_address"].stringValue,infos["default_shipping_address"].stringValue,infos["id"].intValue,infos["billing_address1"].stringValue,infos["billing_address2"].stringValue,infos["billing_city"].stringValue,infos["billing_zipcode"].stringValue,infos["billing_country_name"].stringValue,infos["billing_state_name"].stringValue,infos["billing_fax"].stringValue,infos["shipping_address1"].stringValue,infos["shipping_address2"].stringValue,infos["shipping_city"].stringValue,infos["shipping_zip_code"].stringValue,infos["shipping_country_name"].stringValue,infos["shipping_state_name"].stringValue,infos["shipping_fax"].stringValue]
        
       
        AddContactViewController.SaveArray = [infos["salutation_id"].stringValue,infos["contact_name"].stringValue,infos["attn_name"].stringValue,infos["category_id"].intValue,infos["contact_title_id"].intValue,productStr,infos["primary_speciality_id"].stringValue, infos["secondary_speciality_id"].stringValue,infos["company_name"].stringValue,infos["phone"].stringValue,infos["email"].stringValue,infos["website"].stringValue,infos["lead_status_id"].intValue,infos["lead_source"].stringValue,infos["willingtostock"].stringValue,infos["address1"].stringValue,infos["address2"].stringValue,infos["country_id"].stringValue,infos["state_id"].stringValue,infos["city"].intValue,infos["zipcode"].intValue,infos["fax"].stringValue,infos["type"].stringValue,infos["default_billing_address"].stringValue,infos["default_shipping_address"].stringValue,infos["id"].intValue,infos["billing_address1"].stringValue,infos["billing_address2"].stringValue,infos["billing_city"].stringValue,infos["billing_zipcode"].stringValue,infos["billing_country_id"].stringValue,infos["billing_state_id"].stringValue,infos["billing_fax"].stringValue,infos["shipping_address1"].stringValue,infos["shipping_address2"].stringValue,infos["shipping_city"].stringValue,infos["shipping_zip_code"].stringValue,infos["shipping_country_id"].stringValue,infos["shipping_state_id"].stringValue,infos["shipping_fax"].stringValue]
            
        
        if infos["default_billing_address"].stringValue == "No"
        {
            
            AddContactViewController.DefaultBillingSelection = false
        }
        else
        {
            AddContactViewController.DefaultBillingSelection = true
            
        }
        
        if infos["default_shipping_address"].stringValue == "No"
        {
            
            AddContactViewController.DefaultShippingSelection = false
        }
        else
        {
            AddContactViewController.DefaultShippingSelection = true
            
        }
        AddContactViewController.delegate = self
        
        self.navigationController?.pushViewController(AddContactViewController, animated: true)
        
    }
    
    // MARK: - SetUp SearchBar
    
    func SetUpSearchBar()
    {
        self.resultSearchController = ({
            
            let controller = UISearchController(searchResultsController: nil)
            // controller.delegate = self
            controller.searchBar.delegate = self
            controller.searchBar.placeholder = self.languageKey(key: "Search Contacts".languageSet)
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.hidesNavigationBarDuringPresentation = false
            self.mtableview.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
    }
    
    // MARK: - Fetch Contact Data
    
    fileprivate func DataFetch()
    {
        print("DataFetch called")
        var param :[String: Any]?
        if reportKey != ""
        {
            param = [:]
            if reportKey != nil && reportKey != ""{
                param?["report_type"] = reportKey
            }
             if dateto != nil && dateto != ""{
                param?["dateto"] = dateto
            }
           if datefrom != nil && datefrom != ""{
            param?["datafrom"] = datefrom
            }
            if param?.keys.count == 0{
                param = nil
            }
        }
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            return
        }
        
        ContactListFetch().Fetch( Parama: nil ) { (status, message, data) in
            
            if !status
            {
                AppDelegate.hideWaitView()
                
                self.mtableview.dg_stopLoading()
                self.alert(message)
                return
            }
            
            self.mtableview.dg_stopLoading()
            self.mainJsonData = data ?? [JSON]()
            self.contactsData = self.mainJsonData
            self.mtableview.reloadData()
            
            print("contacts-----------1",self.contactsData)
            AppDelegate.hideWaitView()
            
            
        }
    }
    
    
    // MARK: - Offline Fetching Contact Data Localdatabase
    
    fileprivate func FetchTotalContactsOfContacts ()
    {
        self.totalContacts = DataBaseHelper.ShareInstance.getRecordsCount()
    }
    
    
    fileprivate func FetchingOfflineData(skip : Int , str : String = "")
    {
        
        print("offlineData ---- -----")
        
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        if self.contactsData.count > 1 {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }
        DataBaseHelper.ShareInstance.FetchContactLimitedData(offset : skip ,filterWord: FilterString ){ (json , totalCounts) in
            
            AppDelegate.hideWaitView()
            DispatchQueue.main.async{
                
                self.contactsData = [JSON]()
                
                self.mtableview.dg_stopLoading()
                
                self.mainJsonData.append(json)
                
                self.contactsData = self.mainJsonData
                

                self.mtableview.reloadData()
                print("contacts-----------offLine",self.contactsData)
                
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                
               /* if self.contactsData.count == 0 {
                    self.FetchingOfflineData(skip: 0)
                }
                else
                {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                }*/
                
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                
                if self.contactsData.count == 0 {
                    self.alert("No contacts found")
                }
                 
                
            }
        }
    }
    
    
    
    // MARK: - create New Contact Action
    @IBAction func createNewContact(_ sender: Any) {
        
        
        self.popupAlertWithSheet(title: Bundle.appName(), message: nil, actionTitles: [ self.languageKey(key: "Create contact"),self.languageKey(key: "Express contact")], actions:[{action1 in
            
            //            NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 2)
            
            guard let AddContactViewController =  AddContactVC.instance() else{return}
            
            AddContactViewController.delegate = self
            
            self.navigationController?.pushViewController(AddContactViewController, animated: true)
            
            },{action2 in
                
                //                NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 2)
                
                guard let controller = ExpressContactVC.instance()
                    else{return}
                controller.delegate = self
                self.navigationController?.pushViewController(controller, animated: true)
                
            }, nil])
        
    }
    
    // MARK: - updateSearchResults
    
    // MARK: - UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchBar.text != ""
        {
            FilterString = searchBar.text ?? ""
            self.mainJsonData = [JSON]()
            self.FetchingOfflineData(skip: 0, str: searchBar.text ?? "")
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if searchBar.text != ""
        {
            FilterString = searchBar.text ?? ""
            self.mainJsonData = [JSON]()
            self.FetchingOfflineData(skip: 0, str: searchBar.text ?? "")
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        FilterString = ""
        self.mainJsonData = [JSON]()
        
        self.FetchingOfflineData(skip: 0, str:"")
        
    }
    
    
    /* func updateSearchResults(for searchController: UISearchController) {
     // filteredTableData.removeAll(keepingCapacity: false)
     if !searchController.isActive {
     print("Cancelled")
     }
     
     if searchController.searchBar.text != ""
     {
     
     self.FetchingOfflineData(skip: 0, str: searchController.searchBar.text ?? "")
     
     //            let Filterarray = self.mainJsonData.filter({ (element) -> Bool in
     //                if element["contact_name"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "") || element["category_name"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "") || element["email"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "")  || element["phone"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "") || element["salutation"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "")
     //                {
     //                    return true
     //                }
     //                return false
     //            })
     //
     //
     //            self.contactsData = Filterarray
     //            self.mtableview.reloadData()
     }
     else
     {
     //            self.contactsData = mainJsonData
     //            self.mtableview.reloadData()
     //
     }
     
     }*/
    
    
    // MARK: - Contact Delete
    
    fileprivate func ContactDelete(Contactid:Int)
    {
        
        if !NetworkState.isConnected() {
            mtableview.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        ContactDelete_Api().Request(idvalue: Contactid, url_string: "/contact_delete") { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                // self.mtableview.reloadData()
                return
            }
            
            DataBaseHelper.ShareInstance.DeleteEntry(ClassName: OfflineDataClass.Contact.rawValue, Id: Contactid)
            
            
            // self.mtableview.dg_stopLoading()
            // self.contactsData = data
            // self.mtableview.reloadData()
            return
            
            
        }    }
    
    
}


// MARK: - Create Order Protocol

extension ContactsVC : AddContactsToCreateOrderProtocol
{
    func contactdetails(name : String , Contact_id : Int)
    {
        //  AppDelegate.showWaitView()
        self.mainJsonData = [JSON]()
        self.contactsData = [JSON]()
        self.FetchingOfflineData(skip: 0)
    }
}

// MARK: - Table View Delegate/DataSource Methods

extension ContactsVC : UITableViewDelegate,UITableViewDataSource , SwipeTableViewCellDelegate
{
    // MARK: - Table view data source
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("contacts-----------2")
        if contactsData.count > 0
        {
            return contactsData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
      let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TeamCell
      return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if contactsData.count > 0
        {
            if let cell  = cell as?  TeamCell
            {
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
                            
                let infos = contactsData[indexPath.row]
                
                if infos["is_express"].boolValue
                {
                    cell.viewShadow?.bottomViewShadow(ColorName: UIColor.red)
                }
                else
                {
                    cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
                }
                
                cell.imgUser?.setImage(string: infos["contact_name"].stringValue, color: nil, circular: true)
                                
                cell.lblUserName.text = "\(infos["salutation"].stringValue) \(infos["contact_name"].stringValue)"
                cell.lblMessage.text = infos["category_name"].stringValue
                
                
                if !NetworkState.isConnected() {
                    cell.delegate = nil
                }
                else
                {
                    cell.delegate = self
                }
                
                cell.selectionStyle = .none
                
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        
        let deleteAction = SwipeAction(style:.destructive, title:nil) { action, indexPath in
            
            let list = self.contactsData[indexPath.row]
            guard  let jsonTemp = DataBaseHelper.ShareInstance.DataOfContactWithID(Id: list["id"].intValue, type: .Fetch) else{
                return
            }
            if jsonTemp["is_deleteable"].intValue != 0
            {
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: "Are you sure you want to delete this contact?"), actionTitles: [self.languageKey(key: "NO"),self.languageKey(key: "YES")], actions: [{action1 in
                    return
                },
                {action2 in
                    
                    self.contactsData.remove(at: indexPath.row)
                    self.mtableview.reloadData()
                    DispatchQueue.global(qos: .background).async {
                        DispatchQueue.main.async {
                            
                            self.ContactDelete(Contactid: list["id"].intValue)
                        }
                        
                        
                    }
                    
                }, nil])
                
                // self.DeleteContacts(Contactid: list["id"].intValue)
                
            }
            else
            {
                self.alert(self.languageKey(key: "You are not authorized to Delete this contact"))
            }
            
            
            // handle action by updating model with deletion
        }
        
        // customize the action appearance
        
        let editAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            let infos = self.contactsData[indexPath.row]
            
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 2)
            guard  let jsonTemp = DataBaseHelper.ShareInstance.DataOfContactWithID(Id: infos["id"].intValue, type: .Fetch) else{
                return
            }
            
            
            
            if jsonTemp["is_editable"].boolValue
            {
                self.editContactData(jsonTemp)
                return
            }
                
            else
            {
                //                self.alert(self.languageKey(key: "You are not authorized to edit this contact , you have limited access to edit this contact"))
                
                //                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: "you can change only email and phone number of this contact".languageSet, actionTitles: [self.languageKey(key: "Ok")], actions: [{action1 in
                //
                self.editContactData(jsonTemp)
                //                    return
                //
                //                    }, nil])
                
                
            }
            // handle action by updating model with deletion
        }
        
        // customize the action appearance
        
        deleteAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor: .white, size: CGSize(width: 32, height: 32))
        editAction.backgroundColor = UIColor.lightGray
        editAction.image = UIImage.ionicon(with: .edit, textColor: .white, size: CGSize(width: 32, height: 32))
        
        
        return [deleteAction,editAction]
        
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let contactInfos = contactsData[indexPath.row]
        
        
        
        print(contactInfos)
        
        self.popupAlertWithSheet(title: Bundle.appName(), message: nil, actionTitles: [self.languageKey(key: "View Profile"),self.languageKey(key: "Create Order")/*,self.languageKey(key: "Create Visit")*/,self.languageKey(key: "Duplicate contact"),self.languageKey(key: "Past History")], actions:[{action1 in
            
            
            
            //      NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 2)
            
            
            // let infos = self.contactsData[indexPath.row]
            guard  let infos = DataBaseHelper.ShareInstance.DataOfContactWithID(Id: contactInfos["id"].intValue, type: .Fetch) else{
                return
            }
            
            
            print(infos)
            
            guard let AddContactViewController = AddContactVC.instance() else{return}
            AddContactViewController.Save_Btn = true
            
            
            var productStr = ""
            var ProductToMarket_Array  = [Int]()
            
            if infos["product_ids"].exists()
            {
                
                for index in 0..<((infos["product_ids"].arrayValue).count) {
                    
                    
                    let product = (infos["product_ids"][index])
                    
                    print(product)
                    
                    print(product["product_name"])
                    
                    
                    productStr += "\(product["product_name"])\(",")"
                    
                    
                    let idvalue = product["product_id"].intValue
                    
                    ProductToMarket_Array.append(idvalue)
                    
                    
                    
                    print(productStr)
                }
                
                if productStr.count != 0
                {
                    productStr.removeLast()
                }
            }
            
            
            AddContactViewController.ProductArray = ProductToMarket_Array
            
            AddContactViewController.skippedArray = [infos["salutation"].stringValue,infos["contact_name"].stringValue,infos["attn_name"].stringValue,infos["category_name"].stringValue,infos["contact_title_name"].stringValue,productStr, infos["primary_speciality_name"].stringValue, infos["secondary_specialities_name"].stringValue,infos["company_name"].stringValue,infos["phone"].stringValue,infos["email"].stringValue,infos["website"].stringValue,infos["lead_status_name"].stringValue,infos["lead_source"].stringValue,infos["willingtostock"].stringValue,infos["address1"].stringValue,infos["address2"].stringValue,infos["country_name"].stringValue,infos["state_name"].stringValue,infos["city"].stringValue,infos["zipcode"].stringValue,infos["fax"].stringValue,infos["type"].stringValue,infos["default_billing_address"].stringValue,infos["default_shipping_address"].stringValue,0,infos["billing_address1"].stringValue,infos["billing_address2"].stringValue,infos["billing_country_name"].stringValue,infos["billing_state_name"].stringValue,infos["billing_city"].stringValue,infos["billing_zipcode"].stringValue,infos["billing_fax"].stringValue,infos["shipping_address1"].stringValue,infos["shipping_address2"].stringValue,infos["shipping_country_name"].stringValue,infos["shipping_state_name"].stringValue,infos["shipping_city"].stringValue,infos["shipping_zip_code"].stringValue,infos["shipping_fax"].stringValue]
            
           
            AddContactViewController.SaveArray = [infos["salutation_id"].stringValue,infos["contact_name"].stringValue,infos["attn_name"].stringValue,infos["category_id"].intValue,infos["contact_title_id"].intValue,ProductToMarket_Array,infos["primary_speciality_id"].stringValue, infos["secondary_speciality_id"].stringValue,infos["company_name"].stringValue,infos["phone"].stringValue,infos["email"].stringValue,infos["website"].stringValue,infos["lead_status_id"].intValue,infos["lead_source"].stringValue,infos["willingtostock"].stringValue,infos["address1"].stringValue,infos["address2"].stringValue,infos["country_id"].stringValue,infos["state_id"].stringValue,infos["city"].intValue,infos["zipcode"].intValue,infos["fax"].stringValue,infos["type"].stringValue,infos["default_billing_address"].stringValue,infos["default_shipping_address"].stringValue,0,infos["billing_address1"].stringValue,infos["billing_address2"].stringValue,infos["billing_country_id"].stringValue,infos["billing_state_id"].stringValue,infos["billing_city"].stringValue,infos["billing_zipcode"].stringValue,infos["billing_fax"].stringValue,infos["shipping_address1"].stringValue,infos["shipping_address2"].stringValue,infos["shipping_country_id"].stringValue,infos["shipping_state_id"].stringValue,infos["shipping_city"].stringValue,infos["shipping_zip_code"].stringValue,infos["shipping_fax"].stringValue]
            
            
             
            if infos["default_billing_address"].stringValue == "No"
            {
                
                AddContactViewController.DefaultBillingSelection = false
            }
            else
            {
                AddContactViewController.DefaultBillingSelection = true
                
            }
            
            if infos["default_shipping_address"].stringValue == "No"
            {
                
                AddContactViewController.DefaultShippingSelection = false
            }
            else
            {
                AddContactViewController.DefaultShippingSelection = true
                
            }
            
            
            
            
            
            self.navigationController?.pushViewController(AddContactViewController, animated: true)
            
            }
            
            ,{action2 in
                
                //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 2)
                
                guard let controller = ProductListingVC.instance()
                    else{return}
                controller.ContactsFlag = true
                controller.contact_id = contactInfos["id"].intValue
                self.navigationController?.pushViewController(controller, animated: true)
                
            },/*,{action3 in
             
             //        NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 2)
             
             guard let obj_CreateVisit = CreateVisit.instance()
             else{return}
             obj_CreateVisit.contact_id = infos["id"].intValue
             obj_CreateVisit.contact_name = infos["contact_name"].stringValue
             obj_CreateVisit.salutation = infos["salutation"].stringValue
             obj_CreateVisit.category_name = infos["category_name"].stringValue
             
             
             
             
             
             obj_CreateVisit.skippedArray = ["","",infos["address1"].stringValue,infos["address2"].stringValue,infos["phone"].stringValue,infos["city"].stringValue,infos["zipcode"].stringValue,infos["country_name"].stringValue,infos["state_name"].stringValue,"","","","","",""]
             
             
             
             
             
             
             obj_CreateVisit.SaveArray = ["","",infos["address1"].stringValue,infos["address2"].stringValue,infos["city"].stringValue,infos["zipcode"].stringValue,infos["country_id"].stringValue,infos["state_id"].stringValue,"","","","","",""]
             
             
             
             self.navigationController?.pushViewController(obj_CreateVisit, animated: true)
             
             },*/{action4 in
                
                //       NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 2)
                
                
                //  let infos = self.contactsData[indexPath.row]
                guard  let infos = DataBaseHelper.ShareInstance.DataOfContactWithID(Id: contactInfos["id"].intValue, type: .Fetch) else{
                    return
                }
                
                guard let AddContactViewController = AddContactVC.instance() else{return}
                
                var productStr = ""
                var ProductToMarket_Array  = [Int]()
                
                
                if infos["product_ids"].exists()
                {
                    
                    for index in 0..<((infos["product_ids"].arrayValue).count) {
                        
                        
                        let product = (infos["product_ids"][index])
                        
                        print(product)
                        
                        print(product["product_name"])
                        
                        
                        productStr += "\(product["product_name"])\(",")"
                        let idvalue = product["product_id"].intValue
                        
                        ProductToMarket_Array.append(idvalue)
                        
                        
                        
                        
                        
                        print(productStr)
                    }
                    
                    if productStr.count != 0
                    {
                        productStr.removeLast()
                    }
                }
                
                
                AddContactViewController.ProductArray = ProductToMarket_Array
                 
                AddContactViewController.delegate = self
                AddContactViewController.skippedArray = [infos["salutation"].stringValue,infos["contact_name"].stringValue,infos["attn_name"].stringValue,infos["category_name"].stringValue,infos["contact_title_name"].stringValue,productStr, infos["primary_speciality_name"].stringValue, infos["secondary_specialities_name"].stringValue,infos["company_name"].stringValue,infos["phone"].stringValue,infos["email"].stringValue,infos["website"].stringValue,infos["lead_status_name"].stringValue,infos["lead_source"].stringValue,infos["willingtostock"].stringValue,infos["address1"].stringValue,infos["address2"].stringValue,infos["country_name"].stringValue,infos["state_name"].stringValue,infos["city"].stringValue,infos["zipcode"].stringValue,infos["fax"].stringValue,infos["type"].stringValue,infos["default_billing_address"].stringValue,infos["default_shipping_address"].stringValue,0,infos["billing_address1"].stringValue,infos["billing_address2"].stringValue,infos["billing_country_name"].stringValue,infos["billing_state_name"].stringValue,infos["billing_city"].stringValue,infos["billing_zipcode"].stringValue,infos["billing_fax"].stringValue,infos["shipping_address1"].stringValue,infos["shipping_address2"].stringValue,infos["shipping_country_name"].stringValue,infos["shipping_state_name"].stringValue,infos["shipping_city"].stringValue,infos["shipping_zip_code"].stringValue,infos["shipping_fax"].stringValue]
                
                 
                AddContactViewController.SaveArray = [infos["salutation_id"].stringValue,infos["contact_name"].stringValue,infos["attn_name"].stringValue,infos["category_id"].intValue,infos["contact_title_id"].intValue,productStr,infos["primary_speciality_id"].stringValue, infos["secondary_speciality_id"].stringValue,infos["company_name"].stringValue,infos["phone"].stringValue,infos["email"].stringValue,infos["website"].stringValue,infos["lead_status_id"].intValue,infos["lead_source"].stringValue,infos["willingtostock"].stringValue,infos["address1"].stringValue,infos["address2"].stringValue,infos["country_id"].stringValue,infos["state_id"].stringValue,infos["city"].intValue,infos["zipcode"].intValue,infos["fax"].stringValue,infos["type"].stringValue,infos["default_billing_address"].stringValue,infos["default_shipping_address"].stringValue,0,infos["billing_address1"].stringValue,infos["billing_address2"].stringValue,infos["billing_country_id"].stringValue,infos["billing_state_id"].stringValue,infos["billing_city"].stringValue,infos["billing_zipcode"].stringValue,infos["billing_fax"].stringValue,infos["shipping_address1"].stringValue,infos["shipping_address2"].stringValue,infos["shipping_country_id"].stringValue,infos["shipping_state_id"].stringValue,infos["shipping_city"].stringValue,infos["shipping_zip_code"].stringValue,infos["shipping_fax"].stringValue]
                
                 
                if infos["default_billing_address"].stringValue == "No"
                {
                    
                    AddContactViewController.DefaultBillingSelection = false
                }
                else
                {
                    AddContactViewController.DefaultBillingSelection = true
                    
                }
                
                if infos["default_shipping_address"].stringValue == "No"
                {
                    
                    AddContactViewController.DefaultShippingSelection = false
                }
                else
                {
                    AddContactViewController.DefaultShippingSelection = true
                    
                }
                
                
                //                    AddContactViewController.skippedArray = ["","",infos["category_name"].stringValue,"","",infos["contact_title_name"].stringValue,infos["company_name"].stringValue,"","",infos["website"].stringValue,infos["lead_status_name"].stringValue,infos["lead_source"].stringValue,infos["willingtostock"].stringValue,infos["address1"].stringValue,infos["address2"].stringValue,infos["city"].stringValue,infos["zipcode"].stringValue,infos["country_name"].stringValue,infos["state_name"].stringValue,"",infos["type"].stringValue,"Yes","Yes",0]
                //
                //
                //                    AddContactViewController.SaveArray = ["","",infos["category_id"].intValue,"","",infos["contact_title_id"].intValue,infos["company_name"].stringValue,"","",infos["website"].stringValue,infos["lead_status_id"].intValue,infos["lead_source"].stringValue,infos["willingtostock"].stringValue,infos["address1"].stringValue,infos["address2"].stringValue,infos["city"].stringValue,infos["zipcode"].stringValue,infos["country_id"].intValue,infos["state_id"].intValue,"",infos["type"].stringValue,"Yes","Yes",0]
                
                self.navigationController?.pushViewController(AddContactViewController, animated: true)
                
            },{action5 in
                
                //      NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 2)
                
                self.ActionSheetView(Contact_id:contactInfos["id"].intValue , Title: "\(contactInfos["salutation"].stringValue)  \(contactInfos["contact_name"].stringValue)")
                
                
            }, nil])
        
        
    }
    
    
    func ActionSheetView(Contact_id: Int,Title: String)  {
        
        
        
        self.popupAlertWithSheet(title: Bundle.appName(), message: nil, actionTitles: [self.languageKey(key: "Order"),self.languageKey(key: "Visit")], actions:[{action1 in
            
            
            
            guard let controller = OrderListVC.instance()
                else{return}
            controller.contact_id = Contact_id
            controller.comeFromContact = true
            self.navigationController?.pushViewController(controller, animated: true)
            
            
            }
            
            ,{action2 in
                
                //     NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 2)
                
                guard let controller = DailyVisit.instance()
                    else{return}
                controller.Titlename = Title
                controller.contactId = Contact_id
                controller.comeFromContact = true
                self.navigationController?.pushViewController(controller, animated: true)
                
            }, nil])
        
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        // calculates where the user is in the y-axis
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height {
            
            
            if self.totalContacts > self.contactsData.count
            {
                self.offset = self.offset + 100
                self.FetchingOfflineData(skip: self.offset)
            }
            //            if self.yourCoreDataRecordArray.count > 10 {
            //                fetchOffSet = fetchOffSet + 10
            //                var array: [Any] = self.getCountryFromDB(fetchOffSet)
            //            }
        }
        
        
    }
    
}


// MARK: - Pull To Refresh

extension ContactsVC
{
    
    override func loadView() {
        super.loadView()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = self.navBackgroundColor ?? BillNAVBAR()
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = SettingNAVBAR()
        
        
        
        if reportKey == ""
        {
            //    mtableview.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            
            mtableview.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
                
                self!.contactsData.removeAll()
                self!.mainJsonData.removeAll()
                
                self!.FetchingOfflineData(skip: 0)
                self!.mtableview.dg_stopLoading()
                
                
                //  self!.DataFetch()
                
                
                }, loadingView: loadingView)
            
            
            //            self?.mainJsonData = [JSON]()
            //            self?.contactsData = [JSON]()
            //            self?.FetchingOfflineData(skip: 0)
            //            //            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
            //            //                self?.mtableview.dg_stopLoading()
            //            //            })
            //
            //            }, loadingView: loadingView)
            
        }
        else
        {
            
            mtableview.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
                
                self!.contactsData.removeAll()
                self!.mainJsonData.removeAll()
                
                
                self!.FetchingOfflineData(skip: 0)
                self!.mtableview.dg_stopLoading()
                
                //  self!.DataFetch()
                
                
                }, loadingView: loadingView)
            
            
            
        }
        mtableview.dg_setPullToRefreshFillColor(navBackgroundColor ?? WhiteColor())
        mtableview.dg_setPullToRefreshBackgroundColor(mtableview.backgroundColor!)
    }
}

// MARK: - Class instance

extension ContactsVC
{
    class func instance()->ContactsVC?{
        let storyboard = UIStoryboard(name: "Team", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ContactsVC") as? ContactsVC
        
        
        return controller
    }
}



