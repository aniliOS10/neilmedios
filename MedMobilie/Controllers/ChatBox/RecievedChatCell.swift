

import UIKit

class RecievedChatCell: UITableViewCell {
    @IBOutlet weak var bubbleImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel?
    @IBOutlet weak var dateLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = .clear
        
        setBubbleImage()
        bubbleImageView.image = bubbleImageView.image!.withRenderingMode(.alwaysTemplate)
        bubbleImageView.tintColor = UIColor.lightGray
        messageLabel?.textColor = .white
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setBubbleImage() {
        let image : UIImage = UIImage(named: "chat_bubble_received")!
        bubbleImageView.image = image
            .resizableImage(withCapInsets:
                UIEdgeInsets(top: 12, left: 16, bottom: 12, right: 16),
                            resizingMode: .stretch)
            .withRenderingMode(.alwaysTemplate)
    }
    
}

