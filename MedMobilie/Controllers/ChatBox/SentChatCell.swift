

import UIKit

class SentChatCell: UITableViewCell {
    @IBOutlet weak var bubbleImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel?
    @IBOutlet weak var dateLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor = .clear
        
        setImage()
        
        
        bubbleImageView.image = bubbleImageView.image!.withRenderingMode(.alwaysTemplate)
        bubbleImageView.tintColor = UIColor(red: 107/255.0, green:
            103/255.0, blue: 148/255.0, alpha: 1.0)
        messageLabel?.textColor = .white
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setImage() {
        let image : UIImage = UIImage(named: "chat_bubble_sent")!
        bubbleImageView.image = image
            .resizableImage(withCapInsets:
                UIEdgeInsets(top: 12, left: 16, bottom: 12, right: 16),
                            resizingMode: .stretch)
            .withRenderingMode(.alwaysTemplate)
    }
    
}

