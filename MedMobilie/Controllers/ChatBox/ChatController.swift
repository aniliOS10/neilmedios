

import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON

class ChatController: InterfaceExtendedController {
    
    
    @IBOutlet weak var tableBottom: NSLayoutConstraint!
    @IBOutlet weak var btnBottom: NSLayoutConstraint!
    @IBOutlet weak var inputToolbar: UIView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var BackButton: UIButton!

    @IBOutlet weak var textView: GrowingTextView!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var topLabel: UILabel?
    
    
     fileprivate var JsonData : [JSON]?
    var messages = [String]()
    var param = [String : Any]()
    
    var userid : Int = 0
    var UserName : String = ""
    var idvalue : Int = 0
    var ClassName : String = ""
    
    @objc func Refress_Chat(notification: NSNotification){
        
        let data = notification.object as! Dictionary<String, Any>
        
        userid = data["user_id"] as! Int
        
        print(userid)
        print("user_id VALUE :---->",userid)
        
        ChatListDataFetch()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(Refress_Chat), name: NSNotification.Name( "Refress_Chat"), object: nil)
        
        var parama = [String:Any]()
        parama["user_id"] = userid
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Check_Chat_ID"), object: parama)
        
        
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = false

        topLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        topLabel?.text = UserName
        
        // *** Customize GrowingTextView ***
        textView.layer.cornerRadius = 4.0
        
        // *** Listen to keyboard show / hide ***
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        
        self.navigationController?.isNavigationBarHidden = true
        
        // *** Hide keyboard when tapping outside ***
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
        
        
        AppDelegate.showWaitView()
        ChatListDataFetch()
        
    }
   
    override func viewDidDisappear(_ animated: Bool) {
            super.viewDidDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    // data fetching
    fileprivate func ChatListDataFetch()
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        print(userid)
        
        
        DataFetch_Api().FetchData( ID: userid , urlString: "/showchat", completion:  { (status, message, data) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                return
            }
            self.JsonData = data
            self.tableView!.reloadData()
            self.scrollToLastRow()
            
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        let trimmedString = textView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        print(trimmedString.count)
        print(trimmedString)
        
        if trimmedString.count != 0{
            
            self.param["message"] = textView.text
            self.param["user_id"] = self.userid
            print(self.param)
            self.SendMessaheDataFetch()
            
            self.textView.text = ""
        }
    }
    
    func SendMessaheDataFetch() {
        
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        SendMessage().Request(param: self.param) { (status,message) in
            
            AppDelegate.hideWaitView()
            self.ChatListDataFetch()
        }
    }
    
    func removeDuplicates(array: [String]) -> [String] {
        var encountered = Set<String>()
        var result: [String] = []
        for value in array {
            if encountered.contains(value) {
                // Do not add a duplicate element.
            }
            else {
                // Add value to the set.
                encountered.insert(value)
                // ... Append the value.
                result.append(value)
            }
        }
        return result
    }
    
    
    func scrollToLastRow() {
        let indexPath = IndexPath(row: (self.tableView?.numberOfRows(inSection: 0))! - 1, section: 0)
        self.tableView?.scrollToRow(at: indexPath, at: .none, animated: false)
    }
    
    
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var keyboardHeight = UIScreen.main.bounds.height - endFrame.origin.y
            if #available(iOS 11, *) {
                if keyboardHeight > 0 {
                    keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
                }
            }
            textViewBottomConstraint.constant = keyboardHeight + 8
            btnBottom.constant = keyboardHeight + 8
            // tableBottom.constant = keyboardHeight + inputToolbar.frame.size.height
            
            let lastSectionIndex = self.tableView!.numberOfSections - 1
            
            // Then grab the number of rows in the last section
            let lastRowIndex = self.tableView!.numberOfRows(inSection: lastSectionIndex) - 1
            if lastRowIndex >= 0{
                // Now just construct the index path
                let pathToLastRow = IndexPath(row: lastRowIndex, section: lastSectionIndex)
                
                // Make the last row visible
                self.tableView?.scrollToRow(at: pathToLastRow, at: .none, animated: false)
            }
            
            
            view.layoutIfNeeded()
        }
    }
    
    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0)
        {
            print("up")
            self.view.endEditing(true)
        }
    }
    @IBAction func btnBackAction(_ sender: Any) {
        
        if ClassName == ""
        {
            self.navigationController?.popViewController(animated: false)
        }
        else
        {
           self.navigationController?.popToRootViewController(animated: false)
        }
        
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss Z"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "h:mm a"
        
        return dateFormatter.string(from: dt!)
    }
        
    func millisToDate(milisecond:Int64) -> String {
        
        let dateVar = milisecond.dateFromMilliseconds()
        
        print(dateVar)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        return dateFormatter.string(from: dateVar)
    }
}

extension Int64 {
    func dateFromMilliseconds() -> Date {
        return Date(timeIntervalSince1970: TimeInterval(self)/1000)
    }
}
extension ChatController : GrowingTextViewDelegate{
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

extension ChatController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return JsonData?.count ?? 0

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let list = JsonData?[indexPath.row]
        
        
        if list!["type"].intValue == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! SentChatCell
            cell.messageLabel?.text = list!["message"].stringValue
            cell.dateLabel?.text = list!["created_date"].stringValue
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! RecievedChatCell
            cell.messageLabel?.text = list!["message"].stringValue
        cell.dateLabel?.text = list!["created_date"].stringValue
            
            return cell
        }
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: (self.JsonData?.count)!-1, section: 0)
            self.tableView!.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
   
}

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}
// Swift 3:
extension Date {
    var stamp: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
    func currentTimeMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

extension ChatController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

let sharedUtils : CommonUtility = CommonUtility()
class CommonUtility: NSObject {
    class func getCurrentDate() -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss Z"
        formatter.timeZone = TimeZone(identifier: "UTC")
        let result = formatter.string(from: date)
        return result
    }
}

extension ChatController
{
    class func instance()->ChatController?{
        let storyboard = UIStoryboard(name: "Chat", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChatController") as? ChatController
        
        return controller
    }
}
