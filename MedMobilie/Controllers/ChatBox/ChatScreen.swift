

import Foundation
import AZPeerToPeerConnection
import SwiftyJSON
import IQKeyboardManagerSwift

class ChatScreen: InterfaceExtendedController {
    
     fileprivate var JsonData : [JSON]?
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var toolbar: UIToolbar!
    
    var messages = [String]()
     var param = [String : Any]()
    
    var userid : Int = 0
    var UserName : String = ""
    
     var idvalue : Int = 0
    
    @objc func Refress_Chat(notification: NSNotification){
        
        let data = notification.object as! Dictionary<String, Any>
        
        userid = data["user_id"] as! Int
        print(userid)
        print("user_id VALUE :---->",userid)
        
         ChatListDataFetch()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(Refress_Chat), name: NSNotification.Name( "Refress_Chat"), object: nil)
                
        var parama = [String:Any]()
        parama["user_id"] = userid
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Check_Chat_ID"), object: parama)
        
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
        textField.delegate = self
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: TeamNAVBAR(), BtnColor: UIColor.white)
        
       IQKeyboardManager.shared.enableAutoToolbar = false
      //  IQKeyboardManager.shared.enable = false
        
        
        // *** Hide keyboard when tapping outside ***
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
            
        NavigationBarTitleName(Title: UserName)
        
        AppDelegate.showWaitView()
        ChatListDataFetch()
        
    }
    
    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    
    // data fetching
    fileprivate func ChatListDataFetch()
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        print(userid)
        
        DataFetch_Api().FetchData( ID: userid , urlString: "/showchat", completion:  { (status, message, data) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                
                return
            }
            self.JsonData = data
            self.tableView!.reloadData()
            self.scrollToBottom()
            
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func sendText(_ sender: Any) {
       
        print(textField.text as Any)
        if  self.textField.text != ""{
               
                self.param["message"] = self.textField.text
                self.param["user_id"] = self.userid
                print(self.param)
                self.SendMessaheDataFetch()
                
    self.textField.text = ""
        }
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: (self.JsonData?.count)!-1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
     func SendMessaheDataFetch() {
        
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        SendMessage().Request(param: self.param) { (status,message) in
           
             AppDelegate.hideWaitView()
            self.ChatListDataFetch()
        }
        
    }
}

extension ChatScreen: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField != self.textField {
            textField.inputAccessoryView = toolbar
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
}

extension ChatScreen: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let list = JsonData?[indexPath.row]
            
             if list!["type"].intValue == 1{
        let cell = tableView.dequeueReusableCell(withIdentifier: "SenderCell") as! ChatTableViewCell
        
        cell.lblText.text = list!["message"].stringValue
        return cell
                
        }
        else
            {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "RecieverCell") as! ChatTableViewCell
                
                cell.lblText.text = list!["message"].stringValue
                return cell
                
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return JsonData?.count ?? 0
    }
}

extension ChatScreen
{
    class func instance()->ChatScreen?{
        let storyboard = UIStoryboard(name: "Chat", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChatScreen") as? ChatScreen
        
        return controller
    }
}








