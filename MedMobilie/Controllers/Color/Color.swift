//
//  Color.swift
//  MedMobilie
//
//  Created by MAC on 07/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//


import UIKit

 func ColorValue(Rvalue:CGFloat,Gvalue:CGFloat,Bvalue:CGFloat,alphavalue:CGFloat) -> UIColor {
        
         let Color = UIColor(red: Rvalue/255.0, green: Gvalue/255.0, blue: Bvalue/255.0, alpha: alphavalue)
        return Color
    
}

func PlanNAVBAR() -> UIColor {
    
    let Color = UIColor(red: 61.0/255.0, green: 140.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    return Color
    
}

func VisitNAVBAR() -> UIColor {
    
    let Color = UIColor(red: 76.0/255.0, green: 219.0/255.0, blue: 196.0/255.0, alpha: 1.0)
    return Color
    
}

func TaskNAVBAR() -> UIColor {
    
    let Color = UIColor(red: 248.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
    return Color
    
}

func CalendarNAVBAR() -> UIColor {
    
    let Color = UIColor(red: 237.0/255.0, green: 150.0/255.0, blue: 100.0/255.0, alpha: 1.0)
    return Color
    
}

func TeamNAVBAR() -> UIColor {
    
    let Color = UIColor(red: 211.0/255.0, green: 61.0/255.0, blue: 66.0/255.0, alpha: 1.0)
    return Color
    
}

func BillNAVBAR() -> UIColor {
    
    let Color = UIColor(red: 82.0/255.0, green: 147.0/255.0, blue: 194.0/255.0, alpha: 1.0)
    return Color
    
}



func TargetNAVBAR() -> UIColor {
    
    let Color = UIColor(red: 246.0/255.0, green: 198.0/255.0, blue: 84.0/255.0, alpha: 1.0)
    return Color
    
}
func CreateNavBar() -> UIColor {
    
    let Color = UIColor(red: 100.0/255.0, green: 87.0/255.0, blue: 111.0/255.0, alpha: 1.0)
    return Color
    
}

func OrderNAVBAR() -> UIColor {
    
    let Color = UIColor(red: 99.0/255.0, green: 87.0/255.0, blue: 113.0/255.0, alpha: 1.0)
    return Color
    
}

func RouteNAVBAR() -> UIColor {
    
    let Color = UIColor(red: 45.0/255.0, green: 172.0/255.0, blue: 57.0/255.0, alpha: 1.0)
    return Color
    
}

func ReportNAVBAR() -> UIColor {
    
    let Color = UIColor(red: 61.0/255.0, green: 140.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    return Color
}

func SettingNAVBAR() -> UIColor {
    
    let Color = UIColor(red: 61.0/255.0, green: 140.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    return Color
}


func WhiteColor() -> UIColor {
    
    let Color = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    return Color
}

func OffWhiteColor() -> UIColor {
    
    let Color = UIColor(red: 248.0/255.0, green: 250.0/255.0, blue: 251.0/255.0, alpha: 1.0)
    return Color
}




