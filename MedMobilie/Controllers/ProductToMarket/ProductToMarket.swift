//
//  Speciality_List.swift
//  MedMobilie
//
//  Created by MAC on 06/04/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProductToMarket: InterfaceExtendedController {
    
    @IBOutlet weak var Mtableview: UITableView!
    fileprivate var ProductToMarket = [JSON]()

    var Value = [JSON]()
    var checked = [String]()
    var checkedarray = [Int]()
    var ProductToMarket_STR  = ""
    var ProductToMarket_Array  = [Int]()
    var delegate : SpecialityProtocol?
    
    // MARK: - Class life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Mtableview.delegate = self
        self.Mtableview.dataSource = self
        self.Mtableview.tableFooterView = UIView()
        
        RightActionButton(Title:languageKey(key:"Save"))
        
        ProductDataFetch("product")
        self.LanguageSet()
    }
    
    // MARK: - Navigationbar Title Language Setting
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Product To Market")
    }
    // MARK: - Fetch Data From LocalDatabase
    
//    fileprivate func Listsapidata()
//    {
//
//        DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.ListingData.rawValue) { (data) in
//
//            if let object = data
//            {
//                var jsonArray = [JSON]()
//                for jsonData in object
//                {
//                    if let jsondata = jsonData.value(forKey: "json") as? [Data] {
//                        jsondata.forEach({$0.retrieveJSON(completion: { (json) in
//                            jsonArray.append(json!)
//                        })})
//                    }
//                }
//
//
//
//
//
//                AppDelegate.hideWaitView()
//
//
//
//                self.Value = jsonArray
//
//                self.ProductToMarket = [self.Value[0]["products"]]
//
//
//
//                self.checked = Array(repeating: "", count:
//                    self.ProductToMarket[0].count)
//
//                self.checkedarray = Array(repeating: 0, count:
//                    self.ProductToMarket[0].count)
//
//
//
//
//
//
//
//
//            }
//        }
//
//    }
//
    func ProductDataFetch(_ DetailsType: String)
    {
        if !NetworkState.isConnected() {
            self.Mtableview.dg_stopLoading()
            AppDelegate.hideWaitView()
            
            FetchingOfflineProductData()
            
            return
        }
        
        ProductListingFetch().Fetch(ProductType: DetailsType) { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message ?? "Something Went Wrong")
                self.ProductToMarket = data!
                self.Mtableview!.reloadData()
                self.Mtableview.dg_stopLoading()
                
                self.checked = Array(repeating: "", count:
                self.ProductToMarket.count)
                
                self.checkedarray = Array(repeating: 0, count:
                    self.ProductToMarket.count)
                
                return
            }
            self.ProductToMarket = data!
            
            
            self.checked = Array(repeating: "", count:
                self.ProductToMarket.count)
            
            self.checkedarray = Array(repeating: 0, count:
                self.ProductToMarket.count)
            
            self.Mtableview.reloadData()
            self.Mtableview.dg_stopLoading()
        }
    }
    // MARK: - Fetching Offline ProductData
    
    fileprivate func FetchingOfflineProductData()
    {
        if !NetworkState.isConnected() {
            DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: "Product") { (data) in
                AppDelegate.hideWaitView()
                if let object = data
                {
                    var jsonArray = [JSON]()
                    for jsonData in object
                    {
                        if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                            jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                                jsonArray.append(json!)
                            })})
                        }
                    }
                    self.ProductToMarket = jsonArray
                    self.Mtableview.reloadData()
                    self.Mtableview.dg_stopLoading()
                    
                    AppDelegate.hideWaitView()
                    
                }
            }
        }
    }
    
    // MARK: - Save Speciality List
    
    override func rightButtonAction(){
        
        let filteredItems = checked.filter { $0 != ""}
          let filteredArray = checkedarray.filter { $0 != 0}
        
        
        if  filteredArray.count != 0
        {
            
            for index in 0..<(filteredItems.count) {
                ProductToMarket_STR += "\(filteredItems[index] as String)\(",")"
                ProductToMarket_Array.append(filteredArray[index] as Int)
                print(ProductToMarket_STR)
            }
            ProductToMarket_STR.removeLast()
            
            self.navigationController?.popViewController(animated: true)
            self.delegate?.Speciality_String(List_Iteam: ProductToMarket_STR, ProductArray: ProductToMarket_Array)
            
        }
        else
        {
            self.popupAlert(title: Bundle.appName(), message: self.languageKey(key: "you must select atleast one Product"), actionTitles: [self.languageKey(key: "OK")], actions:[{action1 in
                print("ok")
                return
                }, nil])
        }
    }
}

// MARK: - Table View Delegate/DataSource Methods

extension ProductToMarket: UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !self.ProductToMarket.isEmpty {
            return  self.ProductToMarket.count
        } else {
            return 0
        }
    }
    
    
    
    private func tableView (tableView:UITableView , heightForHeaderInSection section:Int)->Float
    {
        return 20.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
        cell.Title_LBL.font = SystemFont(FontSize: 18)
        cell.Title_LBL.textColor = UIColor.black
        cell.Title_LBL.text = self.ProductToMarket[indexPath.row]["name"].stringValue
        cell.selectionStyle = .none
        if checked[indexPath.row] == "" {
            cell.accessoryType = .none
        } else {
            cell.accessoryType = .checkmark
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark {
                cell.accessoryType = .none
                checked[indexPath.row] = ""
                checkedarray[indexPath.row] = 0
            } else {
                cell.accessoryType = .checkmark
                checked[indexPath.row] = self.ProductToMarket[indexPath.row]["name"].stringValue
                
                checkedarray[indexPath.row] = self.ProductToMarket[indexPath.row]["id"].intValue
            }
        }
    }
}
// MARK: - Class Instance


extension ProductToMarket
{
    class func instance()->ProductToMarket?{
        let storyboard = UIStoryboard(name: "Team", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ProductToMarket") as? ProductToMarket
        
        return controller
    }
}



