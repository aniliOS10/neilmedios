//
//  AddContectVC.swift
//  MedMobilie
//
//  Created by MAC on 29/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
// Rana code // store board

import UIKit
import IoniconsKit
import MMDrawerController
import SwiftyJSON
import Photos
import SwipeCellKit


class CreateBill: InterfaceExtendedController {
    
    fileprivate var selectedContact_array : [String]?
    fileprivate var temp_textField = UITextField()
    fileprivate var Pickerdatavalue = [JSON]()
    fileprivate var ContactName  = [String]()
    fileprivate var contactId = [Int]()
    fileprivate var dataList = [JSON]()
    
    let imagePicker = UIImagePickerController()
    
    var indexvalu = 0
    var RowValue = 0
    var numberOfCell = 2
    var ContactString = String()
    var Datevalue = String()
    var Timevalue = String()
    var ImgArray = [UIImage]()
    var ImgSaveArray = [String]()
    var selectedCountryId : Int = 0
    var dataPickerView = UIPickerView()
    var Time_Picker = UIDatePicker()
    var Date_Picker = UIDatePicker()
    var arrSctionTitle = NSArray()
    var arrTitle = NSArray()
    var  cell = UITableViewCell()
    var toolBar = UIToolbar()
    var delegate  : AddContactsToCreateOrderProtocol?
    var DummyArray = NSMutableArray ()
    var ContactArray = [String : Any]()
    var dicSet = NSMutableDictionary()
    var skippedArray : NSMutableArray =         ["","","","","","",""]
    var SaveArray : NSMutableArray =         ["","","","","","",""]
    
    var BackDataBlock: ((_ json : [JSON]) -> ())?
    
    @IBOutlet weak var mTableview: UITableView!
    
    // MARK: - Class life cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createPickerView()
        imagePicker.delegate = self
        self.mTableview.delegate = self
        self.mTableview.dataSource = self
        self.mTableview.tableFooterView = UIView()
        
        RightActionButton(Title:languageKey(key:"Submit"))
        Listsapidata()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: BillNAVBAR(), BtnColor: UIColor.white)
    }
    
    // MARK: - Language Setting Function
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Create Bills")
        
        arrSctionTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Attached Files", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Description", comment: "")]
        
        arrTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Category", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Invoice Date", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Price", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Location", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Title", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "description", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Description", comment: "")]
        
    }
    // MARK: - Create PickerView
    
    func createPickerView(){
        dataPickerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216)
        dataPickerView.delegate = self
        dataPickerView.dataSource = self
        dataPickerView.backgroundColor = UIColor.white
        dataPickerView.showsSelectionIndicator = true
        
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        if #available(iOS 13.4, *) {
            if #available(iOS 14.0, *) {
                Date_Picker.preferredDatePickerStyle = .wheels
            } else {
                Date_Picker.preferredDatePickerStyle = .wheels
            }
        } else {
            // Fallback on earlier versions
        }
        
        Date_Picker.datePickerMode = UIDatePicker.Mode.date
        Date_Picker.addTarget(self, action: #selector(datePickerChanged), for: UIControl.Event.valueChanged)
        
    }
    // MARK: - Label Border Line Method
    
    func DashedLineBorder (title: String)-> UILabel {
        
        let ContainerView = UILabel(frame: CGRect(x: 10, y: 10, width: mTableview.frame.size.width-20, height: 55))
        ContainerView.layer.addSublayer(Border(YourLable: ContainerView))
        ContainerView.textAlignment = .center
        ContainerView.font = UIFont.boldSystemFont(ofSize: 16)
        ContainerView.textColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        ContainerView.text = title
        return ContainerView
        
    }
    // MARK: - Create TextView Method
    
    func textView (Title: String)-> UITextView
    {
        let view = UITextView(frame: CGRect(x: 10, y: 10, width: mTableview.frame.size.width-20, height: 80))
        view.delegate = self
        view.backgroundColor = OffWhiteColor()
        view.text = Title
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.gray.cgColor
        view.font = UIFont.systemFont(ofSize: 15)
        return view
    }
    
    // MARK: - Fetching Billing Category List
    
    fileprivate func Listsapidata()
    {
        if !NetworkState.isConnected() {
            
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        DataFetch_Api().Fetch( ID: 0 , urlString: "/billapp_expensive", completion:  { (status, message, data) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                return
            }
            self.dataList = data!
        })
    }
    
    // MARK: - Create Billing Action (Save Button)
    
    override func rightButtonAction(){
        
        var datavalues = [String : Any]()
        
        datavalues["expense_id"] = SaveArray[0]
        datavalues["date"] = SaveArray[1]
        datavalues["expense_amount"] = SaveArray[2]
        datavalues["location"] = SaveArray[3]
        datavalues["title"] = SaveArray[4]
        datavalues["description"] = SaveArray[5]
        datavalues["AttachmentFiles"] = ImgArray
        
        print(datavalues)
        
        CreateBillRequest(datavalues, Method: "/billapp_create")
        
        // }
    }
    // MARK: - Create Bill  Request
    
    
    fileprivate func CreateBillRequest(_ param : [String : Any],Method :String)
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        AddContactApi().Fetch(param: param,Url: Method) { (status, message, info) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                self.alert(message)
                return
            }
            else
            {
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: message), actionTitles: [self.languageKey(key: "Ok")], actions: [{action1 in
                    
                    if let CreatedBill = info
                    {
                        self.BackDataBlock?(CreatedBill)
                    }
                    self.navigationController?.popViewController(animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateBillList"), object: nil)
                    
                }, nil])
            }
        }
    }
}

// MARK: - Create Lable Border Method

extension CreateBill
{
    func Border(YourLable: UILabel) -> CAShapeLayer{
        
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.black.cgColor
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = YourLable.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: YourLable.bounds).cgPath
        
        return yourViewBorder
    }
}

// MARK: - Table View Delegate/DataSource Methods

extension CreateBill: UITableViewDelegate,UITableViewDataSource,SwipeTableViewCellDelegate
{
    // MARK: - Table view data source
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 5
            
        case 1:
            
            return (self.ImgArray.count) + 1
            
        case 2:
            
            return 1
            
        default:
            return 0
        }
        //return arrTitle.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrSctionTitle.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = OffWhiteColor()
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: tableView.frame.width, height: 30)
        label.text = arrSctionTitle[section] as? String
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = UIColor.black
        //  label.backgroundColor = UIColor.yellow
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return  5
        }
        else
        {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 2 {
            return  100
        }
        else
        {
            
            return 75
        }
    }
    
    func ShowDropDownImage(Image: Ionicons) -> UIImageView {
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 40, width: 30, height: 30)
        imageView.image = UIImage.ionicon(with: Image, textColor: UIColor.black, size: CGSize(width: 50, height: 50))
        
        return imageView
    }
    
    func HideDropDownImage() -> UIImageView {
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 20, width: 1, height: 1)
        imageView.image = UIImage.ionicon(with: .iosArrowDown, textColor: .clear, size: CGSize(width: 1, height: 1))
        
        return imageView
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
            
            let str2 = arrTitle[indexPath.row] as? String
            
            cell.lbtitleAC.text = arrTitle[indexPath.row] as? String
            
            //str2!.replacingOccurrences(of: "*", with: "", options:
            //    NSString.CompareOptions.literal, range: nil)
            cell.txtTitleAC.text = skippedArray[indexPath.row] as? String
            cell.txtTitleAC.tag = indexPath.row
            cell.txtTitleAC.delegate = self
            cell.selectionStyle = .none
            cell.txtTitleAC.keyboardType = .default
            
            if indexPath.row == 0
            {
                cell.txtTitleAC.rightViewMode = .always
                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosArrowDown)
                
                self.pickUpDate(cell.txtTitleAC)
            }
            else if indexPath.row == 1
            {
                
                cell.txtTitleAC.rightViewMode = .always
                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosCalendarOutline)
                self.pickUpDate(cell.txtTitleAC)
            }
            else if indexPath.row == 2
            {
                cell.txtTitleAC.keyboardType = .default
                cell.txtTitleAC.rightViewMode = .never
                cell.txtTitleAC.rightView = nil
            }
            else
            {
                cell.txtTitleAC.rightViewMode = .never
                cell.txtTitleAC.rightView = nil
            }
            return cell
        }
        
        else if indexPath.section == 1
        {
            let myCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
            myCell.backgroundColor = UIColor.white
            
            let totalRows = tableView.numberOfRows(inSection: indexPath.section)
            
            if indexPath.row == totalRows - 1 {
                
                myCell.contentView .addSubview(DashedLineBorder(title: "Attached file here".languageSet))
            }
            else
            {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "AttachmentCell", for: indexPath) as? TeamCell
                else
                {
                    return UITableViewCell()
                }
                cell.selectionStyle = .none
                
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
                cell.lblUserName.text = ImgSaveArray[indexPath.row]
                cell.imgUser.image = UIImage.fontAwesomeIcon(name: .fileImage, style: .regular, textColor: VisitNAVBAR(), size: CGSize(width: 40.0, height: 40.0))
                cell.delegate = self
                
                return cell
            }
            
            myCell.selectionStyle = .none
            return myCell
        }
        else  if indexPath.section == 2
        {
            let myCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
            myCell.backgroundColor = UIColor.white
            myCell.selectionStyle = .none
            
            myCell.contentView .addSubview(textView(Title: SaveArray[5] as! String))
            return myCell
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        
        let deleteAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            if indexPath.section == 1
            {
                
                self.ImgArray.remove(at: indexPath.row)
                
            }
            self.mTableview.reloadData()
        }
        
        deleteAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor: .white, size: CGSize(width: 32, height: 32))
        
        return [deleteAction]
        
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0
        {
        }
        else if indexPath.section == 1
        {
            
            let totalRows = tableView.numberOfRows(inSection: indexPath.section)
            
            if indexPath.row == totalRows - 1 {
                self.ActionSheet()
                
            }
            else
            {
                
                guard let controller = ShowImage.instance() else{return}
                controller.img_Var = ImgArray[indexPath.row]
                self.navigationController?.pushViewController(controller, animated: true)
                
            }
        }
    }
    
    @IBAction func ChooseAction() {
        
        if ContactString != ""
        {
            ContactArray = ["id":0,"title":ContactString]
            ContactString = ""
            
            print(ContactArray)
            
            DummyArray.add(ContactArray)
            
            print(DummyArray)
            print(DummyArray.count)
            
        }
        
        guard let objContactsVC = SelectContactVC.instance() else{return}
        objContactsVC.delegate = self
        objContactsVC.CheckArray = DummyArray
        self.navigationController?.pushViewController(objContactsVC, animated: true)
    }
    
    @IBAction func AddAction()
    {
        if ContactString != ""
        {
            ContactArray = ["id":0,"title":ContactString]
            ContactString = ""
            
            print(ContactArray)
            
            DummyArray.add(ContactArray)
            
            print(DummyArray)
            print(DummyArray.count)
            
            mTableview.reloadData()
        }
    }
    
    func ActionSheet()
    {
        
        let alert = UIAlertController(title: nil, message: languageKey(key: "Choose option"), preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Take Photo"), style: .default , handler:{ (UIAlertAction)in
            
            self.openCamera()
            
        }))
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Choose Photo"), style: .default , handler:{ (UIAlertAction)in
            
            self.photoLibrary()
        }))
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Cancel") , style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        alert.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        
        self.present(alert, animated: true) {
            print("option menu presented")
        }
    }
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            navigationController!.setNavigationBarHidden(true, animated: false)
            // Add it as a subview
            
            addChild(imagePicker)
            view.addSubview(imagePicker.view)
            self.tabBarController?.tabBar.isHidden = true
        }
    }
    
    func photoLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            // imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            navigationController!.setNavigationBarHidden(true, animated: false)
            // Add it as a subview
            
            addChild(imagePicker)
            view.addSubview(imagePicker.view)
            self.tabBarController?.tabBar.isHidden = true
        }
    }
}
// MARK: - TextField Delegate Methods


extension CreateBill : UITextFieldDelegate
{
    // datepickerView
    
    func pickUpDate(_ textField : UITextField){
        
        print("TagValue:----->>>",textField.tag)
    }
    
    func CurrentDate()->String{
        let currentDateTime = Date()
        
        // initialize the date formatter and set the style
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: currentDateTime)
        
    }
    
    func CurrentTime()->String{
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter.string(from: currentDateTime)
        
    }
    
    @objc func datePickerChanged(datePicker:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        Datevalue = dateFormatter.string(from: datePicker.date)
        print(Datevalue)
    }
    
    @objc func TimePickerChanged(datePicker:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        Timevalue = dateFormatter.string(from: datePicker.date)
        
        print(Timevalue)
        
    }
    
    @objc func doneClick() {
        
        if indexvalu == 0 {
            
            skippedArray[indexvalu] = Pickerdatavalue[RowValue]["title"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[RowValue]["id"].stringValue
            
            print(Pickerdatavalue)
            
        }
        else if indexvalu == 1
        {
            if Datevalue == ""
            {
                skippedArray[1] = CurrentDate()
                SaveArray[1] = CurrentDate()
                
            }
            else
            {
                skippedArray[1] = Datevalue
                SaveArray[1] = Datevalue
            }
        }
        if indexvalu == 0
        {
            self.dataPickerView.reloadAllComponents()
            
            self.dataPickerView.selectRow(0, inComponent: 0, animated: false)
        }
        
        Timevalue = ""
        RowValue = 0
        mTableview.reloadData()
        
        self.view .endEditing(true)
        
    }
    @objc func cancelClick() {
        
        Timevalue = ""
        
        if indexvalu == 0
        {
            
            self.dataPickerView.reloadAllComponents()
            self.dataPickerView.selectRow(0, inComponent: 0, animated: false)
            
        }
        RowValue = 0
        self.view .endEditing(true)
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print("textField-----> ",textField.tag)
        
        indexvalu = textField.tag
        
        if textField.tag == 0 {
            
            textField.inputView = self.dataPickerView
            textField.inputAccessoryView = self.toolBar
            Pickerdatavalue = self.dataList
            
            print(Pickerdatavalue)
            print(Pickerdatavalue.count)
            
            self.dataPickerView.reloadAllComponents()
            
        }
        else if textField.tag == 1
        {
            textField.inputView = self.Date_Picker
            textField.inputAccessoryView = self.toolBar
            Date_Picker.reloadInputViews()
        }
        else
        {
            textField.inputView = nil
            textField.inputAccessoryView = nil
            self.Date_Picker.removeFromSuperview()
            self.Time_Picker.removeFromSuperview()
            self.dataPickerView.removeFromSuperview()
            self.toolBar.removeFromSuperview()
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        print("shouldChangeCharactersIn :---",textField.tag)
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        indexvalu = textField.tag
        
        if indexvalu == 2
        {
            do {
                let regex = try NSRegularExpression(pattern:".*[^0-9-$].*", options: [])
                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                    return false
                }
            }
            catch {
                print("ERROR")
            }
            
        }
        
        if textField.tag < 100
        {
            skippedArray[textField.tag] = newString
            SaveArray[textField.tag] = newString
        }
        else
        {
            ContactString = newString as String
        }
        
        print(skippedArray)
        return true
    }
}

// MARK: - TextView Delegate Methods

extension CreateBill : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
        print("Text:------>  ",text)
        
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        skippedArray[5] = changedText
        SaveArray[5] = changedText
        
        print("TextView:------>  ",changedText)
        
        return true
    }
    
}

// MARK: - PickerView Delegate/DataSource Methods


extension CreateBill : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if !Pickerdatavalue.isEmpty
        {
            return Pickerdatavalue.count
            
        }
        return 0
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Pickerdatavalue[row]["title"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        
        RowValue = row
        
    }
}

// MARK: - ImagePickerController Delegate Methods

extension CreateBill: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        
        //    let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        //    // imgUserPic.contentMode = .scaleAspectFit
        //   // imgUserPic.image = chosenImage
        //    dismiss(animated:true, completion: nil)
        
        var selectedImage: UIImage?
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
            
            
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
            
        }
        
        let fileUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL
        
        ImgArray.append(selectedImage!)
        
        if fileUrl == nil
        {
            ImgSaveArray.append("attachment"+String(format:"%d",ImgArray.count)+String(format:".%@","png"))
        }
        else
        {
            ImgSaveArray.append("attachment"+String(format:"%d",ImgArray.count)+String(format:".%@",fileUrl!.pathExtension))
        }
        
        mTableview.reloadData()
        dismissPicker(picker: picker)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismissPicker(picker: picker)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?) {
        
    }
    private func dismissPicker(picker : UIImagePickerController){
        picker.view!.removeFromSuperview()
        picker.removeFromParent()
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        UIApplication.shared.isStatusBarHidden = false
    }
}
// MARK: - Protocol Methods

extension CreateBill : SelectedContactProtocol
{
    func selected(selected_array: NSMutableArray) {
        
        print(selected_array)
        print(selected_array.count)
        DummyArray = selected_array
        print(DummyArray)
        mTableview.reloadData()
        
    }
}
// MARK: - Class Instance


extension CreateBill
{
    class func instance()->CreateBill?{
        
        let storyboard = UIStoryboard(name: "Billing", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CreateBill") as? CreateBill
        
        return controller
    }
}
