
import UIKit
import SwiftyJSON
class ProductDetailsVC: InterfaceExtendedController {

    
    @IBOutlet weak var mTableview: UITableView!
    @IBOutlet fileprivate var txtNoOfCase : UITextField?
    @IBOutlet fileprivate var txtDiscountPerUnit  : UITextField?
    @IBOutlet fileprivate var Total : UILabel?
    @IBOutlet fileprivate var noOfUnit : UILabel?
    @IBOutlet fileprivate var lbl_Discount : UILabel?
    @IBOutlet fileprivate var lbl_NumberofCase : UILabel?
    @IBOutlet fileprivate var lbl_TotalNumber : UILabel?
    @IBOutlet fileprivate var lbl_TotalCost : UILabel?
    var issueingingSample : Bool = false
    var Product_id : Int?
    var contactFlag : Bool = false
    var contact_id : Int = 0
      var TotalCase : String = "1"
    
      var Discount : String = "0"
    fileprivate var discounValue : Double = 0.0
    
    var Total_Unit : String = "0"
    
     var TotalValue : String = "0"
    
    
   var pricepercase : Double = 0.0
  var  unitspercase : Int = 0
  var  name : String = "0"
  var  product_code : String = "0"
   var descriptions : String = "0"
   var category_name : String = "0"
   var product_image : String = "0"
    
    
    
    fileprivate var productData : [JSON]?
    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        mTableview.delegate = self
        mTableview.dataSource = self
        mTableview.separatorStyle = .none
        mTableview.rowHeight = UITableView.automaticDimension
        mTableview.estimatedRowHeight = 600
        self.noOfUnit?.text = Total_Unit
       self.txtNoOfCase?.text = TotalCase
       
        
        //self.txtDiscountPerUnit?.text = TotalValue
      //  AppDelegate.showWaitView()
        ProductsDetials_Api(productId: Product_id ?? 0)
        
        self.NavBarIcon()
        
        
    }
    
    
    
    
    
    // MARK: - Language Setting

    override func LanguageSet() {
         NavigationBarTitleName(Title: "Product Detail")
         PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        if !issueingingSample
        {
            lbl_NumberofCase?.text = languageKey(key: "No of Cases")
           self.txtDiscountPerUnit?.isUserInteractionEnabled = true
        }
        else
        {
            lbl_NumberofCase?.text = "No of Units".languageSet
            self.txtDiscountPerUnit?.isUserInteractionEnabled = false
        }
       
            guard let symbol = SignedUserInfo.sharedInstance?.currency_symbol else{return}
      lbl_Discount?.text =  "Discount Per Case".languageSet + "(" + "in".languageSet + "\(symbol))"
       lbl_TotalNumber?.text = languageKey(key: "Total no of Units")
       lbl_TotalCost?.text  = languageKey(key: "Total Cost")
    }
    
    
    // MARK: - Products Detials Api

    fileprivate func ProductsDetials_Api(productId : Int)
    {
       
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
          
           
            self.TotalValue = String(pricepercase)
            
            
            guard let symbol = SignedUserInfo.sharedInstance?.currency_symbol else{return}
            
            let tempUnits = unitspercase
            
            self.Total_Unit = String(tempUnits)
            
            self.noOfUnit?.text = self.Total_Unit
            self.Total?.text = "\(symbol) \(self.TotalValue)"
            self.mTableview.reloadData()
            return
        }
        DataFetch_Api().Fetch(ID: productId ,urlString: "/productdetail") { (status, message,data) in
            
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                return
            }
            if data != nil
            {
                AppDelegate.hideWaitView()
                self.productData = data
                let tempPrice = self.productData?[0]["pricepercase"].doubleValue.round(to: 2)
                self.TotalValue = String(tempPrice ?? 0)
                guard let symbol = SignedUserInfo.sharedInstance?.currency_symbol else{return}
                let tempUnits = self.productData?[0]["unitspercase"].intValue
                self.Total_Unit = String(tempUnits ?? 0)
                self.noOfUnit?.text = self.Total_Unit
                self.Total?.text = "\(symbol) \(self.TotalValue)"
                self.mTableview.reloadData()
            }
        }
        
    }
    
   
    }

// MARK: - TextField Delegate Methods

extension ProductDetailsVC: UITextFieldDelegate
{
    
    
    
    
    
    
   
    
    
    // MARK: - TextField Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        print(textField.tag)
        let details = productData?[0]
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if textField.tag == 1 {
            TotalCase = newString as String
            
        }
            else if textField.tag == 1000
        {
            var startString = ""
            
            if textField.text != nil {
                startString += textField.text!
            }
            
            startString += string
            
            let limitNumber = Int(startString)
            
            if limitNumber > 1000 {
                return false
            } else {
                
                TotalCase = newString as String

                
                return true
            }
            
            
        }
        else
        {
            if !NetworkState.isConnected() {
                
                
                if Double(newString as String) ?? 0.0 <  pricepercase.round(to: 2){
                    Discount = newString as String
                    discounValue = Double(Discount) ?? 0.0
                }
                else
                {
                    return false
                }
                
            }
            else
            {
                
                
                
                if Double(newString as String) ?? 0.0 <  details!["pricepercase"].doubleValue.round(to: 2){
                    Discount = newString as String
                    discounValue = Double(Discount) ?? 0.0
                }
                else
                {
                    return false
                }
            }
        }
        
        
        
        TotalValue =   String(Calculation(pricepercase:TotalCase , Discountpricepercase: Discount))
        //self.mTableview.beginUpdates()
        self.noOfUnit?.text = Total_Unit
        guard let currency_symbol = SignedUserInfo.sharedInstance?.currency_symbol else {return false}
        
        self.Total?.text = "\(currency_symbol) \(TotalValue)"
        
        return true
    }
    
    
    func validateMaxValue(textField: UITextField, maxValue: Int, range: NSRange, replacementString string: String) -> Bool {
        
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        //if delete all characteres from textfield
        if(newString.isEmpty) {
            return true
        }
        
        //check if the string is a valid number
        let numberValue = Int(newString)
        
        if(numberValue == nil) {
            return false
        }
        
        return numberValue ?? 0 <= maxValue
    }
    
    
    func Calculation(pricepercase : String,Discountpricepercase : String) -> Double {
        
        
        let details = productData?[0]
        
        // let TempPerPrice = Double(pricepercase)
        // let price =
        
        // Total_Unit = String(details!["unitspercase"].intValue * (pricepercase as NSString).integerValue)
        
        if !NetworkState.isConnected() {
            
            let calcu = ((pricepercase as NSString).doubleValue.round(to: 2) * ((pricepercase as NSString).doubleValue.round(to: 2) - (Discountpricepercase as NSString).doubleValue.round(to: 2)))
            
            if calcu < 0.0
            {
                return 0.0
            }
            else
            {
                
                return calcu.round(to: 2)
            }
            
            
        }
        else
            
        {
            let calcu = ((pricepercase as NSString).doubleValue.round(to: 2) * (details!["pricepercase"].doubleValue.round(to: 2) - (Discountpricepercase as NSString).doubleValue.round(to: 2)))
            
            if calcu < 0.0
            {
                return 0.0
            }
            else
            {
                
                return calcu.round(to: 2)
            }
            
        }
        
        
        
    }
    
}

// MARK: - Table View Delegate/DataSource Methods

extension ProductDetailsVC: UITableViewDelegate , UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !NetworkState.isConnected() {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailsCell", for: indexPath ) as! ProductDetailsCell
            guard let info = SignedUserInfo.sharedInstance else{return cell}
            
            cell.productName?.text = name
            cell.product_code?.text = "Product Code".languageSet +  " : \(product_code)"
            
            cell.desp?.text = descriptions
            cell.unitspercase?.text = "Unit Per Case".languageSet + " : \(unitspercase)"
            
            cell.pricepercase?.text = "\(info.currency_symbol!) \(pricepercase) \(languageKey(key: "Per Case"))"
            
            
            cell.category_name?.text =  "Category".languageSet + " : \(category_name)"
            
            
            let imageUrl = URL(string: product_image)
            
            cell.imgProduct.sd_setImage(with: imageUrl, placeholderImage:UIImage(imageLiteralResourceName: "Product_PH" ), options: .cacheMemoryOnly, completed: nil)
            
            cell.selectionStyle = .none
            return cell
            
            
        }
        else
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailsCell", for: indexPath ) as! ProductDetailsCell
            guard let info = SignedUserInfo.sharedInstance else{return cell}
            guard let details = productData?[indexPath.row] else{return cell}
            cell.productName?.text = details["name"].stringValue
            cell.product_code?.text = "Product Code".languageSet +  " : \(details["product_code"].stringValue)"
            cell.desp?.text = details["description"].stringValue
            cell.unitspercase?.text = "Unit Per Case".languageSet + " : \(details["unitspercase"].intValue)"
            cell.pricepercase?.text = "\(info.currency_symbol!) \(details["pricepercase"].doubleValue.round(to: 2)) \(languageKey(key: "Per Case"))"
            cell.category_name?.text =  "Category".languageSet + " : \(details["category_name"].stringValue)"
            
            if details["product_image"].count != 0
            {
                
                let imageUrl = URL(string: details["product_image"][0]["image"].stringValue)
                
                cell.imgProduct.sd_setImage(with: imageUrl, placeholderImage:UIImage(imageLiteralResourceName: "Product_PH" ), options: .cacheMemoryOnly, completed: nil)
                
            }
            
            cell.selectionStyle = .none
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
}

extension ProductDetailsVC
{
    
    // MARK: - Order Now Button Action

    @IBAction fileprivate func OrderNowBtn(_ sender : UIButton)
    {
       
        
        var param = [String : Any]()
        param["product_id"] = Product_id ?? 0
        param["no_of_cases"] = Int(TotalCase) ?? 0
        param["discountpercase"] = discounValue
        param["pricepercase"] = pricepercase
        param["product_name"] = name
        param["product_image"] = product_image
        
       
        
        var jsons = [JSON]()
        jsons.append(JSON(param))
       
        
        guard let controller = CreateOrderVC.instance() else{return}
        controller.ProductCartData = jsons
        controller.totalValue = Double(self.TotalValue) ?? 0.0
        controller.flagsCheck = true
        if contactFlag
        {
            controller.contactFlag = true
            controller.contact_id = contact_id
        }
        self.navigationController?.pushViewController(controller, animated: true)
        
        
    }
    
    // MARK: - Add To Cart Button Action

    
    @IBAction fileprivate func addTocartBtn(_ sender : UIButton)
    {
        if !NetworkState.isConnected() {
            AppDelegate.alertViewForInterNet()
            return
        }
        
        self.addToCart()
    }
    
    // MARK: - Add To Cart API

    fileprivate func addToCart()
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        ProductAddToCart_Api().Request(id: Product_id ?? 0, cases: Int(TotalCase) ?? 0, discount: discounValue ) { (status, message) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                //completion(false)
                return
            }
            self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: message, actionTitles:["ok"], actions:[{action1 in
                self.navigationController?.popViewController(animated: true)
                }, nil])
            
            
           
        
        }
    }
}


// MARK: - Class Instance

extension ProductDetailsVC
{
    class func instance()->ProductDetailsVC?{
        let storyboard = UIStoryboard(name: "Products", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as? ProductDetailsVC
        
        
        return controller
    }
}
