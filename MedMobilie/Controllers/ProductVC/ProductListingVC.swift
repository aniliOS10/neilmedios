//
//  ProdoctListingVC.swift
//  MedMobilie
//
//  Created by dr.mac on 20/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit
import SwiftyJSON
import IoniconsKit
import SDWebImage
import DGElasticPullToRefresh

class ProductListingVC: InterfaceExtendedController {

   @IBOutlet weak var segmentedControl:UISegmentedControl!

    var ProductData : [JSON]?
    @IBOutlet var tableView: UITableView!
    var ContactsFlag : Bool = false
    var contact_id : Int = 0
    var ViewScreen : String = "product"
    
   
    // MARK: - Class life cycle

    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        AppDelegate.showWaitView()
        self.ProductDataFetch(ViewScreen)
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 110
        paintCartBtn()
        self.tabBarController?.navigationController?.navigationBar.isHidden = true
        paintNavigationTitle(title : "Product".languageSet,Color : .black)
        self.NavBarIcon()
        PaintleftSideMenu()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        PaintNavigationBar(TitleColor: UIColor.black, BackgroundColor:  WhiteColor(), BtnColor: UIColor.black)
        self.LanguageSet()
        
    }
    
    // MARK: - Language Setting
    
    @objc override func LanguageSet(){
        
        segmentedControl = UISegmentedControl (items: [self.languageKey(key: "Product"),self.languageKey(key: "Sample")])
        
    }
 
    // MARK: - Product Data Fetch API

    func ProductDataFetch(_ DetailsType: String)
    {
        if !NetworkState.isConnected() {
            self.tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
            
           FetchingOfflineProductData()
         
            return
        }
        
        ProductListingFetch().Fetch(ProductType: DetailsType) { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message ?? "Something Went Wrong")
                self.ProductData = data
                self.tableView.reloadData()
                self.tableView.dg_stopLoading()
                return
            }
            self.ProductData = data
            self.tableView.reloadData()
            self.tableView.dg_stopLoading()
        }
        
    }
    
    
    // MARK: - Fetching Offline ProductData

    fileprivate func FetchingOfflineProductData()
    {
        if !NetworkState.isConnected() {
            
            var ProductType : String = ""
            
            if ViewScreen == "sample"
            {
                ProductType = "Sample"
            }
            else if ViewScreen == "product"
            {
                ProductType = "Product"
            }
            
            DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: ProductType) { (data) in
                AppDelegate.hideWaitView()
                if let object = data
                {
                    var jsonArray = [JSON]()
                    for jsonData in object
                    {
                        if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                            jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                                jsonArray.append(json!)
                            })})
                        }
                    }
                    
                    self.ProductData = jsonArray
                    self.tableView.reloadData()
                    self.tableView.dg_stopLoading()
                    
                    
                    AppDelegate.hideWaitView()
                   
                }
            }
        }
        
    }
    
   
    
}

// MARK: - Table View Delegate/DataSource Methods

extension ProductListingVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProductData?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        guard let controller = ProductDetailsVC.instance()
        else
        {
            return
        }
        guard let infos = ProductData?[indexPath.row] else{return}
           controller.Product_id = infos["id"].intValue
        
        
        
            
            controller.pricepercase = Double(infos["pricepercase"].doubleValue)
             controller.unitspercase = infos["unitspercase"].intValue
            
             controller.name = infos["name"].stringValue
             controller.product_code = infos["product_code"].stringValue
             controller.descriptions = infos["description"].stringValue
            
              controller.category_name = infos["category_name"].stringValue
            controller.product_image = infos["product_image"].stringValue
         
      
        
        if ContactsFlag
        {
            controller.contactFlag = true
            controller.contact_id = contact_id
        }
        if ViewScreen == "sample"
        {
            controller.issueingingSample = true
        }
        else if ViewScreen == "product"
        {
            controller.issueingingSample = false
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? ProductListingCell
            else{return ProductListingCell()}
        
        guard let infos = ProductData?[indexPath.row] else{return cell}
        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)

       let str = "\(SignedUserInfo.sharedInstance?.currency_symbol ?? "") \(String(infos["pricepercase"].doubleValue.round(to: 2)))"
        cell.Price?.text = str
        cell.Units?.text = infos["unitspercase"].stringValue
        cell.lblProductName?.text = infos["name"].stringValue
        cell.lblCategoryName?.text = infos["category_name"].stringValue
        
        cell.imgProductimg?.sd_setImage(with: infos["image"].url, placeholderImage: UIImage(imageLiteralResourceName: "Product_PH"), options:.cacheMemoryOnly, completed: nil)
        //cell.imgProductimg?
        
       // cell.viewShadow?.backgroundColor = navBackgroundColor
        
       // cell.lblTask?.text = numbers[indexPath.row]
        
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    
}


// MARK: - Segment Action

extension ProductListingVC
{
    @IBAction fileprivate func segmentAction (_ sender : UISegmentedControl )
    {
        switch sender.selectedSegmentIndex {
        case 0:
            
            ViewScreen = "product"
            AppDelegate.showWaitView()
            self.ProductDataFetch(ViewScreen)
            break
        case 1:
            
              ViewScreen = "sample"
            AppDelegate.showWaitView()
            self.ProductDataFetch(ViewScreen)
            break
        default:
            print("dummy")
           // self.ProductDataFetch("product")
        }
    
    }
}


// MARK: - PullToRefresh

extension ProductListingVC
{
    override func loadView() {
        super.loadView()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = self.navBackgroundColor ?? BillNAVBAR()
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = SettingNAVBAR()
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.ProductDataFetch(self!.ViewScreen)
            
            }, loadingView: loadingView)
        tableView.dg_setPullToRefreshFillColor(navBackgroundColor ?? WhiteColor())
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
}


// MARK: - Class Instance

extension ProductListingVC
{
    class func instance()->ProductListingVC?{
        let storyboard = UIStoryboard(name: "Products", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ProductListingVC") as? ProductListingVC
        
        
        return controller
    }
}


