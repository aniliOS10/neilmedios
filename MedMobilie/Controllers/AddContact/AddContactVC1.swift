//
//  AddContactVC1.swift
//  MedMobilie
//
//  Created by Apple on 17/09/21.
//  Copyright © 2021 dr.mac. All rights reserved.
//

import UIKit
import IoniconsKit
import MMDrawerController
import SwiftyJSON
import MMDrawerController
class ContactFormSection {
    var title:String = ""
    var isOpen:Bool? = false
    var showCheckOption:Bool? = false
    var row:[ContactFormRow] = []
    init(title:String,isOpen:Bool,showCheckOption:Bool,row:[ContactFormRow]){
        self.title = title
        self.isOpen = isOpen
        self.showCheckOption = showCheckOption
        self.row = row
    }
    
}
class ContactFormRow {
    var title:String = ""
    var placeHolder:String = ""
    var value:String = ""
    var valueID:Int = 0
    
    
    var sectionTag:Int = 0
    var rowTag:Int = 0
    var togleButtons:Bool? = false
  
    var togleButtonsTitle:[[String:Any]]?
    var isShowDropDown:Bool? = false
    var isShowBelowDropDown:Bool? = false
    var isShowAboveDropDown:Bool? = false
    
    
    init(title:String, placeHolder: String, value: String, valueID:Int = 0, sectionTag: Int, rowTag: Int,isShowDropDown: Bool?  = false,isShowBelowDropDown: Bool? = false,isShowAboveDropDown: Bool? = false,togleButtonsTitle:[[String:Any]]? = [], togleButtons:Bool?  = false){
        self.title = title
        self.placeHolder = placeHolder
        self.value = value
        self.valueID = valueID
        
        self.sectionTag = sectionTag
        self.rowTag = rowTag
        self.togleButtons = togleButtons
        self.togleButtonsTitle = togleButtonsTitle
        
        self.isShowDropDown = isShowDropDown
        self.isShowBelowDropDown = isShowBelowDropDown
        self.isShowAboveDropDown = isShowAboveDropDown
        
    
    }
    
    
}

class AddContactVC1: InterfaceExtendedController, SpecialityProtocol ,UITableViewDataSource,UITableViewDelegate{
    
    var contactID:Int = 0
    func Speciality_String(List_Iteam: String) {
        
    }
    
    func Speciality_String(List_Iteam: String, ProductArray: [Int]) {
        
    }
    var completionUpdateContact:(([String:Any])->Void)?
    var DefaultBillingSelection = true
    var DefaultShippingSelection = true
    fileprivate var datavalue = [JSON]()
    var isAdd:Bool = true
    var sectionArr:[ContactFormSection] = []
    var dataPicker = UIPickerView()
    var toolBar = UIToolbar()
    var RowValue = 0
    @IBOutlet weak var mTableview: UITableView!
    fileprivate var Pickerdatavalue = [JSON]()
    
    fileprivate var selectedCountryIdAddress : Int = 0
    fileprivate var selectedCountryIdBilling : Int = 0
    fileprivate var selectedCountryIdShipping : Int = 0
    fileprivate var dataList = [JSON]()
    var CategaryIndex = 0
    var isAuthrizedContact : Bool = false
    var ProductArray =   [Int]()
   
  
  
    var editContact:[String:Any] = [:]
    var delegate  : AddContactsToCreateOrderProtocol?
   
    var updatedPara:[String:Any] = [:]
    fileprivate var Redline : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
        
        if let billingCountry = SaveArray[17] as? Int
        {
            self.selectedCountryIdAddress = billingCountry
        }
        if let billingCountry = SaveArray[28] as? Int
        {
            self.selectedCountryIdBilling = billingCountry
        }
        if let billingCountry = SaveArray[35] as? Int
        {
            self.selectedCountryIdShipping = billingCountry
        }
        */
      
        RightActionButton(Title:languageKey(key:"Save"))
        
        if isAdd{
            var userInfo:[ContactFormRow] = []
            //first section
            
        
            userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.saluation, placeHolder: AppMessage.AddContact.saluation, value: "", valueID:0, sectionTag: 0, rowTag: 0,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            
            userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.contactName, placeHolder: AppMessage.AddContact.contactName, value: "", sectionTag: 0, rowTag: 1))
            userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.attn, placeHolder: AppMessage.AddContact.attn, value: "", sectionTag: 0, rowTag: 2))
            userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.CustomerCategory, placeHolder: AppMessage.AddContact.CustomerCategory, value: "", valueID:0, sectionTag: 0, rowTag: 3,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.type, placeHolder: AppMessage.AddContact.type, value: "", sectionTag: 0, rowTag: 4,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.primarySpecialty, placeHolder: AppMessage.AddContact.primarySpecialty, value: "", valueID:0, sectionTag: 0, rowTag: 5,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.secondarySpecialty, placeHolder: AppMessage.AddContact.secondarySpecialty, value: "", valueID:0, sectionTag: 0, rowTag: 6,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.companyofficeName, placeHolder: AppMessage.AddContact.companyofficeName, value: "", sectionTag: 0, rowTag: 7))
            userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.phone, placeHolder: AppMessage.AddContact.phone, value: "", sectionTag: 0, rowTag: 8))
            userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.email, placeHolder: AppMessage.AddContact.email, value: "", sectionTag: 0, rowTag: 9))
            userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.website, placeHolder: AppMessage.AddContact.website, value: "", sectionTag: 0, rowTag: 10))
          
            self.sectionArr.append(ContactFormSection.init(title: AppMessage.AddContact.Userinformation, isOpen: true,showCheckOption: false, row: userInfo))
            
            
            
            
            
            
            //second section
            var leadStatus:[ContactFormRow] = []
            leadStatus.append( ContactFormRow.init(title: AppMessage.AddContact.leadStatus, placeHolder: AppMessage.AddContact.leadStatus, value: "", sectionTag: 1, rowTag: 0,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            self.sectionArr.append(ContactFormSection.init(title: AppMessage.AddContact.status, isOpen: true, showCheckOption: false,row: leadStatus))
            
            //third section
            var willingToStock:[ContactFormRow] = []
            willingToStock.append( ContactFormRow.init(title: AppMessage.AddContact.WillingtoStock, placeHolder: AppMessage.AddContact.WillingtoStock, value: "", sectionTag: 2, rowTag: 0,togleButtonsTitle: [["title":"Yes","value":false],["title":"No","value":true]], togleButtons: true))
            self.sectionArr.append(ContactFormSection.init(title: AppMessage.AddContact.WillingtoStock, isOpen: true,showCheckOption: false, row: willingToStock))
            
            //fourth section
            var address:[ContactFormRow] = []
            address.append( ContactFormRow.init(title: AppMessage.AddContact.address1, placeHolder: AppMessage.AddContact.address1, value: "", sectionTag: 3, rowTag: 0))
            
            address.append( ContactFormRow.init(title: AppMessage.AddContact.Address2, placeHolder: AppMessage.AddContact.Address2, value: "", sectionTag: 3, rowTag: 1))
            
            
            address.append( ContactFormRow.init(title: AppMessage.AddContact.country, placeHolder: AppMessage.AddContact.country, value: "", sectionTag: 3, rowTag: 2,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            address.append( ContactFormRow.init(title: AppMessage.AddContact.state, placeHolder: AppMessage.AddContact.state, value: "", sectionTag: 3, rowTag: 3,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            address.append( ContactFormRow.init(title: AppMessage.AddContact.City, placeHolder: AppMessage.AddContact.City, value: "", sectionTag: 3, rowTag: 4))
            address.append( ContactFormRow.init(title: AppMessage.AddContact.ZipPostalCode, placeHolder: AppMessage.AddContact.ZipPostalCode, value: "", sectionTag: 3, rowTag: 5))
            
            //fifth section
            self.sectionArr.append(ContactFormSection.init(title: AppMessage.AddContact.Address, isOpen: true,showCheckOption: false,row:address))
            
            var billingAddress:[ContactFormRow] = []
            billingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.address1, placeHolder: AppMessage.AddContact.address1, value: "", sectionTag: 4, rowTag: 0))
            
            billingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.Address2, placeHolder: AppMessage.AddContact.Address2, value: "", sectionTag: 4, rowTag: 1))
            
            
            billingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.country, placeHolder: AppMessage.AddContact.country, value: "", sectionTag: 4, rowTag: 2,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            billingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.state, placeHolder: AppMessage.AddContact.state, value: "", sectionTag: 4, rowTag: 3,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            billingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.City, placeHolder: AppMessage.AddContact.City, value: "", sectionTag: 4, rowTag: 4))
            billingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.ZipPostalCode, placeHolder: AppMessage.AddContact.ZipPostalCode, value: "", sectionTag: 4, rowTag: 5))
            self.sectionArr.append(ContactFormSection.init(title: AppMessage.AddContact.defaultBillingAddress, isOpen:DefaultBillingSelection, showCheckOption: true, row: billingAddress))
            
            //sixth section
            var shipingAddress:[ContactFormRow] = []
            shipingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.address1, placeHolder: AppMessage.AddContact.address1, value: "", sectionTag: 5, rowTag: 0))
            
            shipingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.Address2, placeHolder: AppMessage.AddContact.Address2, value: "", sectionTag: 5, rowTag: 1))
            
            
            shipingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.country, placeHolder: AppMessage.AddContact.country, value: "", sectionTag: 5, rowTag: 2,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            shipingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.state, placeHolder: AppMessage.AddContact.state, value: "", sectionTag: 5, rowTag: 3,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
           
            shipingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.City, placeHolder: AppMessage.AddContact.City, value: "", sectionTag: 5, rowTag: 4))
            shipingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.ZipPostalCode, placeHolder: AppMessage.AddContact.ZipPostalCode, value: "", sectionTag: 5, rowTag: 5))
            self.sectionArr.append(ContactFormSection.init(title: AppMessage.AddContact.defaultShippingAddress, isOpen:DefaultShippingSelection, showCheckOption: true, row: shipingAddress))
            
        }else{
            
                var userInfo:[ContactFormRow] = []
                //first section
                
                let salutationV = editContact["salutation"] as! String
                let salutationIDV = editContact["salutation_id"] as! Int
            
            let country_nameV = editContact["country_name"] as! String
            let attn_name = editContact["attn_name"] as! String
            
            let category_nameV = editContact["category_name"] as! String
            let category_idV = editContact["category_id"] as! Int
            
            let contact_title_name = editContact["contact_title_name"] as! String
            let contact_title_id = editContact["contact_title_id"] as! Int
           
           
            
                userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.saluation, placeHolder: AppMessage.AddContact.saluation, value: salutationV, valueID:salutationIDV, sectionTag: 0, rowTag: 0,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
                userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.contactName, placeHolder: AppMessage.AddContact.contactName, value: country_nameV, sectionTag: 0, rowTag: 1))
                userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.attn, placeHolder: AppMessage.AddContact.attn, value: attn_name, sectionTag: 0, rowTag: 2))
                userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.CustomerCategory, placeHolder: AppMessage.AddContact.CustomerCategory, value: category_nameV, valueID:category_idV, sectionTag: 0, rowTag: 3,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
                userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.type, placeHolder: AppMessage.AddContact.type, value: contact_title_name,valueID: contact_title_id, sectionTag: 0, rowTag: 4,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            
            let primary_speciality_nameV = editContact["primary_speciality_name"] as! String
            let primary_speciality_idV = editContact["primary_speciality_id"] as! Int
            
                userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.primarySpecialty, placeHolder: AppMessage.AddContact.primarySpecialty, value: primary_speciality_nameV, valueID:primary_speciality_idV, sectionTag: 0, rowTag: 5,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            
            
            let secondary_specialities_nameV = editContact["secondary_specialities_name"] as! String
            let secondary_speciality_idV = editContact["secondary_speciality_id"] as! Int
                userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.secondarySpecialty, placeHolder: AppMessage.AddContact.secondarySpecialty, value: secondary_specialities_nameV, valueID:secondary_speciality_idV, sectionTag: 0, rowTag: 6,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            
                let company_nameV = editContact["company_name"] as! String
                userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.companyofficeName, placeHolder: AppMessage.AddContact.companyofficeName, value: company_nameV, sectionTag: 0, rowTag: 7))
            
                let phoneV = editContact["phone"] as! String
                userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.phone, placeHolder: AppMessage.AddContact.phone, value: phoneV, sectionTag: 0, rowTag: 8))
            
                let emailV = editContact["email"] as! String
                userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.email, placeHolder: AppMessage.AddContact.email, value: emailV, sectionTag: 0, rowTag: 9))
            
            let websiteV = editContact["website"] as! String
                userInfo.append( ContactFormRow.init(title: AppMessage.AddContact.website, placeHolder: AppMessage.AddContact.website, value: websiteV, sectionTag: 0, rowTag: 10))
              
                self.sectionArr.append(ContactFormSection.init(title: AppMessage.AddContact.Userinformation, isOpen: true,showCheckOption: false, row: userInfo))
                
                
                
                
                
                
                //second section
                var leadStatus:[ContactFormRow] = []
                let lead_status_id = editContact["lead_status_id"] as! Int
                 let lead_status_name = editContact["lead_status_name"] as! String
                // let lead_source = editContact["lead_source"] as! String
            
                leadStatus.append( ContactFormRow.init(title: AppMessage.AddContact.leadStatus, placeHolder: AppMessage.AddContact.leadStatus, value: lead_status_name,valueID: lead_status_id, sectionTag: 1, rowTag: 0,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
                self.sectionArr.append(ContactFormSection.init(title: AppMessage.AddContact.status, isOpen: true, showCheckOption: false,row: leadStatus))
                
                //third section
                var willingToStock:[ContactFormRow] = []
            
               let willingtostock = editContact["willingtostock"] as! String
            var arr : [[String:Any]] = []
            if willingtostock == "No"{
                arr  = [["title":"Yes","value":false],["title":"No","value":true]]
            }else{
                arr  = [["title":"Yes","value":true],["title":"No","value":false]]
            }
                willingToStock.append( ContactFormRow.init(title: AppMessage.AddContact.WillingtoStock, placeHolder: AppMessage.AddContact.WillingtoStock, value: "", sectionTag: 2, rowTag: 0,togleButtonsTitle: arr, togleButtons: true))
                self.sectionArr.append(ContactFormSection.init(title: AppMessage.AddContact.WillingtoStock, isOpen: true,showCheckOption: false, row: willingToStock))
                
                //fourth section
                var address:[ContactFormRow] = []
            
            let address1 = editContact["address1"] as! String
                address.append( ContactFormRow.init(title: AppMessage.AddContact.address1, placeHolder: AppMessage.AddContact.address1, value: address1, sectionTag: 3, rowTag: 0))
         
               let address2 = editContact["address2"] as! String
                address.append( ContactFormRow.init(title: AppMessage.AddContact.Address2, placeHolder: AppMessage.AddContact.Address2, value: address2, sectionTag: 3, rowTag: 1))
                
            let contact_name = editContact["contact_name"] as! String
            let contact_id = editContact["contact_id"] as! Int
                address.append( ContactFormRow.init(title: AppMessage.AddContact.country, placeHolder: AppMessage.AddContact.country, value: contact_name,valueID: contact_id, sectionTag: 3, rowTag: 2,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            
            let state_name = editContact["state_name"] as! String
            let state_id = editContact["state_id"] as! Int
                address.append( ContactFormRow.init(title: AppMessage.AddContact.state, placeHolder: AppMessage.AddContact.state, value: state_name,valueID: state_id, sectionTag: 3, rowTag: 3,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            
              let city = editContact["city"] as! String
                address.append( ContactFormRow.init(title: AppMessage.AddContact.City, placeHolder: AppMessage.AddContact.City, value: city, sectionTag: 3, rowTag: 4))
            
            let zipcode = editContact["zipcode"] as! String
                address.append( ContactFormRow.init(title: AppMessage.AddContact.ZipPostalCode, placeHolder: AppMessage.AddContact.ZipPostalCode, value: zipcode, sectionTag: 3, rowTag: 5))
                
                //fifth section
                self.sectionArr.append(ContactFormSection.init(title: AppMessage.AddContact.Address, isOpen: true,showCheckOption: false,row:address))
                
                var billingAddress:[ContactFormRow] = []
                let billing_address1 = editContact["billing_address1"] as! String
                billingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.address1, placeHolder: AppMessage.AddContact.address1, value: billing_address1, sectionTag: 4, rowTag: 0))
                
                let billing_address2 = editContact["billing_address2"] as! String
                billingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.Address2, placeHolder: AppMessage.AddContact.Address2, value: billing_address2, sectionTag: 4, rowTag: 1))
                
            let billing_country_name = editContact["billing_country_name"] as! String
            let billing_country_id = editContact["billing_country_id"] as! Int
                billingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.country, placeHolder: AppMessage.AddContact.country, value: billing_country_name,valueID: billing_country_id, sectionTag: 4, rowTag: 2,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            
            let billing_state_name = editContact["billing_state_name"] as! String
            let billing_state_id = editContact["billing_state_id"] as! Int
                billingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.state, placeHolder: AppMessage.AddContact.state, value: billing_state_name,valueID: billing_state_id, sectionTag: 4, rowTag: 3,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            
            let billing_city = editContact["billing_city"] as! String
                billingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.City, placeHolder: AppMessage.AddContact.City, value: billing_city, sectionTag: 4, rowTag: 4))
            
            let billing_zipcode = editContact["billing_zipcode"] as! String
                billingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.ZipPostalCode, placeHolder: AppMessage.AddContact.ZipPostalCode, value: billing_zipcode, sectionTag: 4, rowTag: 5))
                self.sectionArr.append(ContactFormSection.init(title: AppMessage.AddContact.defaultBillingAddress, isOpen:!DefaultBillingSelection, showCheckOption: true, row: billingAddress))
                
                //sixth section
                var shipingAddress:[ContactFormRow] = []
               let shipping_address1 = editContact["shipping_address1"] as! String
                shipingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.address1, placeHolder: AppMessage.AddContact.address1, value: shipping_address1, sectionTag: 5, rowTag: 0))
                
            let shipping_address2 = editContact["shipping_address2"] as! String
                shipingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.Address2, placeHolder: AppMessage.AddContact.Address2, value: shipping_address2, sectionTag: 5, rowTag: 1))
                
            let shipping_country_name = editContact["shipping_country_name"] as! String
            let shipping_country_id = editContact["shipping_country_id"] as! Int
            
            shipingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.country, placeHolder: AppMessage.AddContact.country, value: shipping_country_name,valueID: shipping_country_id, sectionTag: 5, rowTag: 2,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
            
            let shipping_state_name = editContact["shipping_state_name"] as! String
            let shipping_state_id = editContact["shipping_state_id"] as! Int
            
                shipingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.state, placeHolder: AppMessage.AddContact.state, value: shipping_state_name,valueID: shipping_state_id, sectionTag: 5, rowTag: 3,isShowDropDown: true,isShowBelowDropDown: true,isShowAboveDropDown: false))
               
            let shipping_city = editContact["shipping_city"] as! String
                shipingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.City, placeHolder: AppMessage.AddContact.City, value: shipping_city, sectionTag: 5, rowTag: 4))
            
            
         let shipping_zip_code = editContact["shipping_zip_code"] as! String
                shipingAddress.append( ContactFormRow.init(title: AppMessage.AddContact.ZipPostalCode, placeHolder: AppMessage.AddContact.ZipPostalCode, value: shipping_zip_code, sectionTag: 5, rowTag: 5))
            
            
                self.sectionArr.append(ContactFormSection.init(title: AppMessage.AddContact.defaultShippingAddress, isOpen:!DefaultShippingSelection, showCheckOption: true, row: shipingAddress))
                
            }
        
        Listsapidata()
        createPickerView()
        self.mTableview.reloadData()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        
    }
    override func rightButtonAction(){
        
        var datavalues: [String : Any] = [:]
        
        for section in self.sectionArr{
            if section.title == AppMessage.AddContact.Userinformation{
                for userInfo in section.row{
                    if userInfo.placeHolder == AppMessage.AddContact.saluation {
                        if (userInfo.value != "")
                       {
                        datavalues["salutation"] = userInfo.value
                        datavalues["salutation_id"] = userInfo.valueID
                    }else{
                        
                        self.alert("please fill salutation".languageSet)
                        return
                    }
                      }
                    
                    if userInfo.placeHolder == AppMessage.AddContact.contactName
                    { if (userInfo.value != "")
                    {
                        datavalues["contact_name"] = userInfo.value
                    }else{
                        
                        self.alert("please fill contact Name".languageSet)
                        return
                    }
                    }
                    if userInfo.placeHolder == AppMessage.AddContact.attn { if (userInfo.value != "")
                    {
                        datavalues["attn_name"] = Int(userInfo.value)
                    }else{
                        
                        self.alert("please fill Attn".languageSet)
                        return
                    }
                    }
                    if userInfo.placeHolder == AppMessage.AddContact.CustomerCategory
                    { if (userInfo.value != "") {
                        datavalues["category_name"] = userInfo.valueID
                        datavalues["category_id"] = userInfo.value
                    }else{
                        
                        self.alert("please fill Custom Category".languageSet)
                        return
                    }
                    }
                    if userInfo.placeHolder == AppMessage.AddContact.type  { if (userInfo.value != "")
                    {
                        datavalues["contact_title_name"] = userInfo.value
                        datavalues["contact_title_id"] = userInfo.valueID
                        
                    }else{
                        
                        self.alert("please fill Custom Category".languageSet)
                        return
                    }
                    }
                    if userInfo.placeHolder == AppMessage.AddContact.primarySpecialty  { if (userInfo.value != "")
                    {
                        datavalues["primary_speciality_id"] = userInfo.valueID
                        datavalues["primary_speciality_name"] = userInfo.value
                        
                      //  datavalues["fax"] = SaveArray[21] as! String
                    }else{
                        
                        self.alert("please select primary Specialty".languageSet)
                        return
                    }
                    }
                    if userInfo.placeHolder == AppMessage.AddContact.secondarySpecialty  { if (userInfo.value != "")
                    {
                        datavalues["secondary_speciality_id"] = userInfo.valueID
                        datavalues["primary_speciality_name"] = userInfo.value
                        
                   
                    }else{
                        
                        self.alert("please select Secondary Specialty".languageSet)
                        return
                    }}
                    
                    if userInfo.placeHolder == AppMessage.AddContact.companyofficeName  { if (userInfo.value != "")
                    {
                       
                        datavalues["company_name"] = userInfo.value
                        
                      
                    }else{
                        
                        self.alert("please select Company name".languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.phone  { if (userInfo.value != "")
                    {
                       
                        datavalues["phone"] = userInfo.value
                        
                      
                    }else{
                        
                        self.alert("please select phone number.".languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.email  { if (userInfo.value != "")
                    {
                       
                        datavalues["email"] = userInfo.value
                        
                      
                    }else{
                        
                        self.alert("please select email.".languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.website  { if (userInfo.value != "")
                    {
                       
                        datavalues["website"] = userInfo.value
                        
                      
                    }else{
                        
                        self.alert("please select website.".languageSet)
                        return
                    }}
            }
            } else if section.title == AppMessage.AddContact.Userinformation{
                for userInfo in section.row{
                    if userInfo.placeHolder == AppMessage.AddContact.leadStatus  { if (userInfo.value != "")
                    {
                        datavalues["lead_status_name"] = userInfo.value
                        datavalues["lead_status_id"] = userInfo.valueID
                        
                        if self.isAdd{
                            datavalues["lead_source"] = "NO"
                        }else{
                            datavalues["lead_source"] = editContact["lead_source"] as! String
                        }
                      
                        
                    }else{
                        
                        self.alert("please fill Lead status".languageSet)
                        return
                    }}
                }
                
            }
            else if section.title == AppMessage.AddContact.WillingtoStock {
                for userInfo in section.row{
                    
                        if userInfo.togleButtonsTitle?[0]["value"] as! Bool{
                            datavalues["willingtostock"] = "Yes"
                        }else{
                            datavalues["willingtostock"] = "No"
                        }
                     
                    
                }
                
            }
           
            else if section.title == AppMessage.AddContact.Address && (section.isOpen ?? false){
                for userInfo in section.row{
                    if userInfo.placeHolder == AppMessage.AddContact.address1  { if (userInfo.value != "")
                    {
                        datavalues["address1"] = userInfo.value
                       
                    }else{
                        
                        self.alert("please fill a Address 1 field".languageSet)
                        return
                    }
                    }
                    if userInfo.placeHolder == AppMessage.AddContact.Address2  { if (userInfo.value != "")
                    {
                        datavalues["address2"] = userInfo.value
                       
                    }else{
                        
                        self.alert("please fill a Address 2 field".languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.country  { if (userInfo.value != "")
                    {
                        datavalues["country_id"] = userInfo.valueID
                        datavalues["country_name"] = userInfo.value
                       
                    }else{
                        
                        self.alert("please select a country".languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.state  { if (userInfo.value != "")
                    {
                        datavalues["state_id"] = userInfo.valueID
                        datavalues["state_name"] = userInfo.value
                       
                    }else{
                        
                        self.alert("please select a state".languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.state  { if (userInfo.value != "")
                    {
                        datavalues["city"] = userInfo.value
                       
                       
                    }else{
                        
                        self.alert(("please fill city field".languageSet).languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.ZipPostalCode  { if (userInfo.value != "")
                    {
                        datavalues["zipcode"] = userInfo.value
                       
                       
                    }else{
                        
                        self.alert(("please fill Zipcode field".languageSet).languageSet)
                        return
                    }}
                }
                
            }
            else if section.title == AppMessage.AddContact.defaultBillingAddress && (section.isOpen ?? false){
                for userInfo in section.row{
                    if userInfo.placeHolder == AppMessage.AddContact.address1  { if (userInfo.value != "")
                    {
                        datavalues["billing_address1"] = userInfo.value
                       
                    }else{
                        
                        self.alert("please fill a address 1 field in  billing address".languageSet)
                        return
                    }}
                    
                    if userInfo.placeHolder == AppMessage.AddContact.Address2  { if (userInfo.value != "")
                    {
                        datavalues["billing_address2"] = userInfo.value
                       
                    }else{
                        
                        self.alert("please fill a address 2 field in  billing address".languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.country  { if (userInfo.value != "")
                    {
                        datavalues["billing_country_id"] = userInfo.valueID
                        datavalues["billing_country_name"] = userInfo.value
                       
                    }else{
                        
                        self.alert("please select a country in billing address".languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.state  { if (userInfo.value != "")
                    {
                        datavalues["billing_state_id"] = userInfo.valueID
                        datavalues["billing_state_name"] = userInfo.value
                       
                    }else{
                        
                        self.alert("please select a state in billing address".languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.state  { if (userInfo.value != "")
                    {
                        datavalues["billing_city"] = userInfo.value
                       
                       
                    }else{
                        
                        self.alert(("please fill city field of  Billing Address".languageSet).languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.ZipPostalCode  { if (userInfo.value != "")
                    {
                        datavalues["billing_zipcode"] = userInfo.value
                       
                       
                    }else{
                        
                        self.alert(("please fill zipcode field in  billing address".languageSet).languageSet)
                        return
                    }}
                }
                
            }
            else if section.title == AppMessage.AddContact.defaultShippingAddress && (section.isOpen ?? false){
                for userInfo in section.row{
                    if userInfo.placeHolder == AppMessage.AddContact.address1  { if (userInfo.value != "")
                    {
                        datavalues["shipping_address1"] = userInfo.value
                       
                    }else{
                        
                        self.alert("please fill a Address 1 field in  shipping address".languageSet)
                        return
                    }
                    }
                    if userInfo.placeHolder == AppMessage.AddContact.Address2  { if (userInfo.value != "")
                    {
                        datavalues["shipping_address2"] = userInfo.value
                       
                    }else{
                        
                        self.alert("please fill a Address 2 field in  shipping address".languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.country  { if (userInfo.value != "")
                    {
                        datavalues["shipping_country_id"] = userInfo.valueID
                        datavalues["shipping_country_name"] = userInfo.value
                       
                    }else{
                        
                        self.alert("please select a country in  shipping address".languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.state  { if (userInfo.value != "")
                    {
                        datavalues["shipping_state_id"] = userInfo.valueID
                        datavalues["shipping_state_name"] = userInfo.value
                       
                    }else{
                        
                        self.alert("please select a state in  shipping address".languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.state  { if (userInfo.value != "")
                    {
                        datavalues["shipping_city"] = userInfo.value
                       
                       
                    }else{
                        
                        self.alert(("please fill city field in  shipping address".languageSet).languageSet)
                        return
                    }}
                    if userInfo.placeHolder == AppMessage.AddContact.ZipPostalCode  { if (userInfo.value != "")
                    {
                        datavalues["shipping_zip_code"] = userInfo.value
                       
                       
                    }else{
                        
                        self.alert(("please fill zipcode field in  shipping address".languageSet).languageSet)
                        return
                    }}
                }
                
            }
    }
        let yes = "Yes"
        let no = "No"
        
        
        for section in self.sectionArr{
            if section.title == AppMessage.AddContact.defaultShippingAddress{
                if section.isOpen ?? false
                {
                    datavalues["default_shipping_address"] =  no
                }else{
                    datavalues["default_shipping_address"]  = yes
                    
                    datavalues["shipping_address1"] = datavalues["address1"]
                    datavalues["shipping_address2"] = datavalues["address2"]
                    datavalues["shipping_country_name"] = datavalues["country_name"]
                    datavalues["shipping_country_id"] = datavalues["country_id"]
                    
                    datavalues["shipping_state_name"] = datavalues["state_name"]
                    
                    datavalues["shipping_state_id"] = datavalues["state_id"]
                   
                    datavalues["shipping_city"] = datavalues["city"]
                    datavalues["shipping_zip_code"] = datavalues["zipcode"]
                    
                }
            }
            if section.title == AppMessage.AddContact.defaultBillingAddress{
                if section.isOpen ?? false
                {
                    datavalues["default_billing_address"] =  no
                }else{
                    datavalues["default_billing_address"] =  yes
                    datavalues["billing_address1"] = datavalues["address1"]
                    datavalues["billing_address2"] = datavalues["address2"]
                    datavalues["billing_country_name"] = datavalues["country_name"]
                    datavalues["billing_country_id"] = datavalues["country_id"]
                    datavalues["billing_state_name"] = datavalues["state_name"]
                    
                    datavalues["billing_state_id"] = datavalues["state_id"]
                   
                    datavalues["billing_city"] = datavalues["city"]
                    datavalues["billing_zipcode"] = datavalues["zipcode"]
                  
                    
                    
                }
            }
        }
        datavalues["contact_phone"] =  datavalues["phone"]
        datavalues["contact_address1"] =  datavalues["address1"]
        datavalues["contact_type"] =  datavalues["type"]
        datavalues["contact_city"] =  datavalues["city"]
        datavalues["contact_zip_code"] =  datavalues["zipcode"]
        datavalues["contact_country_id"] =  datavalues["country_id"]
        datavalues["contact_state_id"] =  datavalues["state_id"]
        datavalues["AttachmentFiles"] = []
        
        if self.isAdd{
            datavalues["is_express"] = true
            datavalues["is_editable"] = true
            datavalues["billing_fax"] = ""
            datavalues["shipping_fax"] = ""
            datavalues["fax"] = ""
            datavalues["type"] = "Commercial"
            datavalues["contact_type"] = "Commercial"
            
           // datavalues["id"] = contactID
        }else{
            datavalues["is_express"] = editContact["is_express"]
            datavalues["is_editable"] = editContact["is_editable"]
            datavalues["billing_fax"] = editContact["billing_fax"]
            datavalues["shipping_fax"] = editContact["shipping_fax"]
            datavalues["fax"] = editContact["fax"]
            datavalues["id"] = contactID
            datavalues["type"] = editContact["type"]
            datavalues["contact_type"] = editContact["type"]
        }
       
      
        if contactID == 0 {
            
            AddContactRequest(datavalues, Method: "/contactapp_create")
        }
        else
        {
            AddContactRequest(datavalues, Method: "/contactapp_edit")
        }
    }
    // MARK: - Add Contact Request API
    
    fileprivate func AddContactRequest(_ param : [String : Any],Method :String)
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            OfflineCreateContact(parama: param)
            // AppDelegate.alertViewForInterNet()
            return
        }
        updatedPara = param
        AddContactApi().Fetch(param: param,Url: Method) { (status, message, info) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                self.alert(message)
                return
            }
            else
            {
                if param["id"] as? Int != 0 && param["id"] != nil
                {
                    DataBaseHelper.ShareInstance.DeleteEntry(ClassName: OfflineDataClass.Contact.rawValue, Id: param["id"] as! Int)
                }
                    
                
                if let contactJson = info
                {
                    
                    ///// new coredata code
                     DataBaseHelper.ShareInstance.DeleteContext(ClassName: OfflineDataClass.Contact.rawValue)
                    
                    contactJson.forEach({
                        DataBaseHelper.ShareInstance.AddContact(Object: $0)
                    })
                }
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: message), actionTitles: [self.languageKey(key: "Ok")], actions: [{action1 in
                    
                   
                    // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RfressVisitList"), object: nil)
                 //   NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateVisitList"), object: nil)
                    
                    self.delegate?.contactdetails(name: "", Contact_id: 0)
                   
                  
                    self.completionUpdateContact!(  ["isAdd":self.isAdd,"para":self.updatedPara])
                    self.navigationController?.popViewController(animated: true)
             
                    
                    }, nil])
            }
            
            
            
        }
        
        
    }
    // MARK: - Save Offline Data
    
    fileprivate func OfflineCreateContact (parama : [String : Any])
    {
        let userdefault = UserDefaults.standard
        var id : Int = 30000
        if userdefault.value(forKey: "OfflineNewContactCreate") as? Int != nil
        {
            let tempid = userdefault.value(forKey: "OfflineNewContactCreate") as! Int
            
            id = tempid + 1
            
        }
        
        var param  = [String:Any]()
        param = parama
        
        
        param["id"] = 0
        param["temp_id"] = Int(Date().timeIntervalSince1970)
        param["created_date"] = Int(Date().timeIntervalSince1970)
        let dict = JSON(param)

        DataBaseHelper.ShareInstance.AddContact(Object: dict,NewType : true){status in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert("something went wrong".languageSet)
                return
            }
            userdefault.set(id, forKey: "OfflineNewContactCreate")
            userdefault.synchronize()
            self.navigationController?.popViewController(animated: true)
            self.delegate?.contactdetails(name: "", Contact_id: 0)
            
        }
        print(param)
    }
    // MARK: - Create PickerView
    
    func createPickerView(){
        
        
        self.dataPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216)
        self.dataPicker.delegate = self
        self.dataPicker.dataSource = self
        self.dataPicker.backgroundColor = UIColor.white
        
        
        
        // ToolBar
        
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
    }
    
    // MARK: - Fetch Data From LocalDatabase
    
    fileprivate func Listsapidata()
    {
        
        DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.ListingData.rawValue) { (data) in
            
            if let object = data
            {
                var jsonArray = [JSON]()
                for jsonData in object
                {
                    if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                        jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                            jsonArray.append(json!)
                        })})
                    }
                }
                
                AppDelegate.hideWaitView()
                
                self.dataList = jsonArray
            }
        }
        
    }
    //1. determine number of rows of cells to show data
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if section == 2{
            print(section)
        }
        return sectionArr[section].row.count
        
        
        //return arrTitle.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
       
        return sectionArr.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionV = self.sectionArr[section]
        if sectionV.showCheckOption ?? false
        {
            tableView.register(UINib(nibName: "SelectionHeaderForBilling", bundle: nil), forCellReuseIdentifier: "selectionCell")
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell") as? SelectionHeaderForBilling
                else
            {return UIView()}
            cell.textLbl.text = sectionV.title
            
            cell.CheckANDUncheckBtn.tag = section
           
                cell.CheckANDUncheckBtn.addTarget(self, action: #selector(ClickForBillingBtn(sender:)), for: UIControl.Event.touchUpInside)
            
            
            
            if !(sectionV.isOpen ?? false) ?? false
            {
                cell.ImageOFBtn.image = UIImage.ionicon(with: .androidCheckboxOutline, textColor: UIColor.blue, size: CGSize(width: 50, height: 50))
            }
            else
            {
                cell.ImageOFBtn.image = UIImage.ionicon(with: .androidCheckboxOutlineBlank, textColor: UIColor.blue, size: CGSize(width: 50, height: 50))
            }
            
            
            return cell
        }else{
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
            headerView.backgroundColor = OffWhiteColor()
            let label = UILabel()
            label.frame = CGRect.init(x: 5, y: 5, width: tableView.frame.width, height: 30)
            label.text = sectionV.title
            label.font = UIFont.systemFont(ofSize: 16)
            label.textColor = UIColor.black
            //  label.backgroundColor = UIColor.yellow
            headerView.addSubview(label)
            return headerView
        }
        
        
        
        
    }
    @objc func ClickForBillingBtn(sender : UIButton){
        let sectionV = self.sectionArr[sender.tag]
        sectionV.isOpen =   !(sectionV.isOpen ?? false)
        self.mTableview.reloadData()
        self.mTableview.scrollToRow(at: IndexPath.init(row: sectionV.row.count - 1, section: sender.tag), at: .bottom, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let sectionV = self.sectionArr[indexPath.section]
        
        if !(sectionV.isOpen ?? false){
            return 0.0
        }
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func ShowDropDownImage() -> UIImageView {
        
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 40, width: 30, height: 30)
        imageView.image = UIImage.ionicon(with: .iosArrowDown, textColor: UIColor.black, size: CGSize(width: 50, height: 50))
        
        return imageView
        
    }
    // No Button Action
    @objc func ButtonNoAction() {
        
        for section in self.sectionArr{
            for object in section.row{
                if object.placeHolder ==  AppMessage.AddContact.WillingtoStock {
                    object.togleButtonsTitle?[0]["value"] = false
                    object.togleButtonsTitle?[1]["value"] = true
                }
                
            }
        }
        self.mTableview.reloadData()
        
    }
    
    //  Yes Button Action
    @objc func ButtonYesAction() {
        for section in self.sectionArr{
            for object in section.row{
                if object.placeHolder ==  AppMessage.AddContact.WillingtoStock {
                    object.togleButtonsTitle?[0]["value"] = true
                    object.togleButtonsTitle?[1]["value"] = false
                    
                }
                
            }
        }
        self.mTableview.reloadData()
    }
    
    func HideDropDownImage() -> UIImageView {
        
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 20, width: 1, height: 1)
        imageView.image = UIImage.ionicon(with: .iosArrowDown, textColor: .clear, size: CGSize(width: 1, height: 1))
        
        return imageView
        
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionV = self.sectionArr[indexPath.section]
        let rowV = sectionV.row[indexPath.row]
        
        switch indexPath.section {
        case 0,1,2,3,4,5,6:
            switch rowV.placeHolder{
            case AppMessage.AddContact.saluation,AppMessage.AddContact.contactName,AppMessage.AddContact.attn,AppMessage.AddContact.CustomerCategory,AppMessage.AddContact.companyofficeName,AppMessage.AddContact.type,AppMessage.AddContact.primarySpecialty,AppMessage.AddContact.secondarySpecialty,AppMessage.AddContact.phone,AppMessage.AddContact.email,AppMessage.AddContact.website,AppMessage.AddContact.leadStatus,AppMessage.AddContact.address1,AppMessage.AddContact.Address2,AppMessage.AddContact.country,AppMessage.AddContact.state,AppMessage.AddContact.City,AppMessage.AddContact.ZipPostalCode:
               
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
                cell.lbtitleAC.text = rowV.title
                cell.txtTitleAC.text = rowV.value
                cell.txtTitleAC.placeholder = rowV.placeHolder
                cell.txtTitleAC.tag = indexPath.row
                
                cell.txtTitleAC.delegate = self
                cell.selectionStyle = .none
    //            cell.txtTitleAC.autocapitalizationType = .words
                
                if rowV.placeHolder == AppMessage.AddContact.email{
                cell.txtTitleAC.keyboardType = .emailAddress
                cell.txtTitleAC.isUserInteractionEnabled = true
                cell.isUserInteractionEnabled = true
                }
                if rowV.isShowDropDown ?? false{
                if rowV.isShowBelowDropDown ?? false
                {
                  //  let boldConfig = UIImage.SymbolConfiguration(weight: .bold)
                    //let largeConfig = UIImage.SymbolConfiguration(textStyle: .largeTitle)
                        cell.txtTitleAC.rightViewMode = .always
                    if #available(iOS 13.0, *) {
                        
                        let homeSymbolConfiguration = UIImage.SymbolConfiguration(pointSize: 20, weight: .regular)
                        
                       let image = UIImage.init(systemName: "square.and.arrow.down", withConfiguration: homeSymbolConfiguration) ?? UIImage.init()
                        
                      let imageView = UIImageView.init(image:image)
                        Utility.shared.setColorOfImageView(imgView: imageView, color: UIColor.darkGray, image: image)
                        cell.txtTitleAC.rightView = imageView
                    } else {
                        // Fallback on earlier versions
                    }
                  
                    
                   // self.pickUpDate(cell.txtTitleAC)
                    
                }
                else if !(rowV.isShowDropDown ?? false) ?? false
                {
                        cell.txtTitleAC.rightViewMode = .always
                    if #available(iOS 13.0, *) {
                        //cell.txtTitleAC.rightView = UIImageView.init(image: UIImage(systemName: "forward"))
                    } else {
                        // Fallback on earlier versions
                    }
                }
                }
                else
                {
                    
                    cell.txtTitleAC.rightViewMode = .never
                    cell.txtTitleAC.rightView = nil
                    
                    // cell.accessoryView = HideDropDownImage()
                }
                if self.Redline{
                cell.txtTitleAC.layer.borderColor = UIColor.red.cgColor as CGColor
                cell.txtTitleAC.layer.borderWidth = 1.0
                }else{
                    cell.txtTitleAC.layer.borderColor = UIColor.gray.cgColor as CGColor
                    cell.txtTitleAC.layer.borderWidth = 0
                    
                }
                cell.tag = indexPath.section * 1000 + indexPath.row
                cell.txtTitleAC.tag = indexPath.section * 1000 + indexPath.row
                return cell
            case AppMessage.AddContact.WillingtoStock:
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "AddContactstockCell") as! AddContactstockCell
                
                var type = "No"
                let yes =  rowV.togleButtonsTitle?[0]["value"] as! Bool
                let no =  rowV.togleButtonsTitle?[1]["value"] as! Bool
               
                var selectedImage:UIImage = UIImage.init()
                var unSelectedImage:UIImage = UIImage.init()
                
                if #available(iOS 13.0, *) {
                    let homeSymbolConfiguration = UIImage.SymbolConfiguration(pointSize: 25, weight: .regular)
                    selectedImage = UIImage.init(systemName: "largecircle.fill.circle", withConfiguration: homeSymbolConfiguration) ?? UIImage.init()
                    
                    unSelectedImage = UIImage.init(systemName: "circle", withConfiguration: homeSymbolConfiguration) ?? UIImage.init()
                    
                } else {
                    // Fallback on earlier versions
                }
                
                
                if yes
                {
                   
                    
                   // cell1.btnYes.setImage(selectedImage, for: .normal)
                    Utility.shared.setColorOfButtonImage(btn: cell1.btnYes, color: UIColor.darkGray, image: selectedImage)
                    
                }
                else
                {
                   // cell1.btnYes.setImage(unSelectedImage, for: .normal)
                    Utility.shared.setColorOfButtonImage(btn: cell1.btnYes, color: UIColor.darkGray, image: unSelectedImage)
                   
                }
                
                if no
                {
                   // cell1.btnNo.setImage(selectedImage, for: .normal)
                    
                    Utility.shared.setColorOfButtonImage(btn: cell1.btnNo, color: UIColor.darkGray, image: selectedImage)
                }
                else
                {
                    //cell1.btnNo.setImage(unSelectedImage, for: .normal)
                    Utility.shared.setColorOfButtonImage(btn: cell1.btnNo, color: UIColor.darkGray, image: unSelectedImage)
                   
                   
                }
               
                
                cell1.btnNo.addTarget(self, action: #selector(ButtonNoAction), for: .touchUpInside)
                
                cell1.btnYes.addTarget(self, action: #selector(ButtonYesAction), for: .touchUpInside)
                
                cell1.selectionStyle = .none
                
                
                
                
                cell1.accessoryView = HideDropDownImage()
                
                
                    
                cell1.isUserInteractionEnabled = true
                    
               
                cell1.tag = indexPath.section * 1000 + indexPath.row
                return cell1
            default:
                break
            }
        default:
            break
        }
        
        
        return UITableViewCell.init()
        
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddContactVC1:UITextFieldDelegate {
    // datepickerView
    
    func pickUpDate(_ textField : UITextField){
        
        
        print("Tag value :",textField.tag)
        let sectionV = self.sectionArr[textField.tag]
        if sectionV.title == AppMessage.AddContact.defaultBillingAddress       {
            
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
            
            
        }
            
            
        else
        {
            textField.inputView = nil
            textField.inputAccessoryView = nil
            
        }
        
        
        
    }
    @objc func doneClick() {
        
        let sectionM = self.sectionArr[dataPicker.tag/1000]
        var rowM = sectionM.row[dataPicker.tag % 1000]
        
        rowM.value = Pickerdatavalue[0][RowValue]["name"].stringValue
        
        rowM.valueID =  Int(Pickerdatavalue[0][RowValue]["id"].stringValue) ?? 0
       
        
        if rowM.placeHolder == AppMessage.AddContact.country{
            for rowOb in sectionM.row{
                if rowOb.placeHolder == AppMessage.AddContact.state{
                    rowOb.value = ""
                    rowOb.valueID = 0
                }
            }
        }
        
        
        if rowM.placeHolder == AppMessage.AddContact.CustomerCategory{
            for rowOb in sectionM.row{
                if rowOb.placeHolder == AppMessage.AddContact.CustomerCategory{
                    self.CategaryIndex = RowValue
                }
                if rowOb.placeHolder == AppMessage.AddContact.type{
                    rowOb.value = ""
                    rowOb.valueID = 0
                }
            }
        }
        
        /*
        if rowM.placeHolder == AppMessage.AddContact.saluation{
            
            rowM.value = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            rowM.valueID =  Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
           
           
            
            
            print(Pickerdatavalue)
            
        }
        if rowM.placeHolder == AppMessage.AddContact.CustomerCategory{
            
            rowM.value = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            rowM.valueID =  Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
            print(Pickerdatavalue)
            
        }
        if rowM.placeHolder == AppMessage.AddContact.type{
            
            rowM.value = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            rowM.valueID =  Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
            print(Pickerdatavalue)
            
        }
        if rowM.placeHolder == AppMessage.AddContact.secondarySpecialty{
            
            rowM.value = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            rowM.valueID =  Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
            print(Pickerdatavalue)
            
        }
        if rowM.placeHolder == AppMessage.AddContact.primarySpecialty{
            
            rowM.value = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            rowM.valueID =  Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
            print(Pickerdatavalue)
            
        }
        
        */
        /*
        
        if indexvalu == 0 {
            
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
            print(Pickerdatavalue)
            
        }
        else if indexvalu == 3
        {
            
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            skippedArray[indexvalu+1] = ""
            
            SaveArray[indexvalu+1] = ""
            
            print(Pickerdatavalue)
            
            
            
            CategaryIndex = RowValue

            
        }
            
        else if indexvalu == 4
        {
            
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
            
            
            print(skippedArray)
             print(SaveArray)
            
        }
            
        else if indexvalu == 6
        {
            
            
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
            print(Pickerdatavalue)
            
        }
            
            
        else if indexvalu == 7
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
            print(Pickerdatavalue)
            
        }
            
        else if indexvalu == 12
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            print(Pickerdatavalue)
            
        }
            
            
        else if indexvalu == 13
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
                        
        }
            
        else if indexvalu == 17
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            skippedArray[indexvalu + 1] = ""
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            SaveArray[indexvalu + 1] = ""
            selectedCountryIdAddress = Pickerdatavalue[0][RowValue]["id"].intValue
            print(Pickerdatavalue)
            
        }
            
        else if indexvalu == 18
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            print(Pickerdatavalue)
            
        }
            
        else if indexvalu == 28
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            skippedArray[indexvalu + 1] = ""
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            SaveArray[indexvalu + 1] = ""
            selectedCountryIdBilling = Pickerdatavalue[0][RowValue]["id"].intValue
            print(Pickerdatavalue)
            
        }
            
        else if indexvalu == 29
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            print(Pickerdatavalue)
            
        }
            
            
            
        else if indexvalu == 35
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            skippedArray[indexvalu + 1] = ""
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            SaveArray[indexvalu + 1] = ""
            selectedCountryIdShipping = Pickerdatavalue[0][RowValue]["id"].intValue
            print(Pickerdatavalue)
            
        }
            
        else if indexvalu == 36
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            print(Pickerdatavalue)
            
        }
        
        
        self.dataPicker.reloadAllComponents()
        
        self.dataPicker.selectRow(0, inComponent: 0, animated: false)
        
        RowValue = 0
        
        //  temp_textField.resignFirstResponder()
        
        self.view .endEditing(true)
        
        
        print(SaveArray)
        
        mTableview.reloadData()
        */
        
        for object in sectionM.row{
            if object.placeHolder ==  AppMessage.AddContact.saluation
            {
                print(object.value)
            }
             
            
        }
       // self.dataPicker.reloadAllComponents()
        //self.dataPicker.selectRow(0, inComponent: 0, animated: false)
        
        for object in sectionM.row{
            if object.placeHolder ==  AppMessage.AddContact.saluation
            {
                print(object.value)
            }
             
            
        }
        
        self.view .endEditing(true)
        mTableview.reloadData()
        
        for object in sectionM.row{
            if object.placeHolder ==  AppMessage.AddContact.saluation
            {
                print(object.value)
            }
             
            
        }
       
    }
    
    
    @objc func cancelClick() {
        
        self.dataPicker.reloadAllComponents()
        
        self.dataPicker.selectRow(0, inComponent: 0, animated: false)
        
        RowValue = 0
        
        //   temp_textField.resignFirstResponder()
        
        self.view .endEditing(true)
        // temp_textField.resignFirstResponder()
    }
    
    

    func isValidInput(Input:String) -> Bool {
        let myCharSet=CharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        let output: String = Input.trimmingCharacters(in: myCharSet.inverted)
        let isValid: Bool = (Input == output)
        print("\(isValid)")

        return isValid
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print("textField----- did begin --> ",textField.tag)
        let sectionM = self.sectionArr[textField.tag/1000]
        let rowM = sectionM.row[textField.tag % 1000]
        
        if textField.placeholder == AppMessage.AddContact.saluation{
            Pickerdatavalue = [self.dataList[0]["salutation"]]
            
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
            
            print(Pickerdatavalue)
            //textField.resignFirstResponder()
            self.dataPicker.reloadAllComponents()
            self.dataPicker.tag = textField.tag
        }
       else if textField.placeholder == AppMessage.AddContact.CustomerCategory{
            Pickerdatavalue = [self.dataList[0]["category"]]
           
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
            
            print(Pickerdatavalue)
            self.dataPicker.tag = textField.tag
            self.dataPicker.reloadAllComponents()
        }
        else if textField.placeholder == AppMessage.AddContact.type{
            var isCategoryEmpty:Bool  = false
            for object in sectionM.row{
                if object.placeHolder ==  AppMessage.AddContact.CustomerCategory && object.value == ""{
                    isCategoryEmpty = true
                }
            }
            if isCategoryEmpty{
                self.alert("Please select category First")
               
                self.view .endEditing(true)
                Pickerdatavalue.removeAll()
                self.dataPicker.removeFromSuperview()
                self.toolBar.removeFromSuperview()
            }
            else{
              
              datavalue = [self.dataList[0]["category"]]

              print(datavalue[0][CategaryIndex]["types"])

              
              if datavalue[0][CategaryIndex]["types"].count == 0
              {
                  
                  self.view .endEditing(true)


                  Pickerdatavalue.removeAll()
                  self.dataPicker.removeFromSuperview()
                  self.toolBar.removeFromSuperview()
                  
                  self.alert("Type list is empty")

              }
              else
              {
                  

                  
                  print(datavalue)
                  print([self.datavalue[0]["types"]])
                  print(datavalue[0][RowValue]["types"])
                  
                  Pickerdatavalue =   [datavalue[0][0]["types"]]
                  print(Pickerdatavalue)
                textField.inputView = self.dataPicker
                textField.inputAccessoryView = self.toolBar
                self.dataPicker.tag = textField.tag
                  self.dataPicker.reloadAllComponents()
              }
             
              }
            
            
            
            
        }
        else if textField.placeholder == ""
        {
            
            
            
            guard let controller = ProductToMarket.instance() else
            {
                return
            }
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: false)
            
            self.dataPicker.tag = textField.tag
            textField.inputView = nil
            textField.inputAccessoryView = nil
            self.dataPicker.removeFromSuperview()
            self.toolBar.removeFromSuperview()
            
        }
      else  if textField.placeholder == AppMessage.AddContact.primarySpecialty{
            Pickerdatavalue = [self.dataList[0]["speciality"]]
           
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
            
            print(Pickerdatavalue)
            self.dataPicker.tag = textField.tag
            self.dataPicker.reloadAllComponents()
        }
      else  if textField.placeholder == AppMessage.AddContact.secondarySpecialty || textField.placeholder == AppMessage.AddContact.primarySpecialty{
            
            Pickerdatavalue = [self.dataList[0]["speciality"]]
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
            
            print(Pickerdatavalue)
            self.dataPicker.tag = textField.tag
            self.dataPicker.reloadAllComponents()
        }
     
       else if textField.placeholder == AppMessage.AddContact.leadStatus {
            
            Pickerdatavalue = [self.dataList[0]["leadstatus"]]
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
            
            print(Pickerdatavalue)
            self.dataPicker.tag = textField.tag
            self.dataPicker.reloadAllComponents()
        }
       else if textField.placeholder == AppMessage.AddContact.country {
            
            Pickerdatavalue = [self.dataList[0]["country"]]
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
            
            print(Pickerdatavalue)
            self.dataPicker.tag = textField.tag
            self.dataPicker.reloadAllComponents()
        }
      else  if textField.placeholder == AppMessage.AddContact.state {
            
            var isCountryEmpty:Bool  = false
              var selectedCountryId:Int = 0
            for object in sectionM.row{
                if object.placeHolder ==  AppMessage.AddContact.country && object.value == ""{
                    isCountryEmpty = true
                }
                if object.placeHolder ==  AppMessage.AddContact.country && object.value != ""{
                    selectedCountryId = object.valueID
                }
            }
            if isCountryEmpty{
                self.alert("Please select Country First")
               
                self.view .endEditing(true)
                Pickerdatavalue.removeAll()
                self.dataPicker.removeFromSuperview()
                self.toolBar.removeFromSuperview()
            }else{
                let stateDict = self.dataList[0]["state"]
                let filteredArrayBool = stateDict.filter{$0.1["country_id"].stringValue == "\(selectedCountryId)"}
                let temp = filteredArrayBool.map{$0.1}
                print(temp)
                let filteredarray  = JSON(temp)
                
                
                Pickerdatavalue = [filteredarray]
                textField.inputView = self.dataPicker
                textField.inputAccessoryView = self.toolBar
                
                print(Pickerdatavalue)
                self.dataPicker.tag = textField.tag
                self.dataPicker.reloadAllComponents()
            }
        }else{
            
            textField.inputView = nil
            textField.inputAccessoryView = nil
            self.dataPicker.removeFromSuperview()
            self.toolBar.removeFromSuperview()
        }
            
            
          
          
        /*
        
        
        
       
        
       
            
//        else if textField.tag == 8
//        {
//            Pickerdatavalue = [self.dataList[0]["title"]]
//
//
//            print(Pickerdatavalue)
//
//            self.dataPicker.reloadAllComponents()
//        }
            
        else if textField.tag == 12
        {
            Pickerdatavalue = [self.dataList[0]["leadstatus"]]
            
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
        }
            
        else if textField.tag == 13
        {
            Pickerdatavalue = [self.dataList[0]["source"]]
            
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
        }
            
            
        else if textField.tag == 17
        {
            Pickerdatavalue = [self.dataList[0]["country"]]
            
            print("Pickerdatavalue is - \(Pickerdatavalue)")
            
            self.dataPicker.reloadAllComponents()
        }
            
        else if textField.tag == 28
        {
            Pickerdatavalue = [self.dataList[0]["country"]]
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
        }
            
            
        else if textField.tag == 35
        {
            Pickerdatavalue = [self.dataList[0]["country"]]
            
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
        }
            
            
        else if textField.tag == 18
        {
            // edit by harsh
            let stateDict = self.dataList[0]["state"]
            
            let filteredArrayBool = stateDict.filter{$0.1["country_id"].stringValue == "\(selectedCountryIdAddress)"}
            let temp = filteredArrayBool.map{$0.1}
            print(temp)
            let filteredarray  = JSON(temp)
            
            //filteredArray
            if temp.count > 0{
                Pickerdatavalue = [filteredarray]
                
                
                print(Pickerdatavalue)
                
                self.dataPicker.reloadAllComponents()
            }
            else{
                self.alert("Please select Country First")
            }
            /* Pickerdatavalue = [self.dataList[0]["state"]]
             
             print(Pickerdatavalue)
             
             self.dataPicker.reloadAllComponents()*/
        }
            
        else if textField.tag == 29
        {
            
            
            // edit by harsh
            let stateDict = self.dataList[0]["state"]
            
            let filteredArrayBool = stateDict.filter{$0.1["country_id"].stringValue == "\(selectedCountryIdBilling)"}
            let temp = filteredArrayBool.map{$0.1}
            print(temp)
            let filteredarray  = JSON(temp)
            
            //filteredArray
            if temp.count > 0{
                Pickerdatavalue = [filteredarray]
                
                
                print(Pickerdatavalue)
                
                self.dataPicker.reloadAllComponents()
            }
            else{
                self.alert("Please select Country First")
            }
            /* Pickerdatavalue = [self.dataList[0]["state"]]
             
             print(Pickerdatavalue)
             
             self.dataPicker.reloadAllComponents()*/
            
        }
            
        else if textField.tag == 36
        {
            
            
            // edit by harsh
            let stateDict = self.dataList[0]["state"]
            
            let filteredArrayBool = stateDict.filter{$0.1["country_id"].stringValue == "\(selectedCountryIdShipping)"}
            let temp = filteredArrayBool.map{$0.1}
            print(temp)
            let filteredarray  = JSON(temp)
            
            //filteredArray
            if temp.count > 0{
                Pickerdatavalue = [filteredarray]
                
                
                print(Pickerdatavalue)
                
                self.dataPicker.reloadAllComponents()
            }
            else{
                self.alert("Please select Country First")
            }
            /* Pickerdatavalue = [self.dataList[0]["state"]]
             
             print(Pickerdatavalue)
             
             self.dataPicker.reloadAllComponents()*/
            
        }
            
            
        else
        {
            textField.inputView = nil
            textField.inputAccessoryView = nil
            self.dataPicker.removeFromSuperview()
            self.toolBar.removeFromSuperview()
            
        }*/
           }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let sectionM = self.sectionArr[textField.tag/1000]
        let rowM = sectionM.row[textField.tag % 1000]
        if textField.placeholder != AppMessage.AddContact.saluation &&  textField.placeholder != AppMessage.AddContact.CustomerCategory && textField.placeholder != AppMessage.AddContact.type && textField.placeholder != AppMessage.AddContact.primarySpecialty && textField.placeholder != AppMessage.AddContact.secondarySpecialty &&  textField.placeholder != AppMessage.AddContact.country && textField.placeholder != AppMessage.AddContact.state  && textField.placeholder != AppMessage.AddContact.leadStatus{
            rowM.value = textField.text ?? ""
        }
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.placeholder == AppMessage.AddContact.phone{
            textField.keyboardType = .phonePad
        } else if textField.placeholder == AppMessage.AddContact.attn || textField.placeholder == AppMessage.AddContact.ZipPostalCode {
            textField.keyboardType = .numberPad
        }
        else{
            textField.keyboardType = .default
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == ""{
            return true
        }
        if textField.placeholder == AppMessage.AddContact.phone && textField.text?.count ?? 0 > 17{
           return false
        }
      /*  print(self.dataList)
        
        
        //  let currency_name = self.dataList[data]["name"].stringValue
        
        print("shouldChangeCharactersIn :---",textField.tag)
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        print("textField-----should change> ",textField.tag)
        print("textField-----indexvalu should change> ",indexvalu)
        indexvalu = textField.tag
        
        if indexvalu == 9
        {
//            print("typing in phone number field", indexvalu)
            
            do {
                let regex = try NSRegularExpression(pattern:".*[^0-9- ].*", options: [])
                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                    return false
                }
            }
            catch {
                print("ERROR")
            }
            
        }
        
        if indexvalu == 16
        {
            textField.autocapitalizationType = .words
        }
        
       
        
        skippedArray[textField.tag] = newString
        
        SaveArray[textField.tag] = newString
        
        
        
        print(skippedArray)
       */
        
        return true
    }
    
}
// MARK: - PickerView Delegate/DataSource Methods

extension AddContactVC1 : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // edit by harsh
        if !Pickerdatavalue.isEmpty
        {
            return Pickerdatavalue[0].count
            
        }
        return 0
        // return Pickerdatavalue[0].count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Pickerdatavalue[0][row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        
        RowValue = row
        
    }
}

// MARK: - Class Instance

extension AddContactVC1
{
    class func instance()->AddContactVC1?{
        let storyboard = UIStoryboard(name: "Team", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AddContactVC1") as? AddContactVC1
        
        
        return controller
    }
}
extension String {
    func isValidPhoneNumber() -> Bool {
        let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
                let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
                return phoneTest.evaluate(with: self)
    }
    
    func isValidEmail()->Bool {
                let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
                return  NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
        }
}
