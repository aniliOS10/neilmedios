//
//  NotificationVC.swift
//  MedMobilie
//
//  Created by dr.mac on 19/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit
import IoniconsKit
import DGElasticPullToRefresh
import SwiftyJSON
import MMDrawerController
import PopItUp

class NotificationVC: InterfaceExtendedController {

    
    fileprivate var arrItems_img : [String]?
    fileprivate var img = UIImage()
    fileprivate var BackGround = UIColor()
    fileprivate var NotificationListData : [JSON]?

  
    var class_type : Bool = false

    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.showWaitView()
        self.NotificationList_fetch()
      
        
        arrItems_img = ["AppIcon",
                        "Visit",
                        "Task",
                        "Bills",
                        "Target",
                        "Orders",
                        "Reports",
                        ]
    
            tableView.separatorStyle = .none
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if !class_type
        {
            let btSlider = UIBarButtonItem(image: UIImage.ionicon(with: .androidMenu, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), style: .plain, target: self, action: #selector(OpenSlider))
            btSlider.tintColor = UIColor.white
            navigationItem.leftBarButtonItem = btSlider
        }
   
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: navBackgroundColor ?? SettingNAVBAR(), BtnColor: UIColor.white)
        
    }
    
    // MARK: - Language Setting
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Notifications")
    }
  
    
    // MARK: - Notification List fetch

    func NotificationList_fetch()
    {
        if !NetworkState.isConnected() {
             self.tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        NotificationFetch_Api().Fetch { (status, message, data) in
           AppDelegate.hideWaitView()
            if !status
            {
                self.tableView.dg_stopLoading()
                //self.alert(message ?? "Something Went Wrong")
                return
            }
            self.NotificationListData = data
            self.tableView.reloadData()
            self.tableView.dg_stopLoading()
        }
    }
    
    
    
    // MARK: - Open Slider top left

    @objc func OpenSlider(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }

    
}

// MARK: - Table View Delegate/DataSource Methods

extension NotificationVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NotificationListData?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        
        
        guard let infos = NotificationListData?[indexPath.row]  else{return }
        
        
        if infos["type"].stringValue == "bill"
        {
            guard let controller = BillsDetails.instance()
                else{return}
            controller.BillId = infos["type_id"].intValue
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if infos["type"].stringValue == "target"
        {
            guard let controller = TargetVC.instance()
                else{return}
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if infos["type"].stringValue == "visit"
        {
            guard let controller = VisitDetailsVC.instance()
                else{return}
            controller.visitId = infos["type_id"].intValue
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if infos["type"].stringValue == "task"
        {
            guard let controller = TaskDetails.instance()
                else{return}
            controller.TaskId = infos["type_id"].intValue
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if infos["type"].stringValue == "order"
        {
            guard let controller = OrderDetailsVC.instance()
                else{return}
            controller.ID = infos["type_id"].intValue
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        
        else if infos["type"].stringValue == "admin"
        {

        }
        
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? NotificationListingCell
            else{return NotificationListingCell()}
        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
       
        
        
        guard let infos = NotificationListData?[indexPath.row]  else{return cell}
        cell.lblTask?.text = infos["title"].stringValue.capitalized
        cell.lblDetails?.text = infos["description"].stringValue
        cell.imgiconNotification?.contentMode = .scaleAspectFit
        cell.lblTime?.text = CalculateAgoTime().timeAgoSinceDate(date: infos["created_date"].stringValue.stringToDate(), numericDates: true)
       
        
         if infos["type"].stringValue == "admin"
        {
            img =   UIImage(named: "\(arrItems_img![0])")!
            BackGround = SettingNAVBAR()
            
        }
         else if infos["type"].stringValue == "visit"
         {
             img =   UIImage(named: "\(arrItems_img![1])")!
            BackGround = VisitNAVBAR()
         }
            
         else if infos["type"].stringValue == "task"
         {
            img =   UIImage(named: "\(arrItems_img![2])")!
             BackGround = TaskNAVBAR()
         }
        
      else  if infos["type"].stringValue == "bill"
        {
            img =   UIImage(named: "\(arrItems_img![3])")!
            BackGround = BillNAVBAR()
        }
        else if infos["type"].stringValue == "target"
        {
            img =   UIImage(named: "\(arrItems_img![4])")!
            BackGround = TargetNAVBAR()

        }
        
       
        else if infos["type"].stringValue == "order"
        {
            
             img =   UIImage(named: "\(arrItems_img![5])")!
            BackGround = OrderNAVBAR()

        }
        
         else if infos["type"].stringValue == "reports"
         {
            
            img =   UIImage(named: "\(arrItems_img![6])")!
            BackGround = ReportNAVBAR()

        }
         else
         {
            
            img =   UIImage(named: "\(arrItems_img![0])")!
            BackGround = ReportNAVBAR()
            
        }
        
        
        
       
      cell.viewShadow?.backgroundColor = BackGround
        cell.imgiconNotification?.image = img
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        
//        if editingStyle == .delete {
//            
//         
//            
//        }
//    }
    
}

// MARK: - PullToRefress

extension NotificationVC
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
          
            
            self!.NotificationList_fetch()

            
            }, loadingView: loadingView)
        tableView.dg_setPullToRefreshFillColor(SettingNAVBAR())

        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
}

// MARK: - Class Instance

extension NotificationVC
{
    class func instance()->NotificationVC?{
        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as? NotificationVC
        //self.definesPresentationContext = true
        
        return controller
    }
}
