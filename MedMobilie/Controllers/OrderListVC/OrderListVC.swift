//
//  OrderListVC.swift
//  MedMobilie
//
//  Created by dr.mac on 10/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import IoniconsKit
import DGElasticPullToRefresh
import SwiftyJSON
import SwipeCellKit
class OrderListVC: InterfaceExtendedController , UISearchResultsUpdating {
    fileprivate var orderData : [JSON]?
    fileprivate var resultSearchController = UISearchController()
    fileprivate var mainJsonData : [JSON]?

    
    var datefrom : String = ""
    var dateto : String = ""
    var type : String = ""
     var contact_id : Int = 0
    var reportKey : String = ""
    var comeFromContact : Bool = false
    
   
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
       
       
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelectionDuringEditing = true
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.delegate = self
        tableView.dataSource = self
        NavBarFilter()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SetUpSearchBar()
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: OrderNAVBAR(), BtnColor: UIColor.white)
        
        AppDelegate.showWaitView()
        DataFetch()
      
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.resultSearchController.dismiss(animated: false, completion: nil)
    }
    
    
    // MARK: - Language Setting
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Orders")
    }
    
    // MARK: - Set navigationbar Icons
    func NavBarFilter()
    {
        
        let Filter = UIBarButtonItem(image: UIImage.ionicon(with: .androidOptions, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), style: .plain, target: self, action: #selector(Filter_Action))
        Filter.tintColor = UIColor.white
        
        
        navigationItem.rightBarButtonItem = Filter
        
        
    }
    
    // MARK: - Filter Action

    @objc func Filter_Action(){
        
        self.popupAlertWithSheet(title: nil, message: nil, actionTitles: [ self.languageKey(key: "Pending"),self.languageKey(key: "Confirmed"),self.languageKey(key: "Paid"),self.languageKey(key: "Cancel"),self.languageKey(key: "Dispatched"),self.languageKey(key: "Date range")], actions:[
            {action1 in
                
                
                self.type = "1"
                self.datefrom = ""
                self.dateto = ""

                
                AppDelegate.showWaitView()
                self.DataFetch()
                
                
            },{action2 in
                
                self.type = "2"
                self.datefrom = ""
                self.dateto = ""
                AppDelegate.showWaitView()
                self.DataFetch()
                
                
            },{action3 in
                
                self.type = "3"
                self.datefrom = ""
                self.dateto = ""
                AppDelegate.showWaitView()
                self.DataFetch()
                
                
            },{action4 in
                
                self.type = "4"
                self.datefrom = ""
                self.dateto = ""
                AppDelegate.showWaitView()
                self.DataFetch()
                
                
            },{action5 in
                
                self.type = "5"
                self.datefrom = ""
                self.dateto = ""
                AppDelegate.showWaitView()
                self.DataFetch()
                
                
            },{action6 in
                
                
                
                guard let controller = FilterDateRange.instance()
                    else{return}
                controller.delegate = self
                controller.NavColor = OrderNAVBAR()

                self.navigationController?.pushViewController(controller, animated: true)
                
                
            }, nil])
    }
    
    
    // MARK: - SetUp SearchBar

    func SetUpSearchBar()
    {
        self.resultSearchController = ({
            
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.searchBar.placeholder = "Search Order".languageSet
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.hidesNavigationBarDuringPresentation = false
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
    }
    
   
    
    func updateSearchResults(for searchController: UISearchController) {
        // filteredTableData.removeAll(keepingCapacity: false)
        
        if searchController.searchBar.text != ""
        {
            let Filterarray = self.mainJsonData?.filter({ (element) -> Bool in
                
                if element["salutation"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "") || element["contact_name"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "")
                {
                    return true
                }
                return false
            })
            
            self.orderData = Filterarray
            self.tableView.reloadData()
        }
        else
        {
            self.orderData = mainJsonData
            self.tableView.reloadData()
            
        }
        
    }
    
    
    
    
    
    // data fetching
    fileprivate func DataFetch()
    {
        if !NetworkState.isConnected() {
            self.tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
            
            self.OfflineOrderList()
            return
        }
        
        
        var DataValue = [String : Any]()
        DataValue["datefrom"] = datefrom
        DataValue["dateto"] = dateto
        if reportKey != ""
        {
            DataValue["report_type"] = reportKey
        }
        else
        {
            DataValue["type"] = type
        }
        
        DataValue["URL"] = "/orderslist"
        DataValue["contact_id"] = contact_id
        
        
        
        
        VisitList_Api().fetchData(param: DataValue) { (status, message, data) in
          
           
            
            if !status
            {
                AppDelegate.hideWaitView()
                self.mainJsonData?.removeAll()
                 self.orderData?.removeAll()
                  self.tableView.reloadData()
                self.tableView.dg_stopLoading()
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: message, actionTitles: [self.languageKey(key: "OK")], actions:[{action1 in
                    
                    
                    }, nil])
                return
            }
             AppDelegate.hideWaitView()
            self.reportKey = ""
            self.mainJsonData = data
            self.orderData = self.mainJsonData
            self.tableView.reloadData()
            self.tableView.dg_stopLoading()
            
        }
        
        
    }
    
    
    
    fileprivate func OfflineOrderList()
    {
        
        DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.OrderList.rawValue) { (data) in
            
            if let object = data
            {
                var jsonArray = [JSON]()

                for jsonData in object
                {
                    print("jsonData:---->",jsonData)
                    
                    if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                        
                          print("jsonArray:---->",jsondata)
                    
                        
                        jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                            jsonArray.append(json!)
                            
                            
                            print("jsonArray:---->",jsonArray)
                            
                        })})
                    }
                }
           
                AppDelegate.hideWaitView()
                self.reportKey = ""
                self.mainJsonData = jsonArray
                self.orderData = self.mainJsonData
                self.tableView.reloadData()
                self.tableView.dg_stopLoading()
                    
                }
                AppDelegate.hideWaitView()
                // print("jsondata:------>",jsonArray)
            }
        }
        
   
    // MARK: - Order Delete Request

    fileprivate func OrderDeleteRequest(order: Int)
    {
        
        if !NetworkState.isConnected() {
            self.tableView.dg_stopLoading()

            AppDelegate.alertViewForInterNet()
            return
        }
        OrderDelete_Api().Request(orderID: order) { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                self.DataFetch()
                return
            }
           //self.alert(message)
//
            return
            
            
        }
    }
    
    
   
    
    deinit {
        tableView.dg_removePullToRefresh()
    }
    
}



// MARK: - Table View Delegate/DataSource Methods

extension OrderListVC : UITableViewDelegate , UITableViewDataSource, SwipeTableViewCellDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return orderData?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let list = orderData?[indexPath.row] else{return}
        
        guard let controller = OrderDetailsVC.instance()
            else{return}
        
        
        if !NetworkState.isConnected() {
            
            
            controller.ID = list["id"].intValue
            controller.contact_name = "\(list["salutation"].stringValue) \(list["contact_name"].stringValue)"
            controller.customer_id = "NMAPP" + list["order_no"].stringValue
            controller.TotalPrice = String(list["order_total"].doubleValue.round(to: 2))
            
             controller.product_price = list["product_price"].stringValue
             controller.no_of_cases = list["no_of_cases"].stringValue
             controller.product_name = list["product_name"].stringValue
           
            
        }
        else
        {
            controller.ID = list["id"].intValue
            controller.contact_name = "\(list["salutation"].stringValue) \(list["contact_name"].stringValue)"
            controller.customer_id = "NMAPP" + list["order_no"].stringValue
            controller.TotalPrice = String(list["order_total"].doubleValue.round(to: 2))
        }
        
        
       
        self.navigationController?.pushViewController(controller, animated: true)
        
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "OrderListFetchCell", for: indexPath) as? OrderListFetchCell
            else{return OrderListFetchCell()}
        
        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
        guard let list = self.orderData?[indexPath.row] , let symbol = SignedUserInfo.sharedInstance?.currency_symbol else{return cell}
        cell.lblContact_Name?.text = "\(list["salutation"].stringValue) \(list["contact_name"].stringValue)"
       
       
        let temptext = "\(symbol) \(list["order_total"].doubleValue.round(to: 2))"
        cell.lblprice?.text = temptext
        cell.lblprice?.textColor = .red
        
        if NetworkState.isConnected() {
          
            cell.delegate = self
             cell.lblid?.text = self.languageKey(key: "Id :")
             cell.lblOrder_Id?.text = "NMAPP" + list["order_no"].stringValue
        }
        else
        {
            cell.lblid?.text = ""
            cell.lblOrder_Id?.text = ""
        }
        
        
        
        cell.lblTime?.text = list["created_date"].stringValue
       var str = ""
        switch list["status"].intValue {
        case 1:
            str = "Pending"
            cell.lblstatus?.textColor =  hexStringToUIColor(hex: "#FFA20D")
            break
        case 2:
            str = "Confirmed"
            cell.lblstatus?.textColor = hexStringToUIColor(hex: "#1F713C")
            break
        case 3:
            str = "Paid"
             cell.lblstatus?.textColor = hexStringToUIColor(hex: "#1F713C")
            break
        case 4:
            str = "Cancelled"
             cell.lblstatus?.textColor = .red
            break
        case 5:
            str = "Dispatched"
            cell.lblstatus?.textColor = hexStringToUIColor(hex: "#1F713C")
            break
        default:
            cell.lblstatus?.textColor = UIColor.lightText
            break
        }
        cell.lblstatus?.text = str.languageSet
        
       cell.selectionStyle = .none
        
      //  cell.lblTask?.text = "Going to Warehouse to collect"
        //cell.textLabel?.text = numbers[indexPath.row]
        
        return cell
    }
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
   
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        guard let list = self.orderData?[indexPath.row] else{return []}
        if list["status"].intValue == 1
        {
        let deleteAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
          
            self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: "Are you sure you want to delete this order"), actionTitles: [self.languageKey(key: "NO"),self.languageKey(key: "YES")], actions:[{action1 in
                return
                },{action2 in
                    
                    self.orderData?.remove(at: indexPath.row)
                    self.tableView.reloadData()
                    DispatchQueue.global(qos: .background).async {
                        DispatchQueue.main.async {
                             self.OrderDeleteRequest(order: list["id"].intValue)
                            }
                      // self.OrderDeleteRequest(order: list["id"].intValue)
                        
                    }

                    
                }, nil])
            // handle action by updating model with deletion
        }
        
        // customize the action appearance
        deleteAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor: .white, size: CGSize(width: 40, height: 40))
        
        return [deleteAction]
        }
        return []
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }

    
    
   
    
}

// MARK: - Filter Protocol
extension OrderListVC
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            
            self!.type = ""
            self!.datefrom = ""
            self!.dateto = ""
            
            self?.DataFetch()
            }, loadingView: loadingView)
        tableView.dg_setPullToRefreshFillColor(OrderNAVBAR())
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
}

// MARK: - Filter Protocol

extension OrderListVC : FilterProtocol
{
    func Filterdetails(FromDate : String , ToDate : String)
    {
        
        
        datefrom = FromDate
        dateto = ToDate
        
        print("datefrom :---> ",datefrom)
        print("dateto :---> ",dateto)
        
        AppDelegate.showWaitView()
        self.DataFetch()
        
    }
    
}

// MARK: - Class instance

extension OrderListVC
{
    class func instance()->OrderListVC?{
        let storyboard = UIStoryboard(name: "Products", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "OrderListVC") as? OrderListVC
        //self.definesPresentationContext = true
        
        return controller
    }
}
