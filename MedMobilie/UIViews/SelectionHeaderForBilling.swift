//
//  SelectionHeaderForBilling.swift
//  MedMobilie
//
//  Created by dr.mac on 09/04/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit

class SelectionHeaderForBilling: UITableViewCell {

    @IBOutlet var CheckANDUncheckBtn : UIButton!
    @IBOutlet var textLbl : UILabel!
    @IBOutlet var ImageOFBtn : UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
