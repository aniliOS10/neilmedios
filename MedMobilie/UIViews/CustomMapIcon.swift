//
//  CustomMapIcon.swift
//  MedMobilie
//
//  Created by dr.mac on 21/02/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit

class CustomMapIcon: UIView {

    @IBOutlet var title : UILabel!
    @IBOutlet var snippet : UILabel!
    @IBOutlet var containerView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Setup view from .xib file
        xibSetup()
    }
    func xibSetup() {
        backgroundColor = UIColor.clear
         Bundle.main.loadNibNamed("CustomMapIcon", owner: self, options: nil)
        //addSubview(containerView)
        // use bounds not frame or it'll be offset
        containerView.frame = frame
        draw(containerView.frame)
        // Adding custom subview on top of our view
        addSubview(containerView)
        
        //containerView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        containerView.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": containerView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": containerView]))
        
        
    }
    
    override func draw(_ rect: CGRect) {
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: rect.origin.x, y: rect.origin.y))
        path.addLine(to: CGPoint(x: rect.width, y: rect.origin.y))
        path.addLine(to: CGPoint(x: rect.width, y: rect.height * 0.80))
        
        // Draw arrow
        path.addLine(to: CGPoint(x: rect.midX + 10, y: rect.height * 0.80))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.height))
        path.addLine(to: CGPoint(x: rect.midX - 10, y: rect.height * 0.80))
        
        path.addLine(to: CGPoint(x: rect.origin.x, y: rect.height * 0.80))
        path.close()
        
        let shape = CAShapeLayer()
        shape.fillColor = UIColor.white.cgColor
        shape.strokeColor = UIColor.gray.cgColor
        shape.lineWidth = 3.0
        //shape.fillColor = UIColor.blue.cgColor
        shape.path = path.cgPath
        //containerView.layer.addSublayer(shape)
        
        
        self.layer.addSublayer(shape)
        
        self.addSubview(containerView)
    }
    
    
//    override func draw(_ rect: CGRect) {
//        let mask = CAShapeLayer()
//        mask.frame = self.layer.bounds
//
//        let width = self.layer.frame.size.width
//        let height = self.layer.frame.size.height
//
//        let path = CGMutablePath()
//
//       path.move(to: CGPoint(x: width, y: 0))
//        path.addLine(to: CGPoint(x: width, y: 0))
//        path.addLine(to: CGPoint(x: width/2, y: height ))
//        path.addLine(to: CGPoint(x: 0, y: 0 ))
//       // CGPathAddLineToPoint(path, nil, width, 0)
//        //CGPathAddLineToPoint(path, nil, width/2, height)
//        //CGPathAddLineToPoint(path, nil, 0, 0)
//
//        mask.path = path
//        self.layer.mask = mask
//    }
    
}
