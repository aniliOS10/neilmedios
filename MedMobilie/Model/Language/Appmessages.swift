//
//  Appmessages.swift
//  MedMobilie
//
//  Created by Apple on 17/09/21.
//  Copyright © 2021 dr.mac. All rights reserved.
//

import Foundation
class AppMessage: NSObject {
    struct AddContact {
        static let saluation = NSLocalizedString("Saluation", comment: "")
        static let contactName = NSLocalizedString("Contact Name", comment: "")
        static let attn = NSLocalizedString("Attn", comment: "")
        static let CustomerCategory = NSLocalizedString("Customer Category", comment: "")
        
        static let type = NSLocalizedString("Type", comment: "")
        static let primarySpecialty = NSLocalizedString("Primary Specialty", comment: "")
        static let secondarySpecialty = NSLocalizedString("Secondary Specialty", comment: "")
        static let companyofficeName = NSLocalizedString("CompanyofficeName", comment: "")
        static let phone = NSLocalizedString("Phone", comment: "")
        static let email = NSLocalizedString("Email", comment: "")
        static let website = NSLocalizedString("Website", comment: "")
        static let leadStatus = NSLocalizedString("Lead Status", comment: "")
        static let status = NSLocalizedString("Status", comment: "")
        static let sourceofLead = NSLocalizedString("Source of Lead", comment: "")
        static let LeadStatus = NSLocalizedString("Lead Status *", comment: "")
        static let WillingtoStock = NSLocalizedString("Willing to Stock", comment: "")
        static let Userinformation = NSLocalizedString("User information", comment: "")
        static let Address = NSLocalizedString("Address", comment: "")
        static let other = NSLocalizedString("Other", comment: "")
        static let address1 = NSLocalizedString("Address 1", comment: "")
        static let Address2 = NSLocalizedString("Address 2", comment: "")
        static let country = NSLocalizedString("Country", comment: "")
        static let state = NSLocalizedString("State", comment: "")
        static let City = NSLocalizedString("City", comment: "")
       
        static let ZipPostalCode = NSLocalizedString("Zip/Postal Code", comment: "")
        static let fax = NSLocalizedString("Fax", comment: "")
        static let defaultShippingAddress = NSLocalizedString("Default Shipping Address", comment: "")
        static let defaultBillingAddress = NSLocalizedString("Default Billing Address", comment: "")
        
        static let commercial = NSLocalizedString("Commercial", comment: "")
        static let residential = NSLocalizedString("Residential", comment: "")
    }
}
