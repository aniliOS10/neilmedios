//
//  CameraHandler.swift
//  M1 Pay
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//
import Foundation
import UIKit


class CameraHandler: NSObject{
    static let shared = CameraHandler()
    
    fileprivate var currentVC: UIViewController!
    
    //MARK: Internal Properties
    var imagePickedBlock: ((_ image : UIImage) -> ())?
   // var imagePickedBlock: ((UIImage) -> Void)?
    
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            myPickerController.allowsEditing = true
            currentVC.navigationController!.setNavigationBarHidden(true, animated: false)
            // Add it as a subview
            
            currentVC.addChild(myPickerController)
            currentVC.view.addSubview(myPickerController.view)
            currentVC.tabBarController?.tabBar.isHidden = true
            
            //currentVC.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            currentVC.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    func directOpenCamera(vc: UIViewController)
    {
        currentVC = vc
        self.camera()
    }
    
    func showActionSheet(vc: UIViewController , galleryOptions : Bool) {
        currentVC = vc
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        if galleryOptions
        {
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        }
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        vc.present(actionSheet, animated: true, completion: nil)
        
        
        
        
    }
    
}


extension CameraHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismissPicker(picker: picker)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.editedImage] as? UIImage {
            
            self.imagePickedBlock?(image)
        }
        else if let image = info[.originalImage] as? UIImage {
            
            self.imagePickedBlock?(image)
        }else{
            print("Something went wrong")
        }
        dismissPicker(picker: picker)
    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
//
//            self.imagePickedBlock?(image)
//        }else{
//            print("Something went wrong")
//        }
//        currentVC.dismiss(animated: true, completion: nil)
//    }
   
    private func dismissPicker(picker : UIImagePickerController){
        picker.view!.removeFromSuperview()
        picker.removeFromParent()
        currentVC.navigationController?.setNavigationBarHidden(false, animated: false)
        currentVC.tabBarController?.tabBar.isHidden = false
        UIApplication.shared.isStatusBarHidden = false
    }
    
}
