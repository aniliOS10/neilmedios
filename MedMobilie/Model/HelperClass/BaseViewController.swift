
import UIKit

class BaseViewController: UIViewController {

    func backBtnWithNavigationTitle(title : String) {
        self.title = title
        self.navigationItem.leftBarButtonItem  = UIBarButtonItem(image: UIImage(named: "back")?.maskWithColor(color: UIColor.black), style: .plain, target: self, action:#selector(BaseViewController.backToView))
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor .white
    }
    
    @objc func backToView() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func buttonActForTransitonAnimationBack () {
        let transition = CATransition()
        transition.duration = 0.8
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType(rawValue: "reveal")
        transition.subtype = CATransitionSubtype(rawValue: "fromLeft")
        self.navigationController?.view?.layer.add(transition, forKey: nil)
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
/*extension UIViewController
{
    var topbarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
}*/
