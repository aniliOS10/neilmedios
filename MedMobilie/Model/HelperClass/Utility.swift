//
//  Utility.swift
//  MedMobilie
//
//  Created by Apple on 20/09/21.
//  Copyright © 2021 dr.mac. All rights reserved.
//

import Foundation
import UIKit

class Utility: NSObject {
    static let shared:Utility = Utility()

var appDelegate: AppDelegate {
     return UIApplication.shared.delegate as! AppDelegate
    }
    func setColorOfButtonImage(btn:UIButton,color:UIColor,image:UIImage){
        
        let origImage = image
        let tintedImage = origImage.withRenderingMode(.alwaysTemplate)
        btn.setImage(tintedImage, for: .normal)
        btn.tintColor = color    }
    func setColorOfImageView(imgView:UIImageView,color:UIColor,image:UIImage){
        
        let origImage = image
        let tintedImage = origImage.withRenderingMode(.alwaysTemplate)
        imgView.image = tintedImage
        imgView.tintColor = color    }
    
    
    
    
   
    func makeShadowsOfView(view:UIView,shadowColor:UIColor = UIColor.black){
        
      
        view.layer.shadowColor = shadowColor.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = 4
        
        
        
    }
    func makeShadowsOfView_blow_roundCorner(view:UIView,shadowColor:UIColor = UIColor.lightGray,shadowRadius:CGFloat,shadowOffset:CGSize,cornerRadius:CGFloat,borderWidth:CGFloat = 1,borderColor:UIColor){
        
      
        view.layer.shadowColor = shadowColor.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = shadowOffset
        view.layer.shadowRadius = shadowRadius
        
        view.layer.cornerRadius = cornerRadius
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = borderColor.cgColor
        
    }
    func makeShadowsOfView_roundCorner(view:UIView,shadowColor:UIColor = UIColor.darkGray,shadowRadius:CGFloat,cornerRadius:CGFloat,borderWidth:CGFloat = 1,borderColor:UIColor){
        
      
        view.layer.shadowColor = shadowColor.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = shadowRadius
        
        view.layer.cornerRadius = cornerRadius
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = borderColor.cgColor
        
    }
    func makeRoundCorner(layer:CALayer,color:UIColor,radius:CGFloat,borderWidth:CGFloat = 1){
        layer.cornerRadius = radius
        layer.borderWidth = borderWidth
        layer.borderColor = color.cgColor
        layer.masksToBounds = true
        
    }
    func makeCircular(layer:CALayer,borderColor:UIColor,object:UIView,borderWidth:CGFloat = 1.0){
        layer.borderWidth = borderWidth
        layer.masksToBounds = true
        layer.borderColor = borderColor.cgColor
        layer.cornerRadius = object.frame.height/2
        object.clipsToBounds = true
    }
    func makeCircular_withConstantWidthHeight(layer:CALayer,borderColor:UIColor,object:UIView,width:CGFloat,height:CGFloat){
        layer.borderWidth = 1
        layer.masksToBounds = false
        layer.borderColor = borderColor.cgColor
        layer.cornerRadius = height/2
        object.clipsToBounds = true
    }
    
    func roundCorners_particular(layer:CALayer,cornerRadius: CGFloat,corners:CACornerMask)
    {
     
          layer.cornerRadius = cornerRadius
          layer.maskedCorners =  corners //[.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    func roundCorners(corners: UIRectCorner, radius: CGFloat,view:UIView) {
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
         view.layer.mask = mask
        view.layer.masksToBounds = true
        
        
        
        }
    
    func roundParticular(corners: UIRectCorner, cornerRadius: Double,view1:UIView) {
           
           let size = CGSize(width: cornerRadius, height: cornerRadius)
           let bezierPath = UIBezierPath(roundedRect: view1.bounds, byRoundingCorners: corners, cornerRadii: size)
           let shapeLayer = CAShapeLayer()
        shapeLayer.frame = view1.bounds
             shapeLayer.path = bezierPath.cgPath
        view1.layer.mask = shapeLayer
       }
    func roundedButtonByCorners(button:UIButton,corners:UIRectCorner,cornerRadius:CGSize){
            
        /*let maskPath1 = UIBezierPath(roundedRect: button.bounds,
                byRoundingCorners: [.topLeft , .topRight],
                cornerRadii: CGSize(width: 8, height: 8))
 */
        
      let  maskPath1 = UIBezierPath(roundedRect: button.bounds,
                byRoundingCorners: corners,
                cornerRadii: cornerRadius)
            let maskLayer1 = CAShapeLayer()
            maskLayer1.frame =  button.bounds
            maskLayer1.path = maskPath1.cgPath
            button.layer.mask = maskLayer1
        
        }
    func changeStatusBarColor(view:UIView,color:UIColor){
        let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
        statusbarView.backgroundColor =  color
            view.addSubview(statusbarView)
          
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
    }
    func taxPercenatage(price:Double,tax:Double) -> Double{
        let basicPrice = price
        let tax = (basicPrice * tax) / 100
        return tax
    }
}
