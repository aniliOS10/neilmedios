
import UIKit

class GlobalConstantClass: NSObject {
    
    struct setColor {
        //0xAB47BC

        static var appColor: UIColor = UIColor.init(red: 242.0/255.0, green: 242.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        static let borderColor: UIColor = UIColor(red: 158.0/255.0, green: 161.0/255.0, blue: 160.0/255.0, alpha: 1.0)
        static var appGrayColor: UIColor = UIColor.init(red: 195.0/255.0, green: 195.0/255.0, blue: 201.0/255.0, alpha: 1.0)
        static var appBlueColor: UIColor = UIColor.init(red: 54.0/255.0, green: 143.0/255.0, blue: 242.0/255.0, alpha: 1.0)
    }
    
    struct strCheck {
     //   static var checkForHud:Bool = false
        static var idForDetail:String?
    }
    
}

extension UIImageView {
    
    func dropShadow() {
        
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.init(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0).cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 5.0
        
    }
    
    func setImageFromURl(stringImageUrl url: String){
        
        if let url = NSURL(string: url) {
            if let data = NSData(contentsOf: url as URL) {
                self.image = UIImage(data: data as Data)
            }
        }
    }
}

extension UITextField {
    
    @IBInspectable var leftSide: UIImage {
        get {
            return UIImage ()
        }
        set {
            let left: UIImageView = UIImageView(frame: CGRect(x: 10, y: 0, width: 50.0, height: self.frame.size.height))
            left.image = newValue
            left.backgroundColor = UIColor.clear
            left.contentMode = .center
            leftViewMode = .always
            self.leftView = left
        }
    }
    
    @IBInspectable var rightSide: UIImage {
        get {
            return UIImage()
        }
        set {
            let right: UIImageView = UIImageView(frame: CGRect(x: 10, y: 0, width: 20.0, height: self.frame.size.height))
            right.image = newValue
            right.backgroundColor = UIColor.clear
            right.contentMode = .center
            rightViewMode = .always
            self.rightView = right
        }
        
    }
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
            
            DispatchQueue.global(qos: .userInitiated).async {
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    self.layer.borderColor = GlobalConstantClass.setColor.borderColor.cgColor
                    //self.layer.borderWidth = 1
                   // self.layer.cornerRadius = 4
                   // self.layer.masksToBounds = true
                }
            }
        }
    }
    
}
