//
//  SignInProcessor.swift
//  MedMobilie
//
//  Created by dr.mac on 24/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON

class LoginProcessor : NSObject{
    
    public func Request(email : String, password : String, completion : @escaping ((_ success : Bool,_ message : String, _ info:UserInfo?)->())){
         var lang : String? = ""
         var currency_code : String? = ""
        let ud = UserDefaults.standard
        if ud.value(forKey: "language") as? String != nil
        {
            lang = ud.value(forKey: "language") as? String
            
        }
        if ud.value(forKey: "currency_code") as? Int != nil
        {
            currency_code = "\(ud.value(forKey: "currency_code") as! Int)"
           
        }
        var param = [String : Any]()
        param["email"] = email
        param["password"] = password
        
        param["language"] = lang ?? ""
        param["device_type"] = "i" // for ios device
        param["currency_id"] = currency_code ?? ""
        if ud.value(forKey: "DeviceToken") != nil {
            param["device_token"] = ud.value(forKey: "DeviceToken")
        }else{
            param["device_token"] = ""
        }
       
        let url = AppConnectionConfig.webServiceURL  + "/user_login"
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: false) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String,_ info:UserInfo? )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false,errorMessage, nil)
                return
        }
        
        //        if(!success){
        //            completion(false, errorMessage, .approved, nil)
        //            return
        //        }
        
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        guard let rawinfoData = rawInfo["data"].dictionary
            else
        {
            completion(false, message,nil)
            return
        }
        
        print(rawinfoData)
        guard let accessToken = rawinfoData["access_token"]?.stringValue else
        {
            completion(true,message, nil)
            return
        }
        
        if(successValue == "true"){
            
            let ud = UserDefaults.standard
            ud.set(accessToken, forKey: "accessToken")
            ud.set(nil, forKey: "language")
             ud.set(nil, forKey: "currency_code")
            ud.synchronize()
            
            let userInfo = SignedUserInfo.initSharedInstance(userInfoJSON: rawinfoData)
            test()
            LocalizationSystem.SharedInstance.SetLanguage(languageCode: userInfo.language ?? "en")
            completion(true,message, userInfo)
            return
            
        }
        else
        {
            completion(false,message,nil)
            return
        }
            /*ProfileInfoFetch().fetchInfo(userId: user_id) { (success, userInfo) in
                
                if(!success){
                    completion(false,nil)
                    return
                }
                completion(true, userInfo)
                return
            }
            
            return
        }
        else
        {
            completion(false,nil)
            return
        }*/
    
}
    
    fileprivate func test(){
        guard let info = SignedUserInfo.sharedInstance
            else{
                //Log.echo(key: "", text: "info is null")
                return
        }
        
       // Log.echo(key: "", text: "uesrInfo is --> \(info.user_id)")
    }

}
