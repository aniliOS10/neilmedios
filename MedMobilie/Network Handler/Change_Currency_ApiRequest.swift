//
//  Change_Currency_Api.swift
//  MedMobilie
//
//  Created by dr.mac on 07/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import SwiftyJSON
class Change_Currency_ApiRequest: NSObject  {
    
   
        public func  Request(_ Currency_Code : Int , completion : @escaping ((_ success : Bool,_ message : String  )->())){
            
            var param = [String : Any]()
            
            param["currency_id"] = Currency_Code
            
            
            let url = AppConnectionConfig.webServiceURL  + "/currencyupdate"
            
            ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
                self.handleResponse(withSuccess: success, response: response, completion: completion)
            }
        }
        
        private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String )->())){
            
            //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
            
            let errorMessage = "errorMessage";
            
            guard let rawInfo = response
                else{
                    completion(false, errorMessage)
                    return
            }
            let successValue = rawInfo["status"].stringValue
            let message = rawInfo["message"].stringValue
           
            if (successValue == "fail")
            {
                
                InterfaceExtendedController().sessionExpired()
                
            }
            guard let rawinfoData = rawInfo["data"].dictionary
                else
            {
                completion(false,message)
                return
            }
            
           
            if(successValue == "true"){
                SignedUserInfo.sharedInstance?.currency_symbol = rawinfoData["symbol"]?.stringValue
                SignedUserInfo.sharedInstance?.currency_code = rawinfoData["code"]?.stringValue
               // SignedUserInfo.sharedInstance? = rawinfoData["currency_symbol"]?.stringValue
                
                
                completion(true, message)
                return
            }
                
            else
            {
                completion(false,message)
                return
            }
            
            
        }
        
    }
    


