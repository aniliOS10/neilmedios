
//
//  VisitList_Api.swift
//  MedMobilie
//
//  Created by dr.mac on 21/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON
class VisitList_Api : NSObject{
    

    
    public func  fetch( param : [String: Any],completion : @escaping ((_ success : Bool,_ message : String ,_ data : [JSON]? )->())){
        
        print(param)
        var url :String = ""
        
        url = AppConnectionConfig.webServiceURL  + "/visitlist"
        
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }

        
    }
    
    
    
    public func  fetchData( param : [String: Any],completion : @escaping ((_ success : Bool,_ message : String ,_ data : [JSON]? )->())){
        
        
        print(param)
        
        
        var url :String = (param["URL"] as? String)!
            
        url = AppConnectionConfig.webServiceURL  + url

      //  url = AppConnectionConfig.webServiceURL  + "/billapp_list"
        
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    
        
    }
    
    
    public func  fetchReport( param : [String: Any],completion : @escaping ((_ success : Bool,_ message : String ,_ data : [JSON]? )->())){
        
        
        print(param)
        
        
        var url :String = ""
        
        
        url = AppConnectionConfig.webServiceURL  + "/reportslist"
        
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
        
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String , _ DataList : [JSON]?)->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage , nil)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if(successValue == "fail"){
            InterfaceExtendedController().sessionExpired()
        }
        guard let rawinfoData = rawInfo["data"].array
            else
        {
            completion(false,message , nil )
            return
        }
        
        if(successValue == "true"){
            completion(true, message , rawinfoData )
            return
        }
        else
        {
            completion(false,message , nil)
            return
        }
        
        
    }
}


