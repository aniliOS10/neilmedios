//
//  TakeSignUpload_Api.swift
//  MedMobilie
//
//  Created by dr.mac on 30/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON
class TakeSignUpload_Api: NSObject {
    
    public func  Request(urlString : String,id : Int,signImage : UIImage,imageNameString : String, completion : @escaping ((_ success : Bool,_ message : String  )->())){
        
        var param = [String : Any]()
       param["id"] = id
        
        
        
        //"signature"  for signature image name
        //"selfie" for selfie image name
        //"/visitcontactaddsignature" this urlstring for visitsignature
        // "/visitcontactaddselfie" this urlstring for visit selfie
        
        
        let url = AppConnectionConfig.webServiceURL  + urlString //"/visitcontactaddsignature"
        
        ServerProcessor().upload(image: signImage, imageName: imageNameString, url, parameters: param, headers: nil, authorize: true){ (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
            return
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
        
        /* raw
         let lang = rawinfoData["language"]?.stringValue*/
        if(successValue == "true"){
            completion(true, message )
            return
        }
        else
        {
            completion(false,message )
            return
        }
        
        
    }
    
    
}
