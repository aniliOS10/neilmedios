//
//  ContactListFetch.swift
//  MedMobilie
//
//  Created by dr.mac on 28/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON
class ContactListFetch : NSObject{
    
    public func  Fetch( Parama : [String:Any]? = nil ,completion : @escaping ((_ success : Bool,_ message : String ,_ data : [JSON]? )->())){
        
        var param = [String : Any]()
        param = Parama ?? [String:Any]()
       // param["language"] = lang
        
        
        let url = AppConnectionConfig.webServiceURL  + "/contact_list"
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String,_ data : [JSON]? )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage, nil)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
        guard let rawinfoData = rawInfo["data"].array
            else
        {
            completion(false,message,nil)
            return
        }
        
       /* raw
        let lang = rawinfoData["language"]?.stringValue*/
        if(successValue == "true"){
            completion(true, message,rawinfoData )
            return
        }
        
        else
        {
            completion(false,message ,nil)
            return
        }
        
        
    }
    
}
