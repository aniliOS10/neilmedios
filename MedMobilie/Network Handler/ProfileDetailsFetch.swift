//
//  ProfileDetailsFetch.swift
//  MedMobilie
//
//  Created by dr.mac on 26/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON

class ProfileDetailsFetch : NSObject{
    
    public func  fetch( completion : @escaping ((_ success : Bool,_ message : String?,_ info : [JSON]?)->())){
        
        var param = [String : Any]()
        
        let url = AppConnectionConfig.webServiceURL  + "/show_profile"
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String?, _ info: [JSON]? )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage , nil)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
        guard let rawinfoData = rawInfo["data"].array
            else
        {
            completion(false,message ,nil)
            return
        }
        if(successValue == "true"){
            //let userInfo = SignedUserInfo.initSharedInstance(userInfoJSON: rawinfoData)
            test()
            completion(true,message, rawinfoData)
            return
        }
       
        else
        {
            completion(false,message , nil)
            return
        }
        
        
    }
    fileprivate func test(){
        guard let info = SignedUserInfo.sharedInstance
            else{
                //Log.echo(key: "", text: "info is null")
                return
        }
        
        // Log.echo(key: "", text: "uesrInfo is --> \(info.user_id)")
    }
    
}

