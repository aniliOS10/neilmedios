//
//  Logout_api.swift
//  MedMobilie
//
//  Created by dr.mac on 26/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON
class Logout_api : NSObject{
    
    public func  Request( completion : @escaping ((_ success : Bool,_ message : String )->())){
        
        var param = [String : Any]()
        
        let url = AppConnectionConfig.webServiceURL  + "/user_logout"
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
         if (successValue == "fail")
        {
            
            completion(true,message)
            
        }

       
       else if(successValue == "true"){
            completion(true, message )
            return
        }
        else
        {
            completion(true,message)
            return
        }
        
        
}
}
