import Foundation
import SwiftyJSON
class Listsapi : NSObject{
    
    public func  Request( completion : @escaping ((_ success : Bool,_ message : String , _ dataList : [JSON]? )->())){
        
        var param = [String : Any]()
        
        
        
        
        let url = AppConnectionConfig.webServiceURL  + "/listsapi"
        
        
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
        
      
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String , _ DataList : [JSON]?)->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage , nil)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if(successValue == "fail"){
            InterfaceExtendedController().sessionExpired()
        }
        guard let rawinfoData = rawInfo["data"].array
            else
        {
            completion(false,message , nil )
            return
        }
        
        if(successValue == "true"){
            completion(true, message , rawinfoData )
            return
        }
        else
        {
            completion(false,message , nil)
            return
        }
        
        
    }
}

