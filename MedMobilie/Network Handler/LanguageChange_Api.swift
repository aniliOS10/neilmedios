//
//  LanguageChange_Api.swift
//  MedMobilie
//
//  Created by dr.mac on 26/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON

class LanguageChange_Api : NSObject{
    
    public func  Request(_ lang : String , completion : @escaping ((_ success : Bool,_ message : String? , _ lang : String? )->())){
        
        var param = [String : Any]()
        
        param["language"] = lang
        
        
        let url = AppConnectionConfig.webServiceURL  + "/change_language"
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String? , _ lang : String? )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage, "")
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
        guard let rawinfoData = rawInfo["data"].dictionary
            else
        {
            completion(false,message,nil)
            return
        }
        let lang = rawinfoData["language"]?.stringValue
        if(successValue == "true"){
            guard let userInfo = SignedUserInfo.sharedInstance else{return}
                userInfo.language = lang ?? "en"
            completion(true, message, lang )
            return
        }
       
        else
        {
            completion(false,message ,lang)
            return
        }
        
        
    }
    
}

