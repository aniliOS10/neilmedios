//
//  ProductDetails_ApiRequest.swift
//  MedMobilie
//
//  Created by dr.mac on 07/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//
//ProductDetails_ApiRequest "/productdetail"
import Foundation
import SwiftyJSON
class DataFetch_Api : NSObject{
    
    public func  Fetch( ID : Int, urlString : String,completion : @escaping ((_ success : Bool,_ message : String ,_ data : [JSON]? )->())){
        var param = [String : Any]()
        param["id"] = ID
        let url = AppConnectionConfig.webServiceURL  + urlString
        print("params are \(param)")
        print("url is \(url)")
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    public func  fetch( param : [String : Any], urlString : String,completion : @escaping ((_ success : Bool,_ message : String ,_ data : [JSON]? )->())){
        
        
        let url = AppConnectionConfig.webServiceURL  + urlString
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    public func  FetchData( ID : Int, urlString : String,completion : @escaping ((_ success : Bool,_ message : String ,_ data : [JSON]? )->()))
    {
        
        var param = [String : Any]()
        
        param["user_id"] = ID
        
        
        let url = AppConnectionConfig.webServiceURL  + urlString
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String,_ data : [JSON]? )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage, nil)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
        
        guard let rawinfoData = rawInfo["data"].array
            else
        {
            completion(false,message,nil)
            return
        }
        
        /* raw
         let lang = rawinfoData["language"]?.stringValue*/
        if(successValue == "true"){
            completion(true, message,rawinfoData )
            return
        }
        else
        {
            completion(false,message ,nil)
            return
        }
        
        
    }
    
}
