//
//  deleteprofileimage_Api.swift
//  MedMobilie
//
//  Created by dr.mac on 04/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON
class  Deleteprofileimage_Api: NSObject{
    
    public func  Request( completion : @escaping ((_ success : Bool,_ message : String )->())){
        
        var param = [String : Any]()
        
        
        
        
        let url = AppConnectionConfig.webServiceURL  + "/deleteprofileimage"
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
        
       else if(successValue == "true"){
             SignedUserInfo.sharedInstance?.userImg = nil
            completion(true, message )
           
            return
        }
       
        else
        {
            completion(false,message)
            return
}
}
}
