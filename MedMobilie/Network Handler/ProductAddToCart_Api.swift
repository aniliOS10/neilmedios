//
//  ProductAddToCart_Api.swift
//  MedMobilie
//
//  Created by MAC on 08/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON
class ProductAddToCart_Api: NSObject {
    public func  Request( id : Int , cases : Int , discount : Double, completion : @escaping ((_ success : Bool,_ message : String  )->())){
        
        var param = [String : Any]()
        param["product_id"] = id
        param["no_of_cases"] = cases
        param["discountpercase"] = discount
        
        
        
        
        let url = AppConnectionConfig.webServiceURL  + "/cartproductadd"
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
        
        /* raw
         let lang = rawinfoData["language"]?.stringValue*/
        if(successValue == "true"){
            completion(true, message )
            return
        }
        else
        {
            completion(false,message )
            return
        }
        
        
    }
    
}
    

