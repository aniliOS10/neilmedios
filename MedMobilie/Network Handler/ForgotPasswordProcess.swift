//
//  ForgotPasswordProcess.swift
//  MedMobilie
//
//  Created by dr.mac on 26/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON

class ForgotPasswordProcess : NSObject{
    
    public func  Request(email : String, completion : @escaping ((_ success : Bool,_ message : String?,_ otp : Int?)->())){
        
        var param = [String : Any]()
        param["email"] = email
        param["language"] = "en"
        
        
        let url = AppConnectionConfig.webServiceURL  + "/user_forgotpassword"
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: false) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String?,_ otp : Int? )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, "something went Wrong" , 0)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        guard let rawinfoData = rawInfo["data"].dictionary
            else
        {
            completion(false,message,0)
            return
        }
        print(rawinfoData)

        if(successValue == "true"){
            
            let otp = rawinfoData["otp"]?.intValue
            
            test()
            completion(true, message ,otp ?? 0)
            return
        }
        else
        {
            completion(false,message,0)
            return
        }
       
        
    }
    
    fileprivate func test(){
        guard let info = SignedUserInfo.sharedInstance
            else{
                //Log.echo(key: "", text: "info is null")
                return
        }
        
        // Log.echo(key: "", text: "uesrInfo is --> \(info.user_id)")
    }
    
}
