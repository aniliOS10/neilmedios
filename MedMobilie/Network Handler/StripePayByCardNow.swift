//
//  StripePayByCardNow.swift
//  MedMobilie
//
//  Created by dr.mac on 10/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON
import Stripe
class StripePayByCardNow : NSObject
{
    public func  Request(params : [String:Any], completion : @escaping ((_ success : Bool,_ message : String  )->())){
        
        var param = [String : Any]()
        param = params
       
     
        
        let url = AppConnectionConfig.webServiceURL  + "/make_order_payment"
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
        
        /* raw
         let lang = rawinfoData["language"]?.stringValue*/
        if(successValue == "true"){
            completion(true, message )
            return
        }
        else
        {
            completion(false,message )
            return
        }
        
        
    }
}
