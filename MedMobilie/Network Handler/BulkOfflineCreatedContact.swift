//
//  BulkOfflineCreatedContact.swift
//  MedMobilie
//
//  Created by dr.mac on 09/05/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON


class BulkOfflineCreatedContact : NSObject{
    
    
    
    public func  Request( param : [String: Any],apiUrl:String,completion : @escaping ((_ success : Bool,_ message : String ,_ data : [JSON]? )->())){
        
        
        
        
        print(param)
        
        let url = AppConnectionConfig.webServiceURL  + apiUrl
       ServerProcessor().request(.post, url, parameters: param, encoding: .defaultEncoding, headers: nil, authorize: true)
        { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
            return
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String,_ data : [JSON]? )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage, nil)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }

        if(successValue == "true"){
            
            guard let rawinfoData = rawInfo["data"].array
            else
            {
                    completion(true,message,nil)
                    return
            }
            completion(true, message,rawinfoData)
            return
        }
        else
        {
            completion(false,message ,nil)
            return
        }
        
        
    }
    
}




