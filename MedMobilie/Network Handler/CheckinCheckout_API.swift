
import Foundation
import SwiftyJSON
class CheckinCheckout_API : NSObject{
    
    public func  Fetch( param : [String: Any],Url : String,completion : @escaping ((_ success : Bool,_ message : String ,_ data : [JSON]? )->())){
        
        print(param)
        
        let url = AppConnectionConfig.webServiceURL  + Url
        
        ServerProcessor().uploadmuliple(image: [], imageName : "attachments", url, parameters: param,headers: nil,authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
            return
        }
    }
    
    func jsonToString(json: AnyObject){
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            print(convertedString) // <-- here is ur string
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
    }
    
    
    func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
            options = JSONSerialization.WritingOptions.prettyPrinted
        }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: options)
            if let string = String(data: data, encoding: String.Encoding.utf8) {
                return string
            }
        } catch {
            print(error)
        }
        
        return ""
    }
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String,_ data : [JSON]? )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage, nil)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
       
        /* raw
         let lang = rawinfoData["language"]?.stringValue*/
        if(successValue == "true"){
            guard let rawinfoData = rawInfo["data"].array
                else
            {
                completion(true,message,nil)
                return
            }
            completion(true,message,rawinfoData)
            return
        }
        else
        {
            completion(false,message ,nil)
            return
        }
        
    }
    
}
