//
//  ProfileUpdate_Api.swift
//  MedMobilie
//
//  Created by dr.mac on 04/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ProfileUpdate_Api: NSObject {
    
    public func  request(param : [String:Any] , image : UIImage? , completion : @escaping ((_ success : Bool,_ message : String,_ info : UserInfo?)->())){
       
        let url = AppConnectionConfig.webServiceURL  + "/update_profile"
        
        ServerProcessor().upload(image: image ?? nil, imageName : "image", url, parameters: param,headers: nil,authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
            return
        }

        
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String, _ info: UserInfo? )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage , nil)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
        guard let rawinfoData = rawInfo["data"].dictionary
            else
        {
            completion(false,message ,nil)
            return
        }
        if(successValue == "true"){
            
            
            print("data----->",rawinfoData)
            
            let userInfo = SignedUserInfo.initSharedInstance(userInfoJSON: rawinfoData)
           // test()*/
            completion(true,message, nil)
            return
        }
        else
        {
            completion(false,message , nil)
            return
        }
        
        
    }
    fileprivate func test(){
        guard let info = SignedUserInfo.sharedInstance
            else{
                //Log.echo(key: "", text: "info is null")
                return
        }
        
        // Log.echo(key: "", text: "uesrInfo is --> \(info.user_id)")
    }
    
}

