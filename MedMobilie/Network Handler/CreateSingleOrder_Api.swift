//
//  CreateSingleOrder_Api.swift
//  MedMobilie
//
//  Created by dr.mac on 09/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON

class CreateSingleOrder_Api: NSObject {
    
    public func  Request( parameter : [String:Any],link: String, completion : @escaping ((_ success : Bool,_ message : String ,_ order_id : Int )->())){
        
        var param = [String : Any]()
       param = parameter
        
        print(param)
        
        let url = AppConnectionConfig.webServiceURL  + link
        
        
        print("URL vlue:----->",url)
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String,_ order_id : Int )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage,0)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
        guard let rawinfoData = rawInfo["data"].dictionary
            else
        {
            completion(false,message ,0)
            return
        }
        
        /* raw
         let lang = rawinfoData["language"]?.stringValue*/
        if(successValue == "true"){
            completion(true, message, rawinfoData["orderid"]?.intValue ?? 0 )
            return
        }
        else
        {
            completion(false,message ,0)
            return
        }
        
        
    }
    
}

