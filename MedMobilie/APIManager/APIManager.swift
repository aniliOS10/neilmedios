//
//  APIManager.swift
//  
//
//  Created by Bhushan Rana on 05/12/18.
//  Copyright © 2018 Bhushan Rana. All rights reserved.
//

import UIKit
import Alamofire



class APIManager: NSObject {
    static let shared = APIManager()
    static let appBaseURL = "http://3.16.156.115/medmobilie/public"
    
    
    
    func getDataFrom(urlString : String ,isLoader : Bool, loaderMessage : String, completion: @escaping (_ success: [String : AnyObject]?,_ error : Error?) -> Void) {
        //        if isLoader{
        //            ProgressIndicator.shared.startAnimating(withMessage: loaderMessage)
        //
        //        }
        
        
        
        Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default)
            .responseJSON { response in
                debugPrint(response)
                //                if isLoader{
                //                    ProgressIndicator.shared.stopAnimating()
                //
                //                }
                if let responseDictionary = response.result.value{
                    
                    completion(responseDictionary as? [String : AnyObject] , nil)
                }else{
                    completion(nil , response.error)
                }
        }
    }
    
    
    func postDataFor(urlString : String,parameters : Parameters ,imageData : Data?,isLoader : Bool, loaderMessage : String, completion: @escaping (_ success: [String : AnyObject]?,_ error : Error?) -> Void) {
        //        if isLoader{
        //            ProgressIndicator.shared.startAnimating(withMessage: loaderMessage)
        //
        //        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if imageData != nil{
                multipartFormData.append(imageData!, withName: "image", fileName: "image.jpeg", mimeType: "image/jpeg")
            }
            
        }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: nil) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    //                    if isLoader{
                    //                        ProgressIndicator.shared.stopAnimating()
                    //                    }
                    if let responseDictionary = response.result.value{
                        
                        completion(responseDictionary as? [String : AnyObject] , nil)
                    }else{
                        completion(nil , response.error)
                    }
                }
            case .failure( let error):
                //                if isLoader{
                //                    ProgressIndicator.shared.stopAnimating()
                //                }
                completion(nil , error)
            }
        }
    }
    
    
}
struct GlobalConstants {
    //  COLOR CONSTANT
    
    static let kColor_ThemeColor: UIColor = UIColor(red: 243.0/255.0, green: 69.0/255.0, blue: 96.0/255.0, alpha: 1.0)
    static let kColor_orange: UIColor = UIColor(red: 255.0/255.0, green: 147.0/255.0, blue: 38.0/255.0, alpha: 1.0)
    static let kColor_NonCompliant: UIColor = UIColor(red: 190.0/255.0, green: 15.0/255.0, blue: 52.0/255.0, alpha: 1.0)
}
