//
//  PlaceListCell.swift
//  MedMobilie
//
//  Created by dr.mac on 18/02/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit

class PlaceListCell: UITableViewCell {

    @IBOutlet var lblTitleName : UILabel?
    @IBOutlet var lblCategoryName : UILabel?
    @IBOutlet var viewShadow : UIView?
    @IBOutlet var lblQueryName : UILabel?
    @IBOutlet var lblRadiusRange : UILabel?
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
