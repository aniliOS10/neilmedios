//
//  ProductListingCell.swift
//  MedMobilie
//
//  Created by dr.mac on 20/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit

class ProductListingCell: UITableViewCell {
    
    @IBOutlet var lblProductName : UILabel?
   
    @IBOutlet var lblCategoryName: UILabel?
   
    
    @IBOutlet var imgProductimg : UIImageView?
    @IBOutlet fileprivate var lblQtyText : UILabel?
    @IBOutlet fileprivate var lblPriceText : UILabel?
    @IBOutlet var  Units : UILabel?
    
    @IBOutlet var  Price : UILabel?
    
    @IBOutlet var viewShadow : UIView?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblQtyText?.text = CellLanguageSet(key: "Units Per Case")
        lblPriceText?.text = CellLanguageSet(key: "Per Case")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   

}
