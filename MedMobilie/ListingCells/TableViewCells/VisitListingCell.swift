//
//  VisitListingCell.swift
//  MedMobilie
//
//  Created by dr.mac on 17/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit
import SwipeCellKit

class VisitListingCell: SwipeTableViewCell {

    @IBOutlet var lblTask : UILabel?
    @IBOutlet var lblVisitType : UILabel?
    @IBOutlet var lblToTime: UILabel?
    @IBOutlet var lblFromTime : UILabel?
     @IBOutlet var lbldate : UILabel?
    @IBOutlet var viewShadow : UIView?
    @IBOutlet fileprivate var lblpending : UILabel?
    @IBOutlet fileprivate var lblVisited: UILabel?
    @IBOutlet fileprivate var lblReschedule : UILabel?
    @IBOutlet var lblPendingCount : UILabel?
    @IBOutlet var lblVisitedCount: UILabel?
    @IBOutlet var lblRescheduleCount : UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblpending?.text = self.CellLanguageSet(key: "Pending") + ":"
        lblVisited?.text = self.CellLanguageSet(key: "Visited") + ":"
        lblReschedule?.text = self.CellLanguageSet(key: "Re-Schedule") + ":"
        self.layoutIfNeeded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   

}
