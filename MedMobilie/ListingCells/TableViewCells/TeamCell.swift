//
//  TeamCell.swift
//  MedMobilie
//
//  Created by MAC on 14/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit
import SwipeCellKit
import IoniconsKit
class TeamCell: SwipeTableViewCell {

     @IBOutlet var viewShadow : UIView?
    var gender : String = "Male_PH"
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblChatLogo: UILabel!
    @IBOutlet var lblStatus : UILabel?
    @IBOutlet var waitingTimeView : UIView?
    @IBOutlet fileprivate var ClockTimeIcon : UILabel?
    @IBOutlet var lblWaitingTime : UILabel?
    @IBOutlet var DateTime : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ClockTimeIcon?.text = String.ionicon(with: .iosTimerOutline)
        // Initialization code
       imgUser.createCircleForView()
    }
}
