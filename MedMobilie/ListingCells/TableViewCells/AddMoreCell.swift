//
//  AddMoreCell.swift
//  demo
//
//  Created by MAC on 25/01/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

class AddMoreCell: UITableViewCell {

    
    @IBOutlet weak var ChooseAction: UIButton!
    @IBOutlet weak var AddNewAction: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ChooseAction.setTitle("Choose existing".languageSet, for: .normal)
        AddNewAction.setTitle("Add more".languageSet, for: .normal)

    }
   
}
