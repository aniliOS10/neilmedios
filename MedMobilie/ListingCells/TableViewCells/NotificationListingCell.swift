//
//  NotificationListingCell.swift
//  MedMobilie
//
//  Created by dr.mac on 19/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit

class NotificationListingCell: UITableViewCell {
    @IBOutlet var lblTask : UILabel?
    @IBOutlet var lblDetails : UILabel?
    @IBOutlet var lblTime: UILabel?
    @IBOutlet var viewShadow : UIView?
    @IBOutlet var imgiconNotification : UIImageView?
    
    
    
    
     @IBOutlet var lblTotal : UILabel?
    @IBOutlet var lblTotal_value : UILabel?
    
    
     @IBOutlet var lblSpace1 : UILabel?
    
    
     @IBOutlet var lblSpace2 : UILabel?
    
    @IBOutlet var lblPending : UILabel?
    @IBOutlet var lblPending_value : UILabel?
    
    
    @IBOutlet var lblClose : UILabel?
    @IBOutlet var lblClose_value : UILabel?


    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
