//
//  AddContactstockCell.swift
//  MedMobilie
//
//  Created by MAC on 04/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit

class AddContactstockCell: UITableViewCell {
    
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    
    @IBOutlet weak var lblNo: UILabel!
    @IBOutlet weak var lblYes: UILabel!
}

