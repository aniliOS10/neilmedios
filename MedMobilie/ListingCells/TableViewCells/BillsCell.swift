//
//  BillsCell.swift
//  MedMobilie
//
//  Created by MAC on 19/02/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import SwipeCellKit

class BillsCell: SwipeTableViewCell {

    @IBOutlet var lblExpense_name : UILabel?
    @IBOutlet var lblStatus : UILabel?
    @IBOutlet var lblDate: UILabel?
    @IBOutlet var lblExpense_amount : UILabel?
    
    
    @IBOutlet var viewShadow : UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //lblid?.text = languageSet(key: "Pay by:")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
   
    

}
