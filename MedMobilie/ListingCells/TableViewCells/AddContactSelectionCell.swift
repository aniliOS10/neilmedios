//
//  AddContactSelectionCell.swift
//  MedMobilie
//
//  Created by MAC on 04/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
// rana Code 

import UIKit

class AddContactSelectionCell: UITableViewCell {
    @IBOutlet weak var lblBillingAddress: UILabel!
    @IBOutlet weak var lblShippingAddress: UILabel!
    
    @IBOutlet weak var lblResidential: UILabel!
    @IBOutlet weak var lblCommercial: UILabel!
    @IBOutlet weak var btnCommercial: UIButton!
    @IBOutlet weak var btnResidential: UIButton!
    @IBOutlet weak var btnShippingAddress: UIButton!
    @IBOutlet weak var btnBillingAddress: UIButton!
}
