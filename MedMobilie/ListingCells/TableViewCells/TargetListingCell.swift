//
//  TargetListingCell.swift
//  MedMobilie
//
//  Created by dr.mac on 25/02/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import SwipeCellKit

class TargetListingCell: SwipeTableViewCell {

    @IBOutlet var name: UILabel?
     @IBOutlet var StartDate : UILabel?
     @IBOutlet var EndDate : UILabel?
     @IBOutlet var Status : UILabel?
     @IBOutlet var Percent : UILabel?
     @IBOutlet var viewShadow: UIView?
     @IBOutlet var lblDescription: UILabel?
    @IBOutlet  var tolbl : UILabel?
    @IBOutlet var visitCount : UILabel?
    @IBOutlet var saleCount : UILabel?
    @IBOutlet var sampleCount : UILabel?
    @IBOutlet fileprivate var visitText : UILabel?
        @IBOutlet fileprivate var SaleText : UILabel?
        @IBOutlet fileprivate var SampleText : UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.visitText?.text = self.CellLanguageSet(key: "Visits:")
        self.SaleText?.text = "\(self.CellLanguageSet(key: "Sales"))(\(SignedUserInfo.sharedInstance?.currency_symbol ?? "")):"
        self.SampleText?.text = self.CellLanguageSet(key: "Samples:")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
