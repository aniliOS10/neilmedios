//
//  NotificationCell.swift
//  MedMobilie
//
//  Created by MAC on 13/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var swSwitch: UISwitch!
}
