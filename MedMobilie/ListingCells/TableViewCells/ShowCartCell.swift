//
//  ShowCartCell.swift
//  MedMobilie
//
//  Created by MAC on 07/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
// Update code

import UIKit
import SwipeCellKit

protocol CartFieldDelegate: class {
    
    func updatedCartContent(discount: String, cases: String,index : Int,pricePerCase : Double)
}


class ShowCartCell: SwipeTableViewCell {
    
    weak var dele: CartFieldDelegate!
    
    
    @IBOutlet var lblProductName : UILabel?
    
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet var lblCategoryName: UILabel?
    @IBOutlet weak var lblNoOfCase: UILabel?
    
    @IBOutlet weak var txtDiscountPerCase: CustomField?
    @IBOutlet weak var txtNoOfCase: CustomField?
    
    @IBOutlet var imgProductimg : UIImageView?
    @IBOutlet fileprivate var lblQtyText : UILabel?
    @IBOutlet fileprivate var lblPriceText : UILabel?
    @IBOutlet var  Units : UILabel?
    
    @IBOutlet var  Price : UILabel?
    @IBOutlet var  Discount : UILabel?

    
    @IBOutlet var viewShadow : UIView?
    fileprivate var discountPrice : String = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        
        txtDiscountPerCase?.delegate = self
        txtNoOfCase?.delegate = self
        txtDiscountPerCase?.keyboardType = .decimalPad
        txtNoOfCase?.keyboardType = .numberPad
        // Initialization code
        lblQtyText?.text = CellLanguageSet(key: "Units Per Case")
        lblPriceText?.text = CellLanguageSet(key: "Per Case")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
}


extension ShowCartCell : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var updatedTextString : NSString = textField.text! as NSString
        updatedTextString = updatedTextString.replacingCharacters(in: range, with: string) as NSString
        
        print(updatedTextString)
        
        if textField == txtDiscountPerCase{
            
            
            if Double(updatedTextString as String)?.round(to: 2) ?? 0.0 <  txtDiscountPerCase?.pricevalue ?? 0.0{
                self.dele.updatedCartContent(discount: updatedTextString as String, cases: self.txtNoOfCase?.text! ?? "", index: txtDiscountPerCase?.itemIndex ?? 0, pricePerCase: txtDiscountPerCase?.pricevalue ?? 0.0)
            }
            else
            {
                return false
            }
           
        }
        if textField == txtNoOfCase{
            
            self.dele.updatedCartContent(discount: self.txtDiscountPerCase?.text! ?? "", cases: updatedTextString as String, index: txtNoOfCase?.itemIndex ?? 0, pricePerCase: txtDiscountPerCase?.pricevalue ?? 0.0)
        }
        
        return true
    }
}

class CustomField: UITextField {
    var rowIndex : Int!
    var itemIndex : Int!
    var pricevalue : Double = 0.0
}

