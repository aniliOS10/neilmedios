//
//  TaskListCell.swift
//  MedMobilie
//
//  Created by dr.mac on 22/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import SwipeCellKit

class TaskListCell: SwipeTableViewCell {
    
    @IBOutlet var lblTask_name : UILabel?
    @IBOutlet var lblStatus : UILabel?
    @IBOutlet var lblTime: UILabel?
    @IBOutlet var lblassign_by : UILabel?
   
    @IBOutlet  var lblid : UILabel?
    
    @IBOutlet var viewShadow : UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblid?.text = CellLanguageSet(key: "assign by") + ":"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
