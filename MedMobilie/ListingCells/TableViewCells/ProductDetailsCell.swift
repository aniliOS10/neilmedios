//
//  ProductDetailsCell.swift
//  MedMobilie
//
//  Created by MAC on 27/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit

class ProductDetailsCell: UITableViewCell,UITextFieldDelegate {

    @IBOutlet var productName : UILabel?
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet var desp : UILabel?
     @IBOutlet var product_code : UILabel?
    @IBOutlet var pricepercase : UILabel?
     @IBOutlet var unitspercase : UILabel?
     @IBOutlet var category_name : UILabel?
    
    @IBOutlet fileprivate var lblNoOfCase: UILabel!
    
    @IBOutlet weak var txtDiscountPerUnit: UITextField!
    @IBOutlet fileprivate var lblDiscountPerUnit: UILabel!
    @IBOutlet weak var txtNoOfCase: UITextField!
    @IBOutlet fileprivate var lblTotalUnit: UILabel!
    @IBOutlet fileprivate var lblTotal: UILabel!
    @IBOutlet weak var lblUnitTotal: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        
        
    }
    
}
