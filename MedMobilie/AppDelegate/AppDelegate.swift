
import UIKit
import MMDrawerController
import GoogleMaps
import GooglePlaces
import CoreData
import IQKeyboardManagerSwift
import Reachability
import SpinKit
import Stripe
import Firebase
import FirebaseDatabase
import UserNotifications
import LocalAuthentication

var indexvalue:NSInteger = 0
var elDrawer: KYDrawerController?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var Time_delegate : TimerProtocols?
    var userid : Int = 0
    var reachability : Reachability?
    var drawerContainer: MMDrawerController?
    var loadingView : UIView?
    
    var timing = Timer()
    var timer: Timer? = nil
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        print(DataBaseHelper.ShareInstance.getRecordsCount())
        

        UITextField.appearance().backgroundColor = UIColor.init(hexString: "#f2f2f2")
        
        NotificationCenter.default.addObserver(self, selector: #selector(Check_Chat_ID), name: NSNotification.Name( "Check_Chat_ID"), object: nil)
        UIApplication.shared.registerForRemoteNotifications()
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self //DID NOT WORK WHEN self WAS MyOtherDelegateClass()
        
        center.requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            // Enable or disable features based on authorization.
            if granted {
                // update application settings
            }
        }
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        

        
        FirebaseApp.configure()
        sleep(2)
        GMSServices.provideAPIKey(AppConnectionConfig.googleMapKeyplace)
        GMSPlacesClient.provideAPIKey(AppConnectionConfig.googleMapKeyplace)
        STPPaymentConfiguration.shared().publishableKey = StripeConstants.publishableKey
        if !NetworkState.isConnected() {
            AppDelegate.alertViewForInterNet()
        }
        self.reachability = Reachability.init()
        
        if ((self.reachability!.connection) != .none) {
            
            print("InterNet Working")
        }
        else
        {
            print("InterNet Not Working")
        }
        print(UIScreen.main.bounds.size.height)
        
        var preferredStatusBarStyle: UIStatusBarStyle {
            return .default
        }
        UserDefaults.standard.set(true, forKey: "contactData_Fetch")
        IQKeyboardManager.shared.enable = true
        SetRootAfterAuth()
        return true
    }
    
    fileprivate func SetRootAfterAuth()
    {
        
        
        
        let useInfo = SignedUserInfo.sharedInstance
        let ud = UserDefaults.standard
        if useInfo != nil
        {
            if ud.value(forKey:"FaceIdEnable") as? Bool == true
            {
                if LAContext().biometricType.rawValue == "faceID"
                {
                    self.authenticationWithTouchID()
                }
                else
                {
                    RootControllerManager().setRoot()
                    
                }
                
            }
            else
            {
                RootControllerManager().setRoot()
            }
        }
        else
        {
            RootControllerManager().setRoot()
        }
        
    }
    
    @objc func Check_Chat_ID(notification: NSNotification){
        
        let data = notification.object as! Dictionary<String, Any>
        
        userid = data["user_id"] as! Int
        
        print(userid)
        
        print("user_id VALUE :---->",userid)
        
    }
    
    // For push notification Delegate Method
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        
        print("userInfo :--->",userInfo)
        
        let module_type = userInfo[AnyHashable("module_type")]! as! String
        
        print("\(module_type)")
       
        
        if module_type == "chat"
        {
            
            print("userid :---->",userid)
            print("module_id_identifier :---->",userInfo[AnyHashable("module_id")]! as! Int)
            
            if userid != userInfo[AnyHashable("module_id")]! as! Int
            {
                completionHandler([.alert, .sound,.badge])
                
            }
            else
            {
                var parama = [String:Any]()
                parama["user_id"] = userInfo[AnyHashable("module_id")]! as! Int
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Refress_Chat"), object: parama)
                
            }
            
        }
        else if module_type == "contact_delete"
        {
           // print("contact_delete :---->",userInfo[AnyHashable("module_id")]! as! Int)
            DataBaseHelper.ShareInstance.DataOfContactWithID(Id: userInfo[AnyHashable("module_id")]! as! Int, type: .Delete)
            
     
        }
        else if module_type == "contact_update"
        {
           // print("contact_update :---->",userInfo[AnyHashable("module_id")]! as! Int)
            
         //   DataBaseHelper.ShareInstance.DataOfContactWithID(Id: userInfo[AnyHashable("module_id")]! as! Int, type: .Delete)
            
        }
        else if module_type == "contact_create"
        {
            print("contact_create :---->",userInfo[AnyHashable("module_id")]! as! Int)
            //DataBaseHelper.ShareInstance.AddContact(Object: <#T##JSON#>)
        }
            
        else
        {
            completionHandler([.alert, .sound,.badge])
        }
        print("UnRead Notification")
    }
    
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // 1
        let userInfo = response.notification.request.content.userInfo
        
        print(userInfo)
        
        
        var parama = [String:Any]()
        parama["module_id"] = userInfo[AnyHashable("module_id")]! as! Int
        parama["module_type"] = userInfo[AnyHashable("module_type")]! as! String
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationView"), object: parama)
        
        completionHandler()
        
        print("Read Notification")
        
    }
    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
        ) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        
        UserDefaults.standard.set(token, forKey: "DeviceToken")
        UserDefaults.standard.synchronize()
        
        print("Device Token: \(token)")
    }
    
    func application(
        _ application: UIApplication,
        didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    
    
    func buildNavigationDrawer()
    {
        
        // Instantiate Main.storyboard
        let mainStoryBoard:UIStoryboard = UIStoryboard(name:"Main", bundle:nil)
        
        // Create View Controllers
        let mainPage:UINavigationController = mainStoryBoard.instantiateViewController(withIdentifier: "nav") as! UINavigationController
        
        let leftSideMenu:MenuVC = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        
        
        // Cerate MMDrawerController
        drawerContainer = MMDrawerController(center: mainPage, leftDrawerViewController: leftSideMenu, rightDrawerViewController: nil)
        
        //  drawerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.None
        drawerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.panningCenterView
        
        // Assign MMDrawerController to our window's root ViewController
        window?.rootViewController = drawerContainer
        
    }
    func makingRoot(_ strRoot: String) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        window = UIWindow(frame: UIScreen.main.bounds)
        if (strRoot == "initial") {
            
            let obj: UINavigationController? = storyboard.instantiateViewController(withIdentifier: storyboardID.navigationIdentifier) as? UINavigationController
            
            window?.rootViewController = obj
            
        } else {
            
            elDrawer = (storyboard.instantiateViewController(withIdentifier: storyboardID.kyDrawerIdentifierSegue) as! KYDrawerController)
            window?.rootViewController = elDrawer
            
        }
        
        window?.makeKeyAndVisible()
        
        UIView.transition(with: window!, duration: 0.3, options: .transitionCrossDissolve, animations: nil, completion: { _ in })
        
        
    }
    func StartTimer()
    {
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
        
    }
    func StopTimer() {
        if timer != nil {
            timer!.invalidate()
            timer = nil
        }
        
    }
    @objc func timerAction()
    {
        
        Time_delegate?.TimerProtocols_Call()
        
    }
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    class func showWaitView() {
        let sharedApp = UIApplication.shared.delegate as? AppDelegate
        if sharedApp?.loadingView == nil
        {
            let screenBounds: CGRect = UIScreen.main.bounds
            sharedApp?.loadingView = UIView(frame: screenBounds)
            sharedApp?.loadingView?.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
            let indicatorView = RTSpinKitView(style: .styleCircle, color: SettingNAVBAR())
            indicatorView?.center = CGPoint(x: screenBounds.size.width / 2, y: screenBounds.size.height / 2)
            
            sharedApp?.loadingView?.addSubview(indicatorView!)
            sharedApp?.window?.addSubview((sharedApp?.loadingView)!)
            
        }
    }
    @objc class func hideWaitView() {
        let sharedApp = UIApplication.shared.delegate as? AppDelegate
        
        if sharedApp?.loadingView != nil {
            sharedApp?.loadingView?.removeFromSuperview()
            sharedApp?.loadingView = nil
        }
        
    }
    class func alertViewForInterNet()
    {
        UIAlertController().AlertView(title: Bundle.appName(), message: "Internet Not Working".languageSet, actionTitles: ["OK".languageSet], actions: [{action1 in
            return
            }, nil])
    }
    
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "MedMobileDB")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
            else{
                print("Core data is Okay")
                
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}



extension AppDelegate
{
    
    func authenticationWithTouchID() {
        let localAuthenticationContext = LAContext()
        var authError: NSError?
        let reasonString = "To access the secure data"
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString) { success, evaluateError in
                
                if success {
                    DispatchQueue.main.async {
                        RootControllerManager().setRoot()
                    }
                    
                    
                    
                } else {
                    
                    guard let error = evaluateError else {
                        return
                    }
                    print(FaceIDErrorConstant.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
                    DispatchQueue.main.async {
                        RootControllerManager().setFaceIdNotDetect()
                    }
                    
                    
                }
            }
        } else {
            
            guard let error = authError else {
                return
            }
            
            
            print(FaceIDErrorConstant.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
        }
    }
}

