//
//  OfflineDataPush.swift
//  MedMobilie
//
//  Created by dr.mac on 09/05/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON

class OfflineDataPush : NSObject
{
    func PushData()
    {
        if NetworkState.isConnected()
        {
          
            ContactsDataPush()
            CheckinDataPush()
          //  visitDataPush()
            //
           
        }
    }
    
    fileprivate func ContactsDataPush()
    {
        let userdefault = UserDefaults.standard
        if userdefault.value(forKey: "OfflineNewContactCreate") as? Int != nil
        {
            DispatchQueue.global(qos: .background).async{
                
                DataBaseHelper.ShareInstance.newContactsFetching { (data) in
                    var jsonArray = [JSON]()
                    if let tempData = data
                    {
                        for jsonData in tempData
                        {
                            if let jsondata = jsonData.value(forKey: "json") as? Data {
                                
                                
                                jsondata.retrieveJSON(completion: { (json) in
                                    
                                    jsonArray.append(json!)
                                })
                            }
                        }
                       
                        
                        if jsonArray.count > 0
                        {
                            print(jsonArray)
                            self.BulkDataPush(Params: ["contacts":jsonArray] as [String:Any],url: "/contactapp_createbulk", completion: { (status, message, jsonData) in
                                
                                if !status
                                {
                                    
                                    return
                                }
                                
                                if let Jsonarr = jsonData
                                {
                                    
                                    Jsonarr.forEach({
                                        
                                        DataBaseHelper.ShareInstance.DeleteEntry(ClassName: OfflineDataClass.Contact.rawValue, Id: $0["id"].intValue)
                                        
                                        DataBaseHelper.ShareInstance.AddContact(Object: $0)
                                    })
                                    userdefault.set(nil, forKey: "OfflineNewContactCreate")
                                    userdefault.synchronize()
                                    
                                    
                                }
                                self.visitDataPush()
                                self.taskDataPush()
                                self.orderDataPush()
                            })
                        }
                        else
                        {
                            self.visitDataPush()
                            self.taskDataPush()
                            self.orderDataPush()
                            return
                        }
                       
                    }
                }
            }
        }
        else
        {
            visitDataPush()
            taskDataPush()
             orderDataPush()
            return
        }
        
    }
    
    fileprivate func taskDataPush()
    {
        let userdefault = UserDefaults.standard
        if userdefault.value(forKey: "OfflineNewTaskCreate") as? Int != nil
        {
            DispatchQueue.global(qos: .background).async{
                DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.TaskList.rawValue, completion: { (data) in
                    
                    var jsonArray = [JSON]()
                    if let tempData = data
                    {
                        for jsonData in tempData
                        {
                            if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                                
                                
                                jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                                    jsonArray.append(json!)
                                })})
                            }
                        }
                        
                        if jsonArray.count > 0
                        {
                            print(jsonArray)
                            self.BulkDataPush(Params: ["tasks":jsonArray] as [String:Any] , url: "/taskapp_createbulk", completion: { (status, message, jsonData) in
                                if !status
                                {
                                    return
                                }
                                
                                userdefault.set(nil, forKey: "OfflineNewTaskCreate")
                                userdefault.synchronize()
                                DataBaseHelper.ShareInstance.DeleteContext(ClassName: OfflineDataClass.TaskList.rawValue)
                                
                                
                            })
                        }
                        else
                        {
                            return
                        }
                       
                    }
                })
                
            }
            
        }
        else
        {
            return
        }
        
        
    }
    
    fileprivate func visitDataPush()
    {
        let userdefault = UserDefaults.standard
        if userdefault.value(forKey: "OfflineNewVisitCreate") as? Int != nil
        {
            DispatchQueue.global(qos: .background).async{
                DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.VisitList.rawValue, completion: { (data) in
                    
                    var jsonArray = [JSON]()
                    if let tempData = data
                    {
                        for jsonData in tempData
                        {
                            if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                                jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                                    jsonArray.append(json!)
                                })})
                            }
                        }
                        
                        if jsonArray.count > 0
                        {
                            print(jsonArray)
                            self.BulkDataPush(Params: ["visits":jsonArray] as [String:Any] , url: "/visitcreatebulk", completion: { (status, message, jsonData) in
                                if !status
                                {
                                    return
                                }
                                
                                userdefault.set(nil, forKey: "OfflineNewVisitCreate")
                                userdefault.synchronize()
                                DataBaseHelper.ShareInstance.DeleteContext(ClassName: OfflineDataClass.VisitList.rawValue)
                                
                                
                            })
                        }
                        else
                        {
                            return
                        }
                       
                        
                        
                        
                    }
                })
                
            }
            
        }
        else
        {
            return
        }
        
        
    }
    
    
    fileprivate func orderDataPush()
    {
       
        DispatchQueue.global(qos: .background).async{
            DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.OrderList.rawValue, completion: { (data) in
                
                var jsonArray = [JSON]()
                if let tempData = data
                {
                    for jsonData in tempData
                    {
                        if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                            jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                                jsonArray.append(json!)
                            })})
                        }
                    }
                    if jsonArray.count > 0
                    {
                       print(jsonArray)
                        self.BulkDataPush(Params: ["orders":jsonArray] as [String:Any] , url: "/ordercreatebulk", completion: { (status, message, jsonData) in
                            if !status
                            {
                                
                                return
                            }
                            
                            
                            
                            DataBaseHelper.ShareInstance.DeleteContext(ClassName: OfflineDataClass.OrderList.rawValue)
                            
                            
                        })
                    }
                    else
                    {
                        return
                    }
                    
                }
                else
                {
                    return
                }
            })
            
        }
        
        
        
    }
    
    fileprivate func CheckinDataPush()
    {
        DispatchQueue.global(qos: .background).async{
            DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.CheckInOutTime.rawValue, completion: { (data) in
                
                var jsonArray = [[String : Any]]()
                
                if let ManagedData = data
                {
                        for object in ManagedData
                        {
                            var param = [String:Any]()
                            if let startTime = object.value(forKey: "checkIn") as? String
                            {
                                    param["start"] = startTime
                            }
                            if let endTime = object.value(forKey: "checkout") as? String
                            {
                                param["end"] = endTime
                            }
                            else
                            {
                                param["end"] = ""
                            }
                            if let date = object.value(forKey: "date") as? String
                            {
                                param["date"] = date
                            }
                            
                            
                            jsonArray.append(param)
                            
                        }
                    
                    if jsonArray.count > 0
                    {
                        self.BulkDataPush(Params: ["checkin":jsonArray] as [String:Any] , url: "/checkincheckoutbulk", completion: { (status, message, jsonData) in
                            if !status
                            {
                                return
                            }
                            
                            
                            DataBaseHelper.ShareInstance.DeleteContext(ClassName: OfflineDataClass.CheckInOutTime.rawValue)
                    })
                }
                
                }})
        }
    }
    
    
    
    fileprivate func BulkDataPush(Params : [String:Any],url : String,completion : @escaping ((_ success : Bool,_ message : String ,_ data : [JSON]? )->()))
    {
        
        BulkOfflineCreatedContact().Request(param: Params,apiUrl : url) { (status, message, jsonData) in
                        completion(status,message,jsonData)
                        return
            }
        
        
    }

     
    
}
