//
//  SelectedContactProtocol.swift
//  MedMobilie
//
//  Created by MAC on 22/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON
protocol SelectedContactProtocol {
    func selected(selected_array : NSMutableArray)
}

protocol SelectedContactJSON {
     func selected(selected_array : [JSON])
}
