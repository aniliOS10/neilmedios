//
//  RescheduledDateTime.swift
//  MedMobilie
//
//  Created by dr.mac on 30/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation

protocol RescheduledDateTime  {
    func rescheduledTimePass(id : Int ,RescheduleDate : String,FromTime : String , ToTime : String)
}
