//
//  CategoryList+CoreDataProperties.swift
//  
//
//  Created by apple on 09/05/19.
//
//

import Foundation
import CoreData


extension CategoryList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CategoryList> {
        return NSFetchRequest<CategoryList>(entityName: "CategoryList")
    }

    
    @NSManaged public var json : [Data]


}
