//
//  Contact+CoreDataProperties.swift
//  
//
//  Created by Apple on 09/09/21.
//
//

import Foundation
import CoreData


extension Contact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact")
    }

    @NSManaged public var categoryname: String?
    @NSManaged public var contact_name: String?
    @NSManaged public var email: String?
    @NSManaged public var id: String?
    @NSManaged public var json: NSObject?
    @NSManaged public var limitedData: NSObject?
    @NSManaged public var new: Bool
    @NSManaged public var phone: String?
    @NSManaged public var primaryspecialtiy: String?
    @NSManaged public var salutation: String?
    @NSManaged public var secondaryspecialtiy: String?

}
