
import Foundation
import CoreData

extension ListingData {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<ListingData> {
        return NSFetchRequest<ListingData>(entityName: "ListingData")
    }
    
  
    @NSManaged public var json: [Data]
    
}

