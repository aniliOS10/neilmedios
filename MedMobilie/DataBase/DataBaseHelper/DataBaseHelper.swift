
import UIKit
import Foundation
import CoreData
import SwiftyJSON


enum OfflineDataClass : String {
    case Contact = "Contact"
    case Product = "Product"
    case Sample = "Sample"
    case ListingData = "ListingData"
    case CheckinTime = "CheckinTime"
    case VisitList = "VisitList"
    case TaskList = "TaskList"
    case OrderList = "OrderList"
    case CheckInOutTime = "CheckInOutTime"
}

enum types : Int
{
    case Fetch = 0
    case Update = 1
    case Delete = 2
   
}

class DataBaseHelper {
    
    static var ShareInstance = DataBaseHelper()
    var intValue = 0

    let context = PersistentManager.Share.persistentContainer.viewContext
    
    func AddVisit(VisitData: JSON , completion : @escaping ((_ status : Bool )->())) {
        
        print("VisitData :-----> ",VisitData)
        
        let VisitList = NSEntityDescription.insertNewObject(forEntityName: OfflineDataClass.VisitList.rawValue , into: context) as! VisitList
        NSObject.storeJSON(dataToStore: VisitData) { (data) in
            if data != nil
            {
                VisitList.id.append(VisitData["id"].intValue)
                VisitList.json.append(data!)
                completion(true)
            }
            else
            {
                completion(false)
                return
            }
        }
        self.Save()
    }
    
    func AddTask(TaskData: JSON,TaskId: Int , completion : @escaping ((_ status : Bool )->())) {
        
        print("TaskListData :-----> ",TaskData)
        
        let TaskList = NSEntityDescription.insertNewObject(forEntityName: OfflineDataClass.TaskList.rawValue , into: DataBaseHelper.ShareInstance.context) as! TaskList
      //  TaskList.id = TaskId
        NSObject.storeJSON(dataToStore: TaskData) { (data) in
            if data != nil
            {
                TaskList.id.append(TaskId)
                TaskList.json.append(data!)
                completion(true)
            }
            else
            {
                completion(false)
                return
            }
        }
        self.Save()
      //  FetchcontactRequest(ClassName: "TaskList")
        
    }
    
    func CreateOrder(OrderData: JSON,OrderId: Int , completion : @escaping ((_ status : Bool )->())) {
        
        print("OrderListData :-----> ",OrderData)
        
        let OrderListData = NSEntityDescription.insertNewObject(forEntityName: OfflineDataClass.OrderList.rawValue , into: DataBaseHelper.ShareInstance.context) as! OrderList
        //  TaskList.id = TaskId
        NSObject.storeJSON(dataToStore: OrderData) { (data) in
            if data != nil
            {
                print(data as Any)
                
                OrderListData.id.append(OrderId)
                OrderListData.json.append(data!)
                completion(true)
            }
            else
            {
                completion(false)
                return
            }
        }
        
        self.Save()
        //  FetchcontactRequest(ClassName: "TaskList")
    }
    
    func newContactsFetching(completion : @escaping ((_ data : [NSManagedObject]? )->()) )
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: OfflineDataClass.Contact.rawValue)
        request.returnsObjectsAsFaults = false
        
        request.predicate = NSPredicate(format: "new == %@", NSNumber(value: true))
        
        do {
            let result = try context.fetch(request)
            if let object = result as? [NSManagedObject]
            {
                completion(object)
                return
            }
            else
            {
                completion(nil)
                return
            }
        }
        catch{
            print("failed")
        }
    }
    
    func FetchFilterContactRequest(ClassName:String ,completion : @escaping ((_ data : [NSManagedObject]? )->()) ) {
        
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: ClassName)
        request.returnsObjectsAsFaults = false
         request.predicate = NSPredicate(format: "new == %@", NSNumber(value: true))
        
        do {
            let result = try DataBaseHelper.ShareInstance.context.fetch(request)
            
            
            print("result:------>",result)
            if let object = result as? [NSManagedObject]
            {
                completion(object)
                return

            }
            else
            {
                completion(nil)
                return
            }
            
        } catch {
             print("Failed")
           
        }
    }
   
//    func FetchContactLimitedData(offset : Int,filterWord : String = "",completion : @escaping ((_ data : [JSON] )->()))
//    {
    
         func FetchContactLimitedData(offset : Int,filterWord : String = "",completion : @escaping ((_ data : [JSON] , _ totalContacts :Int )->()))
         {
             
             
            
            let request = NSFetchRequest<Contact>()
            request.fetchLimit = 100
            request.fetchOffset = offset
            if filterWord != ""
            {
                request.predicate = NSPredicate(format:"(contact_name contains[c] %@) OR (email contains[c] %@) OR (phone contains[c] %@) OR (categoryname contains[c] %@) OR (salutation contains[c] %@)",filterWord,filterWord,filterWord,filterWord,filterWord)
            }
            
            
            let entityDesc = NSEntityDescription.entity(forEntityName: OfflineDataClass.Contact.rawValue, in: DataBaseHelper.ShareInstance.context)
                    
            request.entity = entityDesc
            

            request.sortDescriptors = [NSSortDescriptor.init(key: "contact_name", ascending: true)]
            
            let fetchedOjects = try? self.context.fetch(request)
                        
            if let object = fetchedOjects as? [NSManagedObject]
            {
                var jsonArray = [JSON]()
                
                for jsonData in object
                {
                    if let jsondata = jsonData.value(forKey: "json") as? Data {
                        
                        jsondata.retrieveJSON(completion: { (json) in
                            // jsonArray.append(json!)
                            if let jsonObject = json{
                                var dictionary = [String:Any]()
                                dictionary["is_express"] = jsonObject["is_express"].boolValue
                                dictionary["contact_name"] = jsonObject["contact_name"].stringValue
                                dictionary["salutation"] = jsonObject["salutation"].stringValue
                                dictionary["category_name"] = jsonObject["category_name"].stringValue
                                dictionary["is_editable"] = jsonObject["is_editable"].boolValue
                                dictionary["id"] = jsonObject["id"].intValue
                                jsonArray.append(JSON(dictionary))
                            }
                        })
                    }
                }
                
                print(jsonArray)
                
                
//                jsonArray.sorted()
                
//                jsonArray.sor
                
//                jsonArray
//                jsonArray
//                jsonArray.filter(<#T##isIncluded: (JSON) throws -> Bool##(JSON) throws -> Bool#>)
//               hb in progress
                // print(jsonArray)
                completion(jsonArray , 40000)
            }
    }

    func DataOfContactWithID(Id : Int , type : types) -> JSON?
    {
        let request = NSFetchRequest<Contact>(entityName: OfflineDataClass.Contact.rawValue)
        request.returnsObjectsAsFaults = false
        
        request.predicate = NSPredicate(format : "id ==  %@", "\(Id)")
        
        do {
            let result = try context.fetch(request)
            if let object = result as? [NSManagedObject]
            {
                for req in object
                {
                    switch type
                    {
                    case .Fetch :
                        var tempJson : JSON?
                        if let jsondata = req.value(forKey: "json") as? Data {
                            
                            jsondata.retrieveJSON(completion: { (json) in
                                // jsonArray.append(json!)
                              print(json)
                                tempJson = json
                            })
                            
                        }
                        
                        return tempJson
                    case .Update :
                        break
                    case .Delete :
                        context.delete(req)
                        self.Save()
                        break
                    }
                 //   context.delete(req)
                  //  self.Save()
                }
            }
        }catch {
            print ("There was an error")
        }
        return nil
    }
    
    func getRecordsCount() -> Int {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: OfflineDataClass.Contact.rawValue)
        do {
            let count = try context.count(for: fetchRequest)
            print(count)
            return count
        } catch {
            print(error.localizedDescription)
        }
        return 0
    }
    
    func FetchcontactRequest(ClassName:String ,completion : @escaping ((_ data : [NSManagedObject]? )->()) ) {
        
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: ClassName)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try DataBaseHelper.ShareInstance.context.fetch(request)
            
            
           // print("result:------>",result)
            if let object = result as? [NSManagedObject]
            {
                completion(object)
                return

            }
            else
            {
                completion(nil)
                return
            }
            
        } catch {
             print("Failed")
        }
    }
    
    func AddProduct(Object: [JSON] ,completion : @escaping ((_ status : Bool )->())) {
        
        print("AddProduct :-----> ",Object)
        
        let Product = NSEntityDescription.insertNewObject(forEntityName: OfflineDataClass.Product.rawValue, into: context) as! Product
        
        
        Object.forEach { (element) in
            
            NSObject.storeJSON(dataToStore: element) { (data) in
                if data != nil
                {
                    Product.id.append(element["id"].intValue)
                    Product.json.append(data!)
                    self.Save()
                }
                else
                {
                    completion(false)
                    return
                }
                completion(true)
            }
        }
    }
    
    func AddSampleProduct(Object: [JSON] ,completion : @escaping ((_ status : Bool )->())) {
        
        print("AddSampleProduct :-----> ",Object)
        
        let SampleProduct = NSEntityDescription.insertNewObject(forEntityName: OfflineDataClass.Sample.rawValue, into: context) as! Sample
        
        Object.forEach { (element) in
            
            NSObject.storeJSON(dataToStore: element) { (data) in
                if data != nil
                {
                    SampleProduct.id.append(element["id"].intValue)
                    SampleProduct.json.append(data!)
                    self.Save()
                }
                else
                {
                    completion(false)
                    return
                }
                completion(true)    
            }
        }
    }
    
    func AddContact(Object: JSON ,NewType :Bool = false ,completion : ((_ status : Bool )->())? = nil) {
        //  Anil Changed

//        let privateManagedObjectContext: NSManagedObjectContext = {
//               let moc = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
//               moc.parent = context
//               moc.automaticallyMergesChangesFromParent = true
//
//               return moc
//               }()
//

        
        let contact = NSEntityDescription.insertNewObject(forEntityName: OfflineDataClass.Contact.rawValue, into: context) as! Contact

            NSObject.storeJSON(dataToStore: Object) { (data) in
                if data != nil
                {
                   contact.json = data as NSObject? ?? Data() as NSObject?
                
                    if let id = Object["id"].int
                    {
                        contact.id = String(id)
                    }
                    else{
                        contact.id = "0"
                    }
                    
                    if let category_name = Object["category_name"].string{
                        contact.categoryname = category_name
                    }
                    else{
                        contact.categoryname = ""
                    }
                    if let primary_speciality_name = Object["primary_speciality_name"].string{
                        contact.primaryspecialtiy = primary_speciality_name
                    }

                    else{
                        contact.primaryspecialtiy = ""
                    }
                    if let secondary_specialities_name  =  Object["secondary_specialities_name"].string
                    {
                        contact.secondaryspecialtiy = secondary_specialities_name
                    }
                    else{
                        contact.secondaryspecialtiy = ""
                    }
                    
                    if let contact_name = Object["contact_name"].string {
                        contact.contact_name = contact_name
                    }
                    else{
                        contact.contact_name = ""
                    }
                    
                    if let email = Object["email"].string
                    {
                        contact.email = email
                    }
                    else{
                        contact.email = ""
                    }
                    
                    if let salutation = Object["salutation"].string{
                        contact.salutation = salutation
                    }
                    else{
                        contact.salutation = ""
                    }
                    
                    if NewType {
                        contact.new = true
                    }
                    else{
                        contact.new = false
                    }
                    
                    if let phonenumber = Object["phone"].string
                    {
                        contact.phone = phonenumber
//                        self.Save()
                    }
                    else
                    {
                        contact.phone = "0"
                    }
                    

                 //   privateManagedObjectContext.perform {
                        do {
                         //   print(self.intValue)
                         //   self.intValue = self.intValue + 1
                           // try privateManagedObjectContext.save()
                           //
                            
                         //   try privateManagedObjectContext.save()
                           self.Save()
                            
                         //   try privateManagedObjectContext.parent?.save()
                            
                        }catch {
                            print("Save No")
                        }
                  // }
                }
                else
                {
                    completion?(false)
                    return
                }
                completion?(true)
            }
    }
    
    func AddListing(object : [JSON])
    {
        let ListingData = NSEntityDescription.insertNewObject(forEntityName: OfflineDataClass.ListingData.rawValue, into: context) as! ListingData
        
        object.forEach { (element) in
            
            NSObject.storeJSON(dataToStore: element) { (data) in
                if data != nil
                {
                  
                    ListingData.json.append(data!)
                    self.Save()
                }
                else
                {
                    return
                }
            }
        }
    }
    
     func CategoryList(object : [JSON])
    {
        let ListingData = NSEntityDescription.insertNewObject(forEntityName: "CategoryList", into: context) as! CategoryList
        
        object.forEach { (element) in
            
            NSObject.storeJSON(dataToStore: element) { (data) in
                if data != nil
                {
                    
                    ListingData.json.append(data!)
                    self.Save()
                }
                else
                {
                    return
                }
            }
        }
    }
    
    func Checkin(date: String,user_id: String,lat: String,lng: String,time: String) {

        let CheckinTime = NSEntityDescription.insertNewObject(forEntityName: OfflineDataClass.CheckinTime.rawValue , into: context) as! CheckinTime
        CheckinTime.date = date
        CheckinTime.user_id = user_id
        CheckinTime.lat = lat
        CheckinTime.lng = lng
        CheckinTime.time = time
       
        self.Save()
    }
    

    func DeleteContext(ClassName : String)
    {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: ClassName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)

        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
        
       
    }
    
    func CheckInOutTime(id_value: String,date_value: String,checkIn_value: String,checkout_value: String) {
        
         print("id_value",id_value)
        print("date_value",date_value)
        print("checkIn_value",checkIn_value)
        print("checkout_value",checkout_value)
        
        let CheckInOutTime = NSEntityDescription.insertNewObject(forEntityName: OfflineDataClass.CheckInOutTime.rawValue , into: context) as! CheckInOutTime
        CheckInOutTime.id = id_value
        CheckInOutTime.date = date_value
        CheckInOutTime.checkIn = checkIn_value
        CheckInOutTime.checkout = checkout_value
        
        self.Save()
    
    }
    
    func updateRecord(id_value:String,checkout:String) {
       
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "CheckInOutTime")
        let predicate = NSPredicate(format: "id = '\(id_value)'")
        fetchRequest.predicate = predicate
        do
        {
            let result = try context.fetch(fetchRequest)
            if result.count == 1
            {
                let objectUpdate = result[0] as! NSManagedObject
                objectUpdate.setValue(checkout, forKey: "checkout")
                do{
                    Save()
                }
                catch
                {
                    print(error)
                }
            }
        }
        catch
        {
    }
    }
    
    func DeleteEntry(ClassName : String , Id : Int)
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: ClassName)
        request.returnsObjectsAsFaults = false
        
        request.predicate = NSPredicate(format: "id ==  %@", "\(Id)")
        
        do {
            let result = try context.fetch(request)
            if let object = result as? [NSManagedObject]
            {
                    for req in object
                    {
                    
                            context.delete(req)
                            self.Save()
                    }
            }
        }catch {
            print ("There was an error")
        }
    }
    
     func FetchFilterContactRequest(categoryName:String,primaryspeciality:String,completion : @escaping ((_ data : [NSManagedObject]? )->()) )
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: OfflineDataClass.Contact.rawValue)
        request.returnsObjectsAsFaults = false
        
        if primaryspeciality == ""
        {
           request.predicate = NSPredicate(format: "categoryname == %@",categoryName)
        }
        else
        {
            request.predicate = NSPredicate(format: "(categoryname == %@) && (primaryspecialtiy == %@)",categoryName,primaryspeciality)
        }
        
        do {
            let result = try context.fetch(request)
            if let object = result as? [NSManagedObject]
            {
                completion(object)
                return
            }
            else
            {
                completion(nil)
                return
            }
        }
        catch{
            print("failed")
        }
    }

    func Save()
    {
        do {
            try context.save()
            print("Save Data")
        } catch{
            print("Data Not Save")
        }
    }
}
